//import 'package:firebase_analytics/firebase_analytics.dart';
//import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';

class Fullpage extends StatefulWidget {
  Fullpage({
    Key key,
    @required this.widgetpage,
    @required this.pagename,
  }) : super(key: key);

  Widget widgetpage;
  String pagename;

  @override
  _Fullpage createState() => new _Fullpage();
}

class _Fullpage extends State<Fullpage> with NavigatorObserver{
  //FirebaseAnalytics analytics = FirebaseAnalytics();

  @override
  void initState() {
    super.initState();
    //analytics.setCurrentScreen(screenName: widget.pagename);
    //FirebaseAnalyticsObserver(analytics: analytics);
  }

  @override
  Widget build(BuildContext context) {
    return widget.widgetpage;
  }
}
