import 'dart:async';
//import 'package:firebase_analytics/firebase_analytics.dart';
//import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewPage extends StatefulWidget {

  var _dtakun;
  String _url;
  Color _color1;
  Color _color2;
  Color _textColor = Colors.white;
  String _cabang;
  WebViewPage(List params, List akun){
    _dtakun = akun;
    params.forEach((dt){
      this._url = dt["_url"];
      this._cabang = dt["_cabang"];
      if(_cabang=="group"){
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
        this._textColor= Colors.grey[600];
      }
      if(_cabang=="denpasar"){
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
      }
      if(_cabang=="ubung"){
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
      }
      if(_cabang=="nusadua"){
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
      }
      if(_cabang=="kmc"){
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
      }
    });
  }

  @override
  _WebViewPagee createState() => _WebViewPagee();
}

class _WebViewPagee extends State<WebViewPage> {

  //FirebaseAnalytics analytics = FirebaseAnalytics();
  final Completer<WebViewController> _controller = Completer<WebViewController>();
  GlobalKey<ScaffoldState> scafolt_key = GlobalKey<ScaffoldState>();
  bool _isLoadingPage;

  @override
  void initState() {
    super.initState();
    _isLoadingPage = true;
    //analytics.setCurrentScreen(screenName: widget._url);
    //FirebaseAnalyticsObserver(analytics: analytics);
  }

  Future<bool> _onWillPop() async {
    WebViewController controller;
    _controller.future.then((dt) async {
      controller = dt;
      if (await controller.canGoBack()) {
        controller.goBack();
      } else {
        scafolt_key.currentState.removeCurrentSnackBar();
        final snackBar = SnackBar(
          content: Text('Apakah Anda akan keluar dari halaman Webview ini?'),
          action: SnackBarAction(
            label: 'Ya',
            onPressed: () {
              RouteHelper(context, "group", null);
              //Navigator.pop(context);
            },
          ),
        );
        scafolt_key.currentState.showSnackBar(snackBar);
      }
    });
  }

  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.white,
        systemNavigationBarDividerColor: Colors.grey[300],
        systemNavigationBarIconBrightness: Brightness.dark,// navigation bar color
        statusBarColor: widget._color2,
        statusBarIconBrightness: Brightness.dark// statu// status bar color
    ));

    return SafeArea(
      child: Scaffold(
          key: scafolt_key,
          appBar: new PreferredSize(
              preferredSize: Size.fromHeight(200.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Container(
                    color: widget._color1,
                    height: 55,
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Flexible(
                          flex: 1,
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Container(
                                child: new InkWell(
                                  onTap: ()=>RouteHelper(context, "group", null),
                                  child: new Container(
                                    padding: EdgeInsets.only(left: 24, right: 24,),
                                    alignment: Alignment.center,
                                    child: new Icon(
                                      FontAwesomeIcons.arrowLeft,
                                      size: 24.0,
                                      color: widget._textColor,
                                    ),
                                  ),
                                ),
                              ),
                              new Expanded(
                                flex: 1,
                                child: new Text(
                                  widget._url,
                                  overflow: TextOverflow.fade,
                                  softWrap: false,
                                  style: TextStyle(fontSize: 16, color: widget._textColor),
                                ),
                              )
                            ],
                          ),
                        ),
                        NavigationControls(_controller.future, widget._textColor),
                        SampleMenu(_controller.future, widget._url, widget._textColor),
                      ],
                    ),
                  ),
                ],
              )
          ),
          body: WillPopScope(
            onWillPop: (){
              _onWillPop();
            },
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                WebView(
                  initialUrl: widget._url,
                  javascriptMode: JavascriptMode.unrestricted,
                  onWebViewCreated: (WebViewController webViewController) {
                    _controller.complete(webViewController);
                  },
                  javascriptChannels: <JavascriptChannel>[
                    _toasterJavascriptChannel(context),
                  ].toSet(),
                  navigationDelegate: (NavigationRequest request) {
                    if (request.url.startsWith(widget._url)) {
                      return NavigationDecision.prevent;
                    }
                    return NavigationDecision.navigate;
                  },
                  onPageFinished: (String url) {
                    setState(() {
                      _isLoadingPage = false;
                    });//print('Page finished loading: $url');
                  },
                ),
                _isLoadingPage ? Positioned(
                  top: 0,
                  child:  Container(
                    padding: EdgeInsets.only(top: 100),
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      color: Colors.grey[200],
                      child : new Center(
                        child: new Column(
                          children: [
                            SizedBox(
                              width: 80.0,
                              height: 80.0,
                              child: const CircularProgressIndicator(
                                backgroundColor: Colors.grey,
                                strokeWidth: 4,
                              ),
                            ),
                          ],
                        ),
                      )
                  ),
                ) : Container(),
              ],
            ),
          )
      ),
    );
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'Toaster',
        onMessageReceived: (JavascriptMessage message) {
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        }
    );
  }
}

enum MenuOptions {
  openonbrowser,
}

class SampleMenu extends StatelessWidget {

  SampleMenu(this.controller, this.url, this.textColor);
  final Future<WebViewController> controller;
  String url;
  Color textColor;
  final CookieManager cookieManager = CookieManager();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WebViewController>(
      future: controller,
      builder: (BuildContext context, AsyncSnapshot<WebViewController> controller) {
        return PopupMenuButton<MenuOptions>(
          icon: new Icon(
            FontAwesomeIcons.ellipsisV,
            color: textColor,
          ),
          onSelected: (MenuOptions value) {
            switch (value) {
              case MenuOptions.openonbrowser:
                _openonbrowser(url);
                break;
            }
          },
          itemBuilder: (BuildContext context) => <PopupMenuItem<MenuOptions>>[
            PopupMenuItem<MenuOptions>(
              value: MenuOptions.openonbrowser,
              child: const Text('Buka di browser'),
              enabled: controller.hasData,
            ),
          ],
        );
      },
    );
  }

  void _openonbrowser(String url) async {
    await launch(url);
  }

}

class NavigationControls extends StatelessWidget {
  const NavigationControls(this._webViewControllerFuture, this.textColor) : assert(_webViewControllerFuture != null);
  final Future<WebViewController> _webViewControllerFuture;
  final Color textColor;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WebViewController>(
      future: _webViewControllerFuture,
      builder: (BuildContext context, AsyncSnapshot<WebViewController> snapshot) {
        final bool webViewReady = snapshot.connectionState == ConnectionState.done;
        final WebViewController controller = snapshot.data;
        return Row(
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.replay, color: textColor,),
              onPressed: !webViewReady ? null : () {
                controller.reload();
              },
            ),
          ],
        );
      },
    );
  }
}