import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class Http_Controller{

  static final Http_Controller _instance = new Http_Controller.internal();
  factory Http_Controller() => _instance;
  Http_Controller.internal();

  getResponse(String url) async {
    var response = await http.get(url);
    int statusCode = response.statusCode;
    String responseBody = response.body;
    return {'statusCode': statusCode, 'responseBody': responseBody};
  }

  postResponse(String uri, List<Map<dynamic, dynamic>> dtNext) async {
    final encoding = Encoding.getByName('utf-8');
    final headers = {"accept" : "application/json",};
    Response response = await http.post(
      uri,
      headers: headers,
      body: dtNext[0],
      encoding: encoding,
    );
    int statusCode = response.statusCode;
    String responseBody = response.body;
    print(response.toString());
    return {'statusCode':statusCode, 'responseBody': responseBody};
  }

}

