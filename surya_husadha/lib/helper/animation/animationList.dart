import 'package:flutter/material.dart';
import 'package:path/path.dart';
class AnimationList {
  AnimationList(this.controller) :
        Translation_1 = new Tween(begin: Offset(0.0, 10.0), end: Offset(0.0, 0.0)).animate(
          CurvedAnimation(
            parent: controller,
            curve: new Interval(0.14, 0.84, curve: Curves.bounceIn),
          ),
        ),
        Translation_2 = new Tween(begin: Offset(0.0, 10.0), end: Offset(0.0, 0.0)).animate(
          CurvedAnimation(
            parent: controller,
            curve: new Interval(0.24, 0.84, curve: Curves.bounceIn),
          ),
        ),
        Translation_3 = new Tween(begin: Offset(0.0, 10.0), end: Offset(0.0, 0.0)).animate(
          CurvedAnimation(
            parent: controller,
            curve: new Interval(0.34, 0.84, curve: Curves.bounceIn),
          ),
        ),
        Translation_4 = new Tween(begin: Offset(0.0, 10.0), end: Offset(0.0, 0.0)).animate(
          CurvedAnimation(
            parent: controller,
            curve: new Interval(0.44, 0.84, curve: Curves.bounceIn),
          ),
        ),
        backdropBlur = new Tween(begin: 0.0, end: 10.0).animate(
          new CurvedAnimation(
            parent: controller,
            curve: new Interval(
              0.000,
              0.800,
              curve: Curves.ease,
            ),
          ),
        ),
        backdropOpacity = new Tween(begin: 0.2, end: 1.0).animate(
          new CurvedAnimation(
            parent: controller,
            curve: new Interval(
              0.000,
              0.800,
              curve: Curves.ease,
            ),
          ),
        ),
        avatarSize = new Tween(begin: 0.0, end: 1.0).animate(
          new CurvedAnimation(
            parent: controller,
            curve: new Interval(
              0.100,
              0.400,
              curve: Curves.elasticOut,
            ),
          ),
        ),
        nameOpacity = new Tween(begin: 0.0, end: 1.0).animate(
          new CurvedAnimation(
            parent: controller,
            curve: new Interval(
              0.350,
              0.450,
              curve: Curves.easeIn,
            ),
          ),
        ),
        locationOpacity = new Tween(begin: 0.0, end: 0.85).animate(
          new CurvedAnimation(
            parent: controller,
            curve: new Interval(
              0.500,
              0.600,
              curve: Curves.easeIn,
            ),
          ),
        ),
        dividerWidth = new Tween(begin: 0.0, end: 350.0).animate(
          new CurvedAnimation(
            parent: controller,
            curve: new Interval(
              0.650,
              0.750,
              curve: Curves.fastOutSlowIn,
            ),
          ),
        ),
        biographyOpacity = new Tween(begin: 0.0, end: 0.85).animate(
          new CurvedAnimation(
            parent: controller,
            curve: new Interval(
              0.750,
              0.900,
              curve: Curves.easeIn,
            ),
          ),
        );

  final AnimationController controller;
  final Animation<Offset> Translation_1;
  final Animation<Offset> Translation_2;
  final Animation<Offset> Translation_3;
  final Animation<Offset> Translation_4;
  final Animation<double> backdropBlur;
  final Animation<double> backdropOpacity;
  final Animation<double> avatarSize;
  final Animation<double> nameOpacity;
  final Animation<double> locationOpacity;
  final Animation<double> dividerWidth;
  final Animation<double> biographyOpacity;
}
