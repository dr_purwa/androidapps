import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
class Dialogue extends StatefulWidget {

  String _msg;
  BuildContext _context;
  Color _color1;
  Color _color2;
  String _route;
  List _params;

  Dialogue(BuildContext context, String msg, Color color1, Color color2, String route, List params) {
    this._msg = msg;
    this._context = context;
    this._color1 = color1;
    this._color2 = color2;
    this._route = route;
    this._params = params;
  }

  @override
  _DialogueController createState() => new _DialogueController();

}

class _DialogueController extends State<Dialogue>{

  @override
  Widget build(BuildContext context) {
    return _diaogue(widget._context);
  }

  Future<bool> _onWillPop(){
    //Navigator.pop(context);
    // }
  }

  Widget _diaogue (BuildContext context){
    return new WillPopScope(
      onWillPop: _onWillPop,
      child: Material(
        type: MaterialType.transparency,
        child: new Stack(
            children: <Widget>[
              Container(
                color: Colors.black.withOpacity(0.2),
              ),
              Positioned(
                top: 80.0,
                left: 24.0,
                right: 24.0,
                child: Column(
                  children: <Widget>[
                    new Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black26,
                            blurRadius: 10.0,
                            offset: const Offset(0.0, 10.0),
                          ),
                        ],
                      ),
                      padding: EdgeInsets.only(left: 16, right: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Padding(
                            padding: EdgeInsets.only(top: 90),
                          ),
                          new Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Icon(
                                FontAwesomeIcons.infoCircle,
                                size: 14,
                              ),
                              new Padding(padding: EdgeInsets.only(left: 5)),
                              new Container(
                                padding: EdgeInsets.only(top: 2),
                                child: new Text(
                                  "Informasi",
                                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700,),
                                ),
                              ),
                            ],
                          ),
                          new Padding(
                            padding: EdgeInsets.only(top:20),
                          ),
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Expanded(
                                flex: 1,
                                child: new Container(
                                  padding: EdgeInsets.only(left: 10),
                                  child: new Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      new Container(
                                        margin: EdgeInsets.only(bottom: 20),
                                        width: double.infinity,
                                        height: 1.0,
                                        color: Colors.grey[200],
                                      ),
                                      new Text(
                                        widget._msg,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Colors.grey[600],
                                          fontFamily: "NeoSansBold",
                                        ),
                                      ),
                                      new Container(height: 20,),
                                      new Text(
                                        "Terima Kasih.",
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Colors.grey[600],
                                          fontFamily: "NeoSansBold",
                                        ),
                                      ),
                                      new Container(
                                        margin: EdgeInsets.only(top: 20),
                                        width: double.infinity,
                                        height: 1.0,
                                        color: Colors.grey[200],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          new Container(
                            color: Colors.white,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                new FlatButton(
                                    onPressed: (){
                                      if(widget._route != null){
                                        RouteHelper(context, widget._route, widget._params);
                                      }else{
                                        Navigator.of(context).pop(widget._params);
                                      }
                                    },//
                                    child: new Row(
                                      children: <Widget>[
                                        new Icon(
                                          FontAwesomeIcons.times,
                                          size: 16,
                                          color: Colors.grey[600],
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(bottom: 3),
                                          padding: EdgeInsets.only(left: 3, top: 3),
                                          child: new Text(
                                            "Tutup",
                                            style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.grey[600],
                                              fontFamily: "NeoSansBold",
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                top: 20,
                width: MediaQuery.of(context).size.width,
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Container(
                      width: 140.0,
                      height: 140.0,
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.white, width: 5),
                        color: widget._color1,
                      ),
                      padding: EdgeInsets.all(5),
                      child: new ClipOval(
                        child: new Image.asset("assets/suri/suri2.png"),
                      ),
                    ),
                  ],
                ),
              ),
            ]
        ),
      ),
    );
  }

}