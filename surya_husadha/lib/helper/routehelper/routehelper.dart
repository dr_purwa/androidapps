import 'package:flutter/widgets.dart';
import 'package:surya_husadha/components/com_landingpage/home_denpasar.dart';
import 'package:surya_husadha/components/com_landingpage/home_group.dart';
import 'package:surya_husadha/components/com_landingpage/home_nusadua.dart';
import 'package:surya_husadha/components/com_landingpage/home_ubung.dart';
import 'package:surya_husadha/components/com_article/category_page.dart';
import 'package:surya_husadha/components/com_fasilitas/fasilitas.dart';
import 'package:surya_husadha/components/com_fasilitas/fasilitasgroup.dart';
import 'package:surya_husadha/components/com_gmap/gmappage.dart';
import 'package:surya_husadha/components/com_kartuberobat/akunberobatkartu.dart';
import 'package:surya_husadha/components/com_loyalticard/addvoucher.dart';
import 'package:surya_husadha/components/com_loyalticard/detail_merchant.dart';
import 'package:surya_husadha/components/com_loyalticard/loyaltipage.dart';
import 'package:surya_husadha/components/com_loyalticard/mbr_voucher.dart';
import 'package:surya_husadha/components/com_loyalticard/tukarpoint.dart';
import 'package:surya_husadha/components/com_profile/AkunWebApps.dart';
import 'package:surya_husadha/components/com_profile/akunberobat.dart';
import 'package:surya_husadha/components/com_profile/akunberobatkeluarga.dart';
import 'package:surya_husadha/components/com_profile/akunmenu.dart';
import 'package:surya_husadha/components/com_profile/akundetail.dart';
import 'package:surya_husadha/components/com_profile/editform.dart';
import 'package:surya_husadha/components/com_profile/editformklg.dart';
import 'package:surya_husadha/components/com_profile/kartuidentitas.dart';
import 'package:surya_husadha/components/com_profile/keluargadetail.dart';
import 'package:surya_husadha/components/com_reservasi/clinics.dart';
import 'package:surya_husadha/components/com_reservasi/confirmdata.dart';
import 'package:surya_husadha/components/com_reservasi/doctors.dart';
import 'package:surya_husadha/components/com_reservasi/formrsv.dart';
import 'package:surya_husadha/components/com_reservasi/pilihtipePx.dart';
import 'package:surya_husadha/components/com_reservasi/reservasi_list.dart';
import 'package:surya_husadha/components/com_reservasi/schedules.dart';
import 'package:surya_husadha/helper/database/akun/akun_db.dart';
import 'package:surya_husadha/helper/transition/fade_route.dart';
import 'package:surya_husadha/helper/transition/slide_route.dart';
import 'package:surya_husadha/pages/widgetpage.dart';
import 'package:surya_husadha/pages/webview.dart';

class RouteHelper {

  var db = new akunDB();
  List _akun;

  var context;
  var Pagename;
  var params;

  RouteHelper(@required this.context, @required this.Pagename, this.params) {


    switch (Pagename) {
      //BERANDA
      case "group":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          //Navigator.pushAndRemoveUntil(context, SlideTopRoute(page: Fullpage(widgetpage: BerandaGr(dtakun: _akun, params: null,), pagename: "BerandaGr")), (Route<dynamic> route) => false,);
          Navigator.pushReplacement(context, SlideTopRoute(page: Fullpage(widgetpage: BerandaGr(dtakun: _akun, params: null), pagename: "BerandaGr")));
        });
        break;
      case "denpasar":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          //Navigator.pushAndRemoveUntil(context, SlideTopRoute(page: Fullpage(widgetpage: BerandaDps(dtakun: _akun, params: null,), pagename: "BerandaDps")), (Route<dynamic> route) => false,);
          Navigator.pushReplacement(context, SlideTopRoute(page: Fullpage(widgetpage: BerandaDps(dtakun: _akun, params: null), pagename: "BerandaDps")));
        });
        break;
      case "ubung":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context, SlideTopRoute(page: Fullpage(widgetpage: BerandaUb(dtakun: _akun, params: null), pagename: "BerandaUb")));
          //Navigator.pushAndRemoveUntil(context, SlideTopRoute(page: Fullpage(widgetpage: BerandaUb(dtakun: _akun, params: null,), pagename: "BerandaDps")), (Route<dynamic> route) => false,);
        });
        break;
      case "nusadua":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context, SlideTopRoute(page: Fullpage(widgetpage: BerandaNd(dtakun: _akun, params: null), pagename: "BerandaNd")));
          //Navigator.pushAndRemoveUntil(context, SlideTopRoute(page: Fullpage(widgetpage: BerandaNd(dtakun: _akun, params: null,), pagename: "BerandaNd")), (Route<dynamic> route) => false,);
        });
        break;
      case "kmc":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          List<Map<String, dynamic>> params=[];
          params.add({
            "_cabang": "kmc",
            "_url": "http://kmc-clinic.com/",
          });
          Navigator.pushAndRemoveUntil(context, SlideTopRoute(page:WebViewPage(params, _akun)), (Route<dynamic> route) => false,);
        });
        break;
    //RESERVASI
      case "Clinics":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context, SlideLeftRoute(page: Fullpage(widgetpage: Clinics(params, _akun), pagename: "ReservasiClinics",)));
        });
        break;
      case "Doctors":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(
              context, SlideLeftRoute(page: Fullpage(widgetpage: Doctors(params, _akun), pagename: "ReservasiDoctors")));
        });
        break;
      case "Schedules":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(
              context, SlideLeftRoute(page: Fullpage(widgetpage: Schedules(params, _akun), pagename: "ReservasiSchedules")));
        });
        break;
      case "Formrsv":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context, SlideLeftRoute(page: Fullpage(widgetpage: Formrsv(params, _akun), pagename: "ReservasiFormrsv")));
        });
        break;
      case "tipePx":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(
              context, SlideLeftRoute(page: Fullpage(widgetpage: tipePx(params, _akun), pagename: "ReservasitipePx")));
        });
        break;
      case "ConfirmData":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(
              context, SlideLeftRoute(page: Fullpage(widgetpage:  ConfirmData(params, _akun), pagename: "ReservasiConfirmData")));
        });
        break;
      case "ReservasiList":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context, SlideLeftRoute(page: Fullpage(widgetpage: ReservasiList(params, _akun), pagename: "ReservasiList",)));
          //Navigator.push(context, SlideTopRoute(
              //page: Fullpage(widgetpage: ReservasiList(params, _akun), pagename: "ReservasiList",))); //ok
        });
        break;
      case "ReservasiListPop":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(
              context, SlideLeftRoute(page: Fullpage(widgetpage:  ReservasiList(params, _akun), pagename: "ReservasiList")));//ok
        });
        break;
    //KONTEN
      case "CategoryPage":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context, SlideLeftRoute(page: Fullpage(widgetpage: CategoryPage(params, _akun), pagename: "CategoryPage")));
        });
        break;
      case "Fasilitas":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context, SlideLeftRoute(page: Fullpage(widgetpage: Fasilitas(params, _akun), pagename: "Fasilitas")));
        });
        break;
      case "FasilitasGroup":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(
              context, SlideLeftRoute(page: Fullpage(widgetpage: FasilitasGroup(params, _akun), pagename: "FasilitasGroup(params, _akun)")));
        });
        break;
    //AKUN
      case "KartuIdentitas":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context, FadeRoute(
              page: Fullpage(widgetpage: KartuIdentitas(params, _akun), pagename: "KartuIdentitas",))); //ok
        });
        break;
      case "AkunMenu":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }

          Navigator.pushReplacement(
            context,
            SlideLeftRoute(
              page:Fullpage(widgetpage: AkunMenu(params, _akun), pagename: "AkunMenu",)),
          );
        });
        break;
      case "AkunWebApps":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context,
              FadeRoute(page: Fullpage(widgetpage: AkunWebApps(params, _akun), pagename: "AkunWebApps",))); //ok
        });
        break;
      case "AkunDetail":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context,
              FadeRoute(page: Fullpage(widgetpage: AkunDetail(params, _akun), pagename: "AkunDetail",))); //ok
        });
        break;
      case "AkunBerobat":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context,
              FadeRoute(page: Fullpage(widgetpage: AkunBerobat(params, _akun), pagename: "AkunBerobat",))); //ok
        });
        break;
      case "KeluargaDetail":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context, FadeRoute(
              page: Fullpage(widgetpage: KeluargaDetail(params, _akun), pagename: "KeluargaDetail"))); //ok
        });
        break;
      case "AkunBerobatKeluarga":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context, FadeRoute(
              page: Fullpage(widgetpage: AkunBerobatKeluarga(params, _akun), pagename: "AkunBerobatKeluarga"))); //ok
        });
        break;
      case "AkunBerobatKartu":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context, FadeRoute(
              page: Fullpage(widgetpage: AkunBerobatKartu(params, _akun),pagename: "AkunBerobatKartu",))); //ok
        });
        break;
      case "EditForm":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(
              context, FadeRoute(page: Fullpage(pagename: "EditForm", widgetpage: EditForm(params, _akun),)))
              .then((val) {
            print("EditForm: " + val.toString());
            return val;
          }); //ok
        });
        break;
      case "EditformKlg":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(
              context, FadeRoute(page: Fullpage(pagename: "EditformKlg", widgetpage: EditformKlg(params, _akun),)))
              .then((val) {
            print("EditformKlg: " + val.toString());
            return val;
          }); //ok
        });
        break;
      //GmapPage
      case "GmapPage":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context,
              FadeRoute(page: Fullpage(pagename: "GmapPage",widgetpage: GmapPage(params, _akun)))); //ok
        });
        break;
      case "WebView":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushAndRemoveUntil(context, FadeRoute(page:WebViewPage(params, _akun)), (Route<dynamic> route) => false,);
        });
        break;
    //LoyaltiPage
      case "LoyaltiPage":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context, SlideLeftRoute(page: Fullpage(widgetpage: LoyaltiPage(params, _akun), pagename: "LoyaltiPage")));
        });
        break;
      case "TukarPoint":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context, SlideLeftRoute(page: Fullpage(widgetpage: TukarPoint(params, _akun), pagename: "TukarPoint")));
        });
        break;
      case "AddVoucher":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushAndRemoveUntil(context, SlideLeftRoute(page: Fullpage(widgetpage: AddVoucher(params, _akun), pagename: "AddVoucher")), (Route<dynamic> route) => false,);
          //Navigator.push(context, SlideTopRoute(page: Fullpage(widgetpage: AddVoucher(params, _akun), pagename: "AddVoucher")));
        });
        break;
      case "DetailMerchant":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context, FadeRoute(page: Fullpage(widgetpage: DetailMerchant(params, _akun), pagename: "DetailMerchant")));
          //Navigator.pushAndRemoveUntil(context, SlideTopRoute(page: Fullpage(widgetpage: DetailMerchant(params, _akun), pagename: "DetailMerchant")), (Route<dynamic> route) => false,);
        });
        break;
      case "MbrVoucher":
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushReplacement(context, FadeRoute(page: Fullpage(widgetpage: MbrVoucher(params, _akun), pagename: "MbrVoucher")));
          //Navigator.pushAndRemoveUntil(context, SlideTopRoute(page: Fullpage(widgetpage: DetailMerchant(params, _akun), pagename: "DetailMerchant")), (Route<dynamic> route) => false,);
        });
        break;
      default:
        db.getCount().then((i) async {
          if (i > 0) {
            _akun = await db.getAllAkun();
          }
          Navigator.pushAndRemoveUntil(context, SlideTopRoute(page: Fullpage(widgetpage: BerandaGr(dtakun: _akun, params: null,), pagename: "BerandaGr")), (Route<dynamic> route) => false,);
        });
        break;
    };
  }

}