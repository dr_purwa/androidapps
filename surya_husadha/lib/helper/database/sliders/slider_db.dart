import 'dart:async';
import 'package:package_info/package_info.dart';
import 'package:surya_husadha/helper/database/sliders/slider_models.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class slidersDB {

  static final slidersDB _instance = new slidersDB.internal();
  factory slidersDB() => _instance;
  final String tableImage = 'imgsliders';
  final String columnId = 'id';
  final String srvid = 'srvid';
  final String columnTitle = 'title';
  final String columnUrl = 'url';
  final String lastupdate = 'lastupdate';
  static Database _db;
  slidersDB.internal();

  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    _packageInfo = info;
  }

  Future<Database> get db async {
    if (_db != null) {return _db;}
    _db = await initDb();
    return _db;
  }

  initDb() async {
    _initPackageInfo();
    String databasesPath = await getDatabasesPath();
    String path = join(databasesPath, tableImage+'.db');
    var db = await openDatabase(path, version: int.parse(_packageInfo.buildNumber), onCreate: _onCreate);
    return db;
  }

  Future deleteDB() async {
    String databasesPath = await getDatabasesPath();
    String path = join(databasesPath, tableImage+'.db');
    await deleteDatabase(path);
    return db;
  }

  void _onCreate(Database db, int newVersion) async {
    await db.execute('CREATE TABLE $tableImage($columnId INTEGER PRIMARY KEY, $srvid TEXT, $columnTitle TEXT, $columnUrl TEXT, $lastupdate TEXT)');
  }

  Future<int> getCount() async {
    var dbClient = await db;
    return Sqflite.firstIntValue(await dbClient.rawQuery('SELECT COUNT(*) FROM $tableImage'));
  }

  Future<List> getAllImages() async {
    var dbClient = await db;
    var result = await dbClient.query(tableImage, columns: [columnId, srvid, columnTitle, columnUrl, lastupdate]);
    return result;
  }

  Future<int> saveImage(Images img) async {
    var dbClient = await db;
    var result = await dbClient.insert(tableImage, img.toMap());
    return result;
  }

  Future<Images> getImage(int id) async {
    var dbClient = await db;
    List<Map> result = await dbClient.query(tableImage,
        columns: [columnId, srvid, columnTitle, columnUrl, lastupdate],
        where: '$columnId = ?',
        whereArgs: [id]);
    if (result.length > 0) {
      return new Images.fromMap(result.first);
    }
    return null;
  }

  Future<int> updateImage(Images img) async {
    var dbClient = await db;
    return await dbClient.update(tableImage, img.toMap(), where: "$columnId = ?", whereArgs: [img.srvid]);
  }

  Future<int> deleteImage(int id) async {
    var dbClient = await db;
    return await dbClient.delete(tableImage, where: '$columnId = ?', whereArgs: [id]);
  }

  Future close() async {
    var dbClient = await db;
    return dbClient.close();
  }
}
