class Images {
  int _id;
  String _srvid;
  String _title;
  String _url;
  String _lastupdate;

  Images(this._srvid, this._title, this._url, this._lastupdate);

  Images.map(dynamic obj) {
    this._id = obj['id'];
    this._srvid = obj['srvid'];
    this._title = obj['title'];
    this._url = obj['url'];
    this._lastupdate = obj['lastupdate'];
  }

  int get id => _id;
  String get srvid => _srvid;
  String get title => _title;
  String get url => _url;
  String get lastupdate => _lastupdate;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    if (_id != null) {
      map['id'] = _id;
    }
    map['srvid'] = _srvid;
    map['title'] = _title;
    map['url'] = _url;
    map['lastupdate'] = _lastupdate;
    return map;
  }

  Images.fromMap(Map<String, dynamic> rmap) {
    this._id = rmap['id'];
    this._srvid = rmap['srvid'];
    this._title = rmap['title'];
    this._url = rmap['url'];
    this._lastupdate = rmap['lastupdate'];
  }
}
