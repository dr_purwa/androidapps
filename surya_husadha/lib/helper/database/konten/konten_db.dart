import 'dart:async';
import 'package:package_info/package_info.dart';
import 'package:surya_husadha/helper/database/konten/konten_models.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class kontenDB {

  static final kontenDB _instance = new kontenDB.internal();
  factory kontenDB() => _instance;
  final String table = 'tbl_konten';
  final String columnId = 'id';
  final String cat_id = 'cat_id';
  final String cat_name_id = 'cat_name_id';
  final String cat_name_en = 'cat_name_en';
  final String image_default = 'image_default';
  final String galeries = 'galeries';
  final String enabled_en = 'enabled_en';
  final String enabled_id = 'enabled_id';
  final String title_en = 'title_en';
  final String title_id = 'title_id';
  final String short_content_en = 'short_content_en';
  final String short_content_id = 'short_content_id';
  final String full_content_en = 'full_content_en';
  final String full_content_id = 'full_content_id';
  final String start_date = 'start_date';
  final String end_date = 'end_date';
  final String metakey_en = 'metakey_en';
  final String metakey_id = 'metakey_id';
  final String created_time = 'created_time';
  final String edited_time = 'edited_time';
  final String name = 'name';
  final String pp = 'pp';
  final String lastupdate = 'lastupdate';
  static Database _db;
  kontenDB.internal();

  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    _packageInfo = info;
  }

  Future<Database> get db async {
    if (_db != null) {return _db;}
    _db = await initDb();
    return _db;
  }

  initDb() async {
    _initPackageInfo();
    String databasesPath = await getDatabasesPath();
    String path = join(databasesPath, table+'.db');
    var db = await openDatabase(path, version: int.parse(_packageInfo.buildNumber), onCreate: _onCreate);
    return db;
  }

  Future deleteDB() async {
    String databasesPath = await getDatabasesPath();
    String path = join(databasesPath, table+'.db');
    await deleteDatabase(path);
    return db;
  }

  void _onCreate(Database db, int newVersion) async {
    await db.execute('CREATE TABLE $table($columnId INTEGER PRIMARY KEY, '
        '$cat_id TEXT, '
        '$cat_name_id TEXT, '
        '$cat_name_en TEXT, '
        '$image_default TEXT, '
        '$galeries TEXT, '
        '$enabled_en TEXT, '
        '$enabled_id TEXT, '
        '$title_en TEXT, '
        '$title_id TEXT, '
        '$short_content_en TEXT, '
        '$short_content_id TEXT, '
        '$full_content_en TEXT, '
        '$full_content_id TEXT, '
        '$start_date TEXT, '
        '$end_date TEXT, '
        '$metakey_en TEXT, '
        '$metakey_id TEXT, '
        '$created_time TEXT, '
        '$edited_time TEXT, '
        '$name TEXT, '
        '$pp TEXT, '
        '$lastupdate TEXT '
        ')'
    );
  }

  Future<int> getCount() async {
    var dbClient = await db;
    return Sqflite.firstIntValue(await dbClient.rawQuery('SELECT COUNT(*) FROM '+table));
  }

  Future<List> getAllKonten() async {
    var dbClient = await db;
    var result = await dbClient.query(table, columns: [
      columnId,
      cat_id,
      cat_name_id,
      cat_name_en,
      image_default,
      galeries,
      enabled_en,
      enabled_id,
      title_en,
      title_id,
      short_content_en,
      short_content_id,
      full_content_en,
      full_content_id,
      start_date,
      end_date,
      metakey_en,
      metakey_id,
      created_time,
      edited_time,
      name,
      pp,
      lastupdate
    ]);
    return result;
  }

  Future<List> getCatKonten(String catid) async {
    var dbClient = await db;
    var result = await dbClient.query(table, columns: [
      columnId,
      cat_id,
      cat_name_id,
      cat_name_en,
      image_default,
      galeries,
      enabled_en,
      enabled_id,
      title_en,
      title_id,
      short_content_en,
      short_content_id,
      full_content_en,
      full_content_id,
      start_date,
      end_date,
      metakey_en,
      metakey_id,
      created_time,
      edited_time,
      name,
      pp,
      lastupdate
    ], where: "$cat_id = ?", whereArgs: [catid], orderBy: "$columnId DESC");
    return result;
  }

  Future<int> saveKonten(Konten dt) async {
    var dbClient = await db;
    var result = await dbClient.insert(table, dt.toMap());
    return result;
  }

  Future<Konten> getkonten(int id) async {
    var dbClient = await db;
    List<Map> result = await dbClient.query(table,
        columns: [
          columnId,
          cat_id,
          cat_name_id,
          cat_name_en,
          image_default,
          galeries,
          enabled_en,
          enabled_id,
          title_en,
          title_id,
          short_content_en,
          short_content_id,
          full_content_en,
          full_content_id,
          start_date,
          end_date,
          metakey_en,
          metakey_id,
          created_time,
          edited_time,
          name,
          pp,
          lastupdate
        ],
        where: '$columnId = ?',
        whereArgs: [id]);
    if (result.length > 0) {
      return new Konten.fromMap(result.first);
    }else{
      return null;
    }
  }

  Future<int> updateKonten(Konten dt) async {
    var dbClient = await db;
    return await dbClient.update(table, dt.toMap(), where: "$columnId = ?", whereArgs: [dt.id]);
  }

  Future<int> deleteKonten(int id) async {
    var dbClient = await db;
    return await dbClient.delete(table, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<int> deleteTable(int id) async {
    var dbClient = await db;
    return await dbClient.delete(table);
  }

  Future close() async {
    var dbClient = await db;
    return dbClient.close();
  }
}
