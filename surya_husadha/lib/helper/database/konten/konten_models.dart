class Konten {
  int _id;
  String _cat_id;
  String _cat_name_id;
  String _cat_name_en;
  String _image_default;
  String _galeries;
  String _enabled_en;
  String _enabled_id;
  String _title_en;
  String _title_id;
  String _short_content_en;
  String _short_content_id;
  String _full_content_en;
  String _full_content_id;
  String _start_date;
  String _end_date;
  String _metakey_en;
  String _metakey_id;
  String _created_time;
  String _edited_time;
  String _name;
  String _pp;
  String _lastupdate;

  Konten(
      this._id,
      this._cat_id,
      this._cat_name_id,
      this._cat_name_en,
      this._image_default,
      this._galeries,
      this._enabled_en,
      this._enabled_id,
      this._title_en,
      this._title_id,
      this._short_content_en,
      this._short_content_id,
      this._full_content_en,
      this._full_content_id,
      this._start_date,
      this._end_date,
      this._metakey_en,
      this._metakey_id,
      this._created_time,
      this._edited_time,
      this._name,
      this._pp,
      this._lastupdate,
      );

  Konten.map(dynamic obj) {
    this._id = obj['id'];
    this._cat_id = obj['cat_id'];
    this._cat_name_id = obj['cat_name_id'];
    this._cat_name_en = obj['cat_name_en'];
    this._image_default = obj['image_default'];
    this._galeries = obj['galeries'];
    this._enabled_en = obj['enabled_en'];
    this._enabled_id = obj['enabled_id'];
    this._title_en = obj['title_en'];
    this._title_id = obj['title_id'];
    this._short_content_en = obj['short_content_en'];
    this._short_content_id = obj['short_content_id'];
    this._full_content_en = obj['full_content_en'];
    this._full_content_id = obj['full_content_id'];
    this._start_date = obj['start_date'];
    this._end_date = obj['end_date'];
    this._metakey_en = obj['metakey_en'];
    this._metakey_id = obj['metakey_id'];
    this._created_time = obj['created_time'];
    this._edited_time = obj['edited_time'];
    this._name = obj['name'];
    this._pp = obj['pp'];
    this._lastupdate = obj['lastupdate'];
  }

  int get id => _id;
  String get cat_id => _cat_id;
  String get cat_name_id => _cat_name_id;
  String get cat_name_en => _cat_name_en;
  String get image_default => _image_default;
  String get galeries => _galeries;
  String get enabled_en => _enabled_en;
  String get enabled_id => _enabled_id;
  String get title_en => _title_en;
  String get title_id => _title_id;
  String get short_content_en => _short_content_en;
  String get short_content_id => _short_content_id;
  String get full_content_en => _full_content_en;
  String get full_content_id => _full_content_id;
  String get start_date => _start_date;
  String get end_date => _end_date;
  String get metakey_en => _metakey_en;
  String get metakey_id => _metakey_id;
  String get created_time => _created_time;
  String get edited_time => _edited_time;
  String get name => _name;
  String get pp => _pp;
  String get lastupdate => _lastupdate;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    if (_id != null) {
      map['id'] = _id;
    }
    map['cat_id'] = _cat_id;
    map['cat_name_id'] = _cat_name_id;
    map['cat_name_en'] = _cat_name_en;
    map['image_default'] = _image_default;
    map['galeries'] = _galeries;
    map['enabled_en'] = _enabled_en;
    map['enabled_id'] = _enabled_id;
    map['title_en'] = _title_en;
    map['title_id'] = _title_id;
    map['short_content_en'] = _short_content_en;
    map['short_content_id'] = _short_content_id;
    map['full_content_en'] = _full_content_en;
    map['full_content_id'] = _full_content_id;
    map['start_date'] = _start_date;
    map['end_date'] = _end_date;
    map['metakey_en'] = _metakey_en;
    map['metakey_id'] = _metakey_id;
    map['created_time'] = _created_time;
    map['edited_time'] = _edited_time;
    map['name'] = _name;
    map['pp'] = _pp;
    map['lastupdate'] = _lastupdate;
    return map;
  }

  Konten.fromMap(Map<String, dynamic> rmap) {
    this._id = rmap['id'];
    this._cat_id= rmap['cat_id'];
    this._cat_name_id= rmap['cat_name_id'];
    this._cat_name_en= rmap['cat_name_en'];
    this._image_default= rmap['image_default'];
    this._galeries= rmap['galeries'];
    this._enabled_en= rmap['enabled_en'];
    this._enabled_id= rmap['enabled_id'];
    this._title_en= rmap['title_en'];
    this._title_id= rmap['title_id'];
    this._short_content_en= rmap['short_content_en'];
    this._short_content_id= rmap['short_content_id'];
    this._full_content_en= rmap['full_content_en'];
    this._full_content_id= rmap['full_content_id'];
    this._start_date= rmap['start_date'];
    this._end_date= rmap['end_date'];
    this._metakey_en= rmap['metakey_en'];
    this._metakey_id= rmap['metakey_id'];
    this._created_time= rmap['created_time'];
    this._edited_time= rmap['edited_time'];
    this._name= rmap['name'];
    this._pp= rmap['pp'];
    this._lastupdate= rmap['lastupdate'];
  }
}
