import 'dart:async';
import 'package:package_info/package_info.dart';
import 'package:surya_husadha/helper/database/akun/akun_models.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class akunDB {
  static final akunDB _instance = new akunDB.internal();
  factory akunDB() => _instance;
  final String table = 'tbl_akun';
  final String columnId ='id';
  final String group_id ='group_id';
  final String pp ='pp';
  final String name ='name';
  final String bod ='bod';
  final String bodcity ='bodcity';
  final String bpjs_id ='bpjs_id';
  final String nrmshh ='nrmshh';
  final String nrmub ='nrmub';
  final String nrmnd ='nrmnd';
  final String nrmkmc ='nrmkmc';
  final String nik ='nik';
  final String username ='username';
  final String passwd ='passwd';
  final String email ='email';
  final String phone ='phone';
  final String address ='address';
  final String agama ='agama';
  final String pekerjaan ='pekerjaan';
  final String pendidikan ='pendidikan';
  final String ibu ='ibu';
  final String idCardtype ='idCardtype';
  final String NoidCard ='NoidCard';
  final String nasionality ='nasionality';
  final String reg_time ='reg_time';
  final String marital ='marital';
  final String gender ='gender';
  final String enabled ='enabled';
  final String activasi_link ='activasi_link';
  final String nonactive_link ='nonactive_link';
  final String reset_link ='reset_link';
  final String note ='note';
  final String defaultlang ='defaultlang';
  final String edittime ='edittime';
  final String isfirsttime ='isfirsttime';
  static Database _db;
  akunDB.internal();

  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    _packageInfo = info;
  }

  Future<Database> get db async {
    if (_db != null) {return _db;}
    _db = await initDb();
    return _db;
  }

  initDb() async {
    _initPackageInfo();
    String databasesPath = await getDatabasesPath();
    String path = join(databasesPath, table+'.db');
    var db = await openDatabase(path, version: int.parse(_packageInfo.buildNumber), onCreate: _onCreate);
    return db;
  }

  Future deleteDB() async {
    _initPackageInfo();
    String databasesPath = await getDatabasesPath();
    String path = join(databasesPath, table+'.db');
    await deleteDatabase(path);
    return db;
  }

  void _onCreate(Database db, int newVersion) async {
    await db.execute('CREATE TABLE $table('
        '$columnId INTEGER PRIMARY KEY, '
        '$group_id  TEXT, '
        '$pp  TEXT, '
        '$name  TEXT, '
        '$bod  TEXT, '
        '$bodcity  TEXT, '
        '$bpjs_id  TEXT, '
        '$nrmshh  TEXT, '
        '$nrmub  TEXT, '
        '$nrmnd  TEXT, '
        '$nrmkmc  TEXT, '
        '$nik  TEXT, '
        '$username  TEXT, '
        '$passwd  TEXT, '
        '$email  TEXT, '
        '$phone  TEXT, '
        '$address  TEXT, '
        '$agama  TEXT, '
        '$pekerjaan  TEXT, '
        '$pendidikan  TEXT, '
        '$ibu  TEXT, '
        '$idCardtype  TEXT, '
        '$NoidCard  TEXT, '
        '$nasionality  TEXT, '
        '$reg_time  TEXT, '
        '$marital  TEXT, '
        '$gender  TEXT, '
        '$enabled  TEXT, '
        '$activasi_link  TEXT, '
        '$nonactive_link  TEXT, '
        '$reset_link  TEXT, '
        '$note  TEXT, '
        '$defaultlang  TEXT, '
        '$edittime  TEXT, '
        '$isfirsttime  TEXT)'
    );
  }

  Future<int> getCount() async {
    var dbClient = await db;
    return Sqflite.firstIntValue(await dbClient.rawQuery('SELECT COUNT(*) FROM $table'));
  }

  Future<List> getAllAkun() async {
    var dbClient = await db;
    var result = await dbClient.query(table, columns: [
      columnId,
      group_id,
      pp,
      name,
      bod,
      bodcity,
      bpjs_id,
      nrmshh,
      nrmub,
      nrmnd,
      nrmkmc,
      nik,
      username,
      passwd,
      email,
      phone,
      address,
      agama,
      pekerjaan,
      pendidikan,
      ibu,
      idCardtype,
      NoidCard,
      nasionality,
      reg_time,
      marital,
      gender,
      enabled,
      activasi_link,
      nonactive_link,
      reset_link,
      note,
      defaultlang,
      edittime,
      isfirsttime,
    ]);
    return result;
  }

  Future<int> saveAkun(Akun dt) async {
      var dbClient = await db;
      var result = await dbClient.insert(table, dt.toMap()).catchError((e){
      if(e != null){
          deleteDB();
          saveAkun(dt);
        }
      });
      return result;
  }

  Future<Akun> getAkun(int id) async {
    var dbClient = await db;
    List<Map> result = await dbClient.query(table,
        columns: [
          columnId,
          group_id,
          pp,
          name,
          bod,
          bodcity,
          bpjs_id,
          nrmshh,
          nrmub,
          nrmnd,
          nrmkmc,
          nik,
          username,
          passwd,
          email,
          phone,
          address,
          agama,
          pekerjaan,
          pendidikan,
          ibu,
          idCardtype,
          NoidCard,
          nasionality,
          reg_time,
          marital,
          gender,
          enabled,
          activasi_link,
          nonactive_link,
          reset_link,
          note,
          defaultlang,
          edittime,
          isfirsttime,
        ],
        where: '$columnId = ?',
        whereArgs: [id]);
    if (result.length > 0) {
      return new Akun.fromMap(result.first);
    }
    return null;
  }

  Future<int> updateAkun(Akun dt) async {
    var dbClient = await db;
    return await dbClient.update(table, dt.toMap(), where: "$columnId = ?", whereArgs: [dt.id]);
  }

  Future<int> updateColumn(id, column, dt) async {
    var dbClient = await db;
    String updateQuery ="UPDATE $table SET $column='$dt' WHERE $columnId = '$id'";
    return await dbClient.execute(updateQuery, null);
  }

  Future<int> deleteAkun() async {
    var dbClient = await db;
    return await dbClient.delete(table);
  }

  Future close() async {
    var dbClient = await db;
    return dbClient.close();
  }
}
