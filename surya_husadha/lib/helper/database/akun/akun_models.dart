class Akun {
  int _id;
  String _group_id;
  String _pp;
  String _name;
  String _bod;
  String _bodcity;
  String _bpjs_id;
  String _nrmshh;
  String _nrmub;
  String _nrmnd;
  String _nrmkmc;
  String _nik;
  String _username;
  String _passwd;
  String _email;
  String _phone;
  String _address;
  String _agama;
  String _pekerjaan;
  String _pendidikan;
  String _ibu;
  String _idCardtype;
  String _NoidCard;
  String _nasionality;
  String _reg_time;
  String _marital;
  String _gender;
  String _enabled;
  String _activasi_link;
  String _nonactive_link;
  String _reset_link;
  String _note;
  String _defaultlang;
  String _edittime;
  String _isfirsttime;

  Akun(
      this._id,
      this._group_id,
      this._pp,
      this._name,
      this._bod,
      this._bodcity,
      this._bpjs_id,
      this._nrmshh,
      this._nrmub,
      this._nrmnd,
      this._nrmkmc,
      this._nik,
      this._username,
      this._passwd,
      this._email,
      this._phone,
      this._address,
      this._agama,
      this._pekerjaan,
      this._pendidikan,
      this._ibu,
      this._idCardtype,
      this._NoidCard,
      this._nasionality,
      this._reg_time,
      this._marital,
      this._gender,
      this._enabled,
      this._activasi_link,
      this._nonactive_link,
      this._reset_link,
      this._note,
      this._defaultlang,
      this._edittime,
      this._isfirsttime,
  );

  Akun.map(dynamic obj) {
    this._id = obj['id'];
    this._group_id = obj['group_id'];
    this._pp = obj['pp'];
    this._name = obj['name'];
    this._bod = obj['bod'];
    this._bodcity = obj['bodcity'];
    this._bpjs_id = obj['bpjs_id'];
    this._nrmshh = obj['nrmshh'];
    this._nrmub = obj['nrmub'];
    this._nrmnd = obj['nrmnd'];
    this._nrmkmc = obj['nrmkmc'];
    this._nik = obj['nik'];
    this._username = obj['username'];
    this._passwd = obj['passwd'];
    this._email = obj['email'];
    this._phone = obj['phone'];
    this._address = obj['address'];
    this._agama = obj['agama'];
    this._pekerjaan = obj['pekerjaan'];
    this._pendidikan = obj['pendidikan'];
    this._ibu = obj['ibu'];
    this._idCardtype = obj['idCardtype'];
    this._NoidCard = obj['NoidCard'];
    this._nasionality = obj['nasionality'];
    this._reg_time = obj['reg_time'];
    this._marital = obj['marital'];
    this._gender = obj['gender'];
    this._enabled = obj['enabled'];
    this._activasi_link = obj['activasi_link'];
    this._nonactive_link = obj['nonactive_link'];
    this._reset_link = obj['reset_link'];
    this._note = obj['note'];
    this._defaultlang = obj['defaultlang'];
    this._edittime = obj['edittime'];
    this._isfirsttime = obj['isfirsttime'];
  }

  int get id => _id;
  String get group_id => _group_id;
  String get pp => _pp;
  String get name => _name;
  String get bod => _bod;
  String get bodcity => _bodcity;
  String get bpjs_id => _bpjs_id;
  String get nrmshh => _nrmshh;
  String get nrmub => _nrmub;
  String get nrmnd => _nrmnd;
  String get nrmkmc => _nrmkmc;
  String get nik => _nik;
  String get username => _username;
  String get passwd => _passwd;
  String get email => _email;
  String get phone => _phone;
  String get address => _address;
  String get agama => _agama;
  String get pekerjaan => _pekerjaan;
  String get pendidikan => _pendidikan;
  String get ibu => _ibu;
  String get idCardtype => _idCardtype;
  String get NoidCard => _NoidCard;
  String get nasionality => _nasionality;
  String get reg_time => _reg_time;
  String get marital => _marital;
  String get gender => _gender;
  String get enabled => _enabled;
  String get activasi_link => _activasi_link;
  String get nonactive_link => _nonactive_link;
  String get reset_link => _reset_link;
  String get note => _note;
  String get defaultlang => _defaultlang;
  String get edittime => _edittime;
  String get isfirsttime => _isfirsttime;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    if (_id != null) {
      map['id'] = _id;
    }
    map['group_id'] = _group_id;
    map['pp'] = _pp;
    map['name'] = _name;
    map['bod'] = _bod;
    map['bodcity'] = _bodcity;
    map['bpjs_id'] = _bpjs_id;
    map['nrmshh'] = _nrmshh;
    map['nrmub'] = _nrmub;
    map['nrmnd'] = _nrmnd;
    map['nrmkmc'] = _nrmkmc;
    map['nik'] = _nik;
    map['username'] = _username;
    map['passwd'] = _passwd;
    map['email'] = _email;
    map['phone'] = _phone;
    map['address'] = _address;
    map['agama'] = _agama;
    map['pekerjaan'] = _pekerjaan;
    map['pendidikan'] = _pendidikan;
    map['ibu'] = _ibu;
    map['idCardtype'] = _idCardtype;
    map['NoidCard'] = _NoidCard;
    map['nasionality'] = _nasionality;
    map['reg_time'] = _reg_time;
    map['marital'] = _marital;
    map['gender'] = _gender;
    map['enabled'] = _enabled;
    map['activasi_link'] = _activasi_link;
    map['nonactive_link'] = _nonactive_link;
    map['reset_link'] = _reset_link;
    map['note'] = _note;
    map['defaultlang'] = _defaultlang;
    map['edittime'] = _edittime;
    map['isfirsttime'] = _isfirsttime;
    return map;
  }

  Akun.fromMap(Map<String, dynamic> rmap) {
    this._id = rmap['id'];
    this._group_id = rmap['group_id'];
    this._pp = rmap['pp'];
    this._name = rmap['name'];
    this._bod = rmap['bod'];
    this._bodcity = rmap['bodcity'];
    this._bpjs_id = rmap['bpjs_id'];
    this._nrmshh = rmap['nrmshh'];
    this._nrmub = rmap['nrmub'];
    this._nrmnd = rmap['nrmnd'];
    this._nrmkmc = rmap['nrmkmc'];
    this._nik = rmap['nik'];
    this._username = rmap['username'];
    this._passwd = rmap['passwd'];
    this._email = rmap['email'];
    this._phone = rmap['phone'];
    this._address = rmap['address'];
    this._agama = rmap['agama'];
    this._pekerjaan = rmap['pekerjaan'];
    this._pendidikan = rmap['pendidikan'];
    this._ibu = rmap['ibu'];
    this._idCardtype = rmap['idCardtype'];
    this._NoidCard = rmap['NoidCard'];
    this._nasionality = rmap['nasionality'];
    this._reg_time = rmap['reg_time'];
    this._marital = rmap['marital'];
    this._gender = rmap['gender'];
    this._enabled = rmap['enabled'];
    this._activasi_link = rmap['activasi_link'];
    this._nonactive_link = rmap['nonactive_link'];
    this._reset_link = rmap['reset_link'];
    this._note = rmap['note'];
    this._defaultlang = rmap['defaultlang'];
    this._edittime = rmap['edittime'];
    this._isfirsttime = rmap['isfirsttime'];
  }
}
