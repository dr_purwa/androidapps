import 'package:flutter/material.dart';

class WarnaCabang {
  static Color group = Color(0xffD5F0F3);
  static Color group2 = Color(0xffF0FAFB) ;
  static Color shh = Color(0xff22ABF0);
  static Color shh2 = Color(0xff6BC7F5);
  static Color ubung = Color(0xff0F70A1);
  static Color ubung2 = Color(0xff5E9FC0);
  static Color nusadua = Color(0xff37AD90);
  static Color nusadua2 = Color(0xff79C8B5);
  static Color kmc = Color(0xff00DBA5);
  static Color kmc2 = Color(0xff54E7C3);
  static Color shgreen = Color(0xffA9D49E);
  static Color shblue = Color(0xff0F70A1);
}

class SERVER{
  static String domain = "https://suryahusadha.com";
  static String host = "suryahusadha.com";
}