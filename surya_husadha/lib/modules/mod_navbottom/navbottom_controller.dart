import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';

class NavBottom extends StatefulWidget {

  Color _colorCabanggr;
  Color _colordefgrLg;
  Color _colordefgrtxt;

  Color _colorCabangshh;
  Color _colordefshhTxt;

  Color _colorCabangub;
  Color _colordefubTxt;

  Color _colorCabangnd;
  Color _colordefndTxt;

  Color _colorCabangkmc;
  Color _colordefkmcTxt;

  Color _colorCabang;
  Color _colorCabang2;

  String _Cabang;

  NavBottom(String cabang){
    this._Cabang = cabang;
    if(cabang == "group"){
      _colorCabang = WarnaCabang.group;
      _colorCabang2 = WarnaCabang.group2;
      _colorCabanggr = WarnaCabang.group2;
      _colordefgrLg = Colors.lightBlue;
      _colordefgrtxt = Colors.white;

      _colorCabangshh = WarnaCabang.group;
      _colordefshhTxt = Colors.grey[600];

      _colorCabangub = WarnaCabang.group;
      _colordefubTxt = Colors.grey[600];

      _colorCabangnd = WarnaCabang.group;
      _colordefndTxt = Colors.grey[600];

      _colorCabangkmc = WarnaCabang.group;
      _colordefkmcTxt = Colors.grey[600];
    }
    if(cabang == "denpasar"){
      _colorCabanggr = WarnaCabang.shh;
      _colordefgrLg =  Colors.grey[300];
      _colordefgrtxt = Colors.grey[400];

      _colorCabangshh = WarnaCabang.shh2;
      _colorCabang = WarnaCabang.shh;
      _colorCabang2 = WarnaCabang.shh2;
      _colordefshhTxt = Colors.white;

      _colorCabangub = WarnaCabang.shh;;
      _colordefubTxt = Colors.white;

      _colorCabangnd = WarnaCabang.shh;;
      _colordefndTxt = Colors.white;

      _colorCabangkmc = WarnaCabang.shh;;
      _colordefkmcTxt = Colors.white;
    }
    if(cabang == "ubung"){
      _colorCabanggr = WarnaCabang.ubung;
      _colordefgrLg =  Colors.grey[300];
      _colordefgrtxt = Colors.grey[400];

      _colorCabangshh = WarnaCabang.ubung;
      _colordefshhTxt = Colors.white;

      _colorCabangub = WarnaCabang.ubung2;
      _colorCabang = WarnaCabang.ubung;
      _colorCabang2 = WarnaCabang.ubung2;
      _colordefubTxt = Colors.white;

      _colorCabangnd = WarnaCabang.ubung;
      _colordefndTxt = Colors.white;

      _colorCabangkmc = WarnaCabang.ubung;
      _colordefkmcTxt = Colors.white;
    }
    if(cabang == "nusadua"){
      _colorCabanggr = WarnaCabang.nusadua;
      _colordefgrLg =  Colors.grey[300];
      _colordefgrtxt = Colors.grey[400];
      _colorCabang = WarnaCabang.nusadua;
      _colorCabang2 = WarnaCabang.nusadua2;

      _colorCabangshh = WarnaCabang.nusadua;
      _colordefshhTxt = Colors.white;

      _colorCabangub = WarnaCabang.nusadua;
      _colordefubTxt = Colors.white;

      _colorCabangnd = WarnaCabang.nusadua2;
      _colordefndTxt = Colors.white;

      _colorCabangkmc = WarnaCabang.nusadua;
      _colordefkmcTxt = Colors.white;
    }
    if(cabang == "kmc"){
      _colorCabanggr = WarnaCabang.kmc;
      _colordefgrLg =  Colors.grey[300];
      _colordefgrtxt = Colors.grey[400];

      _colorCabangshh = WarnaCabang.kmc;
      _colordefshhTxt = Colors.white;

      _colorCabangub = WarnaCabang.kmc;
      _colordefubTxt = Colors.white;

      _colorCabangnd = WarnaCabang.kmc;
      _colordefndTxt = Colors.white;

      _colorCabangkmc = WarnaCabang.kmc2;
      _colorCabang = WarnaCabang.kmc;
      _colorCabang2 = WarnaCabang.kmc2;
      _colordefkmcTxt = Colors.white;
    }
  }

  @override
  _NavBottomControllerState createState() => new _NavBottomControllerState();

}

class _NavBottomControllerState extends State<NavBottom> with SingleTickerProviderStateMixin {

  AnimationController _controllerbNav;

  void initState(){
    super.initState();
  }

  @override
  void dispose() {
    _controllerbNav.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _controllerbNav = new AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
    _controllerbNav.forward();
  }

  @override
  Widget build(BuildContext context) {
    return _buildBottomNavigation();
  }

  Widget _logoshh() {
    if(widget._Cabang == "denpasar"){
      return new Column(
          children: <Widget>[
            new Image.asset(
              "assets/logo/icon.png",
              height: 24.0,
            ),
          ]
      );
    }else{
      return new Column(
          children: <Widget>[
            new Image.asset(
              "assets/logo/icon.png",
              height: 24.0,
            ),
          ]
      );
    }
  }
  Widget _logoub() {
    if(widget._Cabang == "ubung"){
      return new Column(
          children: <Widget>[
            new Image.asset(
              "assets/logo/icon.png",
              height: 24.0,
            ),
          ]
      );
    }else{
      return new Column(
          children: <Widget>[
            new Image.asset(
              "assets/logo/icon.png",
              height: 24.0,
              //color: widget._colordefubLg,
            ),
          ]
      );
    }
  }
  Widget _logond() {
    if(widget._Cabang == "nusadua"){
      return new Column(
          children: <Widget>[
            new Image.asset(
              "assets/logo/icon.png",
              height: 24.0,
            ),
          ]
      );
    }else{
      return new Column(
          children: <Widget>[
            new Image.asset(
              "assets/logo/icon.png",
              height: 24.0,
              //color: widget._colordefndLg,
            ),
          ]
      );
    }
  }
  Widget _logokmc() {
    if(widget._Cabang == "kmc"){
      return new Column(
          children: <Widget>[
            new Image.asset(
              "assets/logo/icon.png",
              height: 24.0,
            ),
          ]
      );
    }else{
      return new Column(
          children: <Widget>[
            new Image.asset(
              "assets/logo/icon.png",
              height: 24.0,
              //color: widget._colordefkmcLg,
            ),
          ]
      );
    }
  }

  Widget _buildBottomNavigation(){
    return new Container(
      color: Colors.white,
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Container(
              color: widget._colorCabang,
              height: 5,
              child: new SlideTransition(
                position: Tween<Offset>(
                  begin: const Offset(-1, 0),
                  end: const Offset(0, 0),
                ).animate(_controllerbNav),
                child: new SizedBox(
                  child: Container(
                      color: widget._colorCabang2,
                  ),
                ),
              ),
            ),
            new Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Expanded(
                    flex: 2,
                    child: new GestureDetector(
                      child: new Container(
                        height: 55,
                        color: widget._colorCabanggr,
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Container(
                                height: 40.0,
                                width: 40.0,
                                padding: EdgeInsets.all(6.0),
                                decoration: new BoxDecoration(
                                    borderRadius: new BorderRadius.all(new Radius.circular(100.0)),
                                    color: widget._colordefgrLg,
                                ),
                                alignment: Alignment.center,
                                child: new Icon(
                                  FontAwesomeIcons.home,
                                  color: widget._colordefgrtxt,
                                  size: 20.0,
                                ),
                              ),
                            ]
                        ),
                      ),
                      onTap: () =>  onCardTapped(context, "group", null),
                    ),
                  ),
                  new Expanded(
                    flex: 2,
                    child: new GestureDetector(
                      child: new Container(
                        height: 55,
                        color: widget._colorCabangshh,
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              _logoshh(),
                              new Padding(padding:EdgeInsets.only(top:2)),
                              new Text(
                                "Denpasar",
                                style: new TextStyle(color: widget._colordefshhTxt,fontFamily: "NeoSansBold", fontSize: 12.0),
                              )
                            ]
                        ),
                      ),
                      onTap: () =>  onCardTapped(context, "denpasar", null),
                    ),
                  ),
                  new Expanded(
                    flex: 2,
                    child: new GestureDetector(
                      child: new Container(
                        height: 55,
                        color: widget._colorCabangub,
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              _logoub(),
                              new Padding(padding:EdgeInsets.only(top:2)),
                              new Text(
                                "Ubung",
                                style: new TextStyle(color: widget._colordefubTxt,fontFamily: "NeoSansBold", fontSize: 12.0),
                              )
                            ]
                        ),
                      ),
                      onTap: () =>  onCardTapped(context, "ubung", null),
                    ),
                  ),
                  new Expanded(
                    flex: 2,
                    child: new GestureDetector(
                      child: new Container(
                        height: 55,
                        color: widget._colorCabangnd,
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              _logond(),
                              new Padding(padding:EdgeInsets.only(top:2)),
                              new Text(
                                "Nusadua",
                                style: new TextStyle(color: widget._colordefndTxt,fontFamily: "NeoSansBold", fontSize: 12.0),
                              )
                            ]
                        ),
                      ),
                      onTap: () =>  onCardTapped(context, "nusadua", null),
                    ),
                  ),
                  new Expanded(
                    flex: 2,
                    child: new GestureDetector(
                      child: new Container(
                        height: 55,
                        color: widget._colorCabangkmc,
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              _logokmc(),
                              new Padding(padding:EdgeInsets.only(top:2)),
                              new Text(
                                "KMC",
                                style: new TextStyle(color: widget._colordefkmcTxt,fontFamily: "NeoSansBold", fontSize: 12.0),
                              )
                            ]
                        ),
                      ),
                      onTap: (){
                        onCardTapped(context, "kmc", null);
                      }
                    ),
                  ),
                ]
            ),
          ]
      ),
    );
  }

  onCardTapped(context, String pagename, List<Map<String, dynamic>> params) {
    if(widget._Cabang != pagename){
      RouteHelper(context, pagename, params);
    }
  }

}
