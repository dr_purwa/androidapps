import 'package:flutter/material.dart';
import 'package:surya_husadha/components/com_article/detail_article.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/database/konten/konten_db.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';

class FitureContent extends StatefulWidget {
  String _cabang;
  String _fctitle;
  String _fccatid;
  int _fclimit;
  Color _color1;
  Color _color2;
  var _akun;

  FitureContent(String s, String t, int u, color1, color2, cabang, akun){
    this._fctitle = s;
    this._fccatid = t;
    this._fclimit = u;
    this._color1 = color1;
    this._color2 = color2;
    this._cabang = cabang;
    this._akun = akun;
  }
  @override
  _FitureContent createState() => new _FitureContent();
}

class _FitureContent extends State<FitureContent> {

  @override
  Future<List> _getData() async {
    var db = new kontenDB();
    List _data;
    _data = await db.getCatKonten(widget._fccatid);
    return _data;
  }

  @override
  Widget _buildcontent(BuildContext context) {
    bool viewwbtn = false;
    return new FutureBuilder<List>(
        future: _getData(),
        builder: (context, snapshot){
          if(snapshot.hasData && (snapshot.connectionState == ConnectionState.done)){
            viewwbtn = false;
            if(snapshot.data.length > 0){
              viewwbtn = true;
              return new Container(
                color: Colors.white,
                margin: EdgeInsets.only(bottom: 16.0),
                padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    new Text(
                      widget._fctitle,
                      style: new TextStyle(color: Colors.grey[1000], fontFamily: "NeoSansBold", fontWeight: FontWeight.w700, fontSize: 16),
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 8.0),
                    ),
                    new Container(
                      width: double.infinity,
                      height: 1.0,
                      color: Colors.grey[200],
                    ),
                    new Column(
                      children: <Widget>[
                        ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: snapshot.data.length,
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          itemBuilder:(context, index){
                            if(index == 0){
                              return _fitures(snapshot.data, index);
                            }else{
                              if(index <= widget._fclimit){
                                return _rowContentList(snapshot.data, index);
                              }else{
                                return new Container();
                              }
                            }
                          },
                        ),
                        Visibility(
                          visible: viewwbtn,
                          child: new Container(
                            margin: EdgeInsets.only(top:10.0),
                            alignment: Alignment.centerRight,
                            child: new MaterialButton(
                                color: widget._color1,
                                onPressed: (){
                                  List<Map<String, dynamic>> params=[];
                                  params.add({
                                    "_cabang": widget._cabang,
                                    "_ctntid": widget._fccatid,
                                  });
                                  RouteHelper(context, "CategoryPage", params);
                                },
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    new Text(
                                      "Selengkapnya",
                                      style: new TextStyle(
                                          color: widget._color2,
                                          fontFamily: "NeoSansBold",
                                          fontSize: 12.0
                                      ),
                                    ),
                                    new Container(width: 5,),
                                    new Icon(
                                      FontAwesomeIcons.arrowRight,
                                      size: 12,
                                      color: widget._color2,
                                    )
                                  ],
                                )
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              );
            }else{
              return Container();
            }
          }else{
            return Container(
              margin: EdgeInsets.only(top: 20, bottom: 20),
              child : new Column(children: [
                SizedBox(
                  width: 50.0,
                  height: 50.0,
                  child: const CircularProgressIndicator(),
                ),
              ]),
            );
          }
        }
    );
  }
  Widget _fitures(dt, index) {
    return new Container(
      child: new InkWell(
        onTap: (){
          List<Map<String, dynamic>> dtNext=[];
          dtNext.add({"_cabang": widget._cabang});
          dtNext.add({"_ctntid": dt[index]["id"]});
          showDialog(
            barrierDismissible: true,
            context: context,
            builder: (BuildContext context){
              return new DetaiArticle(dtNext, widget._akun);
            },
          );
        },
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            new ClipRRect(
              borderRadius: new BorderRadius.circular(8.0),
              child: new Image.network(
                SERVER.domain + dt[index]["image_default"],
                height: 172.0,
                width: double.infinity,
                fit: BoxFit.cover,
              ),
            ),
            new Padding(
              padding: EdgeInsets.only(top: 16.0),
            ),
            new Text(
              dt[index]["title_id"],
              style: new TextStyle(color: Colors.grey[800],fontFamily: "NeoSansBold", fontSize: 18.0, fontWeight: FontWeight.w800),
            ),
            new Padding(
              padding: EdgeInsets.only(top: 8.0),
            ),
            new Text(
              dt[index]["short_content_id"],
              maxLines: 4,
              softWrap: true,
              style: new TextStyle(color: Colors.grey[600], fontFamily: "NeoSans", fontSize: 14.0),
            ),
            new Padding(
              padding: EdgeInsets.only(bottom: 20),
            ),
            new Container(
              width: double.infinity,
              height: 1.0,
              color: Colors.grey[200],
            ),
            new Padding(
              padding: EdgeInsets.only(bottom: 20),
            ),
          ],
        ),
      )
    );
  }

  Widget _rowContentList(dt, index) {
    return new Container(
      child: InkWell(
        onTap: (){
          List<Map<String, dynamic>> dtNext=[];
          dtNext.add({"_cabang": widget._cabang});
          dtNext.add({"_ctntid": dt[index]["id"]});
          showDialog(
            barrierDismissible: true,
            context: context,
            builder: (BuildContext context){
              return new DetaiArticle(dtNext, widget._akun);
            },
          );
        },
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Padding(
              padding: EdgeInsets.only(top: 8.0),
            ),
            new Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new ClipRRect(
                    borderRadius: new BorderRadius.circular(8.0),
                    child: new Image.network(
                      SERVER.domain + dt[index]["image_default"],
                      height: 80.0,
                      width: 80.0,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Flexible(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 4.0),
                            child: Text(
                              dt[index]["title_id"],
                              maxLines: 1,
                              softWrap: true,
                              overflow: TextOverflow.ellipsis,
                              style: new TextStyle(color: Colors.grey[800],fontFamily: "NeoSansBold", fontSize: 16.0, fontWeight: FontWeight.w600),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 4.0),
                            child: Text(
                              dt[index]["short_content_id"],
                              maxLines: 3,
                              softWrap: true,
                              overflow: TextOverflow.ellipsis,
                              style: new TextStyle(color: Colors.grey[600], fontFamily: "NeoSans", fontSize: 14.0),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ]
            ),
            new Container(
              margin: EdgeInsets.only(top: 8.0),
              width: double.infinity,
              height: 1.0,
              color: Colors.grey[200],
            ),
          ],
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return _buildcontent(context);
  }
}