import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:package_info/package_info.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/helper/transition/slide_route.dart';
import 'package:surya_husadha/components/com_login/com_login.dart';
import 'package:surya_husadha/pages/widgetpage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:convert' as convert;
import 'package:app_review/app_review.dart';

class AppBarController extends StatefulWidget {

  Color _color;
  Color _color2;
  Color _TextColor = Colors.white;
  String _logo;
  double _logoWidth = 2;
  int _indCabang;
  String _cabang;
  var _Scafoltkey;
  var _dtakun;
  String _page;

  AppBarController(String cabang, Scafoltkey, dtakun, String page){
    this._page = page;
    this._cabang = cabang;
    this._Scafoltkey = Scafoltkey;
    this._dtakun = dtakun;
    if(cabang == "group"){
      _indCabang = 0;
      _color = WarnaCabang.group;
      _color2 = WarnaCabang.group2;
      _TextColor = Colors.grey[600];
      _logo = "assets/logo/group.png";
      _logoWidth = 1.8;
    }
    if(cabang == "denpasar"){
      _indCabang = 1;
      _color = WarnaCabang.shh;
      _color2 = WarnaCabang.shh2;
      _logo = "assets/logo/shh2.png";
    }
    if(cabang == "ubung"){
      _indCabang = 2;
      _color = WarnaCabang.ubung;
      _color2 = WarnaCabang.ubung2;
      _logo = "assets/logo/ub2.png";
    }
    if(cabang == "nusadua"){
      _indCabang = 3;
      _color = WarnaCabang.nusadua;
      _color2 = WarnaCabang.nusadua2;
      _logo = "assets/logo/nd2.png";
    }
    if(cabang == "kmc"){
      _indCabang = 4;
      _color = WarnaCabang.kmc;
      _color2 = WarnaCabang.kmc2;
      _logo = "assets/logo/nd2.png";
    }
  }

  @override
  _AppBarController createState() => new _AppBarController();
}

class _AppBarController extends State<AppBarController> with SingleTickerProviderStateMixin{

  bool check = true;
  bool inet = false;
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  Future<String> getUpdateVersi() async {
    if(check){
      try {
        final result = await InternetAddress.lookup(SERVER.host);
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          inet = true;
          _initPackageInfo();
        }
      } on SocketException catch (_){
          inet = false;
      }
      check = false;
    }
  }

  bool showdialogue = false;

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    _packageInfo = info;
    check = true;
    var db = new Http_Controller();
    var http_response = await db.getResponse("https://suryahusadha.com/api/versi");
    var _data = convert.jsonEncode(http_response);
    var decode = convert.jsonDecode(_data);
    var decodeConvert = convert.jsonDecode(decode["responseBody"]);
    final dtkonten = decodeConvert["versi"];
    final dtversi = dtkonten[0]["data"];
    //print(_packageInfo.buildNumber.toString() +" - "+ dtversi[0]["versiCode"].toString());
    if(int.parse(_packageInfo.buildNumber ) < int.parse(dtversi[0]["versiCode"])){
      if(!showdialogue){
        showdialogue = true;
        showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) => _updatnotif(dtversi[0]),
        );
      }
    }
  }

  Widget _updatnotif(dt){
    return new Scaffold(
      backgroundColor: Colors.transparent,
      body: new WillPopScope(
        onWillPop:(){},
        child: new Center(
          child: new Container(
            height: 300,
            width: 300,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(20))
            ),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 16),
                ),
                new Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Icon(
                      FontAwesomeIcons.infoCircle,
                      size: 24,
                      color: Colors.grey[600],
                    ),
                    new Container(
                      margin: EdgeInsets.only(left: 10),
                      child: new Text(
                        "Informasi Update",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w700
                        ),
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(top: 16),
                ),
                new Container(
                  height: 1,
                  color: Colors.grey[300],
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                  ),
                ),
                new Expanded(
                  child: new Column(
                     children: <Widget>[
                       Padding(
                         padding: EdgeInsets.only(top: 16),
                       ),
                       new Container(
                         child: new Text(
                           "Versi Baru: "+dt["versiName"],
                           textAlign: TextAlign.center,
                         ),
                       ),
                       Padding(
                         padding: EdgeInsets.only(top: 16),
                       ),
                       new Container(
                         padding: EdgeInsets.only(left: 24, right: 24),
                         child: new Text(
                           dt["message_in"],
                           textAlign: TextAlign.center,
                         ),
                       ),
                     ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 24),
                ),
                new Container(
                  child: new Text("Terima Kasih"),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20),
                ),
                new Container(
                  height: 1,
                  color: Colors.grey[300],
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                  ),
                ),
                InkWell(
                  onTap: (){
                    AppReview.storeListing.then((String onValue) {
                      print("onValue: "+onValue);
                    });
                  },
                  child: new Container(
                    padding: EdgeInsets.all(16),
                    child: new Row(
                      children: <Widget>[
                        new Expanded(
                            child: new Row(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Text(
                                  "Perbarui Sekarang!",
                                  style: TextStyle(color: Colors.black),
                                ),
                              ],
                            )
                        ),
                        new Container(
                          padding: EdgeInsets.only(right: 10),
                          child: new Icon(
                            FontAwesomeIcons.arrowRight,
                            size: 12,
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      )
    );
  }

  AnimationController _controllerappBar;

  @override
  void initState() {
    getUpdateVersi();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _buildAppBarController();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _controllerappBar = new AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
    _controllerappBar.forward();
  }

  @override
  void dispose(){
    _controllerappBar.dispose();
    super.dispose();
  }


  Widget _buildAppBarController(){
    String _func = "akun";
    IconData iconUser;
    if(widget._page == "AkunMenu"
        || widget._page == "AkunDetail"
        || widget._page == "AkunBerobat"
        || widget._page == "AkunWebApps"
        || widget._page == "EditForm"
        || widget._page == "KartuIdentitas"
        || widget._page == "AkunBerobatKeluarga"
        || widget._page == "TambahKlg"
        || widget._page == "KeluargaDetail"
        || widget._page == "LoyaltiPage"
        || widget._page == "AkunBerobatKartu"
    ){
      _func = "close";
      iconUser = FontAwesomeIcons.times;
    }else{
      iconUser = FontAwesomeIcons.userCircle;
    }

    Widget icon;
    if (widget._dtakun != null ) {
      icon = new Container(
        child: new InkWell(
          onTap: () =>  onCardTapped(context, widget._indCabang, widget._cabang, _func),
          child: new Container(
            padding: EdgeInsets.only(left: 24, right: 24),
            alignment: Alignment.center,
            child: new Icon(
              iconUser,
              color: widget._TextColor,
              size: 24.0,
            ),
          ),
        ),
      );
      return _appbar(icon);
    }else{
      icon  = new Container(
        child: new InkWell(
          onTap: () =>  onCardTapped(context, widget._indCabang, widget._cabang, "login"),
          child: new Container(
            padding: EdgeInsets.only(left: 24, right: 24),
            alignment: Alignment.center,
            child: new Icon(
              FontAwesomeIcons.signInAlt,
              color: widget._TextColor,
              size: 24.0,
            ),
          ),
        ),
      );
      return _appbar(icon);
    };
  }

  Widget _appbar(icon){
    return new Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        new Container(
            color: widget._color,
            height: 55,
            child:Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: new Row(
                    children: <Widget>[
                      new Container(
                        child: new InkWell(
                          onTap: () =>  onCardTapped(context, widget._indCabang, widget._cabang, "drawer"),
                          child: new Container(
                            padding: EdgeInsets.only(left: 24, right: 24),
                            alignment: Alignment.center,
                            child: new Icon(
                              FontAwesomeIcons.bars,
                              color: widget._TextColor,
                              size: 24.0,
                            ),
                          ),
                        ),
                      ),
                      new Container(
                        child: new Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            new Image.asset(
                              widget._logo,
                              //height: 30
                              width: MediaQuery.of(context).size.width / widget._logoWidth,
                            ),
                          ],
                        ),
                      ),

                    ],
                  ),
                ),
                icon
              ],
            )
        ),
        new Container(
          color: widget._color,
          height: 2,
          child: new SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(-1, 0),
              end: const Offset(0, 0),
            ).animate(_controllerappBar),
            child: new SizedBox(
              child: Container(
                  color: Colors.white
              ),
            ),
          ),
        ),
      ],
    );
  }

  onCardTapped(context, int position, String cabang, String func) {
    if(func == "login"){
      Navigator.pushReplacement(context, SlideLeftRoute(page: Fullpage(widgetpage: LoginController(widget._cabang), pagename: "LoginPage",)));//ok
    }
    if(func == "drawer"){
      widget._Scafoltkey.currentState.openDrawer();
    }
    if(func == "akun"){
      List<Map<String, dynamic>> params=[];
      params.add({
        "_cabang": widget._cabang,
      });
      RouteHelper(context, "AkunMenu", params);
    }
    if(func == "close"){
      //Navigator.pop(context);
      List<Map<String, dynamic>> params=[];
      params.add({
        "_cabang": widget._cabang,
      });
      switch(widget._page) {
        case "AkunMenu": {
          RouteHelper(context, widget._cabang, params);
        }
        break;
        case "AkunBerobatKartu": {
          RouteHelper(context, widget._cabang, params);
        }
        break;
        case "TambahKlg": {
          RouteHelper(context, "AkunBerobatKeluarga", params);
        }
        break;
        case "KeluargaDetail": {
          RouteHelper(context, "AkunBerobatKeluarga", params);
        }
        break;
        case "LoyaltiPage": {
          RouteHelper(context, widget._cabang, params);
        }
        break;
        default: {
          RouteHelper(context, "AkunMenu", params);
        }
        break;
      }
    }
  }
}