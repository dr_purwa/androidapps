import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'dart:convert' as convert;

final List<String> imgList = [];

List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }
  return result;
}

class slidersController extends StatefulWidget {
  @override
  _CarouselWithIndicatorState createState() => _CarouselWithIndicatorState();
}

class _CarouselWithIndicatorState extends State<slidersController> {
  int _current = 0;
  bool isloading = true;

  @override
  void initState() {
    super.initState();
  }

  List child;

  @override
  Future<String> _getData() async {
    var _data;
    var db = new Http_Controller();
    var http_response = await db.getResponse("https://suryahusadha.com/api/sliders/get_sliders");
    _data = convert.jsonEncode(http_response);
    return _data;
  }

  @override
  Widget build(BuildContext context) {
    return new FutureBuilder(
        future: _getData(),
        builder: (context, snapshot) {
          if (snapshot.hasData && (snapshot.connectionState == ConnectionState.done) && isloading){
            imgList.clear();
            isloading = false;
            var decode = convert.jsonDecode(snapshot.data);
            var decodeConvert = convert.jsonDecode(decode["responseBody"]);
            final dtkonten = decodeConvert["sliders"];
            final sldata = dtkonten[0]["data"];
            final lastupdate = dtkonten[0]["lastupdate"];
            sldata.forEach((image) async {
              imgList.add(SERVER.domain+image["imagemobile"]);
              //await db.saveImage(new Images(image["id"].toString(), image["title"], SERVER.domain+image["imagemobile"], df.format(DateTime.now()).toString()));
            });
          }
          if(isloading){
            return Container(
              margin: EdgeInsets.only(top: 100, bottom: 100),
              child : new Column(children: [
                SizedBox(
                  width: 50.0,
                  height: 50.0,
                  child: const CircularProgressIndicator(),
                ),
              ]),
            );
          }else{
            child = map<Widget>(
              imgList, (index, i) {
                return Container(
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(0.0)),
                    child: Stack(children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(0.0)),
                        child: Image.network(
                          i,
                          fit: BoxFit.fill,
                          width: MediaQuery.of(context).size.width,
                        ),
                      ),
                    ]),
                  ),
                );
              },
            ).toList();
            return new ClipRRect(
              borderRadius:BorderRadius.all(Radius.circular(0)),
              child: new Stack(
                children: <Widget>[
                  CarouselSlider(
                    items: child,
                    autoPlay: true,
                    enlargeCenterPage: true,
                    aspectRatio: 1.5,
                    viewportFraction: 1.0,
                    onPageChanged: (index) {
                      setState(() {
                        _current = index;
                      });
                    },
                  ),
                  Positioned(
                    bottom: 5,
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: map<Widget>(
                          imgList, (index, url) {
                            return Container(
                              width: 8.0,
                              height: 8.0,
                              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: _current == index ? Colors.white : Colors.grey),
                            );
                          },
                        ),
                      ),
                    )
                  )
                ],
              ),
            );
          }
        }
    );
  }
}
