import 'package:flutter/material.dart';

class SlaveMenuList {

  IconData image;
  Color color;
  String title;
  String page;
  var param;
  bool login;

  SlaveMenuList({
    this.image,
    this.title,
    this.color,
    this.page,
    this.param,
    this.login,
  });

}

class SlaveMenuListFull {
  IconData image;
  Color color;
  String title;
  String page;
  var param;
  bool login;

  SlaveMenuListFull({
    this.image,
    this.title,
    this.color,
    this.param,
    this.page,
    this.login
  });
}