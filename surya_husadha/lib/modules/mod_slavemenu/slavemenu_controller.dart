import 'package:flutter/material.dart';
import 'package:surya_husadha/components/com_login/com_login.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surya_husadha/helper/transition/slide_route.dart';
import 'package:surya_husadha/modules/mod_slavemenu/salvemenu_models.dart';
import 'package:surya_husadha/pages/widgetpage.dart';
import 'package:url_launcher/url_launcher.dart';

class slaveMenuController extends StatefulWidget {
  String _cabang;
  var _akun;
  var _scafolt_key;

  slaveMenuController(String cabang, akun, scafolt_key){
    this._cabang = cabang;
    this._akun = akun;
    this._scafolt_key = scafolt_key;
  }

  @override
  _slaveMenuController createState() => new _slaveMenuController();
}

class _slaveMenuController extends State<slaveMenuController> {

  List<SlaveMenuList> _mSlaveMenuList = [];
  List<SlaveMenuListFull> _SlaveMenuListFull = [];

  @override
  void initState() {
    super.initState();

    //SH-CARD
    List<Map<String, dynamic>> params_lc=[];
    params_lc.add({
      "_cabang": widget._cabang,
      "_page": "shcard",
    });
    _mSlaveMenuList.add(new SlaveMenuList(
        image: FontAwesomeIcons.thumbsUp,
        color: Colors.black,
        title: "SH-CARD",
        param: params_lc,
        page: "LoyaltiPage",
        login: true
    ));

    //KIDS-PREVILAGE
    List<Map<String, dynamic>> params_tdi=[];
    params_tdi.add({
      "_cabang": widget._cabang,
      "_page": "kidscard",
    });
    _mSlaveMenuList.add(new SlaveMenuList(
        image: FontAwesomeIcons.child,
        color: Colors.deepOrange,
        title: "KIDS-PRIVILEGE",
        param: params_tdi,
        page: "LoyaltiPage",
        login: true
    ));

    //TUKAR-POINT
    List<Map<String, dynamic>> tkrpointt=[];
    tkrpointt.add({
      "_cabang": widget._cabang,
      "_page": "shcard",
    });
    _mSlaveMenuList.add(new SlaveMenuList(
        image: FontAwesomeIcons.exchangeAlt,
        color: WarnaCabang.ubung,
        title: "TUKAR-POINT",
        param: tkrpointt,
        page: "TukarPoint",
        login: false
    ));

    //GMAP
    List<Map<String, dynamic>> paramsgmap=[];
    paramsgmap.add({
        "_cabang": widget._cabang,
    });
    _mSlaveMenuList.add(new SlaveMenuList(
        image: FontAwesomeIcons.mapMarkerAlt,
        color: Colors.redAccent,
        title: "GMAP",
        param: paramsgmap,
        page: "GmapPage",
        login: false
    ));

    //FASILITAS
    var Fasilitaspage;
    List<Map<String, dynamic>> paramsfs=[];
    if(widget._cabang == "group"){
        paramsfs.add({
          "_cabang": widget._cabang,
        });
        Fasilitaspage = "FasilitasGroup";
    }else{
        paramsfs.add({
          "_cabang": widget._cabang,
        });
        Fasilitaspage = "Fasilitas";
    }
    _mSlaveMenuList.add(new SlaveMenuList(
        image: FontAwesomeIcons.bed,
        color: WarnaCabang.nusadua,
        title: "FASILITAS",
        param: paramsfs,
        page: Fasilitaspage,
        login: false
    ));

    //PRODUK-SPESIAL
    List<Map<String, dynamic>> params_spc=[];
    params_spc.add({
      "_cabang": widget._cabang,
      "_ctntid": "36",
    });
    _mSlaveMenuList.add(new SlaveMenuList(
        image: FontAwesomeIcons.moneyCheckAlt,
        color: Colors.lightBlue,
        title: "PRODUK-SPESIAL",
        param: params_spc,
        page: "CategoryPage",
        login: false
    ));

    //ACADEMY
    List<Map<String, dynamic>> params_acd=[];
    params_acd.add({
      "_cabang": widget._cabang,
      "_url": "https://academy.suryahusadha.com/new2019/",
    });
    _mSlaveMenuList.add(new SlaveMenuList(
        image: FontAwesomeIcons.graduationCap,
        color: Colors.black87,
        title: "ACADEMY",
        param: params_acd,
        page: "WebView",
        login: false
    ));

    //LAINNYA
    _mSlaveMenuList.add(new SlaveMenuList(
        image: FontAwesomeIcons.chevronDown,
        color: Colors.black87,
        title: "LAINNYA",
        param: null,
        page: "",
        login: false
    ));

    //KARIR
    List<Map<String, dynamic>> params_krr=[];
    params_krr.add({
      "_cabang": widget._cabang,
      "_ctntid": "27",
    });
    _SlaveMenuListFull.add(new SlaveMenuListFull(
        image: FontAwesomeIcons.briefcase,
        color: Colors.black87,
        title: "KARIR",
        param: params_krr,
        page: "CategoryPage",
        login: false
    ));

    _SlaveMenuListFull.add(new SlaveMenuListFull(
        image: FontAwesomeIcons.youtube,
        color: Colors.red[700],
        title: "YT-CHANNEL",
        param: params_lc,
        page: "youtube",
        login: false
    ));
    _SlaveMenuListFull.add(new SlaveMenuListFull(
        image: FontAwesomeIcons.facebook,
        color: Colors.indigo,
        title: "FACEBOOK",
        param: params_lc,
        page: "facebook",
        login: false
    ));
    _SlaveMenuListFull.add(new SlaveMenuListFull(
        image: FontAwesomeIcons.instagram,
        color: Colors.brown,
        title: "INSTAGRAM",
        param: params_lc,
        page: "instagram",
        login: false
    ));
  }

  @override
  Widget _buildslaveMenuController() {
    return new SizedBox(
        width: double.infinity,
        height: 205.0,
        child: new Container(
            //padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
            child: GridView.builder(
                physics: ClampingScrollPhysics(),
                itemCount: 8,
                gridDelegate:
                new SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4
                ),
                itemBuilder: (context, position) {
                  return _rowSlaveMenuList(_mSlaveMenuList[position], position);
                }
           ),
        )
    );
  }

  Widget _rowSlaveMenuList(SlaveMenuList _mnList, int position) {
    return new Container(
      color: Colors.white,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              Scaffold.of(context).removeCurrentSnackBar();
              if(widget._akun == null && _mnList.login){
                final snackBar = SnackBar(
                  content: Text('Diperlukan login'),
                  action: SnackBarAction(
                    label: 'Login?',
                    onPressed: () {
                      Navigator.pushReplacement(context, SlideLeftRoute(page: Fullpage(widgetpage: LoginController(widget._cabang), pagename: "LoginPage",)));
                    },
                  ),
                );
                widget._scafolt_key.currentState.showSnackBar(snackBar);
              }else{
                if(position == 7) {
                  showModalBottomSheet<void>(context: context, builder: (context) {return _buildMenuBottomSheet();});
                }else{
                  RouteHelper(context, _mnList.page, _mnList.param);
                }
              }
            },
            child: new Container(
              decoration: new BoxDecoration(
                  border: Border.all(color: Colors.black12, width: 1.0),
                  borderRadius: new BorderRadius.all(new Radius.circular(10.0))
              ),
              padding: EdgeInsets.all(16.0),
              child: new Icon(
                _mnList.image,
                color: _mnList.color,
                size: 20.0,
              ),
            ),
          ),
          new Padding(
            padding: EdgeInsets.only(top: 6.0),
          ),
          new Text(_mnList.title, style: new TextStyle(fontSize: 10.0))
        ],
      ),
    );
  }

  Widget _rowSlaveMenuListfull(SlaveMenuListFull _mnList, int position) {
    return new Container(
      color: Colors.white,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () async {
              Scaffold.of(context).removeCurrentSnackBar();
              if(widget._akun == null && _mnList.login){
                final snackBar = SnackBar(
                  content: Text('Diperlukan login'),
                  action: SnackBarAction(
                    label: 'Login?',
                    onPressed: () {
                      Navigator.pushReplacement(context, SlideLeftRoute(page: Fullpage(widgetpage: LoginController(widget._cabang), pagename: "LoginPage",)));
                    },
                  ),
                );
                widget._scafolt_key.currentState.showSnackBar(snackBar);
              }else{
                switch (_mnList.page) {
                  case 'youtube':
                    await launch('https://www.youtube.com/channel/UCwQQVERL9twOmpxMtIzqX_A');
                    break;
                  case 'facebook':
                    await launch("fb://page/357694104310430");
                    break;
                  case 'instagram':
                    await launch("instagram://user?username=suryahusadha");
                    break;
                  default:
                    RouteHelper(context, _mnList.page, _mnList.param);
                }
              }
            },
            child: new Container(
              decoration: new BoxDecoration(
                  border: Border.all(color: Colors.black12, width: 1.0),
                  borderRadius: new BorderRadius.all(new Radius.circular(10.0))
              ),
              padding: EdgeInsets.all(16.0),
              child: new Icon(
                _mnList.image,
                color: _mnList.color,
                size: 20.0,
              ),
            ),
          ),
          new Padding(
            padding: EdgeInsets.only(top: 6.0),
          ),
          new Text(_mnList.title, style: new TextStyle(fontSize: 10.0))
        ],
      ),
    );
  }

  Widget _buildMenuBottomSheet() {
    return new StatefulBuilder(builder: (c, s) {
      return new SafeArea(
          child: new Container(
            width: double.infinity,
            decoration: new BoxDecoration(
                borderRadius: BorderRadius.circular(4.0),
                color: Colors.white
            ),
            child: new Column(
                children: <Widget>[
                  new Container(
                    color: Colors.blueAccent,
                    width: double.infinity,
                    child: new Icon(
                      Icons.drag_handle,
                      color: Colors.white,
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(bottom: 0.0),
                    width: double.infinity,
                    height: 1.0,
                    color: Colors.grey[200],
                  ),
                  new Container(
                    padding:  EdgeInsets.only(left: 16, right: 10, top: 10, bottom: 16),
                    child: new Column(
                      children: <Widget>[
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text(
                              "MENU LAINNYA",
                              style: new TextStyle(fontFamily: "NeoSansBold", fontSize: 14.0),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(bottom: 0.0),
                    width: double.infinity,
                    height: 1.0,
                    color: Colors.grey[200],
                  ),
                  new Container(
                    padding: EdgeInsets.all(16.0),
                    height: 200.0,
                    child: new GridView.builder(
                        physics: new NeverScrollableScrollPhysics(),
                        itemCount: _SlaveMenuListFull.length,
                        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4),
                        itemBuilder: (context, position) {
                          return _rowSlaveMenuListfull(
                              _SlaveMenuListFull[position], position
                          );
                        }),
                  ),
                ]
            ),
          ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return _buildslaveMenuController();
  }
}