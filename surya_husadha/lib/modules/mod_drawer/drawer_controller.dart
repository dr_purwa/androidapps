import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:package_info/package_info.dart';
import 'package:surya_husadha/components/com_google_login/sign_in.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/database/akun/akun_db.dart';
import 'package:surya_husadha/helper/database/konten/konten_db.dart';
import 'package:surya_husadha/helper/database/konten/konten_models.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/helper/dropdown/CustomExpansionTitle.dart' as custom;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:app_review/app_review.dart';

class drawerController extends StatefulWidget {

  var _dtakun;
  var _cabang;
  var _Pagename;
  Color _color1;
  Color _color2;
  Color _Selected_color;
  Color _headerText;
  TextStyle textStyle;
  Color iconColor;

  drawerController(dtakun, String cabang, String Pagename){
    this._dtakun = dtakun;
    this._cabang = cabang;
    this._Pagename = Pagename;
    if(cabang=="group"){
      this._color1 = WarnaCabang.group;
      this._color2= WarnaCabang.group2;
      this._Selected_color= WarnaCabang.group2;
      this._headerText = Colors.grey[600];
    }
    if(cabang=="denpasar"){
      this._color1 = WarnaCabang.shh;
      this._color2= WarnaCabang.shh2;
      this._Selected_color= WarnaCabang.shh2.withOpacity(0.3);
      this._headerText = Colors.white;
    }
    if(cabang=="ubung"){
      this._color1 = WarnaCabang.ubung;
      this._color2= WarnaCabang.ubung2;
      this._Selected_color= WarnaCabang.ubung2.withOpacity(0.3);
      this._headerText = Colors.white;
    }
    if(cabang=="nusadua"){
      this._color1 = WarnaCabang.nusadua;
      this._color2= WarnaCabang.nusadua2;
      this._Selected_color= WarnaCabang.nusadua2.withOpacity(0.3);
      this._headerText = Colors.white;
    }
    if(cabang=="kmc"){
      this._color1 = WarnaCabang.kmc;
      this._color2= WarnaCabang.kmc2;
      this._Selected_color= WarnaCabang.kmc2.withOpacity(0.3);
      this._headerText = Colors.white;
    }

  }
  @override
  _drawerController createState() => new _drawerController();
}

class _drawerController extends State<drawerController>{

  bool getinfo = true;

  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  @override
  initState() {
    super.initState();
    if(getinfo){
      _initPackageInfo().then((response){
        setState(() {
          _packageInfo = response;
          getinfo = false;
          print("info: "+_packageInfo.version.toString());
        });
      });
    }
  }

  Future<dynamic> _initPackageInfo() async {
    if(getinfo){
      final PackageInfo info = await PackageInfo.fromPlatform();
      return info;
    }
  }

  Widget ecardberobat(cabang, nrm, bod){
    return new SafeArea(
      child: new Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Center(
          child: RotatedBox(
            quarterTurns: 5,
            child: Stack(
              children: <Widget>[
                new Container(
                 child: Image.network("https://suryahusadha.com/api/imgcard/berobat/?cabang="+cabang+"&nrm="+nrm+"&bodVal="+bod+"&dummy=${new DateTime.now()}"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _buildDrawer(context){
    var dt = widget._dtakun;
    Widget headerview;
    Widget _userLogin;
    Widget _listmenu;
    widget.textStyle = TextStyle(color: Colors.grey[800], fontSize: 14, fontWeight: FontWeight.w500);
    widget.iconColor = Colors.grey[800];

    bool act_home = false;
    if(widget._Pagename == "group"){
      act_home = true;
    }

    bool act_AkunDetail = false;
    if(widget._Pagename == "AkunMenu"){
      act_AkunDetail = true;
    }
    bool act_ReservasiList = false;
    if(widget._Pagename == "ReservasiList"){
      act_ReservasiList = true;
    }

    bool Clinics = false;
    bool actdps_Clinics = false;
    bool actub_Clinics = false;
    bool actnd_Clinics = false;
    if(widget._Pagename == "Clinics"){
      if(widget._cabang == "denpasar"){
        actdps_Clinics = true;
        Clinics = true;
      }
      if(widget._cabang == "ubung"){
        actub_Clinics = true;
        Clinics = true;
      }
      if(widget._cabang == "nusadua"){
        actnd_Clinics = true;
        Clinics = true;
      }
    }

    bool KartuBerobat = false;
    bool dps_KartuBerobat = false;
    bool ub_KartuBerobat = false;
    bool nd_KartuBerobat = false;
    if(widget._Pagename == "KartuBerobat"){
      if(widget._cabang == "denpasar"){
        dps_KartuBerobat = true;
        KartuBerobat = true;
      }
      if(widget._cabang == "ubung"){
        ub_KartuBerobat = true;
        KartuBerobat = true;
      }
      if(widget._cabang == "nusadua"){
        nd_KartuBerobat = true;
        KartuBerobat = true;
      }
    }

    bool KartuBerobatKlg = false;
    if(widget._Pagename == "AkunBerobatKartu"){
      KartuBerobatKlg = true;
    }

    if(dt != null){
      //LOGIN
      String img = SERVER.domain + dt[0]["pp"] +'?dummy=${new DateTime.now()}';
      headerview = _headerDrawer(
        NetworkImage(img),
        dt[0]["name"],
        dt[0]["email"],
      );
      _userLogin = new Container(
        child: Column(
          children: <Widget>[
            _createMenu(
                "Logout",
                FontAwesomeIcons.signOutAlt,
                    (){
                  var db = new akunDB();
                  db.deleteAkun();
                  signOutGoogle();
                  RouteHelper(context, null, null);
                },
                context, false
            ),
            Divider(height: 4.0, color: Colors.grey,),
          ],
        ),
      );
      _listmenu = ListView(
        children: <Widget>[
          _createMenu("Home", FontAwesomeIcons.home, (){
            List<Map<String, dynamic>> params=[];
            params.add({
              "_cabang": "group",
            });
            Navigator.pop(context);
            RouteHelper(context, "group", params);
          }, context, act_home),
          Divider(height: 4.0, color: Colors.grey,),
          _createMenu("User Profile", FontAwesomeIcons.userCircle, (){
            List<Map<String, dynamic>> params=[];
            params.add({
              "_cabang": widget._cabang,
            });
            Navigator.pop(context);
            RouteHelper(context, "AkunMenu", params);
          }, context, act_AkunDetail),
          Divider(height: 4.0, color: Colors.grey,),
          custom.ExpansionTile(
            initiallyExpanded: KartuBerobat,
            headerBackgroundColor: KartuBerobat ? widget._Selected_color : Colors.grey[100],
            leading: Icon(FontAwesomeIcons.userAlt, color: widget.iconColor,),
            title: Text("Kartu Berobat", style: widget.textStyle),
            children: <Widget>[
              Divider(height: 4.0, color: Colors.grey,),
              _createsub('Surya Husadha Denpasar', FontAwesomeIcons.hospital, (){
                showDialog(
                  barrierDismissible: true,
                  context: context,
                  builder: (BuildContext context){
                    return ecardberobat("denpasar", widget._dtakun[0]["nrmshh"].toString(), widget._dtakun[0]["bod"].toString());
                  },
                );
              }, context, 1, dps_KartuBerobat),
              Divider(height: 4.0, color: Colors.grey,),
              _createsub('Surya Husadha Ubung', FontAwesomeIcons.hospital, (){
                showDialog(
                  barrierDismissible: true,
                  context: context,
                  builder: (BuildContext context){
                    return ecardberobat("ubung", widget._dtakun[0]["nrmub"].toString(), widget._dtakun[0]["bod"].toString());
                  },
                );
              }, context, 1, ub_KartuBerobat),
              Divider(height: 4.0, color: Colors.grey,),
              _createsub('Surya Husadha Nusadua', FontAwesomeIcons.hospital, (){
                showDialog(
                  barrierDismissible: true,
                  context: context,
                  builder: (BuildContext context){
                    return ecardberobat("nusadua", widget._dtakun[0]["nrmnd"].toString(), widget._dtakun[0]["bod"].toString());
                  },
                );
              }, context, 1, nd_KartuBerobat),
            ],
          ),
          Divider(height: 4.0, color: Colors.grey,),
          //KartuBerobatKlg
          _createMenu("Kartu Berobat Keluarga", FontAwesomeIcons.users, (){
            List<Map<String, dynamic>> params=[];
            params.add({
              "_cabang": widget._cabang,
            });
            Navigator.pop(context);
            RouteHelper(context, "AkunBerobatKartu", params);
          }, context, KartuBerobatKlg),
          Divider(height: 4.0, color: Colors.grey,),
          custom.ExpansionTile(
            initiallyExpanded: Clinics,
            headerBackgroundColor: Clinics ? widget._Selected_color : Colors.grey[100],
            leading: Icon(FontAwesomeIcons.calendar, color: widget.iconColor,),
            title: Text("Reservasi", style: widget.textStyle),
            children: <Widget>[
              Divider(height: 4.0, color: Colors.grey,),
              _createsub('Surya Husadha Denpasar', FontAwesomeIcons.calendarAlt, (){
                List<Map<String, dynamic>> rsvparams=[];
                rsvparams.add({
                  "_cabang": "denpasar",
                  "_clinicSelect": null,
                });
                RouteHelper(context, "Clinics", rsvparams);
                }, context, 1, actdps_Clinics),
              Divider(height: 4.0, color: Colors.grey,),
              _createsub('Surya Husadha Ubung', FontAwesomeIcons.calendarAlt, (){
                List<Map<String, dynamic>> rsvparams=[];
                rsvparams.add({
                  "_cabang": "ubung",
                  "_clinicSelect": null,
                });
                RouteHelper(context, "Clinics", rsvparams);}, context, 1, actub_Clinics),
              Divider(height: 4.0, color: Colors.grey,),
              _createsub('Surya Husadha Nusadua', FontAwesomeIcons.calendarAlt, (){
                List<Map<String, dynamic>> rsvparams=[];
                rsvparams.add({
                  "_cabang": "nusadua",
                  "_clinicSelect": null,
                });
                RouteHelper(context, "Clinics", rsvparams);}, context, 1, actnd_Clinics),
            ],
          ),
          Divider(height: 4.0, color: Colors.grey,),
          _createMenu("List Reservasi", FontAwesomeIcons.listOl,(){
            var bodval;
            var emailval;
            if(widget._dtakun != null){
              bodval=widget._dtakun[0]["bod"];
              emailval=widget._dtakun[0]["email"];
            }
            List<Map<String, dynamic>> rsvparams=[];
            rsvparams.add({
              "_cabang": widget._cabang,
              "_email": emailval,
              "_bod": bodval,
              "_akun": widget._dtakun,
            });
            Navigator.pop(context);
            RouteHelper(context, "ReservasiList", rsvparams);}, context, act_ReservasiList),
          Divider(height: 4.0, color: Colors.grey,),
          ListTile(
            onTap: (){
              showDialog(
                barrierDismissible: true,
                context: context,
                builder: (BuildContext context){
                  return terms();
                },
              );
            },
            leading: Icon(Icons.insert_drive_file, color: widget.iconColor,),
            title: Text('Syarat dan Ketentuan', style: widget.textStyle),
          ),
          Divider(height: 4.0, color: Colors.grey,),
          //_createMenu('Pesan Notifikasi', FontAwesomeIcons.bell, (){Navigator.pop(context);}, context, false),
          //Divider(height: 4.0, color: Colors.grey,),
        ],
      );
    }else{
      //LOGOUT
      headerview = _headerDrawer(
          AssetImage("assets/logo/icon.png"),
          "Surya Husadha Apps",
          "info@suryahusadha.com"
      );
      _listmenu = ListView(
        children: <Widget>[
         _createMenu("Home", FontAwesomeIcons.home, (){
            List<Map<String, dynamic>> params=[];
            params.add({
              "_cabang": "group",
            });
            Navigator.pop(context);
            RouteHelper(context, "group", params);
        }, context, act_home),
        Divider(height: 4.0, color: Colors.grey,),
          custom.ExpansionTile(
            initiallyExpanded: true,
            headerBackgroundColor: Clinics ? widget._Selected_color : Colors.grey[100],
            leading: Icon(FontAwesomeIcons.calendar, color: widget.iconColor,),
            title: Text("Reservasi", style: widget.textStyle),
            children: <Widget>[
              Divider(height: 4.0, color: Colors.grey,),
              _createsub('Surya Husadha Denpasar', FontAwesomeIcons.calendarAlt, (){
                List<Map<String, dynamic>> rsvparams=[];
                rsvparams.add({
                  "_cabang": "denpasar",
                  "_clinicSelect": null,
                });
                RouteHelper(context, "Clinics", rsvparams);
              }, context, 1, actdps_Clinics),
              Divider(height: 4.0, color: Colors.grey,),
              _createsub('Surya Husadha Ubung', FontAwesomeIcons.calendarAlt, (){
                List<Map<String, dynamic>> rsvparams=[];
                rsvparams.add({
                  "_cabang": "ubung",
                  "_clinicSelect": null,
                });
                RouteHelper(context, "Clinics", rsvparams);}, context, 1, actub_Clinics),
              Divider(height: 4.0, color: Colors.grey,),
              _createsub('Surya Husadha Nusadua', FontAwesomeIcons.calendarAlt, (){
                List<Map<String, dynamic>> rsvparams=[];
                rsvparams.add({
                  "_cabang": "nusadua",
                  "_clinicSelect": null,
                });
                RouteHelper(context, "Clinics", rsvparams);}, context, 1, actnd_Clinics),
            ],
          ),
          Divider(height: 4.0, color: Colors.grey,),
          _createMenu("List Reservasi", FontAwesomeIcons.listOl,(){
            var bodval;
            var emailval;
            if(widget._dtakun != null){
              bodval=widget._dtakun[0]["bod"];
              emailval=widget._dtakun[0]["email"];
            }
            List<Map<String, dynamic>> rsvparams=[];
            rsvparams.add({
              "_cabang": widget._cabang,
              "_email": emailval,
              "_bod": bodval,
              "_akun": widget._dtakun,
            });
            RouteHelper(context, "ReservasiList", rsvparams);}, context, act_ReservasiList),
            Divider(height: 4.0, color: Colors.grey,),
            ListTile(
              onTap: (){
                showDialog(
                  barrierDismissible: true,
                  context: context,
                  builder: (BuildContext context){
                    return terms();
                  },
                );
              },
              leading: Icon(Icons.insert_drive_file, color: widget.iconColor,),
              title: Text('Syarat dan Ketentuan', style: widget.textStyle),
            ),
            Divider(height: 4.0, color: Colors.grey,),
        ],
      );
    }

    return Drawer(
      elevation: 10,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          headerview,
          Divider(height: 4.0, color: Colors.grey,),
          Expanded(
            child: _listmenu,
          ),
          // This container holds the align
          new Container(
              color: Colors.white,
              child: Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: Container(
                      child: Column(
                        children: <Widget>[
                          Divider(height: 4.0, color: Colors.grey,),
                          ListTile(
                            onTap: (){
                              AppReview.requestReview.then((String onValue) {});
                            },
                            leading: Icon(FontAwesomeIcons.edit, color: widget.iconColor,),
                            title: Text('Review Apps', style: widget.textStyle),
                            subtitle: Text("Versi:" + _packageInfo.version.toString(), style: TextStyle(
                              fontSize: 10
                            )),
                          ),
                        ],
                      )
                  )
              )
          ),
        ],
      ),
    );
  }

  @override
  Future<Konten> _getData() async {
    var db = new kontenDB();
    var _data = await db.getkonten(224);
    print(_data.toString());
    return _data;
  }

  Widget terms(){
    return new SafeArea(
        child: new Scaffold(
            backgroundColor: Colors.black.withOpacity(0.8),
            body: new FutureBuilder(
                future: _getData(),
                builder: (context, snapshot) {
                  if (snapshot.hasData && (snapshot.connectionState == ConnectionState.done)) {
                    if(snapshot.data != null){
                      return ListView(
                        children: <Widget>[
                          new Container(
                            padding: EdgeInsets.all(24),
                            color: Colors.white,
                            child: new Column(
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.only(bottom: 28),
                                  child: Text(
                                    "Syarat dan Ketentuan Penggunaan Aplikasi",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700,
                                        color: Colors.grey[800]
                                    ),
                                  ),
                                ),
                                new HtmlWidget(
                                  snapshot.data.full_content_id,
                                  textStyle: TextStyle(
                                      fontSize: 16,
                                      height: 1.4,
                                      fontFamily: "NeoSansBold",
                                      color: Colors.grey[800],
                                      wordSpacing: 2.0
                                  ),
                                  bodyPadding: EdgeInsets.all(0),
                                ),
                              ],
                            ),
                          )
                        ],
                      );
                    }else{
                      return new Stack(
                        children: <Widget>[
                          Center(
                            child: CircularProgressIndicator(),
                          ),
                        ],
                      );
                    }
                  }else{
                    return new Stack(
                      children: <Widget>[
                        Center(
                          child: CircularProgressIndicator(),
                        ),
                      ],
                    );
                  }
                }
            )
        )
    );
  }

  _createMenu(String title, icon, ontap, context, selected){
    if(selected){
      return Ink(
        color: widget._Selected_color,
        child: ListTile(
          selected: selected,
          leading: Icon(icon, color: widget.iconColor,),
          title: Text(title, style: widget.textStyle),
          onTap: ()=> Navigator.pop(context),
        ),
      );
    }else{
      return Ink(
        color: Colors.grey[100],
        child: ListTile(
          selected: selected,
          leading: Icon(icon, color: widget.iconColor,),
          title: Text(title, style: widget.textStyle),
          onTap:ontap,
        ),
      );
    }

  }

  _createsub(String title, icon, ontap, context, int sub, selected){
    var padding = sub * 30.0;
    if(selected){
      return Ink(
        color: widget._color2.withOpacity(0.2),
        child: ListTile(
          selected: true,
          leading: Icon(icon, color: widget.iconColor,),
          title: Text(title, style: widget.textStyle),
          onTap: ()=> Navigator.pop(context),
          contentPadding: EdgeInsets.only(left: padding),
        ),
      );
    }else{
      return Ink(
        color: Colors.grey[100],
        child: ListTile(
          selected: true,
          leading: Icon(icon, color: widget.iconColor,),
          title: Text(title, style: widget.textStyle),
          onTap:ontap,
          contentPadding: EdgeInsets.only(left: padding),
        ),
      );
    }
  }

  Widget _headerDrawer(pp, name, email){
    return Container(
      color: widget._color1,
      padding: EdgeInsets.only(top: 30, bottom: 30),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 16),
          ),
          new Container(
            height: 60.0,
            width: 60.0,
            alignment: Alignment.centerLeft,
            child: CircleAvatar(
              backgroundImage: pp,
              minRadius: 60,
              maxRadius: 150,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 16),
          ),
          Expanded(
              flex: 1,
              child:  Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                        name,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        softWrap: true,
                        style: TextStyle(fontSize: 14,color: widget._headerText, fontWeight: FontWeight.w800),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 2),
                    ),
                    Text(
                        email,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        softWrap: true,
                        style: TextStyle(fontSize: 12,color: widget._headerText)
                    ),
                  ],
                ),
              )
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildDrawer(context);
  }

}