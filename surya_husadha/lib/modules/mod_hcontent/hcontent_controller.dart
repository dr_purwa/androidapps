import 'package:flutter/material.dart';
import 'package:surya_husadha/components/com_article/detail_article.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/database/konten/konten_db.dart';

class HctnController extends StatefulWidget {

  String _cabang;
  String _hCtitle;
  String _hCcatid;
  int _hClimit;
  double _hview;
  var _dtakun;

  HctnController(String s, String t, int u, double d, cabang, akun){
    this._hCtitle = s;
    this._hCcatid = t;
    this._hClimit = u;
    this._hview = d;
    this._cabang = cabang;
    this._dtakun = akun;
  }

  @override
  _HctnController createState() => new _HctnController();

}

class _HctnController extends State<HctnController> {

  List _data;
  double dheight;

  @override
  Future<List> _getData() async {
    var db = new kontenDB();
    _data = await db.getCatKonten(widget._hCcatid);
    return _data;
  }

  @override
  Widget _buildHcontentController(){
    return new FutureBuilder<List>(
        future: _getData(),
        builder: (context, snapshot){
          if(snapshot.hasData && (snapshot.connectionState == ConnectionState.done)){
            if(widget._hview == 1){
              dheight = 100;
            }else{
              dheight = 172.0;
            }
            if(snapshot.data.length > 0){
              return new Container(
                color: Colors.white,
                margin: EdgeInsets.only(bottom: 16),
                padding: EdgeInsets.fromLTRB(16.0, 16.0, 0.0, 16.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    new Text(
                      widget._hCtitle,
                      style: new TextStyle(color: Colors.grey[1000], fontFamily: "NeoSansBold", fontWeight: FontWeight.w700, fontSize: 16),
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 8.0),
                    ),
                    new Container(
                      margin: EdgeInsets.only(bottom: 0.0),
                      width: double.infinity,
                      height: 1.0,
                      color: Colors.grey[200],
                    ),
                    new SizedBox(
                      height: dheight,
                      child: FutureBuilder<List>(
                          future: _getData(),
                          builder: (context, snapshot) {
                            if(snapshot.hasData && (snapshot.connectionState == ConnectionState.done)){
                              return new ListView.builder(
                                itemCount: snapshot.data.length,
                                padding: EdgeInsets.only(top: 12.0),
                                physics: new ClampingScrollPhysics(),
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, index) {
                                  if(index <= widget._hClimit){
                                    return _rowHconten(snapshot.data[index]);
                                  }
                                },
                              );
                            }
                            return Container(
                              margin: EdgeInsets.only(top: 20, bottom: 20),
                              child : new Column(children: [
                                SizedBox(
                                  width: 50.0,
                                  height: 50.0,
                                  child: const CircularProgressIndicator(),
                                ),
                              ]),
                            );
                          }),
                    ),
                  ],
                ),
              );
            }else{
              return new Container();
            }
          }else{
            if(snapshot.connectionState == ConnectionState.waiting){
              return Container(
                margin: EdgeInsets.only(top: 20, bottom: 20),
                child : new Column(children: [
                  SizedBox(
                    width: 50.0,
                    height: 50.0,
                    child: const CircularProgressIndicator(),
                  ),
                ]),
              );
            }else{
              return new Container();
            }
          }
        }
    );
  }

  Widget _rowHconten(dt) {
    var dwidth = MediaQuery.of(context).size.width - 100;
    if(widget._hview == 1){
      return new Container(
        width: dwidth,
        margin: EdgeInsets.only(right: 20),
        child: new InkWell(
          onTap: (){
            List<Map<String, dynamic>> dtNext=[];
            dtNext.add({"_cabang": widget._cabang});
            dtNext.add({"_ctntid": dt["id"]});
            showDialog(
              barrierDismissible: true,
              context: context,
              builder: (BuildContext context){
                return new DetaiArticle(dtNext, widget._dtakun);
              },
            );
          },
          child: new Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.only(top: 5.0),
              ),
              new Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new ClipRRect(
                      borderRadius: new BorderRadius.circular(8.0),
                      child: new Image.network(
                        SERVER.domain + dt["image_default"],
                        height: 80.0,
                        width: 80.0,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(bottom: 8.0),
                              child: Text(
                                dt["title_id"],
                                maxLines: 1,
                                softWrap: true,
                                overflow: TextOverflow.ellipsis,
                                style: new TextStyle(color: Colors.grey[800],fontFamily: "NeoSansBold", fontSize: 14.0, fontWeight: FontWeight.w600),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 4.0),
                              child: Text(
                                dt["short_content_id"],
                                maxLines: 3,
                                softWrap: true,
                                overflow: TextOverflow.ellipsis,
                                style: new TextStyle(color: Colors.grey[600], fontFamily: "NeoSans", fontSize: 12.0),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ]
              ),
            ],
          ),
        ),
      );
    }else{
      return new Container(
        margin: EdgeInsets.only(right: 16.0),
        child: new InkWell(
          onTap: (){
            List<Map<String, dynamic>> dtNext=[];
            dtNext.add({"_cabang": widget._cabang});
            dtNext.add({"_ctntid": dt["id"]});
            showDialog(
              barrierDismissible: true,
              context: context,
              builder: (BuildContext context){
                return new DetaiArticle(dtNext, widget._dtakun);
              },
            );
          },
          child: new Column(
            children: <Widget>[
              new ClipRRect(
                borderRadius: new BorderRadius.circular(8.0),
                child: new Image.network (
                  SERVER.domain + dt["image_default"],
                  width: 132.0,
                  height: 132.0,
                ),
              ),
              new Padding(
                padding: EdgeInsets.only(top: 8.0),
              ),
              new Text(
                dt["title_id"],
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                maxLines: 1,
                style: new TextStyle(color: Colors.grey[800],fontFamily: "NeoSansBold", fontSize: 14.0, fontWeight: FontWeight.w600),
              ),
            ],
          ),
        )
      );
    }

  }

  @override
  Widget build(BuildContext context) {
    return _buildHcontentController();
  }
}