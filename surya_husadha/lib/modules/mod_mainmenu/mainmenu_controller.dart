import 'package:flutter/material.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/dialogue/customDialogue.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:convert' as convert;

class mainMenuController extends StatefulWidget {
  String _txtCabang;
  String _Cabang;
  Color _bg1 = Colors.white;
  Color _bg2 = Colors.white;
  Color _colordefgrTxt = Colors.grey[600];
  Color _colordefshhTxt = Colors.grey[600];
  Color _colordefubTxt = Colors.grey[600];
  Color _colordefndTxt = Colors.grey[600];
  Color _colordefkmcTxt = Colors.grey[600];

  mainMenuController(String Cabang) {
    _Cabang = Cabang;
    ;
    if(Cabang == "group"){
      this._bg1 = WarnaCabang.group;
      this._colordefgrTxt = Colors.black;
      this._txtCabang = "Group";
    }
    if(Cabang == "denpasar"){
      this._bg1 = WarnaCabang.shh;
      this._colordefshhTxt = Colors.white;
      this._txtCabang = "Denpasar";
    }
    if(Cabang == "ubung"){
      this._bg1 = WarnaCabang.ubung;
      this._colordefubTxt = Colors.white;
      this._txtCabang = "Ubung";
    }
    if(Cabang == "nusadua"){
      this._bg1 = WarnaCabang.nusadua;
      this._colordefndTxt = Colors.white;
      this._txtCabang = "Nusadua";
    }
    if(Cabang == "kmc"){
      this._bg1 = WarnaCabang.kmc;
      this._colordefkmcTxt = Colors.white;
      this._txtCabang = "KMC";
    }
  }

  @override
  _mainMenuController createState() => new _mainMenuController();
}

class _mainMenuController extends State<mainMenuController> {

  @override
  Future<String> _fmainMenu() async {
    setState(() {});
    return widget._Cabang;
  }

  void initState(){
    super.initState();
    _fmainMenu().then((String Cabang) => setState(() {
      widget._Cabang = Cabang;
    }));
  }

  @override
  Widget build(BuildContext context) {
    return _buildmainMenuController();
  }

  bool isget_erloading = true;
  var _dataer;
  Future<String> _geter() async{
    //if(isget_erloading){
      var db = new Http_Controller();
      var http_response = await db.getResponse("https://suryahusadha.com/api/er_phone/"+widget._Cabang);
      _dataer = convert.jsonEncode(http_response);
      return _dataer;
    //}
  }

  bool isget_waloading = true;
  var _datawa;
  Future<String> _getwa() async{
    //if(isget_waloading){
      var db = new Http_Controller();
      var http_response = await db.getResponse("https://suryahusadha.com/api/wa/"+widget._Cabang);
      _datawa = convert.jsonEncode(http_response);
      return _datawa;
    //}
  }

  bool isget_teleloading = true;
  var _data_tele;
  Future<String> _get_tele() async{
    //if(isget_waloading){
      var db = new Http_Controller();
      var http_response = await db.getResponse("https://suryahusadha.com/api/telegram/"+widget._Cabang);
      _data_tele = convert.jsonEncode(http_response);
      return _data_tele;
    //}
  }

  @override
  Widget _buildmainMenuController() {
    return new Container(
        padding: EdgeInsets.only(bottom: 10.0,),
        decoration: new BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                widget._bg1,
                widget._bg1]
            ),
            borderRadius: new BorderRadius.all(new Radius.circular(3.0))
        ),
        child: new Column(
          children: <Widget>[
            new Container(
              padding: EdgeInsets.all(12.0),
                decoration: new BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [widget._bg1,  widget._bg1],
                  ),
                  borderRadius: new BorderRadius.only(
                      topLeft: new Radius.circular(3.0),
                      topRight: new Radius.circular(3.0))
                ),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Container(
                    child: new Row(
                      children: <Widget>[
                        new Container(
                          child: new Icon(
                            FontAwesomeIcons.bars,
                            size: 14,
                            color: Colors.white,
                          ),
                        ),
                        new Container(
                          margin: EdgeInsets.only(left: 5.0),
                          child: new Text(
                            "Menu Cabang",
                            style: new TextStyle(
                                fontSize: 12.0,
                                color: Colors.white,
                                fontFamily: "NeoSansBold"),
                          ),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    alignment: Alignment.bottomRight,
                    child: new Row(
                      children: <Widget>[
                        new Container(
                          child: new Icon(
                              FontAwesomeIcons.building,
                              color: Colors.white,
                              size:14.0
                          ),
                        ),
                        new Container(
                          margin: EdgeInsets.only(left: 5.0),
                          child: new Text(
                            widget._txtCabang,
                            style: TextStyle(fontSize: 12.0, color: Colors.white),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            new Container(
              margin: EdgeInsets.only(bottom: 0.0),
              width: double.infinity,
              height: 1.0,
              color: Colors.grey[200],
            ),
            new Container(
              padding: EdgeInsets.only(left: 28.0, right: 32.0, top: 12.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Container(
                    child: FutureBuilder(
                        future: _geter(),
                        builder: (context, snapshot){
                          if(snapshot.hasData && _dataer != null){
                            var decode = convert.jsonDecode(_dataer);
                            var decodeConvert = convert.jsonDecode(decode["responseBody"]);
                            final dtkonten = decodeConvert["er_phone"];
                            final response = dtkonten[0]["response"];
                            final dt = dtkonten[0]["data"];
                            if(response == "1"){
                              isget_erloading= false;
                              return new GestureDetector(
                                onTap: (){
                                  var phoneUrl = "tel:"+dt[0]["number"].toString();
                                  _launchURL(phoneUrl);
                                },
                                child: new Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Container(
                                      decoration: new BoxDecoration(
                                          color: Colors.red,
                                          border: Border.all(color: Colors.grey[200], width: 1.0),
                                          borderRadius: new BorderRadius.all(new Radius.circular(20.0))
                                      ),
                                      padding: EdgeInsets.all(12.0),
                                      child: new Icon(
                                        FontAwesomeIcons.ambulance,
                                        size: 28,
                                        color: Colors.white,
                                      ),
                                    ),
                                    new Padding(
                                      padding: EdgeInsets.only(top: 10.0),
                                    ),
                                    new Text(
                                      "Emergency",
                                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                                    ),
                                    new Padding(
                                      padding: EdgeInsets.only(top: 2.0),
                                    ),
                                    new Text(
                                      "On Call",
                                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                                    )
                                  ],
                                ),
                              );
                            }else{
                              return er_loading();
                            }
                          }else{
                            return er_loading();
                          }

                        }
                    ),
                  ),
                  new Container(
                    child: new GestureDetector(
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Container(
                            decoration: new BoxDecoration(
                                color: Colors.white,
                                border: Border.all(color: Colors.grey[200], width: 1.0),
                                borderRadius: new BorderRadius.all(new Radius.circular(20.0))
                            ),
                            padding: EdgeInsets.only(left:12.0,right:12.0,top:8.0,bottom:12.0),
                            //padding: EdgeInsets.all(12.0),
                            child: new Icon(
                              FontAwesomeIcons.calendarAlt,
                              size: 30,
                              color: Colors.lightBlue,
                            ),
                          ),
                          new Padding(
                            padding: EdgeInsets.only(top: 10.0),
                          ),
                          new Text(
                            "Reservasi",
                            style: TextStyle(color: Colors.white, fontSize: 12.0),
                          ),
                          new Padding(
                            padding: EdgeInsets.only(top: 2.0),
                          ),
                          new Text(
                            widget._txtCabang,
                            style: TextStyle(color: Colors.white, fontSize: 12.0),
                          ),
                        ],
                      ),
                      onTap: (){
                        List<Map<String, dynamic>> newpage=[];
                        newpage.add({
                          "_cabang": widget._Cabang,
                          "_clinicSelect": null,
                        });
                        RouteHelper(context, "Clinics", newpage);
                      }
                    ),
                  ),
                  new Container(
                    child: FutureBuilder(
                      future: _getwa(),
                        builder: (context, snapshot){
                          if(snapshot.hasData && _datawa != null){
                            var decode = convert.jsonDecode(_datawa);
                            var decodeConvert = convert.jsonDecode(decode["responseBody"]);
                            final dtkonten = decodeConvert["wa"];
                            final response = dtkonten[0]["response"];
                            final wa = dtkonten[0]["data"];
                            if(response == "1"){
                              isget_waloading = false;
                              return new GestureDetector(
                                onTap: (){
                                  showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (BuildContext context) => _waList(context, wa),
                                  );
                                },
                                child: new Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Container(
                                      decoration: new BoxDecoration(
                                          color: Colors.white,
                                          border: Border.all(color: Colors.grey[200], width: 1.0),
                                          borderRadius: new BorderRadius.all(new Radius.circular(20.0))
                                      ),
                                      padding: EdgeInsets.only(left:10.0,right:10.0,top:6.0,bottom:10.0),
                                      child: new Icon(
                                        FontAwesomeIcons.whatsapp,
                                        size: 35,
                                        color: Colors.green,
                                      ),
                                    ),
                                    new Padding(
                                      padding: EdgeInsets.only(top: 10.0),
                                    ),
                                    new Text(
                                      "WhatsApp",
                                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                                    ),
                                    new Padding(
                                      padding: EdgeInsets.only(top: 2.0),
                                    ),
                                    new Text(
                                      "Chat",
                                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                                    )
                                  ],
                                ),
                              );
                            }else{
                              return wa_loading();
                            }
                          }else{
                            return wa_loading();
                          }

                        }
                    ),
                  ),
                  new Container(
                    child: FutureBuilder(
                        future: _get_tele(),
                        builder: (context, snapshot){
                          if(snapshot.hasData && _data_tele != null){
                            var decode = convert.jsonDecode(_data_tele);
                            var decodeConvert = convert.jsonDecode(decode["responseBody"]);
                            final dtkonten = decodeConvert["telegram"];
                            final response = dtkonten[0]["response"];
                            final telegram = dtkonten[0]["data"];
                            if(response == "1"){
                              isget_waloading = false;
                              return new GestureDetector(
                                onTap: (){
                                  showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (BuildContext context) => _telegramList(context, telegram),
                                  );
                                },
                                child: new Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Container(
                                      decoration: new BoxDecoration(
                                          color: Colors.white,
                                          border: Border.all(color: Colors.grey[200], width: 1.0),
                                          borderRadius: new BorderRadius.all(new Radius.circular(20.0))
                                      ),
                                      padding: EdgeInsets.only(left:10.0,right:10.0,top:6.0,bottom:10.0),
                                      child: new Icon(
                                        FontAwesomeIcons.telegram,
                                        size: 35,
                                        color: Colors.blueAccent,
                                      ),
                                    ),
                                    new Padding(
                                      padding: EdgeInsets.only(top: 10.0),
                                    ),
                                    new Text(
                                      "Telegram",
                                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                                    ),
                                    new Padding(
                                      padding: EdgeInsets.only(top: 2.0),
                                    ),
                                    new Text(
                                      "Chat",
                                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                                    )
                                  ],
                                ),
                              );
                            }else{
                              return tele_loading();
                            }
                          }else{
                            return tele_loading();
                          }

                        }
                    ),
                  ),
                ],
              ),
            )
          ],
        ));
  }

  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      String msg ="open whatsapp app link or do a snackbar with notification that there is no whatsapp installed";
      return Dialogue(context, msg, Colors.red, Colors.red, null, null);
    }
  }

  //Emegency Call
  Widget er_loading(){
    return new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Container(
          decoration: new BoxDecoration(
              color: Colors.grey[200],
              border: Border.all(color: Colors.grey[200], width: 1.0),
              borderRadius: new BorderRadius.all(new Radius.circular(20.0))
          ),
          padding: EdgeInsets.all(12.0),
          child: new Icon(
            FontAwesomeIcons.ambulance,
            size: 28,
            color: Colors.white,
          ),
        ),
        new Padding(
          padding: EdgeInsets.only(top: 10.0),
        ),
        new Text(
          "Tidak",
          style: TextStyle(color: Colors.white, fontSize: 12.0),
        ),
        new Padding(
          padding: EdgeInsets.only(top: 2.0),
        ),
        new Text(
          "Tersedia",
          style: TextStyle(color: Colors.white, fontSize: 12.0),
        )
      ],
    );
  }

  //whatsApp
  Widget _waList(BuildContext context, wa){
    List<Widget> WArow = new List<Widget>();
    WArow.add(new Container());
    wa.forEach((dt){
      WArow.add(
        new Container(
          child: InkWell(
            onTap: (){
              var whatsappUrl ="whatsapp://send?phone=$wa['wa_no']";
              _launchURL(whatsappUrl);
            },
            child: new Column(
              children: <Widget>[
                new Padding(
                  padding: EdgeInsets.only(top: 20),
                ),
                new Row(
                  children: <Widget>[
                    new Expanded(
                      flex: 1,
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Icon(
                            FontAwesomeIcons.whatsapp,
                            size: 24,
                            color: Colors.green,
                          ),
                          new Container(
                            padding: EdgeInsets.only(left: 5, top: 2),
                            child: Text(dt["wa_name"], style: TextStyle(
                              fontSize: 14,
                              color: Colors.grey[600],
                              fontWeight: FontWeight.w700
                            )),
                          )
                        ],
                      ),
                    ),
                    new Icon(
                      FontAwesomeIcons.arrowRight,
                      size: 12,
                      color: Colors.grey[600],
                    )
                  ],
                ),
                new Padding(
                  padding: EdgeInsets.only(top: 20),
                ),
                new Container(
                  width: double.infinity,
                  height: 1.0,
                  color: Colors.grey[200],
                ),
              ],
            )
          )
        ),

      );
    });
    return new WillPopScope(
      onWillPop: (){Navigator.pop(context);} ,
      child: Material(
        type: MaterialType.transparency,
        child: new Stack(
            children: <Widget>[
              Container(
                color: Colors.black.withOpacity(0.2),
              ),
              Positioned(
                top: 80.0,
                left: 24.0,
                right: 24.0,
                child: Column(
                  children: <Widget>[
                    new Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black26,
                            blurRadius: 10.0,
                            offset: const Offset(0.0, 10.0),
                          ),
                        ],
                      ),
                      padding: EdgeInsets.only(left: 16, right: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Padding(
                            padding: EdgeInsets.only(top: 90),
                          ),
                          new Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Icon(
                                FontAwesomeIcons.infoCircle,
                                size: 14,
                              ),
                              new Padding(padding: EdgeInsets.only(left: 5)),
                              new Container(
                                padding: EdgeInsets.only(top: 2),
                                child: new Text(
                                  "WhatsApp Kami!",
                                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700,),
                                ),
                              ),
                            ],
                          ),
                          new Padding(
                            padding: EdgeInsets.only(top:20),
                          ),
                          new Container(
                            width: double.infinity,
                            height: 1.0,
                            color: Colors.grey[200],
                          ),
                          new Column(children: WArow),
                          new Padding(
                            padding: EdgeInsets.only(top:20),
                          ),
                          new Container(
                            color: Colors.white,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                new FlatButton(
                                    onPressed: (){
                                      Navigator.of(context).pop();
                                    },//
                                    child: new Row(
                                      children: <Widget>[
                                        new Icon(
                                          FontAwesomeIcons.times,
                                          size: 16,
                                          color: Colors.grey[600],
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(bottom: 3),
                                          padding: EdgeInsets.only(left: 3, top: 3),
                                          child: new Text(
                                            "Tutup",
                                            style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.grey[600],
                                              fontFamily: "NeoSansBold",
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                top: 20,
                width: MediaQuery.of(context).size.width,
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Container(
                      width: 140.0,
                      height: 140.0,
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.white, width: 5),
                        color: widget._bg1,
                      ),
                      padding: EdgeInsets.all(5),
                      child: new ClipOval(
                        child: new Image.asset("assets/suri/suri2.png"),
                      ),
                    ),
                  ],
                ),
              ),
            ]
        ),
      ),
    );
  }

  Widget wa_loading(){
    return new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Container(
          decoration: new BoxDecoration(
              color: Colors.grey[200],
              border: Border.all(color: Colors.grey[200], width: 1.0),
              borderRadius: new BorderRadius.all(new Radius.circular(20.0))
          ),
          padding: EdgeInsets.only(left:10.0,right:10.0,top:6.0,bottom:10.0),
          child: new Icon(
            FontAwesomeIcons.whatsapp,
            size: 35,
            color: Colors.white,
          ),
        ),
        new Padding(
          padding: EdgeInsets.only(top: 10.0),
        ),

        new Text(
          "WhatsApp",
          style: TextStyle(color: Colors.white, fontSize: 12.0),
        ),
        new Padding(
          padding: EdgeInsets.only(top: 2.0),
        ),
        new Text(
          "Ofline",
          style: TextStyle(color: Colors.white, fontSize: 12.0),
        )
      ],
    );
  }

  //telegram
  Widget _telegramList(BuildContext context, tele){
    print(tele.toString());
    List<Widget> telerow = new List<Widget>();
    telerow.add(new Container());
    tele.forEach((dt){
      telerow.add(
        new Container(
            child: InkWell(
                onTap: (){
                  //var teleUrl = "tg://user?id="+dt["akuntele"];
                  var teleUrl = "https://telegram.me/suryahusadha_dps";
                  print(teleUrl);
                  _launchURL(teleUrl);
                },
                child: new Column(
                  children: <Widget>[
                    new Padding(
                      padding: EdgeInsets.only(top: 20),
                    ),
                    new Row(
                      children: <Widget>[
                        new Expanded(
                          flex: 1,
                          child: new Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              new Icon(
                                FontAwesomeIcons.telegram,
                                size: 24,
                                color: Colors.lightBlue,
                              ),
                              new Container(
                                padding: EdgeInsets.only(left: 5, top: 2),
                                child: Text(dt["wa_name"], style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.grey[600],
                                    fontWeight: FontWeight.w700
                                )),
                              )
                            ],
                          ),
                        ),
                        new Icon(
                          FontAwesomeIcons.arrowRight,
                          size: 12,
                          color: Colors.grey[600],
                        )
                      ],
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 20),
                    ),
                    new Container(
                      width: double.infinity,
                      height: 1.0,
                      color: Colors.grey[200],
                    ),
                  ],
                )
            )
        ),

      );
    });
    return new WillPopScope(
      onWillPop: () => _onWillPop(),
      child: Material(
        type: MaterialType.transparency,
        child: new Stack(
            children: <Widget>[
              Container(
                color: Colors.black.withOpacity(0.2),
              ),
              Positioned(
                top: 80.0,
                left: 24.0,
                right: 24.0,
                child: Column(
                  children: <Widget>[
                    new Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black26,
                            blurRadius: 10.0,
                            offset: const Offset(0.0, 10.0),
                          ),
                        ],
                      ),
                      padding: EdgeInsets.only(left: 16, right: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Padding(
                            padding: EdgeInsets.only(top: 90),
                          ),
                          new Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Icon(
                                FontAwesomeIcons.infoCircle,
                                size: 14,
                              ),
                              new Padding(padding: EdgeInsets.only(left: 5)),
                              new Container(
                                padding: EdgeInsets.only(top: 2),
                                child: new Text(
                                  "Akun Telegram Kami!",
                                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700,),
                                ),
                              ),
                            ],
                          ),
                          new Padding(
                            padding: EdgeInsets.only(top:20),
                          ),
                          new Container(
                            width: double.infinity,
                            height: 1.0,
                            color: Colors.grey[200],
                          ),
                          new Column(children: telerow),
                          new Padding(
                            padding: EdgeInsets.only(top:20),
                          ),
                          new Container(
                            color: Colors.white,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                new FlatButton(
                                    onPressed: (){
                                      Navigator.of(context).pop();
                                    },//
                                    child: new Row(
                                      children: <Widget>[
                                        new Icon(
                                          FontAwesomeIcons.times,
                                          size: 16,
                                          color: Colors.grey[600],
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(bottom: 3),
                                          padding: EdgeInsets.only(left: 3, top: 3),
                                          child: new Text(
                                            "Tutup",
                                            style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.grey[600],
                                              fontFamily: "NeoSansBold",
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                top: 20,
                width: MediaQuery.of(context).size.width,
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Container(
                      width: 140.0,
                      height: 140.0,
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.white, width: 5),
                        color: widget._bg1,
                      ),
                      padding: EdgeInsets.all(5),
                      child: new ClipOval(
                        child: new Image.asset("assets/suri/suri2.png"),
                      ),
                    ),
                  ],
                ),
              ),
            ]
        ),
      ),
    );
  }

  _onWillPop(){
    Navigator.pop(context);
  }

  Widget tele_loading(){
    return new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Container(
          decoration: new BoxDecoration(
              color: Colors.grey[200],
              border: Border.all(color: Colors.grey[200], width: 1.0),
              borderRadius: new BorderRadius.all(new Radius.circular(20.0))
          ),
          padding: EdgeInsets.only(left:10.0,right:10.0,top:6.0,bottom:10.0),
          child: new Icon(
            FontAwesomeIcons.telegram,
            size: 35,
            color: Colors.white,
          ),
        ),
        new Padding(
          padding: EdgeInsets.only(top: 10.0),
        ),
        new Text(
          "Tidak",
          style: TextStyle(color: Colors.white, fontSize: 12.0),
        ),
        new Padding(
          padding: EdgeInsets.only(top: 2.0),
        ),
        new Text(
          "Tersedia",
          style: TextStyle(color: Colors.white, fontSize: 12.0),
        )
      ],
    );
  }
}