import 'package:flutter/material.dart';
import 'package:surya_husadha/components/com_login/com_login.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'dart:convert' as convert;
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/helper/transition/slide_route.dart';
import 'package:surya_husadha/pages/widgetpage.dart';

class UserStatusController extends StatefulWidget {

  String _cabang;
  var _dtakun;
  String _email;
  var _scafolt_key;

  UserStatusController(String cabang, dtakun, scafolt_key){
    this._dtakun = dtakun;
    this._cabang = cabang;
    this._scafolt_key = scafolt_key;
    if(dtakun!=null){
      this._email = dtakun[0]["email"];
    }
  }

  @override
  _UserStatusController createState() => new _UserStatusController();
}

class _UserStatusController extends State<UserStatusController>{

  Widget imgUser;
  String namaUser = "";

  var mbr_voucher = [];
  bool isloadingMbr = true;

  @override
  initState() {
    super.initState();
    getmbr_voucher().then((String response){
      var decode = convert.jsonDecode(response);
      var decodeConvert = convert.jsonDecode(decode["responseBody"]);
      final dtkonten = decodeConvert["mbr_voucher"];
      mbr_voucher = dtkonten[0]["data"];
    });
  }

  Future<String> getmbr_voucher() async {
    var _data;
    if(isloadingMbr){
      var api = new Http_Controller();
      List<Map<String, dynamic>> dtNext=[];
      dtNext.add({
        "email": widget._email.toString(),
        "shv_status": "1",
      });
      var http_response = await api.postResponse("https://suryahusadha.com/api/loyalti/mbr_voucher", dtNext);
      _data = convert.jsonEncode(http_response);
    }
    return _data;
  }

  @override
  Widget _buildUserstatusController(){

    if(int.parse(DateTime.now().hour.toString()) > 00){
      namaUser = "Selamat pagi!";
    }
    if(int.parse(DateTime.now().hour.toString()) > 11){
      namaUser = "Selamat siang!";
    }
    if(int.parse(DateTime.now().hour.toString()) > 14){
      namaUser = "Selamat sore!";
    }
    if(int.parse(DateTime.now().hour.toString()) > 18){
      namaUser = "Selamat malam!";
    }

    if( widget._dtakun != null) {
      namaUser = "Hi, "+widget._dtakun[0]["name"];
      String img = SERVER.domain + widget._dtakun[0]["pp"] +'?dummy=${new DateTime.now()}';
      imgUser = new Container(
        height: 55.0,
        width: 55.0,
        padding: EdgeInsets.all(2.0),
        decoration: new BoxDecoration(
            borderRadius:
            new BorderRadius.all(new Radius.circular(100.0)),
            color: Colors.lightBlue
        ),
        alignment: Alignment.center,
        child: CircleAvatar(
          backgroundImage: NetworkImage(img),
          minRadius: 90,
          maxRadius: 150,
        ),
      );
    } else {
      imgUser = new Container(
        height: 55.0,
        width: 55.0,
        padding: EdgeInsets.all(6.0),
        decoration: new BoxDecoration(
            borderRadius:
            new BorderRadius.all(new Radius.circular(100.0)),
            color: Colors.lightBlue
        ),
        alignment: Alignment.center,
        child: new Icon(
          FontAwesomeIcons.user,
          color: Colors.white,
          size: 24.0,
        ),
      );
    }
    return new Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: Colors.grey[300]
        )
      ),
      margin: EdgeInsets.only(bottom:16.0, top:16.0),
      padding: EdgeInsets.only(top:16.0, bottom:16.0, left: 16.0, right: 16.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          new Container(
            child: new Row(
              children: <Widget>[
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    imgUser,
                  ],
                ),
                new Column(
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.only(left:10.0, right:10.0),
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                margin: EdgeInsets.only(left: 3),
                                child:new Text(
                                  namaUser,
                                  style: TextStyle(fontFamily: "NeoSansBold", fontSize: 16.0),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              new Padding(
                                padding: EdgeInsets.only(top: 2.0),
                              ),
                              new Stack(
                                children: <Widget>[
                                  mbr_voucher.length > 0 ?
                                  new Container(
                                    margin: EdgeInsets.only(left: 0, top:0),
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.red,
                                    ),
                                  ): Container(),
                                  new Row(
                                    children: <Widget>[
                                      new InkWell(
                                        onTap: (){
                                          if(widget._dtakun == null){
                                            widget._scafolt_key.currentState.removeCurrentSnackBar();
                                            final snackBar = SnackBar(
                                              content: Text('Diperlukan login'),
                                              action: SnackBarAction(
                                                label: 'Login?',
                                                onPressed: () {
                                                  Navigator.pushReplacement(context, SlideLeftRoute(page: Fullpage(widgetpage: LoginController(widget._cabang), pagename: "LoginPage",)));
                                                },
                                              ),
                                            );
                                            widget._scafolt_key.currentState.showSnackBar(snackBar);
                                          }else{
                                            List<Map<String, dynamic>> params=[];
                                            params.add({
                                              "_cabang": widget._cabang,
                                            });
                                            RouteHelper(context, "MbrVoucher", params);
                                          }
                                        },
                                        child: new Container(
                                          margin: EdgeInsets.only(top: 3, left: 3),
                                          decoration: new BoxDecoration(
                                              borderRadius:
                                              new BorderRadius.all(new Radius.circular(5.0)),
                                              color: Color(0x50FFD180)
                                          ),
                                          child: new Row(
                                            children: <Widget>[
                                              new Container(
                                                alignment: Alignment.center,
                                                padding: EdgeInsets.only(left:10.0),
                                                child:  new Icon(
                                                  FontAwesomeIcons.ticketAlt,
                                                  size: 16.0,
                                                ),
                                              ),
                                              new Container(
                                                alignment: Alignment.center,
                                                padding: EdgeInsets.all(6.0),
                                                child: new Text(
                                                  "Voucher",
                                                  style: TextStyle(fontSize: 12.0),
                                                ),
                                              ),
                                              new Container(
                                                alignment: Alignment.center,
                                                padding: EdgeInsets.only(right:5.0),
                                                child:  new Icon(
                                                  FontAwesomeIcons.arrowRight,
                                                  size: 10.0,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      new Container(
                                          margin: EdgeInsets.only(left: 10.0),
                                          decoration: new BoxDecoration(
                                              borderRadius:
                                              new BorderRadius.all(new Radius.circular(5.0)),
                                              color: Color(0x50FFD180)
                                          ),
                                          child: new InkWell(
                                            onTap: (){
                                              var bodval;
                                              var emailval;
                                              if(widget._dtakun != null){
                                                bodval=widget._dtakun[0]["bod"];
                                                emailval=widget._dtakun[0]["email"];
                                              }
                                              List<Map<String, dynamic>> rsvparams=[];
                                              rsvparams.add({
                                                "_drawer": "reservasi_list",
                                                "_cabang": widget._cabang,
                                                "_email": emailval,
                                                "_bod": bodval,
                                                "_akun": widget._dtakun,
                                              });
                                              RouteHelper(context, "ReservasiList", rsvparams);},
                                            child: new Row(
                                              children: <Widget>[
                                                new Container(
                                                  alignment: Alignment.center,
                                                  padding: EdgeInsets.only(left:10.0),
                                                  child:  new Icon(
                                                    FontAwesomeIcons.listOl,
                                                    size: 16.0,
                                                  ),
                                                ),
                                                new Container(
                                                  alignment: Alignment.center,
                                                  padding: EdgeInsets.all(6.0),
                                                  child: new Text(
                                                    "Reservasi Anda",
                                                    style: TextStyle(fontSize: 12.0),
                                                  ),
                                                ),
                                                new Container(
                                                  alignment: Alignment.center,
                                                  padding: EdgeInsets.only(right:5.0),
                                                  child:  new Icon(
                                                    FontAwesomeIcons.arrowRight,
                                                    size: 10.0,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                      )
                                    ],
                                  ),
                                ],
                              ),



                            ]
                        ),
                      ),

                    ]
                ),

              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildUserstatusController();
  }
}