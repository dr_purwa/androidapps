import 'dart:convert' as convert;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surya_husadha/components/com_article/detail_article.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/dropdown/CustomExpansionTitle.dart' as custom;
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';

class Fasilitas extends StatefulWidget {
  var _dtakun;
  String _cabang;
  Color _color1;
  Color _color2;
  Fasilitas(List params, akun){
    _dtakun = akun;
    print(params.toString());
    params.forEach((dt){
      this._cabang = dt["_cabang"];
      if(_cabang=="group"){
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
      }
      if(_cabang=="denpasar"){
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
      }
      if(_cabang=="ubung"){
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
      }
      if(_cabang=="nusadua"){
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
      }
      if(_cabang=="kmc"){
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
      }
    });
  }
  @override
  _Fasilitas createState() => new _Fasilitas();
}

class _Fasilitas extends State<Fasilitas> {

  GlobalKey<ScaffoldState> scafolt_key = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.white,
        systemNavigationBarDividerColor: Colors.grey[300],
        systemNavigationBarIconBrightness: Brightness.dark,// navigation bar color
        statusBarColor: widget._color2,
        statusBarIconBrightness: Brightness.dark,
    ));
    return new SafeArea(
      child: Scaffold(
          key: scafolt_key,
          drawer: drawerController(widget._dtakun, widget._cabang, "Fasilitas"),
          appBar: new PreferredSize(
            preferredSize: Size.fromHeight(200.0),
            child: AppBarController(widget._cabang, scafolt_key, widget._dtakun, "Fasilitas"),
          ),
          backgroundColor: Colors.grey[200],
          body: WillPopScope(
           onWillPop: _onWillPop,
            child: _buildContentList(),
          )
      ),
    );
  }

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> dtNext=[];
    dtNext.add({"_cabang": widget._cabang});
    RouteHelper(context, widget._cabang, dtNext);
  }

  var http = new Http_Controller();

  @override
  Future<String> _getFasilitasDPS() async {
    var http_response = await http.getResponse("https://suryahusadha.com/api/konten/getcat/21");
    var _data = convert.jsonEncode(http_response);
    return _data;
  }
  @override
  Future<String> _getFasilitasUB() async {
    var http_response = await http.getResponse("https://suryahusadha.com/api/konten/getcat/22");
    var _data = convert.jsonEncode(http_response);
    return _data;
  }
  @override
  Future<String> _getFasilitasND() async {
    var http_response = await http.getResponse("https://suryahusadha.com/api/konten/getcat/23");
    var _data = convert.jsonEncode(http_response);
    return _data;
  }

  Widget _buildContentList() {
    Widget _header;
    var getFuture;
    if(widget._cabang == "denpasar"){
      getFuture = _getFasilitasDPS();
      _header = cardHeader("Fasilitas Surya Husadha Denpasar");
    }
    if(widget._cabang == "ubung"){
      getFuture = _getFasilitasUB();
      _header = cardHeader("Fasilitas Surya Husadha Ubung");
    }
    if(widget._cabang == "nusadua"){
      getFuture = _getFasilitasND();
      _header = cardHeader("Fasilitas Surya Husadha Nusadua");
    }
    return new Container(
        margin: EdgeInsets.only(bottom: 1.0),
        child: FutureBuilder(
          future: getFuture,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              var decode = convert.jsonDecode(snapshot.data);
              var decodeConvert = convert.jsonDecode(decode["responseBody"]);
              final dtkonten = decodeConvert["cats"];
              final cats = dtkonten[0]["data"];
              return new Container(
                child: new Column(
                  children: <Widget>[
                    _header,
                    new Expanded(
                      child: ListView(
                        children: cats.map<Widget>((data) {
                          return _categories(data);
                        }).toList(),
                      ),
                    )
                  ],
                ),
              );
            }
            return Center(
              child: SizedBox(
                  width: 40.0,
                  height: 40.0,
                  child: const CircularProgressIndicator()),
            );
          },
        )
    );
  }

  Widget cardHeader(String title){
    return new Container(
      color: Colors.grey[200],
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 20),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Icon(
                        FontAwesomeIcons.thList,
                        size: 18,
                        color: Colors.grey[800],
                      ),
                      new Container(
                        padding: EdgeInsets.only(left: 10, top: 4),
                        child: new Text(
                          title,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          softWrap: true,
                          style: TextStyle(color: Colors.grey[800], fontSize: 16, fontWeight: FontWeight.w700),
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  color: Colors.grey[300],
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Future<String> _getContent(catid) async {
    var http_response = await http.getResponse("https://suryahusadha.com/api/konten/getkontencat/"+catid);
    var _data = convert.jsonEncode(http_response);
    return _data;
  }

  Widget _categories(dt) {
    return new Column(
      children: <Widget>[
        custom.ExpansionTile(
          initiallyExpanded: false,
          headerBackgroundColor: Colors.white,
          title: Container(
            child: Row(
              children: <Widget>[
                new Icon(
                  FontAwesomeIcons.handPointRight,
                  size: 16,
                  color: Colors.grey[600],
                ),
                new Container(
                  padding: EdgeInsets.only(left: 10),
                  child: Text(
                    dt["cat_name_id"],
                    style: TextStyle(color: Colors.grey[800], fontWeight: FontWeight.w700, fontSize: 14),
                    overflow:  TextOverflow.ellipsis,
                    maxLines: 1,
                    softWrap: true,
                  ),
                )
              ],
            )
          ),
          children: <Widget>[
            FutureBuilder(
              future: _getContent( dt["id_cat"]),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  var decode = convert.jsonDecode(snapshot.data);
                  var decodeConvert = convert.jsonDecode(decode["responseBody"]);
                  final dtkonten = decodeConvert["kontencat"];
                  final cats = dtkonten[0]["data"];
                  return new Column(
                    children: cats.map<Widget>((data) {
                      return _contendata(data);
                    }).toList(),
                  );
                }
                return Center(
                  child: SizedBox(
                      width: 40.0,
                      height: 40.0,
                      child: const CircularProgressIndicator()),
                );
              },
            )
          ],
        ),
      ],
    );
  }

  Widget _contendata(dt){
    return new Container(
      color: Colors.white70,
      margin: EdgeInsets.only(top: 5),
      child: new InkWell(
          onTap: (){
            List<Map<String, dynamic>> dtNext=[];
            dtNext.add({
              "_cabang": widget._cabang,
              "_ctntid": int.parse(dt["Aid"])
            });
            showDialog(
              barrierDismissible: true,
              context: context,
              builder: (BuildContext context){
                return new DetaiArticle(dtNext, widget._dtakun);
              },
            );
          },
          child: new Column(
            children: <Widget>[
              new Container(
                padding: EdgeInsets.only(top:5, bottom: 5, left: 32, right: 32),
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new ClipRRect(
                      borderRadius: new BorderRadius.circular(8.0),
                      child: new Image.network(
                        SERVER.domain + dt["image_default"],
                        height: 55.0,
                        width: 55.0,
                        fit: BoxFit.cover,
                      ),
                    ),
                    new Padding(padding: EdgeInsets.only(left: 10)),
                    new Expanded(
                      flex: 2,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(left:10.0, right:10.0),
                              child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child:new Text(
                                        dt["title_en"],
                                        style: TextStyle(fontFamily: "NeoSansBold", fontSize: 14.0, color: Colors.grey[800]),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                  ]
                              ),
                            ),
                          ]
                      ),
                    ),
                    new Container(
                      height: 40.0,
                      width: 40.0,
                      alignment: Alignment.centerRight,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.all(6.0),
                              alignment: Alignment.centerRight,
                              child: new Icon(
                                FontAwesomeIcons.angleRight,
                                color: Colors.grey,
                                size: 18.0,
                              ),
                            ),
                          ]
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                margin: EdgeInsets.only(top: 5.0),
                width: double.infinity,
                height: 1.0,
                color: Colors.grey[200],
              ),
            ],
          )
      ),
    );
  }

  Widget _contendata2(dt) {
    return new Container(
      color: Colors.grey[100],
      padding: EdgeInsets.only(left: 24, right: 24),
      child: InkWell(
        onTap: (){
          List<Map<String, dynamic>> dtNext=[];
          dtNext.add({"_cabang": widget._cabang});
          dtNext.add({"_ctntid": dt["id"]});
          //showDialog(
            //barrierDismissible: true,
            //context: context,
            //builder: (BuildContext context){
             // return new DetaiArticle(dtNext, widget._dtakun);
            //},
          //);
        },
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            new Padding(
              padding: EdgeInsets.only(top: 8.0),
            ),


            new Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new ClipRRect(
                    borderRadius: new BorderRadius.circular(8.0),
                    child: new Image.network(
                      SERVER.domain + dt["image_default"],
                      height: 55.0,
                      width: 55.0,
                      fit: BoxFit.cover,
                    ),
                  ),
                  new Padding(padding: EdgeInsets.only(left: 10)),
                  new Expanded(
                    child: Container(
                      height: 55,
                      child: Text(
                        dt["title_en"],
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        softWrap: true,
                        style: new TextStyle(color: Colors.grey[800],fontFamily: "NeoSansBold", fontSize: 14.0),
                      ),
                    ),
                  ),
                ]
            ),
            new Container(
              margin: EdgeInsets.only(top: 8.0),
              width: double.infinity,
              height: 1.0,
              color: Colors.grey[300],
            ),
          ],
        ),
      ),
    );
  }

}