import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class FasilitasGroup extends StatefulWidget {
  var _dtakun;
  String _cabang;
  String _ctntid;
  Color _color1;
  Color _color2;
  Color _btnText;
  FasilitasGroup(List params, akun){
    _dtakun = akun;
    params.forEach((dt){
      this._cabang = dt["_cabang"];
      this._ctntid = dt["_ctntid"];
      if(_cabang=="group"){
        this._btnText = Colors.grey;
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
      }
      if(_cabang=="denpasar"){
        this._btnText = WarnaCabang.shh;
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
      }
      if(_cabang=="ubung"){
        this._btnText = WarnaCabang.ubung;
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
      }
      if(_cabang=="nusadua"){
        this._btnText =  WarnaCabang.nusadua;
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
      }
      if(_cabang=="kmc"){
        this._btnText = WarnaCabang.kmc;
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
      }
    });
  }
  @override
  _FasilitasGroup createState() => new _FasilitasGroup();
}

class _FasilitasGroup extends State<FasilitasGroup> {

  GlobalKey<ScaffoldState> scafolt_key = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.white,
        systemNavigationBarDividerColor: Colors.grey[300],
        systemNavigationBarIconBrightness: Brightness.dark,// navigation bar color
        statusBarColor: widget._color2,
        statusBarIconBrightness: Brightness.dark,
    ));
    return new SafeArea(
      child: Scaffold(
        key: scafolt_key,
        drawer: drawerController(widget._dtakun, widget._cabang, "FasilitasGroup"),
        appBar: new PreferredSize(
          preferredSize: Size.fromHeight(200.0),
          child: AppBarController(widget._cabang, scafolt_key, widget._dtakun, "FasilitasGroup"),
        ),
        backgroundColor: Colors.grey[200],
        body: WillPopScope(
          onWillPop: _onWillPop,
          child: _buildContentList(context),
        )
      ),
    );
  }

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> dtNext=[];
    dtNext.add({"_cabang": widget._cabang});
    RouteHelper(context, widget._cabang, dtNext);
  }

  Widget _buildContentList(BuildContext context) {
    return new Container(
      child: new Column(
        children: <Widget>[
          cardHeader("Fasilitas Surya Husadha Denpasar", 'denpasar', context),
          cardHeader("Fasilitas Surya Husadha Ubung", 'ubung', context),
          cardHeader("Fasilitas Surya Husadha Nusadua", 'nusadua', context),
        ],
      ),
    );
  }

  Widget cardHeader(String title, String cabang, BuildContext context){
    return new InkWell(
      onTap: (){
        List<Map<String, dynamic>> params=[];
        params.add({
          "_cabang": cabang,
        });
        RouteHelper(context, "Fasilitas", params);
      },
      child: new Container(
        color: Colors.white,
        child: new Row(
          children: <Widget>[
            new Expanded(
              child: new Column(
                children: <Widget>[
                  new Container(
                    padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 20),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: new Row(
                            children: <Widget>[
                              new Icon(
                                FontAwesomeIcons.handPointRight,
                                size: 18,
                                color: Colors.grey[800],
                              ),
                              new Container(
                                padding: EdgeInsets.only(left: 10, top: 4),
                                child: new Text(
                                  title,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  softWrap: true,
                                  style: TextStyle(color: Colors.grey[800], fontSize: 14, fontWeight: FontWeight.w700),
                                ),
                              ),
                            ],
                          )
                        ),
                        new Icon(
                          FontAwesomeIcons.chevronRight,
                          size: 12,
                          color: Colors.grey[800],
                        ),

                      ],
                    ),
                  ),
                  new Container(
                    color: Colors.grey[300],
                    width: MediaQuery.of(context).size.width,
                    height: 1.0,
                  ),
                ],
              ),
            ),
          ],
        ),
      )
    );
  }
}