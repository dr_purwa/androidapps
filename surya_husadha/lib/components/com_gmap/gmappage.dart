import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class GmapPage extends StatefulWidget {

  var _dtakun;
  String _cabang;
  String _ctntid;
  Color _color1;
  Color _color2;
  Color _btnText;

  GmapPage(List params, akun){
    _dtakun = akun;
    params.forEach((dt){
      this._cabang = dt["_cabang"];
      this._ctntid = dt["_ctntid"];
      if(_cabang=="group"){
        this._btnText = Colors.grey;
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
      }
      if(_cabang=="denpasar"){
        this._btnText = WarnaCabang.shh;
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
      }
      if(_cabang=="ubung"){
        this._btnText = WarnaCabang.ubung;
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
      }
      if(_cabang=="nusadua"){
        this._btnText =  WarnaCabang.nusadua;
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
      }
      if(_cabang=="kmc"){
        this._btnText = WarnaCabang.kmc;
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
      }
    });
  }

  @override
  _GmapPage createState() => new _GmapPage();

}

class _GmapPage extends State<GmapPage> with SingleTickerProviderStateMixin{

  GlobalKey<ScaffoldState> scafolt_key = GlobalKey<ScaffoldState>();
  Completer<GoogleMapController> _controller = Completer();
  BitmapDescriptor _markerIcon;
  bool zoomview = false;
  String _title = "";
  String _desc = "";
  LatLng _latLng;
  String _latitude;
  String _longitude;
  static final CameraPosition _posisiAwal = CameraPosition(
    target: LatLng(-8.67828825, 115.21278315),
    zoom: 11,
  );

  Future<bool> _onWillPop(){
    print("zoomview: "+zoomview.toString());
    if(zoomview){
      _closezoom();
    } else{
      List<Map<String, dynamic>> dtNext=[];
      dtNext.add({"_cabang": widget._cabang});
      RouteHelper(context, widget._cabang, dtNext);
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.white,
        systemNavigationBarDividerColor: Colors.grey[300],
        systemNavigationBarIconBrightness: Brightness.dark,// navigation bar color
        statusBarColor: widget._color2,
        statusBarIconBrightness: Brightness.dark// statu// status bar color
    ));

    return new SafeArea(
      child: new Scaffold(
        key: scafolt_key,
        drawer: drawerController(widget._dtakun, widget._cabang, "GmapPage"),
        appBar: new PreferredSize(
          preferredSize: Size.fromHeight(200.0),
          child: AppBarController(widget._cabang, scafolt_key, widget._dtakun, "GmapPage"),
        ),
        backgroundColor: Colors.grey[200],
        body: WillPopScope(
          onWillPop: _onWillPop,
          child: new Stack(
            children: <Widget>[
              new Container(
                margin: EdgeInsets.only(top: 70),
                child: new GoogleMap(
                  initialCameraPosition: _posisiAwal,
                  markers: _createMarker(),
                  compassEnabled: true,
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);
                  },
                ),
              ),
              new Visibility(
                visible: zoomview ? false : true,
                child: new Container(
                  color: Colors.white,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Container(
                          padding: EdgeInsets.only(top: 10, left: 16, right: 16, bottom: 16),
                          child: new Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: new Container(
                                  padding: EdgeInsets.only(right: 10, top: 5),
                                  child: new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Container(
                                        child: new Text(
                                          "Unit Bisnis PT. Surya Husadha Group",
                                          style: TextStyle(color: Colors.grey[800],fontSize: 16, fontWeight: FontWeight.w700),
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 1,
                                          softWrap: true,
                                        ),
                                      ),
                                      new Container(
                                        child: new Text(
                                          "Klik pada pin untuk info lebih detail",
                                          style: TextStyle(color: Colors.grey[600]),
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 1,
                                          softWrap: true,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              new Icon(
                                FontAwesomeIcons.map,
                                size: 30,
                                color: Colors.grey[800],
                              ),
                            ],
                          )
                      ),
                      new Container(
                        height: 1,
                        color: Colors.grey[200],
                      )
                    ],
                  ),
                ),
              ),
              new Visibility(
                visible: zoomview,
                child: new Container(
                  color: Colors.white,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.only(top: 10, left: 16, right: 16, bottom: 16),
                        color: Colors.white.withOpacity(0.5),
                        child: new Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Expanded(
                                flex: 1,
                                child: new InkWell(
                                  onTap: (){
                                    _tozoom(_latLng, _title, _desc, _latitude, _longitude);
                                  },
                                  child: new Container(
                                      child: new Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          new Icon(
                                            FontAwesomeIcons.mapMarkerAlt,
                                            size: 30,
                                            color: Colors.grey[800],
                                          ),
                                          new Container(
                                            padding: EdgeInsets.only(left: 5, top: 5),
                                            child: new Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisSize: MainAxisSize.min,
                                              children: <Widget>[
                                                new Container(
                                                  child: new Text(
                                                    _title,
                                                    style: TextStyle(color: Colors.grey[800],fontSize: 16, fontWeight: FontWeight.w700),
                                                    overflow: TextOverflow.ellipsis,
                                                    maxLines: 1,
                                                    softWrap: true,
                                                  ),
                                                ),
                                                new Container(
                                                  child: new Text(
                                                    _desc,
                                                    style: TextStyle(color: Colors.grey[600]),
                                                    overflow: TextOverflow.ellipsis,
                                                    maxLines: 1,
                                                    softWrap: true,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                                  ),
                                )
                            ),
                            new InkWell(
                              onTap: (){
                                _closezoom();
                              },
                              child: new Container(
                                height: 35,
                                width: 35,
                                decoration: ShapeDecoration(
                                    color: Colors.redAccent,
                                    shape: CircleBorder(
                                        side: BorderSide(
                                            color: Colors.white
                                        )
                                    )
                                ),
                                child: Icon(
                                  FontAwesomeIcons.times,
                                  color: Colors.white,
                                  size: 18,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      new Container(
                        height: 1,
                        color: Colors.grey[200],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: new Visibility(
          visible: zoomview,
          child: FloatingActionButton(
              backgroundColor: Colors.redAccent,
              onPressed: (){
                _launchURL("google.navigation:q=$_latitude,$_longitude");
                _closezoom();
              },
              child: Icon(
                FontAwesomeIcons.directions,
                color: Colors.white,
              )
          )
        )
      ),
    );
  }

  _launchURL(url) async {
    await launch(url);
  }

  Future<void> _tozoom(LatLng latLng, String title, String desc, String latitude, String longitude) async {
    final GoogleMapController controller = await _controller.future;
    final CameraPosition _zoom = CameraPosition(
      bearing: 160,
      target: latLng,
      tilt: 24,
      zoom: 14,
    );
    setState(() {
      _title = title;
      _desc = desc;
      zoomview = true;
      _latLng = latLng;
      _latitude = latitude;
      _longitude = longitude;
    });
    controller.animateCamera(CameraUpdate.newCameraPosition(_zoom)).whenComplete((){
      new Timer(const Duration(seconds: 1), () {
        setState(() {
          _tozoom2(latLng, _title, _desc, _latitude, _longitude);
        });
      });
    });
  }

  Future<void> _tozoom2(LatLng latLng, String title, String desc, String latitude, String longitude) async {
    final GoogleMapController controller = await _controller.future;
    final CameraPosition _zoom = CameraPosition(
      bearing: 195,
      target: latLng,
      tilt: 35,
      zoom: 14,
    );
    final CameraPosition _zoom2 = CameraPosition(
      bearing: 325,
      target: latLng,
      tilt: 0,
      zoom: 18,
    );
    setState(() {
      _title = title;
      _desc = desc;
      zoomview = true;
      _latLng = latLng;
      _latitude = latitude;
      _longitude = longitude;
    });
    controller.animateCamera(CameraUpdate.newCameraPosition(_zoom2));
  }

  Future<void> _closezoom() async {
    final GoogleMapController controller = await _controller.future;
    setState(() {
      zoomview = false;
    });
    controller.animateCamera(CameraUpdate.newCameraPosition(_posisiAwal));
  }

  Set<Marker> _createMarker() {
    return <Marker>[
      Marker(
        markerId: MarkerId("Surya Husadha Hospital"),
        position: LatLng(-8.67828825, 115.21278315),
        icon: _markerIcon,
        infoWindow: InfoWindow(
            title: "Surya Husadha Hospital",
            snippet: "Jl. Pulau Serangan No.7 Dauh Puri Klod",
        ),
        onTap: (){
          _tozoom(
              LatLng(-8.67828825, 115.21278315),
              "Surya Husadha Hospital",
              "Jl. Pulau Serangan No.7 Dauh Puri Klod",
              "-8.67828825",
              "115.21278315"
          );
        },
        consumeTapEvents: true
      ),
      Marker(
        markerId: MarkerId("Surya Husadha Nusadua"),
        position: LatLng(-8.8044116, 115.2172359),
        icon: _markerIcon,
        infoWindow: InfoWindow(
          title: "Surya Husadha Nusadua",
          snippet: "Jalan Siligita, Jl. Nusa Dua No.14 Benoa",
        ),
        onTap: (){
          _tozoom(
              LatLng(-8.8044116, 115.2172359),
              "Surya Husadha Nusadua",
              "Jalan Siligita,"
                  " Jl. Nusa Dua No.14 Benoa",
              "-8.8044116",
              "115.2172359"
          );
        },
          consumeTapEvents: true
      ),
      Marker(
        markerId: MarkerId("Surya Husadha Ubung"),
        position: LatLng(-8.6228132, 115.1990344),
        icon: _markerIcon,
        infoWindow: InfoWindow(
          title: "Surya Husadha Ubung",
          snippet: "Jl. Cokroaminoto No.356 Ubung Kaja",
        ),
        onTap: (){
          _tozoom(
            LatLng(-8.6228132, 115.1990344),
            "Surya Husadha Ubung",
            "Jl. Cokroaminoto No.356 Ubung Kaja",
            "-8.6228132",
            "115.1990344",
          );
        },
          consumeTapEvents: true
      ),
      Marker(
        markerId: MarkerId("Kuta Medical Center (KMC) Clinic"),
        position: LatLng(-8.7337588, 115.1655994),
        icon: _markerIcon,
        infoWindow: InfoWindow(
          title: "Kuta Medical Center (KMC) Clinic",
          snippet: "Jl. Kartika Plaza No.90 Tuban",
        ),
        onTap: (){
          _tozoom(
              LatLng(-8.7337588, 115.1655994),
              "Kuta Medical Center (KMC) Clinic",
              "Jl. Kartika Plaza No.90 Tuban",
              "-8.7337588",
              "115.1655994"
          );
        },
          consumeTapEvents: true
      ),
    ].toSet();
  }

}