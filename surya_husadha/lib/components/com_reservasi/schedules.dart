import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart' show CalendarCarousel;
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/classes/event_list.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'dart:convert' as convert;
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Schedules extends StatefulWidget {

  DateTime _currentDate2Select;
  int _clinicSelect;
  int _drSelect;
  String _Namacabang;
  String _BranchCode;
  String _cabang;
  Color _color1;
  Color _color2;
  Color _Selected_color;
  String _idsp;
  String _drimage;
  String _drname;
  String _titlesp_id;
  String _titlesp_en;
  String _dridweb;
  String _secid;
  String _dridapi;
  bool _activePage = true;
  var _akun;
  Schedules(List Schedulesdt, akun){
    this._akun = akun;
    Schedulesdt.forEach((dt) => {
      this._currentDate2Select = dt["_currentDate2Select"],
      this._clinicSelect = dt["_clinicSelect"],
      this._drSelect = dt["_drSelect"],
      this._Namacabang = dt["_Namacabang"],
      this._BranchCode = dt["_BranchCode"],
      this._cabang = dt["_cabang"],
      this._color1 = dt["_color1"],
      this._color2 = dt["_color2"],
      this._Selected_color = dt["_Selected_color"],
      this._idsp = dt["_idsp"],
      this._drimage = dt["_drimage"],
      this._drname = dt["_drname"],
      this._titlesp_id = dt["_titlesp_id"],
      this._titlesp_en = dt["_titlesp_en"],
      this._dridweb = dt["_dridweb"],
      this._secid = dt["_secid"],
      this._dridapi = dt["_dridapi"],
    });
  }

  @override
  _SchedulesState createState() => new _SchedulesState();
}


class _SchedulesState extends State<Schedules>{

  double cHeight;
  double cWidth;
  CalendarCarousel _calendarCarouselNoHeader;
  EventList<Event> _markedDateMap = new EventList<Event>();
  List<DateTime> _tglschedules = [];
  String _currentMonth = '';
  String _currentY = DateFormat.y().format(DateTime.now());
  String _currentM = DateFormat.M().format(DateTime.now());
  int _nowMy = int.parse(DateFormat.y().format(DateTime.now()).toString() + DateFormat.M().format(DateTime.now()).toString());
  int _curentMY = int.parse(DateFormat.y().format(DateTime.now()).toString() + DateFormat.M().format(DateTime.now()).toString());
  DateTime _currentDate = DateTime.now();
  DateTime _currentDate2 = DateTime.now();
  DateTime _maxSelectedDate = DateTime.now();
  var lastDayDateTime = DateTime.now();
  Color _flatleft =Colors.grey[600];
  String tgl;
  int d;
  int m;
  int y;
  List<String> tglSplit;
  var len = 0;
  bool select = false;
  final _Scafoltkey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    widget._activePage = true;
    super.initState();
  }

  @override
  void dispose(){
    widget._activePage = false;
    super.dispose();
  }

  int _inDays = 0;
  bool isloading = true;

  Future<String> _getData() async {
    var _data;
    var api = new Http_Controller();
    var uri = "https://suryahusadha.com/api/get_calendar?month="+_currentM+"&year="+_currentY+"&BranchCode="+widget._BranchCode+"&secid="+widget._secid+"&dokterId="+widget._dridapi;
    var uriencoded = Uri.encodeFull(uri);
    var http_response = await api.getResponse(uriencoded);
    _data = convert.jsonEncode(http_response);
    return _data;
  }
  Future _getAPI() async {
    if(isloading){
      if(widget._currentDate2Select != null){
        isloading = true;
        _currentDate2 = widget._currentDate2Select;
        widget._Selected_color = Colors.red[300];
        _currentMonth = DateFormat.yMMM().format(_currentDate2);
        _currentM = DateFormat.M().format(_currentDate2);
        _currentY = DateFormat.y().format(_currentDate2);
      }
      _curentMY = int.parse(_currentY + _currentM);
      if(_curentMY <= _nowMy){
        _flatleft = Colors.grey[100];
      }
      if(_tglschedules.isEmpty && _tglschedules.length == 0){
        _tglschedules.clear();
        if(_markedDateMap.events != null){
          _markedDateMap.events.clear();
        }
        lastDayDateTime = (_currentDate2.month < 12) ? new DateTime(_currentDate2.year, _currentDate2.month + 1, 0) : new DateTime(_currentDate2.year + 1, 1, 0);
        _getData().then((String response){
          setState(() {
            var decode = convert.jsonDecode(response);
            var decodeConvert = convert.jsonDecode(decode["responseBody"]);
            final dtkonten = decodeConvert["schedules"];
            var schedule = dtkonten[0]["data"];
            if(schedule.length > 0) {
              setState(() {
                schedule.forEach((dt) => {
                  tgl = dt["tgl"],
                  tglSplit = tgl.split("-"),
                  d = int.parse(tglSplit[0]),
                  m = int.parse(tglSplit[1]),
                  y = int.parse(tglSplit[2]),
                  _tglschedules.add(DateTime(y, m, d)),
                  if(!_tglschedules.isEmpty){
                    len = _tglschedules.length,
                    if(int.parse(y.toString()+""+m.toString()+""+d.toString()) >= _inDays){
                      _inDays = int.parse(y.toString()+""+m.toString()+""+d.toString()),
                      _maxSelectedDate = DateTime(y, m, d),
                    },
                  },
                });
                for (int i = 0; i < len; i++) {
                  if( DateFormat.yM().format(_currentDate).toString().compareTo(DateFormat.yM().format(DateTime(y, m, d)).toString()) == 0){
                    if(!select) {
                      setState(() {
                        widget._Selected_color = Colors.deepOrange.withOpacity(0.3);
                        _currentDate2 = _currentDate;
                      });
                    }
                  }
                  if(_currentDate.difference(DateTime(y, m, _tglschedules[i].day)).inDays <= 0){
                    _markedDateMap.add(
                      _tglschedules[i],
                      new Event(
                        date: _tglschedules[i],
                        title: _tglschedules[i].toString(),
                        icon: _schIcon(
                          _tglschedules[i].day.toString(),
                        ),
                      ),
                    );
                  };
                }
              });
            }else{
              isloading = false;
              if( DateFormat.yM().format(_currentDate).toString().compareTo(DateFormat.yM().format(_currentDate2).toString()) == 0){
                setState(() {
                  _currentDate2 = _currentDate;
                  widget._Selected_color = Colors.deepOrange.withOpacity(0.3);
                });
              }
            }
          });
        });
      };
    }
    return _tglschedules;
  }

  Widget _schIcon(String day) => Container(
    decoration: BoxDecoration(
      color: widget._color1,
      borderRadius: BorderRadius.all(
        Radius.circular(1000),
      ),
    ),
    child: Center(
      child: Text(
        day,
        style: TextStyle(
          color: Colors.white,
        ),
      ),
    ),
  );

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> dtNext=[];
    dtNext.add({
      "_clinicSelect": widget._clinicSelect,
      "_drSelect": widget._drSelect,
      "_Namacabang": widget._Namacabang,
      "_BranchCode": widget._BranchCode,
      "_cabang": widget._cabang,
      "_color1": widget._color1,
      "_color2": widget._color2,
      "_idsp": widget._idsp,
      "_titlesp_en": widget._titlesp_en,
      "_titlesp_id": widget._titlesp_id,
      "_secid": widget._secid,
    });
    RouteHelper(context, "Doctors", dtNext);
  }

  @override
  Widget build(BuildContext context) {
    cHeight = MediaQuery.of(context).size.height;
    cWidth = MediaQuery.of(context).size.width;
    return new SafeArea(
      child: new Scaffold(
        backgroundColor: Colors.white,
        key: _Scafoltkey,
        drawer: drawerController(widget._akun, widget._cabang, "Clinics"),
        appBar: new PreferredSize(
          preferredSize: Size.fromHeight(200.0),
          child: AppBarController(widget._cabang, _Scafoltkey, widget._akun, "Schedules"),
        ),
        body:WillPopScope(
          onWillPop: _onWillPop,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                _header_spc(),
                _header_sch(),
                new FutureBuilder(
                    future: _getAPI(),
                    builder: (context, snapshot) {
                      if(snapshot.hasData && _tglschedules.isNotEmpty && _tglschedules.length > 0){
                        isloading = false;
                        _calendarCarouselNoHeader = CalendarCarousel<Event>(
                          height: 330,
                          onDayPressed: (DateTime date, List<Event> events) {
                            setState(() {
                              if(_tglschedules.contains(date)){
                                select = true;
                                widget._activePage = true;
                                _currentDate2 = date;
                                widget._Selected_color = Colors.red[300];
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) => _pilihJadwal(date, context),
                                );
                              }
                            });
                          },
                          weekendTextStyle: TextStyle(
                            color: Colors.red,
                          ),
                          customGridViewPhysics: NeverScrollableScrollPhysics(),
                          minSelectedDate: _currentDate.subtract(Duration(days: 1)),
                          maxSelectedDate: _maxSelectedDate,
                          selectedDateTime: _currentDate2,
                          selectedDayButtonColor: widget._Selected_color,
                          todayButtonColor: Colors.deepOrange.withOpacity(0.3),
                          markedDatesMap: _markedDateMap,
                          markedDateShowIcon: true,
                          markedDateIconMaxShown: 1,
                          markedDateMoreShowTotal: null,
                          showHeader: false,
                          showOnlyCurrentMonthDate: true,
                          isScrollable: false,
                          markedDateIconBuilder: (event) {
                            return event.icon;
                          },
                          onCalendarChanged: (DateTime date) {
                            setState((){
                              _currentMonth = DateFormat.yMMM().format(date);
                              _currentM = DateFormat.M().format(date);
                              _currentY = DateFormat.y().format(date);
                            });
                          },
                        );
                        return _calendar();
                      }else{
                        if(isloading){
                          return _loading_calendar();
                        }else{
                          return _calendar();
                        }

                      }
                    }
                ),
                new Container(
                  padding: EdgeInsets.only(top: 16, bottom: 16, left: 24, right: 24),
                  decoration: new BoxDecoration(
                      border: Border(
                        top: BorderSide(
                            style: BorderStyle.solid,
                            width: 1.0,
                            color: Colors.grey[300]
                        ),
                        bottom: BorderSide(
                            style: BorderStyle.solid,
                            width: 1.0,
                            color: Colors.grey[300]
                        ),
                        right: BorderSide(
                            style: BorderStyle.solid,
                            width: 1.0,
                            color: Colors.grey[300]
                        ),
                        left: BorderSide(
                            style: BorderStyle.solid,
                            width: 1.0,
                            color: Colors.grey[300]
                        ),
                      ),
                      color: Colors.grey[100]
                  ),
                  child: new Column(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      markerRepresent(Colors.deepOrange.withOpacity(0.3), "Hari ini"),
                      markerRepresent(widget._color1, "Jadwal dokter Surya Husadha " +widget._Namacabang),
                      markerRepresent(Colors.red[300], "Jadwal dipilih"),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      )
    );
  }
  @override
  Future<String> _listJadwal() async {
    var _data;
    var api = new Http_Controller();
    String datesch = _currentDate2.year.toString()+"-"+_currentDate2.month.toString()+"-"+_currentDate2.day.toString();
    var uri = "https://suryahusadha.com/api/get_jadwal?BranchCode="+widget._BranchCode+"&secid="+widget._secid+"&dokterId="+widget._dridapi+"&datesch="+datesch+"&dridWeb="+widget._dridweb;
    var uriencoded = Uri.encodeFull(uri);
    var http_response = await api.getResponse(uriencoded);
    _data = convert.jsonEncode(http_response);
    return _data;
  }

  Widget _pilihJadwal(DateTime date, BuildContext context) {
    return new Stack(
      children: <Widget>[
        GestureDetector(
            onTap: ()=>{Navigator.of(context).pop()},
            child: Container(
              color: Colors.black.withOpacity(0.2),
            )
        ),
        Positioned(
          top: 48.0,
          left: 10.0,
          right: 10.0,
          child: Column(
            children: <Widget>[
              Card(
                color: widget._color1,
                elevation: 8.0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 10, bottom: 15),
                      child: new Container(
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Icon(
                                  FontAwesomeIcons.calendarCheck,
                                  color: Colors.white,
                                  size: 16,
                                ),
                                Container(width: 10,),
                                Container(
                                  padding: EdgeInsets.only(top: 5),
                                  child: new Text(
                                    "Jadwal Tgl: "+ date.day.toString()+"-"+DateFormat.MMM().format(date).toString()+"-"+date.year.toString(),
                                    style: new TextStyle(color: Colors.white, fontFamily: "NeoSansBold", fontSize: 16.0),
                                  ),
                                ),

                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    new FutureBuilder(
                      future: _listJadwal(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          var decode = convert.jsonDecode(snapshot.data);
                          var decodeConvert = convert.jsonDecode(decode["responseBody"]);
                          final dtkonten = decodeConvert["schlist"];
                          final schedule = dtkonten[0]["data"];
                          return new Container(
                            margin: EdgeInsets.only(left: 1, right: 1),
                            color: Colors.white,
                            child: new Column(
                              children: <Widget>[
                                new Visibility(
                                  child: new Container(
                                    decoration: new BoxDecoration(
                                        border: Border(
                                          right: BorderSide(
                                              style: BorderStyle.solid,
                                              width: 1.0,
                                              color: Colors.grey[200]
                                          ),
                                          left: BorderSide(
                                              style: BorderStyle.solid,
                                              width: 1.0,
                                              color: Colors.grey[200]
                                          ),
                                          top: BorderSide(
                                              style: BorderStyle.solid,
                                              width: 1.0,
                                              color: Colors.grey[300]
                                          ),
                                        ),
                                        color: Colors.white
                                    ),
                                    padding: EdgeInsets.only(top:10, bottom: 10, left: 16, right: 16),
                                    child:new Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.max,
                                      children: <Widget>[
                                        new Container(
                                          width: 80.0,
                                          height: 80.0,
                                          padding: EdgeInsets.all(1.0),
                                          decoration: new BoxDecoration(
                                              borderRadius: new BorderRadius.all(new Radius.circular(100.0)),
                                              color: Colors.grey[300]
                                          ),
                                          alignment: Alignment.center,
                                          child: CircleAvatar(
                                            backgroundImage: NetworkImage(widget._drimage),
                                            minRadius: 90,
                                            maxRadius: 150,
                                          ),
                                        ),
                                        new Expanded(
                                          flex: 2,
                                          child: new Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                new Container(
                                                  alignment: Alignment.topLeft,
                                                  padding: EdgeInsets.only(left:10.0, right:10.0),
                                                  child: new Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: <Widget>[
                                                        new Container(
                                                          alignment: Alignment.topLeft,
                                                          child:new Text(
                                                            widget._drname,
                                                            overflow: TextOverflow.ellipsis,
                                                            maxLines: 1,
                                                            softWrap: false,
                                                            style: TextStyle(color: Colors.grey[800], fontFamily: "NeoSansBold", fontSize: 14.0),
                                                            textAlign: TextAlign.left,
                                                          ),
                                                        ),
                                                        new Container(
                                                          alignment: Alignment.topLeft,
                                                          child: new Text(
                                                            widget._titlesp_id+" ("+widget._titlesp_en+")",
                                                            overflow: TextOverflow.ellipsis,
                                                            maxLines: 1,
                                                            softWrap: false,
                                                            style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                                          ),
                                                        ),
                                                      ]
                                                  ),
                                                ),
                                              ]
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                new Container(
                                  margin: EdgeInsets.only(left: 16, right: 16),
                                  width: double.infinity,
                                  height: 1.0,
                                  color: Colors.grey[200],
                                ),
                                new Container(
                                  padding: EdgeInsets.only(bottom: 10,left: 10, right: 10),
                                  color: Colors.grey[100],
                                  child: _jadwal(Sch: schedule),
                                ),
                              ],
                            ),
                          );
                        }else{
                          return new Container(
                              color: Colors.white,
                              padding: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Center(
                                    child: SizedBox(
                                        width: 80.0,
                                        height: 80.0,
                                        child: const CircularProgressIndicator(
                                          backgroundColor: Colors.grey,
                                        )),
                                  )
                                ],
                              )
                          );
                        }
                      },
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _jadwal({Sch}){
    List<Widget> row = new List<Widget>();
    Color ColorText1 = Colors.grey[800];
    Color ColorText2 = Colors.grey[600];
    Icon _iconRight = new Icon(
      FontAwesomeIcons.angleRight,
      color: Colors.grey[800],
      size: 18,
    );
    Sch.forEach((dt) => {
      if(dt["closed"].toString()=="1"){
        ColorText1 = Colors.grey[400],
        ColorText2 = Colors.grey[400],
        _iconRight = new Icon(
          FontAwesomeIcons.ban,
          color: Colors.red[200],
          size: 18,
        ),
      },
      row.add(new Container(
        color: Colors.white,
        margin: EdgeInsets.only(top: 5),
        child: new InkWell(
            onTap: (){
              var asgeriatri = true;
              if(dt["geriatri"] != ""){
                asgeriatri = false;
              }
              if(dt["closed"].toString()=="0"){
                setState((){
                  widget._activePage = false;
                });
                widget._currentDate2Select = _currentDate2;
                Navigator.of(context).pop();
                List<Map<String, dynamic>> dtNext=[];
                dtNext.add({
                  "_currentDate2Select": _currentDate2,
                  "_clinicSelect": widget._clinicSelect,
                  "_drSelect": widget._drSelect,
                  "_Namacabang": widget._Namacabang,
                  "_BranchCode": widget._BranchCode,
                  "_cabang": widget._cabang,
                  "_color1": widget._color1,
                  "_color2": widget._color2,
                  "_Selected_color": widget._Selected_color,
                  "_idsp": widget._idsp,
                  "_drimage": widget._drimage,
                  "_drname": widget._drname,
                  "_titlesp_id": widget._titlesp_id,
                  "_titlesp_en": widget._titlesp_en,
                  "_dridweb": widget._dridweb,
                  "_secid": widget._secid,
                  "_dridapi": widget._dridapi,
                  "_spc_id": dt["spc_id"],
                  "_spc_en": dt["spc_en"],
                  "_drname": dt["name"],
                  "_NamaDokterPengganti": dt["NamaDokterPengganti"],
                  "_DokterID": dt["DokterID"],
                  "_SectionID": dt["SectionID"],
                  "_Tanggal": dt["Tanggal"],
                  "_WaktuID": dt["WaktuID"],
                  "_Hari": dt["Hari"],
                  "_JmlAntrian": dt["JmlAntrian"],
                  "_Realisasi": dt["Realisasi"],
                  "_Cancel": dt["Cancel"],
                  "_DokterPenggantiID": dt["DokterPenggantiID"],
                  "_NoAntrianTerakhir": dt["NoAntrianTerakhir"],
                  "_SectionName": dt["SectionName"],
                  "_FromJam": dt["FromJam"],
                  "_ToJam": dt["ToJam"],
                  "_time": dt["FromJam"]+"-"+dt["ToJam"],
                  "_WaktuFromJam": dt["WaktuFromJam"],
                  "_WaktuToJam": dt["WaktuToJam"],
                  "_sch": dt["sch"],
                  "_asgeriatri": asgeriatri,
                  "_geriatri": _geriatri(dt["geriatri"], dt["closed"]),
                  "_BPJS": dt["BPJS"],
                  "_imgbpjs": dt["imgbpjs"],
                  "_bpjslogo": _bpjslogo(dt["imgbpjs"], dt["closed"]),
                  "_closed": dt["closed"],
                  "_statussch": dt["statussch"],
                  "_status_id": dt["status_id"],
                  "_status_en": dt["status_en"],
                });
                RouteHelper(context, "Formrsv", dtNext);
              };
            },
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  margin: EdgeInsets.only(top:10, bottom: 10),
                  child:new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      new Expanded(
                        flex: 2,
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                alignment: Alignment.topLeft,
                                child: new Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      new Container(
                                          alignment: Alignment.topLeft,
                                          child: new Row(
                                            children: <Widget>[
                                              new Icon(
                                                  FontAwesomeIcons.calendarAlt,
                                                  size: 14,
                                                  color: ColorText1
                                              ),
                                              new Container(
                                                width: 5,
                                              ),
                                              new Text(
                                                dt["sch"],
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 1,
                                                softWrap: false,
                                                style: TextStyle(fontFamily: "NeoSansBold", fontSize: 14.0, color: ColorText1),
                                                textAlign: TextAlign.left,
                                              ),
                                            ],
                                          )
                                      ),
                                      new Container(
                                          alignment: Alignment.topLeft,
                                          child: new Row(children: <Widget>[
                                            new Icon(
                                                FontAwesomeIcons.clock,
                                                size: 12,
                                                color: ColorText2
                                            ),
                                            new Container(
                                              width: 5,
                                            ),
                                            new Text(
                                              dt["FromJam"]+"-"+dt["ToJam"],
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                              softWrap: false,
                                              style: TextStyle(color: ColorText2, fontSize: 12.0),
                                              textAlign: TextAlign.left,
                                            ),
                                            _statussch(dt["statussch"]),
                                          ],)
                                      ),
                                      _geriatri(dt["geriatri"], dt["closed"]),
                                    ]
                                ),
                              ),
                            ]
                        ),
                      ),
                      _bpjslogo(dt["imgbpjs"], dt["closed"]),
                      new Container(
                        alignment: Alignment.centerRight,
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Container(
                                  alignment: Alignment.centerRight,
                                  child: _iconRight
                              ),
                            ]
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  width: double.infinity,
                  height: 1.0,
                  color: Colors.grey[200],
                ),
              ],
            )
        ),
      )
      )
    });

    return new Column(children: row);
  }

  Widget _statussch(String statussch){
    if(statussch!=""){
      return new Container(
        decoration: BoxDecoration(
            color: Colors.red[200],
            borderRadius: BorderRadius.circular(3.0)
        ),
        padding: EdgeInsets.only(left: 5, right: 5),
        margin: EdgeInsets.only(left: 10),
        child: new Text(
          statussch,
          style: TextStyle(color: Colors.white, fontSize: 10),
        ),
      );
    }else{
      return new Container();
    }
  }

  Widget _bpjslogo(String bpjs, int closed){
    if(bpjs != ""){
      if(closed.toString() == "1"){
        return new Container(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                new Container(
                  height: 60,
                  width: 70,
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new NetworkImage(bpjs),
                      fit: BoxFit.cover,
                      colorFilter: new ColorFilter.mode(Colors.white.withOpacity(0.2), BlendMode.dstATop),
                    ),
                  ),
                )
              ],
            )
        );
      }else{
        return new Container(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                new Container(
                  height: 60,
                  width: 80,
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new NetworkImage(bpjs),
                      fit: BoxFit.cover,
                    ),
                  ),
                )
              ],
            )
        );
      }
    }else{
      return new Container();
    }
  }
  Widget _geriatri(String geriatri, int closed){
    if(geriatri != ""){
      if(closed.toString() == "1"){
        return new Container(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                new Container(
                  height: 30,
                  width: 80,
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new NetworkImage(geriatri),
                      fit: BoxFit.cover,
                      colorFilter: new ColorFilter.mode(Colors.white.withOpacity(0.2), BlendMode.dstATop),
                    ),
                  ),
                )
              ],
            )
        );
      }else {
        return new Container(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                new Container(
                  height: 30,
                  width: 80,
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new NetworkImage(geriatri),
                      fit: BoxFit.cover,
                    ),
                  ),
                )
              ],
            )
        );
      }
    }else{
      return new Container();
    }
  }

  Widget _loading_calendar() {
    return new Container(
        padding: EdgeInsets.only(left: 24, right: 24),
        decoration: new BoxDecoration(
          border: Border(
              left: BorderSide(
                  style: BorderStyle.solid,
                  width: 1.0,
                  color: Colors.grey[200]
              ),
              right: BorderSide(
                  style: BorderStyle.solid,
                  width: 1.0,
                  color: Colors.grey[200]
              ),
              bottom: BorderSide(
                  style: BorderStyle.solid,
                  width: 1.0,
                  color: Colors.grey[200]
              )
          ),
          color: Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Row(
              children: <Widget>[
                Expanded(
                    child: Text(
                      _currentMonth,
                      style: TextStyle(
                        fontFamily: "NeoSansBold",
                        fontSize: 24.0,
                        color: Colors.grey[800],
                      ),
                    )),
                new Container(
                    width: 50,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        FlatButton(
                          child: Icon(
                            FontAwesomeIcons.chevronCircleLeft,
                            color: _flatleft,
                          ),
                          onPressed: () {
                            if(_curentMY > _nowMy){
                              setState(() {
                                select = false;
                                widget._currentDate2Select = null;
                                _currentDate2 = _currentDate2.subtract(Duration(days: lastDayDateTime.day));
                                _currentMonth = DateFormat.yMMM().format(_currentDate2);
                                _currentM = DateFormat.M().format(_currentDate2);
                                _currentY = DateFormat.y().format(_currentDate2);
                                isloading = true;
                                _tglschedules.clear();
                                if(_markedDateMap.events != null){
                                  _markedDateMap.events.clear();
                                }
                                widget._Selected_color = Colors.transparent;
                              });
                            }
                          },
                        ),
                      ],
                    )
                ),
                new Container(
                    width: 50,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        FlatButton(
                          child: Icon(
                            FontAwesomeIcons.chevronCircleRight,
                            color: Colors.grey[600],
                          ),
                          onPressed: () {
                            setState(() {
                              select = false;
                              widget._currentDate2Select = null;
                              _flatleft = Colors.grey[600];
                              _currentDate2 = _currentDate2.add(Duration(days: lastDayDateTime.day));
                              _currentMonth = DateFormat.yMMM().format(_currentDate2);
                              _currentM = DateFormat.M().format(_currentDate2);
                              _currentY = DateFormat.y().format(_currentDate2);
                              isloading = true;
                              _tglschedules.clear();
                              if(_markedDateMap.events != null){
                                _markedDateMap.events.clear();
                              }
                              widget._Selected_color = Colors.transparent;
                            });
                          },
                        )
                      ],
                    )
                ),
              ],
            ),
            new Container(
              width: double.infinity,
              height: 1.0,
              color: Colors.grey[200],
            ),
            new Container(
              height: 346,
              child: Center(
                child: SizedBox(
                    width: 80.0,
                    height: 80.0,
                    child: const CircularProgressIndicator(
                      backgroundColor: Colors.grey,
                    )),
              ),
            ),
          ],
        )
    );
  }

  Widget _calendar() {
    return new Container(
        padding: EdgeInsets.only(left: 24, right: 24),
        decoration: new BoxDecoration(
          border: Border(
              left: BorderSide(
                  style: BorderStyle.solid,
                  width: 1.0,
                  color: Colors.grey[200]
              ),
              right: BorderSide(
                  style: BorderStyle.solid,
                  width: 1.0,
                  color: Colors.grey[200]
              ),
          ),
          color: Colors.white,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            new Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Row(
                    children: <Widget>[
                      Expanded(
                          child: Text(
                            _currentMonth,
                            style: TextStyle(
                              fontFamily: "NeoSansBold",
                              fontSize: 24.0,
                              color: Colors.grey[800],
                            ),
                          )),
                      new Container(
                          width: 50,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              FlatButton(
                                child: Icon(
                                  FontAwesomeIcons.chevronCircleLeft,
                                  color: _flatleft,
                                ),
                                onPressed: () {
                                  //if(_currentDate2.difference(DateTime.now()).inDays > 0){
                                  if(_curentMY > _nowMy){
                                    setState(() {
                                      select = false;
                                      widget._currentDate2Select = null;
                                      _currentDate2 = _currentDate2.subtract(Duration(days: lastDayDateTime.day));
                                      _currentMonth = DateFormat.yMMM().format(_currentDate2);
                                      _currentM = DateFormat.M().format(_currentDate2);
                                      _currentY = DateFormat.y().format(_currentDate2);
                                      isloading = true;
                                      _tglschedules.clear();
                                      if(_markedDateMap.events != null){
                                        _markedDateMap.events.clear();
                                      }
                                      widget._Selected_color = Colors.transparent;
                                    });
                                  }
                                },
                              ),
                            ],
                          )
                      ),
                      new Container(
                          width: 50,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              FlatButton(
                                child: Icon(
                                  FontAwesomeIcons.chevronCircleRight,
                                  color: Colors.grey[600],
                                ),
                                onPressed: () {
                                  setState(() {
                                    select = false;
                                    _flatleft = Colors.grey[600];
                                    widget._currentDate2Select = null;
                                    _currentDate2 = _currentDate2.add(Duration(days: lastDayDateTime.day));
                                    _currentMonth = DateFormat.yMMM().format(_currentDate2);
                                    _currentM = DateFormat.M().format(_currentDate2);
                                    _currentY = DateFormat.y().format(_currentDate2);
                                    isloading = true;
                                    _tglschedules.clear();
                                    if(_markedDateMap.events != null){
                                      _markedDateMap.events.clear();
                                    }
                                    widget._Selected_color = Colors.transparent;
                                  });
                                },
                              )
                            ],
                          )
                      ),
                    ],
                  ),
                  new Container(
                    margin: EdgeInsets.only(bottom: 16),
                    width: double.infinity,
                    height: 1.0,
                    color: Colors.grey[200],
                  ),
                  _calendarCarouselNoHeader,
                ],),
            ),
          ],
        )
    );
  }

  Widget markerRepresent(Color color, String data) {
    return new Container(
      margin: EdgeInsets.only(bottom: 2),
      child: Row(children: <Widget>[
        new CircleAvatar(
          backgroundColor: color,
          radius: 8,
        ),
        new Container(
          width: 5,
        ),
        new Text(
          data,
          style: TextStyle(color: Colors.grey[600], fontSize: 12),
        ),
      ]),
    );
  }

  Widget _header_spc(){
    return new Container(
        //margin: EdgeInsets.only(bottom: 5),
        color: Colors.white,
        child: new Column(
          children: <Widget>[
            new Visibility(
              child: new Container(
                padding: EdgeInsets.only(top:10, bottom: 10, left: 16, right: 16),
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Container(
                      width: 50.0,
                      height: 50.0,
                      padding: EdgeInsets.all(8.0),
                      decoration: new BoxDecoration(
                          borderRadius: new BorderRadius.all(new Radius.circular(100.0)),
                          color: Colors.grey[600]
                      ),
                      alignment: Alignment.center,
                      child: new Icon(
                        FontAwesomeIcons.calendarAlt,
                        color: Colors.white,
                        size: 24.0,
                      ),
                    ),
                    new Expanded(
                      flex: 2,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(left:10.0, right:10.0),
                              child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child:new Text(
                                        "Reservasi "+widget._Namacabang,
                                        style: TextStyle(fontFamily: "NeoSansBold", fontSize: 18.0),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Pilih Jadwal Praktek Dokter",
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                        style: TextStyle(color: Colors.grey, fontSize: 14.0),
                                      ),
                                    ),
                                  ]
                              ),
                            ),
                          ]
                      ),
                    ),
                  ],
                ),
              ),
            ),
            new Container(
              margin: EdgeInsets.only(top: 5.0),
              width: double.infinity,
              height: 1.0,
              color: Colors.grey[200],
            ),
          ],
        )
    );
  }
  Widget _header_sch(){
    return new Container(
        child: new Column(
          children: <Widget>[
            new Container(
              padding: EdgeInsets.only(top: 16, bottom: 16),
              decoration: new BoxDecoration(
                  border: Border(
                    top: BorderSide(
                        style: BorderStyle.solid,
                        width: 1.0,
                        color: Colors.grey[300]
                    ),
                    right: BorderSide(
                        style: BorderStyle.solid,
                        width: 1.0,
                        color: Colors.grey[300]
                    ),
                    left: BorderSide(
                        style: BorderStyle.solid,
                        width: 1.0,
                        color: Colors.grey[300]
                    ),
                  ),
                  color: Colors.grey[100]
              ),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Icon(
                            FontAwesomeIcons.userMd,
                            color: Colors.grey[800],
                            size: 12,
                          )
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Text(
                            " Jadwal Praktek Dokter",
                            style: TextStyle(
                              color: Colors.grey[800],
                              fontSize: 14,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            new Visibility(
              child: new Container(
                decoration: new BoxDecoration(
                    border: Border(
                      right: BorderSide(
                          style: BorderStyle.solid,
                          width: 1.0,
                          color: Colors.grey[200]
                      ),
                      left: BorderSide(
                          style: BorderStyle.solid,
                          width: 1.0,
                          color: Colors.grey[200]
                      ),
                      top: BorderSide(
                          style: BorderStyle.solid,
                          width: 1.0,
                          color: Colors.grey[300]
                      ),
                    ),
                    color: Colors.white
                ),
                padding: EdgeInsets.only(top:16, bottom: 16, left: 24, right: 24),
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Container(
                      width: 80.0,
                      height: 80.0,
                      padding: EdgeInsets.all(1.0),
                      decoration: new BoxDecoration(
                          borderRadius: new BorderRadius.all(new Radius.circular(100.0)),
                          color: Colors.grey[300]
                      ),
                      alignment: Alignment.center,
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(widget._drimage),
                        minRadius: 90,
                        maxRadius: 150,
                      ),
                    ),
                    new Expanded(
                      flex: 2,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(left:10.0, right:10.0),
                              child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child:new Text(
                                        widget._drname,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                        style: TextStyle(color: Colors.grey[800], fontFamily: "NeoSansBold", fontSize: 14.0),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        widget._titlesp_id+" ("+widget._titlesp_en+")",
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                      ),
                                    ),
                                  ]
                              ),
                            ),
                          ]
                      ),
                    ),
                  ],
                ),
              ),
            ),
            new Container(
              margin: EdgeInsets.only(left: 16, right: 16),
              width: double.infinity,
              height: 1.0,
              color: Colors.grey[200],
            ),
          ],
        )
    );
  }
}