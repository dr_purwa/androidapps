import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:surya_husadha/components/com_reservasi/reservasi_models.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/dialogue/customDialogue.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:convert' as convert;

class Clinics extends StatefulWidget {

  int _clinicSelect;
  String _cabang;
  String _Namacabang;
  String _BranchCode;

  var _akun;

  Color _color1;
  Color _color2;
  Color _Selected_color;

  Clinics(List params, akun){
    this._akun = akun;
    params.forEach((dt){
      this._clinicSelect = dt["_clinicSelect"];
      this._cabang = dt["_cabang"];
      if(_cabang=="denpasar"){
        this._Namacabang = "Denpasar";
        this._BranchCode = "SHH";
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
        this._Selected_color= WarnaCabang.shh2.withOpacity(0.3);
      }
      if(_cabang=="ubung"){
        this._Namacabang = "Ubung";
        this._BranchCode = "UB";
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
        this._Selected_color= WarnaCabang.ubung2.withOpacity(0.3);
      }
      if(_cabang=="nusadua"){
        this._Namacabang = "Nusadua";
        this._BranchCode = "ND";
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
        this._Selected_color= WarnaCabang.nusadua2.withOpacity(0.3);
      }
      if(_cabang=="kmc"){
        this._Namacabang = "kmc";
        this._BranchCode = "KMC";
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
        this._Selected_color= WarnaCabang.kmc2.withOpacity(0.3);
      }
    });
  }

  @override
  _ClinicsState createState() => new _ClinicsState();

}

class _ClinicsState extends State<Clinics> {
  String filter;
  int _active;
  int _needrates;
  bool focused=false;
  bool viewsearch=false;
  bool viewheader=true;
  List<Clinics_mdl> _mClinics_mdl = [];
  FocusNode _search  = FocusNode();
  TextEditingController editingController = new TextEditingController();
  final _Scafoltkey = GlobalKey<ScaffoldState>();
  bool isloading = true;
  List showitem = [];
  int showitemCount = 0;
  ScrollController listviewCon;

  @override
  initState() {
    super.initState();
    _active = widget._clinicSelect;
    editingController.addListener(() {
        filter = editingController.text;
    });
    fetchList().then((String response){
        var decode = convert.jsonDecode(response);
        var decodeConvert = convert.jsonDecode(decode["responseBody"]);
        final dtkonten = decodeConvert["rsvlist"];
        var rsvlist = dtkonten[0]["data"];
        _needrates = int.parse(rsvlist[0]["count"].toString());
    }).whenComplete((){
      if(_needrates > 0){
        showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) => showNotif(context),
        );
      }
    });
  }

  Widget showNotif(BuildContext context){
    List<Map<String, dynamic>> params = [];
    params.add({
      "_email": widget._akun[0]["email"].toString(),
      "_bod": widget._akun[0]["bod"].toString(),
      "_cabang": widget._cabang.toString(),
    });
    return Dialogue(
        context,
        "Untuk melanjutkan reservasi online, silahkan berikan rating pelayanan kami beserta kritik dan saran pada List Reservasi Anda sebeumnya!",
        Colors.redAccent, widget._color2,
        "ReservasiListPop", params);
  }

  @override
  Future<String> fetchList() async {
      var api = new Http_Controller();
      List<Map<String, dynamic>> dtNext = [];
      dtNext.add({
        "email": widget._akun[0]["email"].toString(),
        "bod": widget._akun[0]["bod"].toString(),
      });
      var http_response = await api.postResponse("https://suryahusadha.com/api/rsvlist/count/", dtNext);
      var _data = convert.jsonEncode(http_response);
      return _data;
  }

  @override
  void dispose() {
    editingController.removeListener((){
      filter = editingController.text;
    });
    super.dispose();
  }

  Future<String> _getData() async {
    var _data;
    var db = new Http_Controller();
    var http_response = await db.getResponse("https://suryahusadha.com/api/clinics");
    _data = convert.jsonEncode(http_response);
    return _data;
  }

  final AsyncMemoizer _memoizer = AsyncMemoizer();
  Future<List<Clinics_mdl>> fetchClinic() async {
    if(isloading) {
      this._memoizer.runOnce(() async {
        _getData().then((String response){
          setState(() {
            var decode = convert.jsonDecode(response);
            var decodeConvert = convert.jsonDecode(decode["responseBody"]);
            final dtkonten = decodeConvert["clinics"];
            final clinic = dtkonten[0]["data"];
            if(clinic.length > 0) {
              isloading = false;
              clinic.forEach((dt) => {
                _mClinics_mdl.add(new Clinics_mdl(
                  idsp: dt["idsp"],
                  titleen: dt["titlesp_en"],
                  titleid: dt["titlesp_id"],
                  secid: dt["secid"],
                  secid_ub: dt["secid_ub"],
                  secid_nd: dt["secid_nd"],
                )),
              });
            }else{
              isloading = false;
            }
          });
        });
      });
    }
    return _mClinics_mdl;
  }

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> dtNext=[];
    dtNext.add({"_cabang": widget._cabang});
    RouteHelper(context, widget._cabang, dtNext);
  }

  @override
  Widget build(BuildContext context) {

    if(viewsearch && focused){
      setState(() {
        FocusScope.of(context).requestFocus(_search);
      });
    }else{
      setState(() {
        FocusScope.of(context).requestFocus(FocusNode());
      });
    }

    return new SafeArea(
      child: new Scaffold(
        key: _Scafoltkey,
        drawer: drawerController(widget._akun, widget._cabang, "Clinics"),
        appBar: new PreferredSize(
          preferredSize: Size.fromHeight(200.0),
          child: AppBarController(widget._cabang, _Scafoltkey, widget._akun, "Clinics"),
        ),
        body: new WillPopScope(
          onWillPop: _onWillPop,
          child: new Column(
            children: <Widget>[
              _header_spc(),
              new FutureBuilder(
                future: fetchClinic(),
                builder: (context, snapshot) {
                  if (snapshot.hasData && _mClinics_mdl.length > 0 && _mClinics_mdl.isNotEmpty){
                    showitem.clear();
                    return new Expanded(
                      child: ListView.builder(
                          controller: listviewCon,
                          itemCount: snapshot.data.length,
                          itemBuilder:(BuildContext context, int index) {
                            if(filter == null || filter == ""){
                              showitemCount = _mClinics_mdl.length;
                              return rowlist(index);
                            }else{
                              if(_mClinics_mdl.elementAt(index).titleid.toLowerCase().contains(filter.toLowerCase())){
                                showitem.add(index);
                                showitemCount = showitem.length;
                                return rowlist(index);
                              }else{
                                showitemCount = 0;
                                return new Container();
                              }
                            }
                          }
                      ),
                    );
                  }else{
                    if(isloading){
                      return Container(
                        margin: EdgeInsets.only(top: 40),
                        child: Column(
                          children: <Widget>[
                            Center(
                              child: SizedBox(
                                  width: 80.0,
                                  height: 80.0,
                                  child: const CircularProgressIndicator(
                                    backgroundColor: Colors.grey,
                                  )),
                            )
                          ],
                        ),
                      );
                    }else{
                      return Container(
                        margin: EdgeInsets.only(top: 40),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Text("Data tidak ditemukan!")
                          ],
                        ),
                      );
                    }
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _header_spc(){
    return new Container(
        margin: EdgeInsets.only(bottom: 5),
        color: Colors.white,
        child: new Column(
          children: <Widget>[
            new Visibility(
              visible: viewsearch,
              child: new Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: TextField(
                  textInputAction: TextInputAction.done,
                  onTap: ()=> setState((){
                    _focused(true);
                  }),//
                  onEditingComplete: () => setState((){
                    _focused(false);
                  }),//
                  focusNode: _search,
                  controller: editingController,
                  cursorColor: Colors.grey,
                  style: TextStyle(color: Colors.grey,fontSize: 18),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(top: 12),
                    hintText: "Cari Klinik",
                    border: InputBorder.none,
                    labelStyle: TextStyle(
                      color: Colors.grey,
                    ),
                    prefixIcon: Icon(
                      FontAwesomeIcons.search,
                      color: Colors.grey,
                    ),
                    /*suffix: new Container(
                      padding: EdgeInsets.only(left: 10, right: 10, top: 3, bottom: 3),
                      decoration: BoxDecoration(
                          color: Colors.redAccent,
                          borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: Column(
                        children: <Widget>[
                          new Text(
                            filter.length.toString(),
                            style: TextStyle(color: Colors.white, fontSize: 12),
                          )
                        ],
                      ),
                    ),*/
                    suffixIcon: IconButton(
                      onPressed: ()=> setState((){
                        headerChange(true, false);
                        _focused(false);
                        editingController.clear();
                      }),
                      icon: Icon(
                        FontAwesomeIcons.times,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            new Visibility(
              visible: viewheader,
              child: new Container(
                padding: EdgeInsets.only(top:10, bottom: 10, left: 16, right: 16),
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Container(
                      width: 50.0,
                      height: 50.0,
                      padding: EdgeInsets.all(8.0),
                      decoration: new BoxDecoration(
                          borderRadius: new BorderRadius.all(new Radius.circular(100.0)),
                          color: Colors.grey[600]
                      ),
                      alignment: Alignment.center,
                      child: new Icon(
                        FontAwesomeIcons.medkit,
                        color: Colors.white,
                        size: 24.0,
                      ),
                    ),
                    new Expanded(
                      flex: 2,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(left:10.0, right:10.0),
                              child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child:new Text(
                                        "Reservasi "+widget._cabang,
                                        style: TextStyle(fontFamily: "NeoSansBold", fontSize: 18.0),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    new Row(
                                      children: <Widget>[
                                        new Container(
                                          child: new Row(
                                            children: <Widget>[
                                              new Container(
                                                alignment: Alignment.topLeft,
                                                child: new Text(
                                                  "Pilih klinik tujuan, dari "+_mClinics_mdl.length.toString()+" klinik!",
                                                  style: TextStyle(color: Colors.grey, fontSize: 14.0),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ]
                              ),
                            ),
                          ]
                      ),
                    ),
                    new Container(
                      child: new GestureDetector(
                          child: new Container(
                            height: 40.0,
                            width: 40.0,
                            alignment: Alignment.centerRight,
                            child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Container(
                                    padding: EdgeInsets.all(6.0),
                                    alignment: Alignment.centerRight,
                                    child: new Icon(
                                      FontAwesomeIcons.search,
                                      color: Colors.grey,
                                      size: 24.0,
                                    ),
                                  ),
                                ]
                            ),
                          ),
                          onTap: ()=> setState((){
                            headerChange(false, true);
                            _focused(true);
                          }),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            new Container(
              margin: EdgeInsets.only(top: 5.0),
              width: double.infinity,
              height: 1.0,
              color: Colors.grey[200],
            ),
          ],
        )
    );
  }

  Widget rowlist(int index){
    Color ActivColor;
    if(_active == index){
      ActivColor = widget._Selected_color;
    }else{
      ActivColor = Colors.white;
    }
    return new Container(
        margin: EdgeInsets.only(bottom: 5),
        color: ActivColor,
        child: new InkWell(
            onTap: (){
              setState(() {
                _active = index;
              });
              List<Map<String, dynamic>> dtNext=[];
              String _SectionId = "";
              if(widget._BranchCode == "SHH"){
                _SectionId = _mClinics_mdl.elementAt(index).secid;
              }
              if(widget._BranchCode == "UB"){
                _SectionId = _mClinics_mdl.elementAt(index).secid_ub;
              }
              if(widget._BranchCode == "ND"){
                _SectionId = _mClinics_mdl.elementAt(index).secid_nd;
              }
              dtNext.add({
                "_clinicSelect": index,
                "_Namacabang": widget._Namacabang,
                "_BranchCode": widget._BranchCode,
                "_cabang": widget._cabang,
                "_color1": widget._color1,
                "_color2": widget._color2,
                "_Selected_color": widget._Selected_color,
                "_idsp": _mClinics_mdl.elementAt(index).idsp,
                "_titlesp_id": _mClinics_mdl.elementAt(index).titleid,
                "_titlesp_en": _mClinics_mdl.elementAt(index).titleen,
                "_secid": _SectionId,
              });
              RouteHelper(context, "Doctors", dtNext);
            },
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(top:5, bottom: 5, left: 16, right: 16),
                  child:new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      new Container(
                        width: 40.0,
                        height: 40.0,
                        alignment: Alignment.center,
                        child: new Icon(
                          FontAwesomeIcons.handPointRight,
                          color: Colors.grey[600],
                          size: 24.0,
                        ),
                      ),
                      new Expanded(
                        flex: 2,
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                alignment: Alignment.topLeft,
                                padding: EdgeInsets.only(left:10.0, right:10.0),
                                child: new Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      new Container(
                                        alignment: Alignment.topLeft,
                                        child:new Text(
                                          _mClinics_mdl.elementAt(index).titleid,
                                          style: TextStyle(fontFamily: "NeoSansBold", fontSize: 14.0, color: Colors.grey[800]),
                                          textAlign: TextAlign.left,
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 1,
                                          softWrap: false,
                                        ),
                                      ),
                                      new Container(
                                        alignment: Alignment.topLeft,
                                        child: new Text(
                                          _mClinics_mdl.elementAt(index).titleen,
                                          style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                          textAlign: TextAlign.left,
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 1,
                                          softWrap: false,
                                        ),
                                      ),
                                    ]
                                ),
                              ),
                            ]
                        ),
                      ),
                      new Container(
                        height: 40.0,
                        width: 40.0,
                        alignment: Alignment.centerRight,
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Container(
                                padding: EdgeInsets.all(6.0),
                                alignment: Alignment.centerRight,
                                child: new Icon(
                                  FontAwesomeIcons.angleRight,
                                  color: Colors.grey,
                                  size: 18.0,
                                ),
                              ),
                            ]
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  margin: EdgeInsets.only(top: 5.0),
                  width: double.infinity,
                  height: 1.0,
                  color: Colors.grey[200],
                ),
              ],
            )
        ),
    );
  }

  _focused(bool focus){
    focused=focus;
  }

  headerChange(bool header, bool search) {
    viewsearch=search;
    viewheader=header;
  }
}
