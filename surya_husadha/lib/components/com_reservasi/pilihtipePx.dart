import 'dart:async';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:surya_husadha/components/com_reservasi/reservasi_models.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:convert' as convert;

class tipePx extends StatefulWidget {
  bool _pxBaru;
  String _rm;
  String _namapx;
  String _gender;
  String _tgllahir;
  String _email;
  String _mobile;
  String _alamat;
  String _bpjs_id;
  int _selectedTipepx;
  String _typepx;
  String _typepx_id;
  String _typepx_en;
  String _nobpjs;
  String _norujukan;
  String _note;
  DateTime _currentDate2Select;
  int _clinicSelect;
  int _drSelect;
  String _Namacabang;
  String _BranchCode;
  String _cabang;
  Color _color1;
  Color _color2;
  Color _Selected_color;
  String _idsp;
  String _drimage;
  String _secid;
  String _dridapi;
  String _spc_id;
  String _spc_en;
  String _drname;
  String _titlesp_id;
  String _titlesp_en;
  String _dridweb;
  String _NamaDokterPengganti;
  String _DokterID;
  String _SectionID;
  String _Tanggal;
  int _WaktuID;
  String _Hari;
  int _JmlAntrian;
  bool _Realisasi;
  bool _Cancel;
  String _DokterPenggantiID;
  int _NoAntrianTerakhir;
  String _SectionName;
  String _FromJam;
  String _ToJam;
  String _time;
  String _WaktuFromJam;
  String _WaktuToJam;
  String _sch;
  bool _asgeriatri;
  Widget _geriatri;
  String _BPJS;
  String _imgbpjs;
  Widget _bpjslogo;
  int _closed;
  String _statussch;
  String _status_id;
  String _status_en;
  var _akun;

  tipePx(List tipepx, akun){
    this._akun = akun;
    tipepx.forEach((dt) => {
      this._pxBaru=dt["_pxBaru"],
      this._rm=dt["_rm"],
      this._namapx=dt["_namapx"],
      this._gender=dt["_gender"],
      this._tgllahir=dt["_tgllahir"],
      this._email=dt["_email"],
      this._mobile=dt["_mobile"],
      this._alamat=dt["_alamat"],
      this._bpjs_id=dt["_bpjs_id"],
      print("_bpjs_id: "+dt["_bpjs_id"].toString()),
      this._selectedTipepx= dt["_selectedTipepx"],
      this._typepx= dt["_typepx"],
      this._typepx_id= dt["_typepx_id"],
      this._typepx_en= dt["_typepx_en"],
      this._nobpjs= dt["_nobpjs"],
      this._norujukan=dt["_norujukan"],
      this._note=dt["_note"],
      this._currentDate2Select = dt["_currentDate2Select"],
      this._clinicSelect = dt["_clinicSelect"],
      this._drSelect = dt["_drSelect"],
      this._Namacabang = dt["_Namacabang"],
      this._BranchCode = dt["_BranchCode"],
      this._cabang = dt["_cabang"],
      this._color1 = dt["_color1"],
      this._color2 = dt["_color2"],
      this._Selected_color = dt["_Selected_color"],
      this._idsp = dt["_idsp"],
      this._drimage = dt["_drimage"],
      this._drname = dt["_drname"],
      this._titlesp_id = dt["_titlesp_id"],
      this._titlesp_en = dt["_titlesp_en"],
      this._dridweb = dt["_dridweb"],
      this._secid = dt["_secid"],
      this._dridapi = dt["_dridapi"],
      this._spc_id=dt["_spc_id"],
      this._spc_en=dt["_spc_en"],
      this._NamaDokterPengganti=dt["_NamaDokterPengganti"],
      this._DokterID=dt["_DokterID"],
      this._SectionID=dt["_SectionID"],
      this._Tanggal=dt["_Tanggal"],
      this._WaktuID=dt["_WaktuID"],
      this._Hari=dt["_Hari"],
      this._JmlAntrian=dt["_JmlAntrian"],
      this._Realisasi=dt["_Realisasi"],
      this._Cancel=dt["_Cancel"],
      this._DokterPenggantiID=dt["_DokterPenggantiID"],
      this._NoAntrianTerakhir=dt["_NoAntrianTerakhir"],
      this._SectionName=dt["_SectionName"],
      this._FromJam=dt["_FromJam"],
      this._ToJam=dt["_ToJam"],
      this._time=dt["_time"],
      this._WaktuFromJam=dt["_WaktuFromJam"],
      this._WaktuToJam=dt["_WaktuToJam"],
      this._sch=dt["_sch"],
      this._asgeriatri=dt["_asgeriatri"],
      this._geriatri=dt["_geriatri"],
      this._BPJS=dt["_BPJS"],
      this._imgbpjs=dt["_imgbpjs"],
      this._bpjslogo=dt["_bpjslogo"],
      this._closed=dt["_closed"],
      this._statussch=dt["_statussch"],
      this._status_id=dt["_status_id"],
      this._status_en = dt["_status_en"],
    });
  }

  @override
  _tipePxController createState() => new _tipePxController();
}

class _tipePxController extends State<tipePx> with WidgetsBindingObserver{

  Color _fillColor = Colors.white.withOpacity(0.6);
  ScrollController listScrollController = ScrollController();
  final _Scafoltkey = GlobalKey<ScaffoldState>();

  bool bpjsView = false;

  //_note
  FocusNode _note = new FocusNode();
  TextEditingController _noteController = new TextEditingController();
  //_noRujukan
  bool _noRujukanNull = false;
  FocusNode _noRujukan = new FocusNode();
  TextEditingController _noRujukanController = new TextEditingController();
  //_nobpjs
  bool _nobpjsNull = false;
  FocusNode _nobpjs = new FocusNode();
  TextEditingController _nobpjsController = new TextEditingController();
  //_tipepx
  bool _tipepxEna = false;
  bool _tipepxNull = false;
  TipePX selectedTipepx;
  List<TipePX> _mTipePX = [];

  double paddingtop;
  bool visibleDesc = true;
  double maxheight;
  String msg;

  @override
  Future<String> _get_tipepx() async {
    var _data = null;
    if(!_tipepxEna) {
      var db = new Http_Controller();
      var http_response = await db.getResponse("https://suryahusadha.com/api/get_tipepx?BranchCode=" + widget._BranchCode + "&Section=" + widget._secid + "&BPJS=" + widget._BPJS);
      _data = convert.jsonEncode(http_response);
      //print("https://suryahusadha.com/api/get_tipepx?BranchCode=" + widget._BranchCode + "&Section=" + widget._secid + "&BPJS=" + widget._BPJS);
    }
    return _data;
  }

  @override
  void initState(){
    if(widget._bpjs_id != null){
      _nobpjsController.text = widget._bpjs_id;
    }
    paddingtop = 0;
    _mTipePX.add(const TipePX(0, "", "", "Pilih Tipe Pasien", "Pilih Tipe Pasien"));
    selectedTipepx=_mTipePX[0];
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose(){
    WidgetsBinding.instance.removeObserver(this);
    listScrollController.dispose();
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    final value = WidgetsBinding.instance.window.viewInsets.bottom;
    if (value > 0) {
      _onKeyboardChanged(true);
    } else {
      _onKeyboardChanged(false);
    }
  }

  _onKeyboardChanged(bool isVisible){
    if (isVisible) {
      setState(() {
        paddingtop = 0;
        visibleDesc = false;
      });
    } else {
      setState(() {
        visibleDesc = true;
        if(!bpjsView){
          paddingtop = maxheight * (1/5.3);
          if(paddingtop < 0){
            paddingtop = 0;
          }
        }else{
          paddingtop = 20;
        }
      });
      FocusScope.of(context).requestFocus(new FocusNode());
      listScrollController.jumpTo(listScrollController.position.minScrollExtent);
    }
  }

  @override
  void didChangeDependencies() {
    setState((){
      maxheight = MediaQuery.of(context).size.height;
      if(!bpjsView){
        paddingtop = maxheight * (1/5.3);
        if(paddingtop < 0){
          paddingtop = 0;
        }
      }else{
        paddingtop = 20;
      }
    });
    super.didChangeDependencies();
  }

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> dtNext=[];
    dtNext.add({
      "_pxBaru": widget._pxBaru,
      "_rm": widget._rm,
      "_namapx": widget._namapx,
      "_gender": widget._gender,
      "_tgllahir":widget._tgllahir,
      "_email": widget._email,
      "_mobile": widget._mobile,
      "_alamat": widget._alamat,
      "_bpjs_id": widget._bpjs_id,
      "_currentDate2Select": widget._currentDate2Select,
      "_clinicSelect": widget._clinicSelect,
      "_drSelect": widget._drSelect,
      "_Namacabang": widget._Namacabang,
      "_BranchCode": widget._BranchCode,
      "_cabang": widget._cabang,
      "_color1": widget._color1,
      "_color2": widget._color2,
      "_Selected_color": widget._Selected_color,
      "_idsp": widget._idsp,
      "_drimage": widget._drimage,
      "_drname": widget._drname,
      "_titlesp_id": widget._titlesp_id,
      "_titlesp_en": widget._titlesp_en,
      "_dridweb": widget._dridweb,
      "_secid": widget._secid,
      "_dridapi": widget._dridapi,
      "_spc_id": widget._spc_id,
      "_spc_en": widget._spc_en,
      "_drname": widget._drname,
      "_NamaDokterPengganti": widget._NamaDokterPengganti,
      "_DokterID": widget._DokterID,
      "_SectionID": widget._SectionID,
      "_Tanggal": widget._Tanggal,
      "_WaktuID": widget._WaktuID,
      "_Hari": widget._Hari,
      "_JmlAntrian": widget._JmlAntrian,
      "_Realisasi": widget._Realisasi,
      "_Cancel": widget._Cancel,
      "_DokterPenggantiID": widget._DokterPenggantiID,
      "_NoAntrianTerakhir": widget._NoAntrianTerakhir,
      "_SectionName": widget._SectionName,
      "_FromJam": widget._FromJam,
      "_ToJam": widget._ToJam,
      "_time": widget._FromJam+"-"+widget._ToJam,
      "_WaktuFromJam": widget._WaktuFromJam,
      "_WaktuToJam": widget._WaktuToJam,
      "_sch": widget._sch,
      "_asgeriatri": widget._asgeriatri,
      "_geriatri": widget._geriatri,
      "_BPJS": widget._BPJS,
      "_imgbpjs": widget._imgbpjs,
      "_bpjslogo": widget._bpjslogo,
      "_closed": widget._closed,
      "_statussch": widget._statussch,
      "_status_id": widget._status_id,
      "_status_en": widget._status_en,
    });
    RouteHelper(context, "Formrsv", dtNext);
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(
        child: new Scaffold(
            key: _Scafoltkey,
            drawer: drawerController(widget._akun, widget._cabang,"Clinics"),
            appBar: new PreferredSize(
              preferredSize: Size.fromHeight(200.0),
              child: AppBarController(widget._cabang, _Scafoltkey, widget._akun,"tipePx"),
            ),
            body:new WillPopScope(
                onWillPop: _onWillPop,
                child: new FutureBuilder(
                    future: _get_tipepx(),
                    builder: (context, snapshot){
                      if(snapshot.hasData && (snapshot.connectionState == ConnectionState.done)) {
                        _tipepxEna = true;
                        var decode = convert.jsonDecode(snapshot.data);
                        var decodeConvert = convert.jsonDecode(decode["responseBody"]);
                        final dtkonten = decodeConvert["tipePx"];
                        final data = dtkonten[0]["data"];
                        var i = 1;
                        data.forEach((dt) => {
                          _mTipePX.add(
                            new TipePX(
                              i,
                              dt["simrsId"].toString(),
                              dt["advanceData"].toString(),
                              dt["typepx_id"].toString(),
                              dt["typepx_en"].toString(),
                            ),
                          ),
                          i++,
                        });

                        if(_mTipePX.length > 1){
                          if(widget._selectedTipepx != null){
                            selectedTipepx=_mTipePX[widget._selectedTipepx];
                            _nobpjsController.text = widget._nobpjs;
                            _noRujukanController.text = widget._norujukan;
                            _noteController.text = widget._note;
                            if(widget._typepx == "9"){
                                bpjsView = true;
                                if(widget._bpjs_id != null){
                                  _nobpjsController.text = widget._bpjs_id;
                                }
                                print(widget._bpjs_id);
                                paddingtop = 20;
                            }else{
                                bpjsView = false;
                                paddingtop = maxheight * (1/5.3);
                                if(paddingtop < 0){
                                  paddingtop = 0;
                                }
                            }
                          }else{
                            selectedTipepx=_mTipePX[0];
                          }
                        }
                      }
                      if(_tipepxEna){
                        return new Stack(
                          fit: StackFit.expand,
                          children: <Widget>[
                            new Container(
                              child: new Column(
                                children: <Widget>[
                                  Expanded(
                                    child: ListView(
                                      controller: listScrollController,
                                      scrollDirection: Axis.vertical,
                                      children: <Widget>[
                                        _buildInfo(),
                                        new Padding(padding: EdgeInsets.only(top: paddingtop)),
                                        _buildForm(),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        );
                      }else{
                        return new Center(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              CircularProgressIndicator(
                                backgroundColor: Colors.grey,
                              ),
                            ],
                          ),
                        );
                      }
                    }
                )
            )
        )
    );
  }

  Widget _buildInfo() {
    return new Padding(
      padding: const EdgeInsets.only(top: 30, left: 24.0, right: 24.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Row(
              children: <Widget>[
                new Column(
                    children: <Widget>[
                      new Container(
                        width: 80.0,
                        height: 80.0,
                        decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          border: new Border.all(color: Colors.grey[300]),
                        ),
                        margin: const EdgeInsets.only(top: 16.0, right: 16.0),
                        padding: const EdgeInsets.all(3.0),
                        child: new ClipOval(
                          child: new Image.asset("assets/suri/suri2.png"),
                        ),
                      )
                    ]
                ),
                new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 18.0),
                      ),
                      new Text(
                        "Tipe Pasien",
                        style: new TextStyle(
                          color: Colors.grey[800],
                          fontWeight: FontWeight.bold,
                          fontSize: 24.0,
                        ),
                      ),
                      new Text(
                        "Silahkan Pilih Tipe Pasien!",
                        style: new TextStyle(
                          color: Colors.grey[800],
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ]
                )
              ]
          ),
          new Container(
            color: Colors.grey[300],
            margin: const EdgeInsets.symmetric(vertical: 16.0),
            width: MediaQuery.of(context).size.width,
            height: 1.0,
          ),
          new Visibility(
            visible: visibleDesc,
            child: Text(
              "Silahkan pilih tipe pasien berobat Anda sebelum mengirimkan data reservasi ini!",
              style: new TextStyle(
                color: Colors.grey[800],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildForm(){
    return new Container(
      padding: EdgeInsets.all(24),
      child: Column(
        children: <Widget>[
          new Container(
            margin: EdgeInsets.only(top:20),
            child: new DropdownButtonFormField(
              value: selectedTipepx,
              onChanged: (TipePX newValue) {
                if(_tipepxEna){
                  setState((){
                    _noRujukanNull = false;
                    _nobpjsNull = false;
                    _tipepxNull = false;
                    if(newValue.simrsId == ""){
                      selectedTipepx = _mTipePX[0];
                    }else{
                      selectedTipepx = _mTipePX[newValue.index];
                    }
                    if(selectedTipepx.simrsId == "9"){
                      setState(() {
                        bpjsView = true;
                        paddingtop = 20;
                      });
                    }else{
                      setState(() {
                        bpjsView = false;
                        paddingtop = maxheight * (1/5.3);
                        if(paddingtop < 0){
                          paddingtop = 0;
                        }
                      });
                    }
                  });
                }
              },
              items: _mTipePX.map((TipePX _mTipePX){
                return new DropdownMenuItem<TipePX>(
                    value: _mTipePX,
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Container(
                          padding: EdgeInsets.only(top:3),
                          child:new Text(
                            _mTipePX.typepx_id,
                            style: new TextStyle(color: Colors.black, fontSize: 14),
                          ),
                        ),
                      ],
                    )
                );
              }).toList(),
              decoration: InputDecoration(
                filled: true,
                fillColor: _fillColor,
                labelStyle: TextStyle(
                  color: Colors.grey[800],
                ),
                labelText: 'Tipe Pasien',
                errorText: _tipepxNull ? "" : null,
                prefixIcon: Icon(
                  FontAwesomeIcons.stethoscope,
                  color: Colors.grey[800],
                ),
                focusedBorder: const OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                ),
                enabledBorder: const OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                  borderSide: BorderSide(
                    color: Colors.grey,
                  ),
                ),
              ),
            ),
          ),

          new Visibility(
            visible: bpjsView,
            child: Column(children: <Widget>[
              new Container(
                  margin: EdgeInsets.only(top:20),
                  child: TextField(
                    focusNode: _nobpjs,
                    controller: _nobpjsController,
                    textInputAction: TextInputAction.next,
                    onEditingComplete: () => {_next(_noRujukan, "next"),},
                    cursorColor: Colors.grey[800],
                    style: TextStyle(color: Colors.grey[800], fontSize: 14),
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: _fillColor,
                      labelStyle: TextStyle(
                        color: Colors.grey[800],
                      ),
                      errorText: _nobpjsNull ? "" : null,
                      labelText: 'No. Kartu BPJS',
                      prefixIcon: Icon(
                        FontAwesomeIcons.idCard,
                        color: Colors.grey[800],
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                        borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                      ),
                      enabledBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                        borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        borderSide: BorderSide(
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  )
              ),
              new Container(
                  margin: EdgeInsets.only(top:20),
                  child: TextField(
                    focusNode: _noRujukan,
                    controller: _noRujukanController,
                    textInputAction: TextInputAction.next,
                    onEditingComplete: () => {_next(_note, "next"),},
                    cursorColor: Colors.grey[800],
                    style: TextStyle(color: Colors.grey[800], fontSize: 14),
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: _fillColor,
                      labelStyle: TextStyle(
                        color: Colors.grey[800],
                      ),
                      errorText: _noRujukanNull ? "" : null,
                      labelText: 'No. Rujukan BPJS',
                      prefixIcon: Icon(
                        FontAwesomeIcons.fileMedical,
                        color: Colors.grey[800],
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                        borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                      ),
                      enabledBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                        borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        borderSide: BorderSide(
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  )
              ),
            ]
            ),
          ),
          new Stack(
            children: <Widget>[
              new Container(
                  padding: EdgeInsets.only(top: 20, bottom: 10),
                  child: TextField(
                    focusNode: _note,
                    controller: _noteController,
                    maxLines: null,
                    textInputAction: TextInputAction.done,
                    onEditingComplete: () => _next(_note, "done"),
                    cursorColor: Colors.grey[800],
                    style: TextStyle(color: Colors.grey[800], fontSize: 14),
                    keyboardType: TextInputType.multiline,
                    minLines: 5,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: _fillColor,
                      labelStyle: TextStyle(
                        color: Colors.grey[800],
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                        borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                      ),
                      enabledBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                        borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        borderSide: BorderSide(
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  )
              ),
              Positioned(
                top: -6,
                child: new Container(
                  color: Colors.white,
                  margin: EdgeInsets.only(left: 45, top:20, bottom: 5),
                  child: new Row(
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.only(left: 5, right: 5),
                        child: Column(
                          children: <Widget>[
                            Text("Note:", style: TextStyle(fontSize: 12),)
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
          new Container(
            margin: EdgeInsets.only(top:40, bottom: 20),
            child: Column(
                children: <Widget>[
                  new Align(
                    alignment: Alignment.bottomCenter,
                    child: RawMaterialButton(
                      onPressed: (){
                        var _Next = true;
                        setState(() {
                          _tipepxNull = false;
                          if(selectedTipepx.simrsId == ""){
                            _tipepxNull = true;
                            _Next = false;
                          }
                          if(bpjsView){
                            _nobpjsNull = false;
                            if(_nobpjsController.text.isEmpty){
                              _nobpjsNull = true;
                              _Next = false;
                            }
                            _noRujukanNull = false;
                            if(_nobpjsController.text.isEmpty){
                              _noRujukanNull = true;
                              _Next = false;
                            }
                          }
                        });
                        if(_Next){
                          List<Map<String, dynamic>> dtNext=[];
                          dtNext.add({
                            "_pxBaru": widget._pxBaru,
                            "_rm": widget._rm,
                            "_namapx": widget._namapx,
                            "_gender": widget._gender,
                            "_tgllahir": widget._tgllahir,
                            "_email": widget._email,
                            "_mobile": widget._mobile,
                            "_alamat": widget._alamat,
                            "_bpjs_id": widget._bpjs_id,
                            "_selectedTipepx": selectedTipepx.index,
                            "_typepx": selectedTipepx.simrsId,
                            "_typepx_id": selectedTipepx.typepx_id,
                            "_typepx_en": selectedTipepx.typepx_en,
                            "_nobpjs": _nobpjsController.text,
                            "_norujukan": _noRujukanController.text,
                            "_note": _noteController.text,
                            "_currentDate2Select": widget._currentDate2Select,
                            "_clinicSelect": widget._clinicSelect,
                            "_drSelect": widget._drSelect,
                            "_Namacabang": widget._Namacabang,
                            "_BranchCode": widget._BranchCode,
                            "_cabang": widget._cabang,
                            "_color1": widget._color1,
                            "_color2": widget._color2,
                            "_Selected_color": widget._Selected_color,
                            "_idsp": widget._idsp,
                            "_drimage": widget._drimage,
                            "_drname": widget._drname,
                            "_titlesp_id": widget._titlesp_id,
                            "_titlesp_en": widget._titlesp_en,
                            "_dridweb": widget._dridweb,
                            "_secid": widget._secid,
                            "_dridapi": widget._dridapi,
                            "_spc_id": widget._spc_id,
                            "_spc_en": widget._spc_en,
                            "_drname": widget._drname,
                            "_NamaDokterPengganti": widget._NamaDokterPengganti,
                            "_DokterID": widget._DokterID,
                            "_SectionID": widget._SectionID,
                            "_Tanggal": widget._Tanggal,
                            "_WaktuID": widget._WaktuID,
                            "_Hari": widget._Hari,
                            "_JmlAntrian": widget._JmlAntrian,
                            "_Realisasi": widget._Realisasi,
                            "_Cancel": widget._Cancel,
                            "_DokterPenggantiID": widget._DokterPenggantiID,
                            "_NoAntrianTerakhir": widget._NoAntrianTerakhir,
                            "_SectionName": widget._SectionName,
                            "_FromJam": widget._FromJam,
                            "_ToJam": widget._ToJam,
                            "_time": widget._FromJam+"-"+widget._ToJam,
                            "_WaktuFromJam": widget._WaktuFromJam,
                            "_WaktuToJam": widget._WaktuToJam,
                            "_sch": widget._sch,
                            "_asgeriatri": widget._asgeriatri,
                            "_geriatri": widget._geriatri,
                            "_BPJS": widget._BPJS,
                            "_imgbpjs": widget._imgbpjs,
                            "_bpjslogo": widget._bpjslogo,
                            "_closed": widget._closed,
                            "_statussch": widget._statussch,
                            "_status_id": widget._status_id,
                            "_status_en": widget._status_en,
                          });
                          RouteHelper(context, "ConfirmData", dtNext);
                        }
                      },
                      elevation: 0,
                      padding: EdgeInsets.all(16),
                      animationDuration: Duration(milliseconds: 500),
                      fillColor: widget._color1,
                      splashColor: Colors.red,
                      highlightColor:widget._color2,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                      ),
                      constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Expanded(
                            flex: 1,
                            child: new Row(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Column(children: <Widget>[
                                  Text(
                                    "Berikutnya",
                                    style: TextStyle(color: Colors.white, fontSize: 18),
                                  ),
                                ],),
                              ],
                            ),
                          ),
                          Icon(
                            FontAwesomeIcons.arrowRight,
                            color: Colors.white,
                            size: 16,
                          ),
                        ],
                      ),
                    ),
                  ),
                ]
            ),
          ),
        ],
      )
    );
  }

  _next(FocusNode reqfocus, String s){
    FocusScope.of(context).requestFocus(reqfocus);
    if(s == "last"){
      listScrollController.jumpTo(listScrollController.position.maxScrollExtent);
    }
    if(s == "done"){
      listScrollController.jumpTo(listScrollController.position.minScrollExtent);
      FocusScope.of(context).requestFocus(new FocusNode());
    }
  }
}