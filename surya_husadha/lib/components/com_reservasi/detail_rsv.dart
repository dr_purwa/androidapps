import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:surya_husadha/components/com_rating/rsv_comment.dart';
import 'package:surya_husadha/components/com_reservasi/reservasi_models.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surya_husadha/helper/constans/constans.dart';

class DetailRsv extends StatefulWidget {

  Reservasi_Mdl _dt;
  bool _active;
  Color _textColor;
  Color _TextColorBar = Colors.white;
  Color _color;
  Color _color2;
  String _logo;
  double _logoHeight = 35;
  int _indCabang;
  String _cabang;
  var _dtakun;

  DetailRsv(Reservasi_Mdl _dt, bool active, String cabang, akun){
    this._dt = _dt;
    this._dtakun = akun;
    this._active = active;
    this._cabang = cabang;
    if(active){
      _textColor = Colors.grey[800];
    }else{
      _textColor = Colors.grey[500];
    }
    if(cabang == "group"){
      _indCabang = 0;
      _color = WarnaCabang.group;
      _color2 = WarnaCabang.group2;
      _TextColorBar = Colors.grey[600];
      _logo = "assets/logo/group.png";
      _logoHeight = 30;
    }
    if(cabang == "denpasar"){
      _indCabang = 1;
      _color = WarnaCabang.shh;
      _color2 = WarnaCabang.shh2;
      _logo = "assets/logo/shh2.png";
    }
    if(cabang == "ubung"){
      _indCabang = 2;
      _color = WarnaCabang.ubung;
      _color2 = WarnaCabang.ubung2;
      _logo = "assets/logo/ub2.png";
    }
    if(cabang == "nusadua"){
      _indCabang = 3;
      _color = WarnaCabang.nusadua;
      _color2 = WarnaCabang.nusadua2;
      _logo = "assets/logo/nd2.png";
    }
    if(cabang == "kmc"){
      _indCabang = 4;
      _color = WarnaCabang.kmc;
      _color2 = WarnaCabang.kmc2;
      _logo = "assets/logo/nd2.png";
    }
  }

  @override
  _DetailRsv createState() => new _DetailRsv();

}

class _DetailRsv extends State<DetailRsv> with SingleTickerProviderStateMixin, NavigatorObserver{

  var newdata;
  bool then = true;
  GlobalKey<ScaffoldState> scafolt_key = GlobalKey<ScaffoldState>();
  AnimationController _controllerappBar;
  String newdtstar = "0.0";
  String newdtcomment;

  @override
  initState() {
    super.initState();
    _controllerappBar = new AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
    _controllerappBar.forward();
  }

  @override
  void dispose(){
    _controllerappBar.dispose();
    super.dispose();
  }

  Widget _detail(context, Reservasi_Mdl _dt){
    var dt = _dt;
    Widget _qrcode;
    if(dt.qrcode!=""){
      _qrcode = new Image.network(
        dt.qrcode,
        fit: BoxFit.none,
      );
    }else{
      _qrcode = new Container();
    }

    Widget _Expired;
    if(!widget._active){
      _Expired = new Container(
        margin: EdgeInsets.only(top: 5),
        decoration: BoxDecoration(
            color: Colors.redAccent,
            borderRadius: BorderRadius.all(Radius.circular(5.0))
        ),
        padding: EdgeInsets.only(left: 10, right: 10, top: 2, bottom: 2),
        child: new Text(
          "Sudah melewati jadwal!",
          style: TextStyle(color: Colors.white, fontSize: 12),
        ),
      );
    }else{
      _Expired = new Container();
    }

    return new SafeArea(
        child: new Scaffold(
          key: scafolt_key,
            appBar: new PreferredSize(
              preferredSize: Size.fromHeight(200.0),
              child: _appbar(),
            ),
          body: WillPopScope(
            onWillPop: (){
              Navigator.pop(context, newdata);
            },
            child: new Stack(
              fit: StackFit.expand,
              children: <Widget>[
                new Container(
                  child: new Column(
                    children: <Widget>[
                      new Expanded(
                        child: new ListView(
                          scrollDirection: Axis.vertical,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.all(16),
                              color: Colors.white,
                              child: new Row(
                                children: <Widget>[
                                  new Expanded(
                                      flex: 1,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          new Text(
                                              "ID Reservasi:"
                                          ),
                                          new Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: <Widget>[
                                              new Column(
                                                children: <Widget>[
                                                  Icon(
                                                    FontAwesomeIcons.ticketAlt,
                                                    color: Colors.grey[800],
                                                    size: 16,
                                                  )
                                                ],
                                              ),
                                              new Container(
                                                padding: EdgeInsets.only(left: 5, top: 0),
                                                child: new Column(
                                                  children: <Widget>[
                                                    Text(
                                                      _dt.rsv_no,
                                                      overflow: TextOverflow.ellipsis,
                                                      maxLines: 1,
                                                      softWrap: true,
                                                      style: TextStyle(
                                                          color: Colors.grey[800],
                                                          fontSize: 14,
                                                          fontWeight: FontWeight.w700
                                                      ),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          Padding(padding: EdgeInsets.only(top: 5)),
                                          new Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: <Widget>[
                                              new Text(
                                                "No. urut:",
                                                style: TextStyle(
                                                  color: Colors.grey[800],
                                                  fontSize: 14,
                                                ),
                                              ),
                                              new Container(
                                                padding: EdgeInsets.only(left: 5, top: 0),
                                                child: new Column(
                                                  children: <Widget>[
                                                    Text(
                                                      _dt.noUrut,
                                                      style: TextStyle(
                                                        color: Colors.grey[800],
                                                        fontSize: 14,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          _Expired,
                                        ],
                                      )
                                  ),
                                  _qrcode,
                                ],
                              ),
                            ),
                            new Container(
                              width: double.infinity,
                              height: 1.0,
                              color: Colors.grey[400].withOpacity(0.6),
                            ),
                            _builddtPX(_dt),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        )
    );
  }

  Widget _appbar(){
    return new Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        new Container(
            color: widget._color,
            height: 55,
            child:Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: new Row(
                    children: <Widget>[
                      new Container(
                        child: new InkWell(
                          onTap: () =>  Navigator.pop(context),
                          child: new Container(
                            padding: EdgeInsets.only(left: 24, right: 24),
                            alignment: Alignment.center,
                            child: new Icon(
                              FontAwesomeIcons.arrowLeft,
                              color: widget._TextColorBar,
                              size: 24.0,
                            ),
                          ),
                        ),
                      ),
                      new Text(
                        "DETAIL RESERVASI",
                        style: TextStyle(
                          fontSize: 16,
                          color: widget._TextColorBar,
                          fontWeight: FontWeight.w700
                        ),
                      ),

                    ],
                  ),
                ),
              ],
            )
        ),
        new Container(
          color: widget._color,
          height: 2,
          child: new SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(-1, 0),
              end: const Offset(0, 0),
            ).animate(_controllerappBar),
            child: new SizedBox(
              child: Container(
                  color: Colors.white
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _bpjslogo(String bpjs, closed){
    if(bpjs != ""){
      if(!closed){
        return new Container(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                new Container(
                  height: 60,
                  width: 70,
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new NetworkImage(bpjs),
                      fit: BoxFit.cover,
                      colorFilter: new ColorFilter.mode(Colors.white.withOpacity(0.2), BlendMode.dstATop),
                    ),
                  ),
                )
              ],
            )
        );
      }else{
        return new Container(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                new Container(
                  height: 60,
                  width: 80,
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new NetworkImage(bpjs),
                      fit: BoxFit.cover,
                    ),
                  ),
                )
              ],
            )
        );
      }
    }else{
      return new Container();
    }
  }

  Widget _builddtPX(Reservasi_Mdl dt){

    Widget tipepxInfo;
    Widget viewLogoBpjs;
    if(dt.typepx_id == "9"){
      viewLogoBpjs = new Container(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: new Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            _bpjslogo("https://suryahusadha.com/images/bpjs.png", true),
          ],
        ),
      );
      tipepxInfo = new Container(
        child: Column(
          children: <Widget>[
            cardHeader("Detail Tipe Pasien", FontAwesomeIcons.thList),
            new Container(
              child: Column(children: <Widget>[
                cardDt("Tipe Pasien", dt.typepx_id, FontAwesomeIcons.stethoscope),
                cardDt("No. BPJS", dt.bpjsid, FontAwesomeIcons.idCard),
                cardDt("No. Rujukan", dt.NoRujukanBPJS, FontAwesomeIcons.fileMedical),
              ],),
            ),
          ],
        ),
      );
    }else{
      tipepxInfo = new Container(
        child: Column(
          children: <Widget>[
            cardHeader("Detail Tipe Pasien", FontAwesomeIcons.thList),
            new Container(
              child: Column(children: <Widget>[
                cardDt("Tipe Pasien", dt.typepx_id, FontAwesomeIcons.stethoscope),
              ],),
            ),
          ],
        ),
      );
      viewLogoBpjs = new Container();
    }
    return new Container(
      child: Column(children: <Widget>[
        cardHeader("Rating / Penilaian Anda", FontAwesomeIcons.star),
        cardRating("Berikan Rating / Penilaian Anda", dt, FontAwesomeIcons.star),
        cardHeader("Detail Dokter", FontAwesomeIcons.userMd),
        _header_sch(dt),
        cardHeader("Detail Jadwal Praktek", FontAwesomeIcons.thList),
        new Container(
          color: Colors.white,
          child: new Column(
            children: <Widget>[
              new Row(
                  children: <Widget>[
                    new Expanded(
                      flex: 1,
                      child: new Column(
                        children: <Widget>[
                          cardsch(dt.day +", "+dt.date, FontAwesomeIcons.calendarAlt, true),
                          cardsch(dt.timeopen, FontAwesomeIcons.clock, false),
                        ],
                      ),
                    ),
                    viewLogoBpjs,
                  ]
              ),
              new Container(
                color: Colors.grey[300],
                width: MediaQuery.of(context).size.width,
                height: 1.0,
              ),
            ],
          ),
        ),
        tipepxInfo,
        cardHeader("Detail Data Pasien", FontAwesomeIcons.thList),
        new Container(
          child: Column(children: <Widget>[
            cardDt("No. Rekam Medis", dt.nrm, FontAwesomeIcons.idBadge),
            cardDt("Nama", dt.namepx,  FontAwesomeIcons.tag),
            cardDt("Tgl. Lahir",  dt.TanggalLahir, FontAwesomeIcons.calendarCheck),
            cardDt("Jenis Kelamin", dt.genderpx, FontAwesomeIcons.transgender),
            cardDt("Email", dt.emailpx, FontAwesomeIcons.at),
            cardDt("Mobile", dt.phonepx, FontAwesomeIcons.mobileAlt),
          ],),
        ),
        cardHeader("Informasi Lainnya", FontAwesomeIcons.thList),
        new Container(
          child: Column(children: <Widget>[
            cardDtAlamat("Note", dt.note, FontAwesomeIcons.stickyNote),
          ],),
        ),
        new Container(
          margin: EdgeInsets.only(bottom: 1),
          width: double.infinity,
          height: 1.0,
          color: Colors.grey[300],
        ),
      ],),
    );
  }

  Widget cardsch(String title, IconData icon, bool borderbtm){
    Widget borderBottom = new Container();
    if(borderbtm){
      borderBottom = new Container(
        color: Colors.grey[300],
        width: MediaQuery.of(context).size.width,
        height: 1.0,
      );
    }

    return new Container(
        color: Colors.white,
        child: new Row(
          children: <Widget>[
            new Expanded(
                child: new Column(
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Row(
                            children: <Widget>[
                              new Icon(
                                icon,
                                size: 16,
                                color: widget._textColor,
                              ),
                              new Container(width: 5,),
                              new Text(
                                title,
                                style: TextStyle(color: widget._textColor, fontSize: 14),
                                maxLines: 1,
                                softWrap: false,
                                overflow: TextOverflow.ellipsis,
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    borderBottom,
                  ],
                )
            ),
          ],
        )
    );
  }

  Widget cardHeader(String title, IconData icon){
    return new Container(
      color: Colors.grey[100],
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Icon(
                        icon,
                        size: 16,
                        color: widget._textColor,
                      ),
                      new Container(
                        padding: EdgeInsets.only(left: 5, top: 3),
                        child:  new Text(
                          title,
                          style: TextStyle(color: widget._textColor, fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  color: Colors.grey[300],
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget cardRating(String title, Reservasi_Mdl dt, IconData icon){
    if(!then){
      return InkWell(
          onTap: (){
              showDialog(
                barrierDismissible: true,
                context: context,
                builder: (BuildContext context){
                  return new RsvComment(dt, widget._dtakun, widget._cabang);
                },
              );
          },
          child: new Container(
              color: Colors.white,
              child: new Row(
                children: <Widget>[
                  new Expanded(
                      child: new Column(
                        children: <Widget>[
                          new Container(
                            padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new FlutterRatingBarIndicator(
                                  fillColor: Colors.yellow[600],
                                  rating: double.parse(newdtstar.toString()),
                                  itemCount: 5,
                                  itemSize: 20.0,
                                  emptyColor: Colors.grey[300],
                                ),
                                new Row(
                                  children: <Widget>[
                                    new Container(
                                      padding: EdgeInsets.only(right: 10, top: 3),
                                      child: new Text(
                                        newdtstar.toString(),
                                        style: TextStyle(
                                          color: widget._textColor,
                                          fontSize: 16,
                                        ),
                                      ),
                                    ),
                                    new Icon(
                                      FontAwesomeIcons.chevronRight,
                                      size: 14,
                                      color: widget._textColor,
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          new Container(
                            color: Colors.grey[300],
                            width: MediaQuery.of(context).size.width,
                            height: 1.0,
                          ),
                        ],
                      )
                  ),
                ],
              )
          )
      );
    }else{
      if(dt.star.toString() == "0" || dt.star.toString() == "" ){
        return InkWell(
          onTap: (){
            if(!widget._active){
              showDialog(
                barrierDismissible: true,
                context: context,
                builder: (BuildContext context){
                  return new RsvComment(dt, widget._dtakun, widget._cabang);
                },
              ).then((newdt){
                if(newdt!=null){
                  setState(() {
                    then = false;
                    newdata = newdt;
                    newdtstar = newdt[0]["star"].toString();
                    newdtcomment = newdt[0]["comment"].toString();
                  });
                }
              });
            }else{
              scafolt_key.currentState.removeCurrentSnackBar();
              final snackBar = SnackBar(
                content: new Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Row(
                        children: <Widget>[
                          Icon(FontAwesomeIcons.infoCircle, size: 16,),
                          new Container(
                              padding: EdgeInsets.only(left: 10),
                              child: Text('Informasi', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),)
                          )
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 20)),
                      Text('Setelah jadwal berobat selesai, Anda dapat memberikan Rating / Penilaian layanan ini!'),
                      Padding(padding: EdgeInsets.only(top: 20)),
                      Text('Terima Kasih.')
                    ],
                  ),
                ),
              );
              scafolt_key.currentState.showSnackBar(snackBar);
            }
          },
          child: new Container(
              color: Colors.white,
              child: new Row(
                children: <Widget>[
                  new Expanded(
                      child: new Column(
                        children: <Widget>[
                          new Container(
                            padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Text(
                                  title,
                                  style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                ),
                                new Row(
                                  children: <Widget>[
                                    new Container(
                                      padding: EdgeInsets.only(top: 2),
                                      child:  new Icon(
                                        FontAwesomeIcons.solidCircle,
                                        size: 8,
                                        color: Colors.redAccent,
                                      ),
                                    ),
                                    new Icon(
                                      FontAwesomeIcons.chevronRight,
                                      size: 14,
                                      color: Colors.grey[800],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          new Container(
                            color: Colors.grey[300],
                            width: MediaQuery.of(context).size.width,
                            height: 1.0,
                          ),
                        ],
                      )
                  ),
                ],
              )
          ),
        );
      }else{
        return InkWell(
            onTap: (){
              if(!widget._active){
                showDialog(
                  barrierDismissible: true,
                  context: context,
                  builder: (BuildContext context){
                    return new RsvComment(dt, widget._dtakun, widget._cabang);
                  },
                );
              }else{
                scafolt_key.currentState.removeCurrentSnackBar();
                final snackBar = SnackBar(
                  content: new Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Row(
                          children: <Widget>[
                            Icon(FontAwesomeIcons.infoCircle, size: 16,),
                            new Container(
                                padding: EdgeInsets.only(left: 10),
                                child: Text('Informasi', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),)
                            )
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(top: 20)),
                        Text('Setelah jadwal berobat selesai, Anda dapat memberikan Rating / Penilaian layanan ini!'),
                        Padding(padding: EdgeInsets.only(top: 20)),
                        Text('Terima Kasih.')
                      ],
                    ),
                  ),
                );
                scafolt_key.currentState.showSnackBar(snackBar);
              }
            },
            child: new Container(
                color: Colors.white,
                child: new Row(
                  children: <Widget>[
                    new Expanded(
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new FlutterRatingBarIndicator(
                                    fillColor: Colors.yellow[600],
                                    rating: double.parse(dt.star.toString()),
                                    itemCount: 5,
                                    itemSize: 20.0,
                                    emptyColor: Colors.grey[300],
                                  ),
                                  new Row(
                                    children: <Widget>[
                                      new Container(
                                        padding: EdgeInsets.only(right: 10, top: 3),
                                        child: new Text(
                                          dt.star.toString(),
                                          style: TextStyle(
                                            color: widget._textColor,
                                            fontSize: 16,
                                          ),
                                        ),
                                      ),
                                      new Icon(
                                        FontAwesomeIcons.chevronRight,
                                        size: 14,
                                        color: widget._textColor,
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            new Container(
                              color: Colors.grey[300],
                              width: MediaQuery.of(context).size.width,
                              height: 1.0,
                            ),
                          ],
                        )
                    ),
                  ],
                )
            )
        );
      }
    }
  }

  Widget cardDt(String title, String dt, IconData icon){
    return new Container(
        color: Colors.white,
        child: new Row(
          children: <Widget>[
            new Expanded(
                child: new Column(
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Row(
                            children: <Widget>[
                              new Icon(
                                icon,
                                size: 16,
                                color: widget._textColor,
                              ),
                              new Container(width: 5,),
                              new Text(
                                title,
                                style: TextStyle(color: widget._textColor, fontSize: 14),
                              ),
                            ],
                          ),
                          new Container(width: 10),
                          new Flexible(
                            child: new Text(
                              dt,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              softWrap: false,
                              style: TextStyle(color: widget._textColor, fontSize: 14),
                            ),
                          )
                        ],
                      ),
                    ),
                    new Container(
                      color: Colors.grey[300],
                      width: MediaQuery.of(context).size.width,
                      height: 1.0,
                    ),
                  ],
                )
            ),
          ],
        )
    );
  }

  Widget cardDtAlamat(String title, String dt, IconData icon){
    return Container(
        color: Colors. white,
        child: new Row(
          children: <Widget>[
            new Expanded(
                child: new Column(
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Row(
                                children: <Widget>[
                                  new Icon(
                                    icon,
                                    size: 16,
                                    color: widget._textColor,
                                  ),
                                  new Container(width: 5,),
                                  new Text(
                                    title,
                                    style: TextStyle(color: widget._textColor, fontSize: 14),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 16),
                          ),
                          new Column(
                            children: <Widget>[
                              new Text(
                                dt,
                                style: TextStyle(color: widget._textColor, fontSize: 14),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      color: Colors.grey[300],
                      width: MediaQuery.of(context).size.width,
                      height: 1.0,
                    ),
                  ],
                )
            )
          ],
        )
    );
  }

  Widget _header_sch(Reservasi_Mdl dt){

    Widget _imgActive;

    if(widget._active){
      _imgActive = new Container(
        width: 80.0,
        height: 80.0,
        padding: EdgeInsets.all(1.0),
        decoration: new BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.all(new Radius.circular(100.0)),
          border: Border.all(width: 1, color: Colors.grey[200]),
          image: new DecorationImage(
            image: new NetworkImage(dt.image),
            fit: BoxFit.cover,
          ),
        ),
        alignment: Alignment.center,
      );
    }else{
      _imgActive = new Container(
        width: 80.0,
        height: 80.0,
        padding: EdgeInsets.all(1.0),
        decoration: new BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.all(new Radius.circular(100.0)),
          border: Border.all(width: 1, color: Colors.grey[200]),
          image: new DecorationImage(
            image: new NetworkImage(dt.image),
            fit: BoxFit.cover,
            colorFilter: new ColorFilter.mode(Colors.white.withOpacity(0.4), BlendMode.dstATop),
          ),
        ),
        alignment: Alignment.center,
      );
    }

    return new Container(
        child: new Column(
          children: <Widget>[
            new Container(
              color: Colors.white,
              padding: EdgeInsets.only(top:16, bottom: 16, left: 24, right: 24),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  _imgActive,
                  new Expanded(
                    flex: 2,
                    child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Container(
                            alignment: Alignment.topLeft,
                            padding: EdgeInsets.only(left:10.0, right:10.0),
                            child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    alignment: Alignment.topLeft,
                                    child:new Text(
                                      dt.gelardepan+dt.officer_name+dt.gelarbelakang,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      softWrap: false,
                                      style: TextStyle(fontWeight: FontWeight.w700, color: widget._textColor, fontFamily: "NeoSansBold", fontSize: 14.0),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                  new Container(
                                    alignment: Alignment.topLeft,
                                    child: new Text(
                                      dt.titlesp_id+" ("+dt.titlesp_en+")",
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      softWrap: false,
                                      style: TextStyle(color: widget._textColor, fontSize: 12.0),
                                    ),
                                  ),
                                ]
                            ),
                          ),
                        ]
                    ),
                  ),
                ],
              ),
            ),
            new Container(
              width: double.infinity,
              height: 1.0,
              color: Colors.grey[300],
            ),
          ],
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return _detail(context, widget._dt);
  }

}