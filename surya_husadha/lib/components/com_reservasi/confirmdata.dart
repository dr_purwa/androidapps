import 'dart:async';
import 'package:flutter/material.dart';
import 'package:surya_husadha/helper/dialogue/customDialogue.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:ui' as ui;
import 'dart:convert' as convert;

class ConfirmData extends StatefulWidget {
  bool _pxBaru;
  String _rm;
  String _namapx;
  String _gender;
  String _gendertext;
  String _tgllahir;
  String _email;
  String _mobile;
  String _alamat;
  String _bpjs_id;
  int _selectedTipepx;
  String _typepx;
  String _typepx_id;
  String _typepx_en;
  String _nobpjs;
  String _norujukan;
  String _note;
  DateTime _currentDate2Select;
  int _clinicSelect;
  int _drSelect;
  String _Namacabang;
  String _BranchCode;
  String _cabang;
  Color _color1;
  Color _color2;
  Color _Selected_color;
  String _idsp;
  String _drimage;
  String _secid;
  String _dridapi;
  String _spc_id;
  String _spc_en;
  String _drname;
  String _titlesp_id;
  String _titlesp_en;
  String _dridweb;
  String _NamaDokterPengganti;
  String _DokterID;
  String _SectionID;
  String _Tanggal;
  int _WaktuID;
  String _Hari;
  int _JmlAntrian;
  bool _Realisasi;
  bool _Cancel;
  String _DokterPenggantiID;
  int _NoAntrianTerakhir;
  String _SectionName;
  String _FromJam;
  String _ToJam;
  String _time;
  String _WaktuFromJam;
  String _WaktuToJam;
  String _sch;
  bool _asgeriatri;
  Widget _geriatri;
  String _BPJS;
  String _imgbpjs;
  Widget _bpjslogo;
  int _closed;
  String _statussch;
  String _status_id;
  String _status_en;

  var _akun;

  ConfirmData(List confirmdt, akun){
    this._akun = akun;
    confirmdt.forEach((dt) => {
      this._pxBaru=dt["_pxBaru"],
      this._rm=dt["_rm"],
      this._namapx=dt["_namapx"],
      this._gender=dt["_gender"],
      if(dt["_gender"] == "M"){
        this._gendertext = "Laki - Laki",
      },
      if(dt["_gender"] == "F"){
        this._gendertext = "Perempuan",
      },
      this._tgllahir=dt["_tgllahir"],
      this._email=dt["_email"],
      this._mobile=dt["_mobile"],
      this._alamat=dt["_alamat"],
      this._bpjs_id=dt["_bpjs_id"],
      this._selectedTipepx= dt["_selectedTipepx"],
      this._typepx= dt["_typepx"],
      this._typepx_id= dt["_typepx_id"],
      this._typepx_en= dt["_typepx_en"],
      this._nobpjs= dt["_nobpjs"],
      this._norujukan=dt["_norujukan"],
      this._note=dt["_note"],
      this._currentDate2Select = dt["_currentDate2Select"],
      this._clinicSelect = dt["_clinicSelect"],
      this._drSelect = dt["_drSelect"],
      this._Namacabang = dt["_Namacabang"],
      this._BranchCode = dt["_BranchCode"],
      this._cabang = dt["_cabang"],
      this._color1 = dt["_color1"],
      this._color2 = dt["_color2"],
      this._idsp = dt["_idsp"],
      this._drimage = dt["_drimage"],
      this._drname = dt["_drname"],
      this._titlesp_id = dt["_titlesp_id"],
      this._titlesp_en = dt["_titlesp_en"],
      this._dridweb = dt["_dridweb"],
      this._secid = dt["_secid"],
      this._dridapi = dt["_dridapi"],
      this._spc_id=dt["_spc_id"],
      this._spc_en=dt["_spc_en"],
      this._NamaDokterPengganti=dt["_NamaDokterPengganti"],
      this._DokterID=dt["_DokterID"],
      this._SectionID=dt["_SectionID"],
      this._Tanggal=dt["_Tanggal"],
      this._WaktuID=dt["_WaktuID"],
      this._Hari=dt["_Hari"],
      this._JmlAntrian=dt["_JmlAntrian"],
      this._Realisasi=dt["_Realisasi"],
      this._Cancel=dt["_Cancel"],
      this._DokterPenggantiID=dt["_DokterPenggantiID"],
      this._NoAntrianTerakhir=dt["_NoAntrianTerakhir"],
      this._SectionName=dt["_SectionName"],
      this._FromJam=dt["_FromJam"],
      this._ToJam=dt["_ToJam"],
      this._time=dt["_time"],
      this._WaktuFromJam=dt["_WaktuFromJam"],
      this._WaktuToJam=dt["_WaktuToJam"],
      this._sch=dt["_sch"],
      this._asgeriatri=dt["_asgeriatri"],
      this._geriatri=dt["_geriatri"],
      this._BPJS=dt["_BPJS"],
      this._imgbpjs=dt["_imgbpjs"],
      this._bpjslogo=dt["_bpjslogo"],
      this._closed=dt["_closed"],
      this._statussch=dt["_statussch"],
      this._status_id=dt["_status_id"],
      this._status_en = dt["_status_en"],
    });
  }

  @override
  _ConfirmDataController createState() => new _ConfirmDataController();
}

class _ConfirmDataController extends State<ConfirmData>{

  @override
  void initState(){
    super.initState();
  }

  @override
  void dispose(){
    super.dispose();
  }

  final _Scafoltkey = GlobalKey<ScaffoldState>();

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> dtNext=[];
    dtNext.add({
      "_pxBaru": widget._pxBaru,
      "_rm": widget._rm,
      "_namapx": widget._namapx,
      "_gender": widget._gender,
      "_tgllahir": widget._tgllahir,
      "_email": widget._email,
      "_mobile": widget._mobile,
      "_alamat": widget._alamat,
      "_bpjs_id": widget._bpjs_id,
      "_selectedTipepx": widget._selectedTipepx,
      "_typepx":widget._typepx,
      "_typepx_id": widget._typepx_id,
      "_typepx_en": widget._typepx_en,
      "_nobpjs": widget._nobpjs,
      "_norujukan": widget._norujukan,
      "_note": widget._note,
      "_currentDate2Select": widget._currentDate2Select,
      "_clinicSelect": widget._clinicSelect,
      "_drSelect": widget._drSelect,
      "_Namacabang": widget._Namacabang,
      "_BranchCode": widget._BranchCode,
      "_cabang": widget._cabang,
      "_color1": widget._color1,
      "_color2": widget._color2,
      "_Selected_color": widget._Selected_color,
      "_idsp": widget._idsp,
      "_drimage": widget._drimage,
      "_drname": widget._drname,
      "_titlesp_id": widget._titlesp_id,
      "_titlesp_en": widget._titlesp_en,
      "_dridweb": widget._dridweb,
      "_secid": widget._secid,
      "_dridapi": widget._dridapi,
      "_spc_id": widget._spc_id,
      "_spc_en": widget._spc_en,
      "_drname": widget._drname,
      "_NamaDokterPengganti": widget._NamaDokterPengganti,
      "_DokterID": widget._DokterID,
      "_SectionID": widget._SectionID,
      "_Tanggal": widget._Tanggal,
      "_WaktuID": widget._WaktuID,
      "_Hari": widget._Hari,
      "_JmlAntrian": widget._JmlAntrian,
      "_Realisasi": widget._Realisasi,
      "_Cancel": widget._Cancel,
      "_DokterPenggantiID": widget._DokterPenggantiID,
      "_NoAntrianTerakhir": widget._NoAntrianTerakhir,
      "_SectionName": widget._SectionName,
      "_FromJam": widget._FromJam,
      "_ToJam": widget._ToJam,
      "_time": widget._FromJam+"-"+widget._ToJam,
      "_WaktuFromJam": widget._WaktuFromJam,
      "_WaktuToJam": widget._WaktuToJam,
      "_sch": widget._sch,
      "_asgeriatri": widget._asgeriatri,
      "_geriatri": widget._geriatri,
      "_BPJS": widget._BPJS,
      "_imgbpjs": widget._imgbpjs,
      "_bpjslogo": widget._bpjslogo,
      "_closed": widget._closed,
      "_statussch": widget._statussch,
      "_status_id": widget._status_id,
      "_status_en": widget._status_en,
    });
    RouteHelper(context, "tipePx", dtNext);
  }

  @override
  Widget build(BuildContext context) {

    return new SafeArea(
        child: new Scaffold(
        body:new WillPopScope(
            onWillPop: _onWillPop,
            child: new Scaffold(
                key: _Scafoltkey,
                drawer: drawerController(widget._akun , widget._cabang, "Clinics"),
                appBar: new PreferredSize(
                  preferredSize: Size.fromHeight(200.0),
                  child: AppBarController(widget._cabang, _Scafoltkey,widget._akun, "ConfirmData"),
                ),
                body: new Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    new Container(
                      child: new Column(
                        children: <Widget>[
                          new Expanded(
                            child: new ListView(
                              scrollDirection: Axis.vertical,
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.only(top: 12, bottom: 16),
                                  color: Colors.white,
                                  child: new Column(
                                    children: <Widget>[
                                      new Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Column(
                                            children: <Widget>[
                                              Icon(
                                                FontAwesomeIcons.solidEdit,
                                                color: Colors.grey[800],
                                                size: 16,
                                              )
                                            ],
                                          ),
                                          new Container(
                                            padding: EdgeInsets.only(left: 5, top: 4),
                                            child: new Column(
                                              children: <Widget>[
                                                Text(
                                                  "Konfirmasi Data",
                                                  style: TextStyle(
                                                    color: Colors.grey[800],
                                                    fontSize: 18,
                                                  ),
                                                  textAlign: TextAlign.left,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                new Container(
                                  width: double.infinity,
                                  height: 1.0,
                                  color: Colors.grey[400].withOpacity(0.6),
                                ),
                                _builddtPX(),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                )
            ),
          )
        )
    );
  }

  Widget _builddtPX(){
    Widget tipepxInfo;
    Widget viewLogoBpjs;
    if(widget._typepx == "9"){
      viewLogoBpjs = new Container(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: new Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            widget._bpjslogo,
          ],
        ),
      );
      tipepxInfo = new Container(
        child: Column(
          children: <Widget>[
            cardHeader("Detail Tipe Pasien", FontAwesomeIcons.thList),
            new Container(
              child: Column(children: <Widget>[
                cardDt("Tipe Pasien", widget._typepx_id, FontAwesomeIcons.stethoscope),
                cardDt("No. BPJS", widget._nobpjs, FontAwesomeIcons.idCard),
                cardDt("No. Rujukan", widget._norujukan, FontAwesomeIcons.fileMedical),
              ],),
            ),
          ],
        ),
      );
    }else{
      tipepxInfo = new Container(
        child: Column(
          children: <Widget>[
            cardHeader("Detail Tipe Pasien", FontAwesomeIcons.thList),
            new Container(
              child: Column(children: <Widget>[
                cardDt("Tipe Pasien", widget._typepx_id, FontAwesomeIcons.stethoscope),
              ],),
            ),
          ],
        ),
      );
      viewLogoBpjs = new Container();
    }

    return new Container(
      child: Column(children: <Widget>[
        cardHeader("Detail Dokter", FontAwesomeIcons.userMd),
        _header_sch(),
        cardHeader("Detail Jadwal Praktek", FontAwesomeIcons.thList),
        new Container(
          color: Colors.white,
          child: new Column(
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Expanded(
                    flex: 1,
                    child: new Column(
                      children: <Widget>[
                        cardsch(widget._sch, FontAwesomeIcons.calendarAlt, true),
                        cardsch(widget._time, FontAwesomeIcons.clock, false),
                      ],
                    ),
                  ),
                  viewLogoBpjs,
                ]
              ),
              new Container(
                color: Colors.grey[300],
                width: MediaQuery.of(context).size.width,
                height: 1.0,
              ),
            ],
          ),
        ),
        tipepxInfo,
        cardHeader("Detail Data Pasien", FontAwesomeIcons.thList),
        new Container(
          child: Column(children: <Widget>[
            cardDt("No. Rekam Medis", widget._rm, FontAwesomeIcons.idBadge),
            cardDt("Nama", widget._namapx,  FontAwesomeIcons.tag),
            cardDt("Tgl. Lahir", widget._tgllahir, FontAwesomeIcons.calendarCheck),
            cardDt("Jenis Kelamin", widget._gendertext, FontAwesomeIcons.transgender),
            cardDt("Email", widget._email, FontAwesomeIcons.at),
            cardDt("Mobile", widget._mobile, FontAwesomeIcons.mobileAlt),
            cardDtAlamat("Alamat", widget._alamat, FontAwesomeIcons.home),
          ],),
        ),
        cardHeader("Informasi Lainnya", FontAwesomeIcons.thList),
        new Container(
          child: Column(children: <Widget>[
            cardDtAlamat("Note", widget._note, FontAwesomeIcons.stickyNote),
          ],),
        ),
        _btnNext(),
        new Container(
          margin: EdgeInsets.only(bottom: 1),
          width: double.infinity,
          height: 1.0,
          color: Colors.grey[300],
        ),
      ],),
    );
  }

  Widget cardsch(String title, IconData icon, bool borderbtm){
    Widget borderBottom = new Container();
    if(borderbtm){
      borderBottom = new Container(
        color: Colors.grey[300],
        width: MediaQuery.of(context).size.width,
        height: 1.0,
      );
    }

    return new Container(
        color: Colors.white,
        child: new Row(
          children: <Widget>[
            new Expanded(
                child: new Column(
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Row(
                            children: <Widget>[
                              new Icon(
                                icon,
                                size: 16,
                                color: Colors.grey[800],
                              ),
                              new Container(width: 5,),
                              new Text(
                                title,
                                style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                maxLines: 1,
                                softWrap: false,
                                overflow: TextOverflow.ellipsis,
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    borderBottom,
                  ],
                )
            ),
          ],
        )
    );
  }

  Widget cardHeader(String title, IconData icon){
    return new Container(
      color: Colors.grey[100],
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Icon(
                        icon,
                        size: 16,
                        color: Colors.grey[800],
                      ),
                      new Container(width: 5,),
                      new Text(
                        title,
                        style: TextStyle(color: Colors.grey[800], fontSize: 14),
                      ),
                    ],
                  ),
                ),
                new Container(
                  color: Colors.grey[300],
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget cardDt(String title, String dt, IconData icon){
    return new Container(
      color: Colors.white,
      child: new Row(
        children: <Widget>[
          new Expanded(
              child: new Column(
                children: <Widget>[
                  new Container(
                    padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Row(
                          children: <Widget>[
                            new Icon(
                              icon,
                              size: 16,
                              color: Colors.grey[800],
                            ),
                            new Container(width: 5,),
                            new Text(
                              title,
                              style: TextStyle(color: Colors.grey[800], fontSize: 14),
                            ),
                          ],
                        ),
                        new Container(width: 10),
                        new Flexible(
                          child: new Text(
                            dt,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            softWrap: false,
                            style: TextStyle(color: Colors.grey[800], fontSize: 14),
                          ),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    color: Colors.grey[300],
                    width: MediaQuery.of(context).size.width,
                    height: 1.0,
                  ),
                ],
              )
          ),
        ],
      )
    );
  }

  Widget cardDtAlamat(String title, String dt, IconData icon){
    return Container(
      color: Colors. white,
      child: new Row(
        children: <Widget>[
          new Expanded(
              child: new Column(
                children: <Widget>[
                  new Container(
                    padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Row(
                              children: <Widget>[
                                new Icon(
                                  icon,
                                  size: 16,
                                  color: Colors.grey[800],
                                ),
                                new Container(width: 5,),
                                new Text(
                                  title,
                                  style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 16),
                        ),
                        new Column(
                          children: <Widget>[
                            new Text(
                              dt,
                              style: TextStyle(color: Colors.grey[800], fontSize: 14),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  new Container(
                    color: Colors.grey[300],
                    width: MediaQuery.of(context).size.width,
                    height: 1.0,
                  ),
                ],
              )
          )
        ],
      )
    );
  }

  Widget _header_sch(){
    Widget geriatri;
    if(widget._asgeriatri){
      geriatri = new Container(
        child: widget._geriatri,
      );
    }else{
      geriatri = new Container();
    }
    return new Container(
        child: new Column(
          children: <Widget>[
            new Container(
              color: Colors.white,
              padding: EdgeInsets.only(top:16, bottom: 16, left: 24, right: 24),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Container(
                    width: 80.0,
                    height: 80.0,
                    padding: EdgeInsets.all(1.0),
                    decoration: new BoxDecoration(
                        borderRadius: new BorderRadius.all(new Radius.circular(100.0)),
                        color: Colors.grey[300]
                    ),
                    alignment: Alignment.center,
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(widget._drimage),
                      minRadius: 90,
                      maxRadius: 150,
                    ),
                  ),
                  new Expanded(
                    flex: 2,
                    child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Container(
                            alignment: Alignment.topLeft,
                            padding: EdgeInsets.only(left:10.0, right:10.0),
                            child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    alignment: Alignment.topLeft,
                                    child:new Text(
                                      widget._drname,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      softWrap: false,
                                      style: TextStyle(fontWeight: FontWeight.w700, color: Colors.grey[800], fontFamily: "NeoSansBold", fontSize: 14.0),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                  new Container(
                                    alignment: Alignment.topLeft,
                                    child: new Text(
                                      widget._spc_id+" ("+widget._spc_en+")",
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      softWrap: false,
                                      style: TextStyle(color: Colors.grey[800], fontSize: 12.0),
                                    ),
                                  ),
                                  geriatri,
                                ]
                            ),
                          ),
                        ]
                    ),
                  ),
                ],
              ),
            ),
            new Container(
              width: double.infinity,
              height: 1.0,
              color: Colors.grey[300],
            ),
          ],
        )
    );
  }

  Widget _btnNext(){
    return new Container(
      color: Colors.white,
      padding: EdgeInsets.only(top: 40, bottom: 40, left: 24, right: 24),
      child: Column(
          children: <Widget>[
            new Align(
              alignment: Alignment.bottomCenter,
              child: RawMaterialButton(
                onPressed: (){
                  List<Map<String, dynamic>> dtNext=[];
                  dtNext.add({
                    "_cabang": widget._cabang.toString(),
                    "_pxBaru": widget._pxBaru.toString(),
                    "_rm": widget._rm.toString(),
                    "_namapx": widget._namapx.toString(),
                    "_gender": widget._gender.toString(),
                    "_tgllahir": widget._tgllahir.toString(),
                    "_email": widget._email.toString(),
                    "_mobile": widget._mobile.toString(),
                    "_alamat": widget._alamat.toString(),
                    "_bpjs_id": widget._bpjs_id.toString(),
                    "_typepx": widget._typepx.toString(),
                    "_typepx_id": widget._typepx_id.toString(),
                    "_typepx_en": widget._typepx_en.toString(),
                    "_nobpjs": widget._nobpjs.toString(),
                    "_norujukan": widget._norujukan.toString(),
                    "_note": widget._note.toString(),
                    "_Namacabang": widget._Namacabang.toString(),
                    "_BranchCode": widget._BranchCode.toString(),
                    "_cabang": widget._cabang.toString(),
                    "_dridweb": widget._dridweb.toString(),
                    "_drimage": widget._drimage.toString(),
                    "_secid": widget._secid.toString(),
                    "_spc_id": widget._spc_id.toString(),
                    "_spc_en": widget._spc_en.toString(),
                    "_drname": widget._drname.toString(),
                    "_NamaDokterPengganti": widget._NamaDokterPengganti.toString(),
                    "_DokterID": widget._DokterID.toString(),
                    "_SectionID": widget._SectionID.toString(),
                    "_Tanggal": widget._Tanggal.toString(),
                    "_WaktuID": widget._WaktuID.toString(),
                    "_Hari": widget._Hari.toString(),
                    "_JmlAntrian": widget._JmlAntrian.toString(),
                    "_Realisasi": widget._Realisasi.toString(),
                    "_Cancel": widget._Cancel.toString(),
                    "_DokterPenggantiID": widget._DokterPenggantiID.toString(),
                    "_NoAntrianTerakhir": widget._NoAntrianTerakhir.toString(),
                    "_SectionName": widget._SectionName.toString(),
                    "_FromJam": widget._FromJam.toString(),
                    "_ToJam": widget._ToJam.toString(),
                    "_time": widget._FromJam+"-"+widget._ToJam.toString(),
                    "_WaktuFromJam": widget._WaktuFromJam.toString(),
                    "_WaktuToJam": widget._WaktuToJam.toString(),
                    "_sch": widget._sch.toString(),
                    "_BPJS": widget._BPJS.toString(),
                    "_closed": widget._closed.toString(),
                  });
                  showDialog(
                    barrierDismissible: false,
                    context: context,
                    builder: (BuildContext context) => showNotif(context, null, dtNext),
                  );
                  //_postData(dtNext);
                },
                elevation: 0,
                padding: EdgeInsets.all(16),
                animationDuration: Duration(milliseconds: 500),
                fillColor: widget._color1,
                splashColor: widget._color2,
                highlightColor: widget._color2,
                highlightElevation: 0,
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.grey[400]),
                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                ),
                constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Expanded(
                      flex: 1,
                      child: new Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Column(children: <Widget>[
                            Text(
                              "Kirim",
                              style: TextStyle(color: Colors.white, fontSize: 18),
                            ),
                          ],),
                        ],
                      ),
                    ),
                    Icon(
                      FontAwesomeIcons.arrowRight,
                      color: Colors.white,
                      size: 16,
                    ),
                  ],
                ),
              ),
            ),
          ]
      ),
    );
  }

  bool sendingData = true;
  @override
  Future<String> _postData(context, List<Map<String, dynamic>> dtNext) async {
    var jsonResponse;
    if(sendingData){
      var db = new Http_Controller();
      var _data = await db.postResponse("https://suryahusadha.com/api/send", dtNext);
      jsonResponse = convert.jsonEncode(_data);
      Navigator.pop(context);
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => showNotif(context, jsonResponse.toString(), null),
      );
    }
    return jsonResponse.toString();
  }

  Widget showNotif(BuildContext context, _jsonResponse, List<Map<String, dynamic>> dtNext){
    if(dtNext != null){
      _postData(context, dtNext);
      sendingData = false;
      return new Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.grey,
        ),
      );
    } else {
      if(_jsonResponse != "" || _jsonResponse != null) {
        var decode = convert.jsonDecode(_jsonResponse);
        if(decode["statusCode"] == 200){
          var jsonResponse = convert.jsonDecode(decode["responseBody"]);
          print("msg: "+jsonResponse["rsv"][0]["msg"].toString());
          String norsv = jsonResponse["rsv"][0]["norsv"];
          String msg = jsonResponse["rsv"][0]["msg"];
          if(norsv!=""){
            msg = "No. Rservasai:\n"+jsonResponse["rsv"][0]["norsv"] + "\n\n" + jsonResponse["rsv"][0]["msg"];
          };
          //gagal
          if(jsonResponse["rsv"][0]["response"] == "0"){
            return Dialogue(context, msg, Colors.redAccent, widget._color2, null, null);
          }
          //sukses
          if(jsonResponse["rsv"][0]["response"] == "1"){
            return Dialogue(context, msg, widget._color1, widget._color2, widget._cabang, null);
          }
          //sudah reservasi
          if(jsonResponse["rsv"][0]["response"] == "2"){
            return Dialogue(context, msg, Colors.redAccent, widget._color2, "ReservasiList", dtNext);
          }
        }else{
          return Dialogue(context,
              "Mohon maaf, terjadi kesalahan pengolahan data pada saat simpan data, silahkan ulangi beberapa saat lagi!",
              Colors.red, Colors.red, null, null);
        }
      }else {
        return Dialogue(context,
            "Mohon maaf, terjadi kesalahan koneksi pada saat simpan data, silahkan ulangi beberapa saat lagi!",
            Colors.red, Colors.red, null, null);
      }
    }
  }

}