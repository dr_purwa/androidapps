import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:surya_husadha/components/com_reservasi/reservasi_models.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:convert' as convert;

class Doctors extends StatefulWidget {

  int _clinicSelect;
  int _drSelect;
  String _cabang;
  String _Namacabang;
  String _BranchCode;
  String _idsp;
  String _titlesp_en;
  String _titlesp_id;
  String _secid;

  Color _color1;
  Color _color2;
  Color _Selected_color;
  var _akun;
  Doctors(List dtDoctors,akun){
    this._akun = akun;
    dtDoctors.forEach((dt){
      this._clinicSelect = dt["_clinicSelect"];
      this._drSelect = dt["_drSelect"];
      this._Namacabang = dt["_Namacabang"];
      this._BranchCode = dt["_BranchCode"];
      this._cabang = dt["_cabang"];
      this._color1 = dt["_color1"];
      this._color2 = dt["_color2"];
      this._Selected_color = dt["_Selected_color"];
      this._idsp=dt["_idsp"];
      this._titlesp_id=dt["_titlesp_id"];
      this._titlesp_en=dt["_titlesp_en"];
      this._secid=dt["_secid"];
      if(_cabang=="denpasar"){
        this._Selected_color= WarnaCabang.shh2.withOpacity(0.3);
      }
      if(_cabang=="ubung"){
        this._Selected_color= WarnaCabang.ubung2.withOpacity(0.3);
      }
      if(_cabang=="nusadua"){
        this._Selected_color= WarnaCabang.nusadua2.withOpacity(0.3);
      }
      if(_cabang=="kmc"){
        this._Selected_color= WarnaCabang.kmc2.withOpacity(0.3);
      }
    });
  }

  @override
  _DoctorsState createState() => new _DoctorsState();
}

class _DoctorsState extends State<Doctors>{
  String filter;
  bool focused=false;
  bool viewsearch=false;
  bool viewheader=true;
  int itemsLength;
  List<Doctors_mdl> _mDoctors_mdl = [];
  FocusNode _search  = FocusNode();
  TextEditingController editingController = new TextEditingController();
  final AsyncMemoizer _memoizer = AsyncMemoizer();
  final _Scafoltkey = GlobalKey<ScaffoldState>();

  @override
  initState() {
    super.initState();
    editingController.addListener(() {
      setState(() {
        filter = editingController.text;
      });
    });
  }

  @override
  void dispose() {
    editingController.removeListener((){
      filter = editingController.text;
    });
    super.dispose();
  }

  Future<String> _getData() async {
    var _data;
    var db = new Http_Controller();
    var http_response = await db.getResponse("https://suryahusadha.com/api/doctors/"+widget._idsp+"/"+widget._cabang+"/"+widget._secid);
    _data = convert.jsonEncode(http_response);
    return _data;
  }

  bool isloading = true;

  Future<List<Doctors_mdl>> fetchDocs() async {
    if(isloading) {
      if (_mDoctors_mdl.length == 0 && _mDoctors_mdl.isEmpty) {
        this._memoizer.runOnce(() async {
          _getData().then((String response) {
            setState(() {
              var decode = convert.jsonDecode(response);
              var decodeConvert = convert.jsonDecode(decode["responseBody"]);
              final dtkonten = decodeConvert["doctors"];
              final doctor = dtkonten[0]["data"];
              if(doctor.length > 0) {
                isloading = false;
                doctor.forEach((dt) => {
                  if(dt["secid"] != ""){
                    widget._secid = dt["secid"],
                  },
                  _mDoctors_mdl.add(new Doctors_mdl(
                    BranchCode: dt["BranchCode"],
                    image: dt["image"],
                    nama: dt["nama"],
                    dridapi: dt["dridapi"],
                    secid: dt["secid"],
                    officer_id: dt["officer_id"],
                    titlesp_id: dt["titlesp_id"],
                    titlesp_en: dt["titlesp_en"],
                  )),
                });
              }else{
                isloading = false;
              }
            });
          });
        });
      }
    }
    return _mDoctors_mdl;
  }

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> dtBack=[];
    dtBack.add({
      "_cabang": widget._cabang,
      "_clinicSelect": widget._clinicSelect,
    });
    RouteHelper(context, "Clinics", dtBack);
  }

  @override
  Widget build(BuildContext context) {
    if(viewsearch && focused){
      setState(() {
        FocusScope.of(context).requestFocus(_search);
      });
    }else{
      setState(() {
        FocusScope.of(context).requestFocus(FocusNode());
      });
    }
    return new SafeArea(
      child: new Scaffold(
        key: _Scafoltkey,
        drawer: drawerController(widget._akun, widget._cabang, "Clinics"),
        appBar: new PreferredSize(
          preferredSize: Size.fromHeight(200.0),
          child: AppBarController(widget._cabang, _Scafoltkey, widget._akun, "Doctors"),
        ),
        body:new WillPopScope(
          onWillPop: _onWillPop,
          child: new Column(
            children: <Widget>[
              _header_spc(),
              new FutureBuilder(
                future: fetchDocs(),
                builder: (context, snapshot) {
                  if(snapshot.hasData && _mDoctors_mdl.isNotEmpty) {
                    return new Expanded(
                      child: ListView.builder(
                          itemCount: snapshot.data.length,
                          itemBuilder:(BuildContext context, int index) {
                            if(filter == null || filter == ""){
                              return rowlist(index);
                            }else{
                              if(_mDoctors_mdl.elementAt(index).nama.toLowerCase().contains(filter.toLowerCase())){
                                return rowlist(index);
                              }else{
                                new Container();
                              }
                            }
                          }
                      ),
                    );
                  }else{
                    if(isloading){
                      return Container(
                        margin: EdgeInsets.only(top: 40),
                        child: Column(
                          children: <Widget>[
                            Center(
                              child: SizedBox(
                                  width: 80.0,
                                  height: 80.0,
                                  child: const CircularProgressIndicator(
                                    backgroundColor: Colors.grey,
                                  )),
                            )
                          ],
                        ),
                      );
                    }else{
                      return Container(
                        margin: EdgeInsets.only(top: 40),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Text("Data tidak ditemukan!")
                          ],
                        ),
                      );
                    }
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _header_spc(){
    return new Container(
        margin: EdgeInsets.only(bottom: 5),
        color: Colors.white,
        child: new Column(
          children: <Widget>[
            new Visibility(
              visible: viewsearch,
              child: new Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: TextField(
                  textInputAction: TextInputAction.done,
                  onTap: ()=> setState((){
                    _focused(true);
                  }),//
                  onEditingComplete: () => setState((){
                    _focused(false);
                  }),//
                  focusNode: _search,
                  controller: editingController,
                  cursorColor: Colors.grey,
                  style: TextStyle(color: Colors.grey,fontSize: 18),
                  decoration: InputDecoration(
                    hintText: "Cari Dokter",
                    border: InputBorder.none,
                    labelStyle: TextStyle(
                      color: Colors.grey,
                    ),
                    prefixIcon: Icon(
                      FontAwesomeIcons.search,
                      color: Colors.grey,
                    ),
                    suffixIcon: IconButton(
                      onPressed: ()=> setState((){
                        headerChange(true, false);
                        _focused(false);
                        editingController.clear();
                      }),
                      icon: Icon(
                        FontAwesomeIcons.times,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            new Visibility(
              visible: viewheader,
              child: new Container(
                padding: EdgeInsets.only(top:10, bottom: 10, left: 16, right: 16),
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Container(
                      width: 50.0,
                      height: 50.0,
                      padding: EdgeInsets.all(8.0),
                      decoration: new BoxDecoration(
                          borderRadius: new BorderRadius.all(new Radius.circular(100.0)),
                          color: Colors.grey[600]
                      ),
                      alignment: Alignment.center,
                      child: new Icon(
                        FontAwesomeIcons.userMd,
                        color: Colors.white,
                        size: 24.0,
                      ),
                    ),
                    new Expanded(
                      flex: 2,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(left:10.0, right:10.0),
                              child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child:new Text(
                                        "Reservasi "+widget._cabang,
                                        style: TextStyle(fontFamily: "NeoSansBold", fontSize: 18.0),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Pilih Dokter "+ widget._titlesp_id,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                        style: TextStyle(color: Colors.grey, fontSize: 14.0),
                                      ),
                                    ),
                                  ]
                              ),
                            ),
                          ]
                      ),
                    ),
                    new Container(
                      child: new GestureDetector(
                        child: new Container(
                          height: 40.0,
                          width: 40.0,
                          alignment: Alignment.centerRight,
                          child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.all(6.0),
                                  alignment: Alignment.centerRight,
                                  child: new Icon(
                                    FontAwesomeIcons.search,
                                    color: Colors.grey,
                                    size: 24.0,
                                  ),
                                ),
                              ]
                          ),
                        ),
                        onTap: ()=> setState((){
                          headerChange(false, true);
                          _focused(true);
                        }),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            new Container(
              margin: EdgeInsets.only(top: 5.0),
              width: double.infinity,
              height: 1.0,
              color: Colors.grey[200],
            ),
          ],
        )
    );
  }

  Widget rowlist(int index){
    Color ActivColor;
    if(widget._drSelect == index){
      ActivColor = widget._Selected_color;
    }else{
      ActivColor = Colors.white;
    }
    return new Container(
      margin: EdgeInsets.only(bottom: 5),
      color: ActivColor,
      child: new InkWell(
          onTap:(){
            List<Map<String, dynamic>> dtNext=[];
            setState((){
              widget._drSelect = index;
            });
            dtNext.add({
              "_clinicSelect": widget._clinicSelect,
              "_drSelect": index,
              "_Namacabang": widget._Namacabang,
              "_BranchCode": widget._BranchCode,
              "_cabang": widget._cabang,
              "_color1": widget._color1,
              "_color2": widget._color2,
              "_Selected_color": widget._Selected_color,
              "_idsp": widget._idsp,
              "_secid": widget._secid,
              "_drimage": _mDoctors_mdl.elementAt(index).image,
              "_drname": _mDoctors_mdl.elementAt(index).nama,
              "_titlesp_en": _mDoctors_mdl.elementAt(index).titlesp_en,
              "_titlesp_id": _mDoctors_mdl.elementAt(index).titlesp_id,
              "_dridweb": _mDoctors_mdl.elementAt(index).officer_id,
              "_secid": widget._secid,
              "_dridapi": _mDoctors_mdl.elementAt(index).dridapi,
            });
            RouteHelper(context, "Schedules", dtNext);
          },
          child: new Column(
            children: <Widget>[
              new Container(
                padding: EdgeInsets.only(top:5, bottom: 5, left: 16, right: 16),
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Container(
                      width: 60.0,
                      height: 60.0,
                      padding: EdgeInsets.all(1.0),
                      decoration: new BoxDecoration(
                          borderRadius: new BorderRadius.all(new Radius.circular(100.0)),
                          color: Colors.grey[300]
                      ),
                      alignment: Alignment.center,
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(_mDoctors_mdl.elementAt(index).image),
                        minRadius: 90,
                        maxRadius: 150,
                      ),
                    ),
                    new Expanded(
                      flex: 2,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(left:10.0, right:10.0),
                              child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child:new Text(
                                        _mDoctors_mdl.elementAt(index).nama,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                        style: TextStyle(fontFamily: "NeoSansBold", fontSize: 14.0, color: Colors.grey[800]),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        _mDoctors_mdl.elementAt(index).titlesp_id +" ("+_mDoctors_mdl.elementAt(index).titlesp_en+")",
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ]
                              ),
                            ),
                          ]
                      ),
                    ),
                    new Container(
                      height: 40.0,
                      width: 40.0,
                      alignment: Alignment.centerRight,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.all(6.0),
                              alignment: Alignment.centerRight,
                              child: new Icon(
                                FontAwesomeIcons.angleRight,
                                color: Colors.grey,
                                size: 18.0,
                              ),
                            ),
                          ]
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                margin: EdgeInsets.only(top: 5.0),
                width: double.infinity,
                height: 1.0,
                color: Colors.grey[200],
              ),
            ],
          )
      ),
    );
  }
  _focused(bool focus){
    focused=focus;
  }

  headerChange(bool header, bool search) {
    viewsearch=search;
    viewheader=header;
  }
}
