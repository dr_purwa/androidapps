import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_picker/Picker.dart';
import 'package:surya_husadha/components/com_login/com_login.dart';
import 'package:surya_husadha/components/com_reservasi/detail_rsv.dart';
import 'package:surya_husadha/components/com_reservasi/reservasi_models.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/dialogue/customDialogue.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/helper/transition/slide_route.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:surya_husadha/pages/widgetpage.dart';
import 'dart:convert' as convert;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'dart:ui' as ui;

import 'package:masked_text_input_formatter/masked_text_input_formatter.dart';

class ReservasiList extends StatefulWidget {
  String _cabang;
  Color calselect;
  Color _color1;
  Color _color2;
  bool _viewlist;
  String _emailval;
  String _bodval;
  var _akun;

  ReservasiList(List params, akun){
    this._akun = akun;
    params.forEach((dt){
      this._emailval = dt["_email"];
      this._bodval = dt["_bod"];
      this._cabang = dt["_cabang"];
      if(_cabang=="group"){
        this.calselect = WarnaCabang.shh;
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
      }
      if(_cabang=="denpasar"){
        this.calselect = WarnaCabang.shh;
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
      }
      if(_cabang=="ubung"){
        this.calselect = WarnaCabang.ubung;
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
      }
      if(_cabang=="nusadua"){
        this.calselect = WarnaCabang.nusadua;
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
      }
      if(_cabang=="kmc"){
        this.calselect = WarnaCabang.kmc;
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
      }
    });
  }
  @override
  _FitureContent createState() => new _FitureContent();
}

class _FitureContent extends State<ReservasiList> with WidgetsBindingObserver, NavigatorObserver {
  List<Reservasi_Mdl> _mReservasiMdl = [];
  final _Scafoltkey = GlobalKey<ScaffoldState>();

  var msg;
  String filter;
  FocusNode _search  = FocusNode();
  TextEditingController _searchController = new TextEditingController();

  //_tgllahir
  bool _tgllahirEna = true;
  bool _tgllahirNull = false;
  FocusNode _tgllahir = new FocusNode();
  TextEditingController _tgllahirController = new TextEditingController();
  //_email
  bool _emailNull = false;
  FocusNode _email = new FocusNode();
  TextEditingController _emailController = new TextEditingController();

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _searchController.addListener(() {
      filter = _searchController.text;
    });
  }
  @override
  void dispose(){
    WidgetsBinding.instance.removeObserver(this);
    _searchController.removeListener((){
      filter = _searchController.text;
    });
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    final value = WidgetsBinding.instance.window.viewInsets.bottom;
    if (value > 0) {
      _onKeyboardChanged(true);
    } else {
      _onKeyboardChanged(false);
    }
  }

  _onKeyboardChanged(bool isVisible) {
    if (!isVisible) {
      FocusScope.of(context).requestFocus(new FocusNode());
    }
  }

  //Future<bool> _onWillPop(){
    //Navigator.pop(context);
  //}

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> dtBack=[];
    dtBack.add({
      "_cabang": widget._cabang,
    });
    RouteHelper(context, widget._cabang, null);
  }

  Widget _showDiaogue(BuildContext context){
    return new Stack(
      children: <Widget>[
        Container(
          color: Colors.black.withOpacity(0.3),
        ),
        new ListView(
          physics: ScrollPhysics(),
          scrollDirection: Axis.vertical,
          children: <Widget>[
            new Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 120, left: 24, right: 24),
                  child: Column(
                    children: <Widget>[
                      new Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(20.0)),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 10.0,
                              offset: const Offset(0.0, 10.0),
                            ),
                          ],
                        ),
                        padding: EdgeInsets.only(left: 16, right: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                                margin: EdgeInsets.only(top: 10),
                                child: InkWell(
                                  onTap: (){
                                    _onWillPop();
                                  },
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Icon(
                                        FontAwesomeIcons.times,
                                        size: 22,
                                        color: Colors.grey[400],
                                      )
                                    ],
                                  ),
                                )
                            ),
                            new Padding(
                              padding: EdgeInsets.only(top: 55),
                            ),
                            new Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Icon(
                                  FontAwesomeIcons.infoCircle,
                                  size: 14,
                                ),
                                new Padding(padding: EdgeInsets.only(left: 5)),
                                new Container(
                                  padding: EdgeInsets.only(top: 2),
                                  child: new Text(
                                    "Informasi",
                                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700,),
                                  ),
                                ),
                              ],
                            ),
                            new Padding(
                              padding: EdgeInsets.only(top:20),
                            ),
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                new Expanded(
                                  flex: 1,
                                  child: new Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: new Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        new Container(
                                          margin: EdgeInsets.only(bottom: 20),
                                          width: double.infinity,
                                          height: 1.0,
                                          color: Colors.grey[200],
                                        ),
                                        new Text(
                                          "Untuk lebih mudah menggunakan aplikasi ini, kami anjurkan untuk login terlebih dahulu, atau lengkapi formulir berikut unutk melanjutkan!",
                                          style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.grey[600],
                                            fontFamily: "NeoSansBold",
                                          ),
                                        ),
                                        //FORM
                                        new Container(
                                            margin: EdgeInsets.only(top:20),
                                            child: TextField(
                                              focusNode: _email,
                                              controller: _emailController,
                                              textInputAction: TextInputAction.next,
                                              onEditingComplete: () => {
                                                FocusScopeNode().requestFocus(_tgllahir),
                                                showPickerDate(context),
                                              },
                                              cursorColor: Colors.grey[800],
                                              style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                              keyboardType: TextInputType.emailAddress,
                                              decoration: InputDecoration(
                                                filled: true,
                                                fillColor: Colors.white.withOpacity(0.6),
                                                labelStyle: TextStyle(
                                                  color: Colors.grey[800],
                                                ),
                                                labelText: 'Email',
                                                errorText: _emailNull ? "" : null,
                                                prefixIcon: Icon(
                                                  FontAwesomeIcons.at,
                                                  color: Colors.grey[800],
                                                ),
                                                focusedBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                enabledBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                  borderSide: BorderSide(
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                              ),
                                            )
                                        ),

                                        new Container(
                                            margin: EdgeInsets.only(top:20),
                                            child: TextField(
                                              enabled: _tgllahirEna,
                                              onTap: (){
                                                showPickerDate(context);
                                                FocusScope.of(context).requestFocus(new FocusNode());
                                              },
                                              inputFormatters: [
                                                MaskedTextInputFormatter(
                                                  mask: 'xx-xx-xxxx',
                                                  separator: '-',
                                                ),
                                              ],
                                              focusNode: _tgllahir,
                                              controller: _tgllahirController,
                                              textInputAction: TextInputAction.done,
                                              cursorColor: Colors.grey[800],
                                              style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                              keyboardType: TextInputType.number,
                                              decoration: InputDecoration(
                                                filled: true,
                                                fillColor: Colors.white.withOpacity(0.6),
                                                labelStyle: TextStyle(
                                                  color: Colors.grey[800],
                                                ),
                                                labelText: 'Tgl Lahir (dd-mm-YYYY)',
                                                errorText: _tgllahirNull ? "" : null,
                                                prefixIcon: Icon(
                                                  FontAwesomeIcons.calendarCheck,
                                                  color: Colors.grey[800],
                                                ),
                                                focusedBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                enabledBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                  borderSide: BorderSide(
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                              ),
                                            )
                                        ),

                                        new Container(
                                          margin: EdgeInsets.only(top: 20),
                                          width: double.infinity,
                                          height: 1.0,
                                          color: Colors.grey[200],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            new Container(
                                margin: EdgeInsets.only(top: 16),
                                color: Colors.white,
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: <Widget>[
                                        new FlatButton(
                                            onPressed: (){
                                              //_onWillPop();
                                              Navigator.push(context, SlideLeftRoute(page: Fullpage(widgetpage: LoginController(widget._cabang),pagename: "LoginPage",)));//ok
                                              //Navigator.of(context).pop();
                                            },//
                                            color: Colors.redAccent,
                                            child: new Row(
                                              children: <Widget>[
                                                new Container(
                                                  padding: EdgeInsets.only(right: 5, top: 2),
                                                  child: new Text(
                                                    "Login",
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: Colors.white,
                                                      fontFamily: "NeoSansBold",
                                                    ),
                                                  ),
                                                ),
                                                new Icon(
                                                  FontAwesomeIcons.signInAlt,
                                                  size: 14,
                                                  color: Colors.white,
                                                ),
                                              ],
                                            )
                                        )
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: <Widget>[
                                        new FlatButton(
                                            onPressed: (){
                                              bool next = true;
                                              setState(() {
                                                if(_emailController.text.isEmpty){
                                                  next = false;
                                                  _emailNull = true;
                                                }
                                                if(_tgllahirController.text.isEmpty){
                                                  next = false;
                                                  _tgllahirNull = true;
                                                }
                                                if(next){
                                                  widget._emailval = _emailController.text;
                                                  widget._bodval = _tgllahirController.text;
                                                  showDialog(
                                                    barrierDismissible: false,
                                                    context: context,
                                                    builder: (BuildContext context) => _validasi(context),
                                                  );
                                                }
                                              });
                                              //Navigator.of(context).pop();
                                            },//
                                            shape: Border.all(width: 1,color: Colors.grey[300],style: BorderStyle.solid),
                                            child: new Row(
                                              children: <Widget>[
                                                new Container(
                                                  padding: EdgeInsets.only(right: 5, top: 4),
                                                  child: new Text(
                                                    "OK",
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: Colors.grey[500],
                                                      fontFamily: "NeoSansBold",
                                                    ),
                                                  ),
                                                ),
                                                new Icon(
                                                  FontAwesomeIcons.arrowRight,
                                                  size: 14,
                                                  color: Colors.grey[500],
                                                ),
                                              ],
                                            )
                                        )
                                      ],
                                    ),
                                  ],
                                )
                            ),
                            new Container(
                              margin: EdgeInsets.only(bottom: 20),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: 60,
                  width: MediaQuery.of(context).size.width,
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Container(
                        width: 140.0,
                        height: 140.0,
                        decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white, width: 5),
                          color: widget._color1,
                        ),
                        padding: EdgeInsets.all(5),
                        child: new ClipOval(
                          child: new Image.asset("assets/suri/suri2.png"),
                        ),
                      ),
                    ],
                  ),
                ),

              ],
            )
          ],
        ),
      ],
    );
  }

  Widget _buildcontent(BuildContext context) {
    return new Column(
      children: <Widget>[
        _searchHeader(),
        new FutureBuilder(
            future: fetchList(),
            builder: (context, snapshot){
              if(snapshot.hasData){
                return new Expanded(
                  child: ListView.builder(
                    itemCount: _mReservasiMdl.length,
                    scrollDirection: Axis.vertical,
                    itemBuilder: (context, index) {
                      if(index <= _mReservasiMdl.length){
                        if(filter == null || filter == ""){
                          return _rowContentList(_mReservasiMdl, index);
                        }else{
                          if(_mReservasiMdl.elementAt(index).officer_name.toLowerCase().contains(filter.toLowerCase()) ||
                              _mReservasiMdl.elementAt(index).gelardepan.toLowerCase().contains(filter.toLowerCase()) ||
                              _mReservasiMdl.elementAt(index).gelarbelakang.toLowerCase().contains(filter.toLowerCase()) ||
                              _mReservasiMdl.elementAt(index).rsv_no.toLowerCase().contains(filter.toLowerCase()) ||
                              _mReservasiMdl.elementAt(index).date.toLowerCase().contains(filter.toLowerCase()) ||
                              _mReservasiMdl.elementAt(index).day.toLowerCase().contains(filter.toLowerCase()) ||
                              _mReservasiMdl.elementAt(index).timeopen.toLowerCase().contains(filter.toLowerCase()) ||
                              _mReservasiMdl.elementAt(index).branch_name.toLowerCase().contains(filter.toLowerCase())
                          ){
                            return _rowContentList(_mReservasiMdl, index);
                          }else{
                            return new Container();
                          }
                        }
                      }else{
                        return new Container();
                      }
                    },
                  ),
                );
              }else{
                return new Container(
                    margin: EdgeInsets.only(top: 40, bottom: 10),
                    child: Center(
                      child: SizedBox(
                          width: 80.0,
                          height: 80.0,
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.grey[300],
                          )
                      ),
                    )
                );
              }
            }
        ),
      ],
    );
  }

  Widget _rowContentList(List<Reservasi_Mdl> dtrsv, index) {
    var dt = dtrsv[index];
    var sch = dt.date.toString() +" "+dt.close.toString();
    var schdate = DateTime.parse(sch);
    var timeLeft = schdate.difference(DateTime.now()).inHours;
    Widget cardmodel;
    bool activePage;
    if(timeLeft > 0 ){
      cardmodel = _active(dt);
      activePage = true;
    }else{
      cardmodel = _nonactive(dt);
      activePage = false;
    }

    return new Container(
      color: Colors.white,
      margin: EdgeInsets.only(top: 5.0),
      child: InkWell(
        onTap: (){
          showDialog(
            barrierDismissible: true,
            context: context,
            builder: (BuildContext context){
              return new DetailRsv(dt, activePage, widget._cabang, widget._akun);
            },
          ).then((dt){
            if(dt!=null){
              setState(() {
                _validasitrue = false;
                _mReservasiMdl.length = 0;
                _mReservasiMdl.clear();
              });
            }
          });
        },
        child: cardmodel,
      ),
    );
  }

  Widget _active(dt){
    Widget rate;
    if(dt.star == "" || dt.star == "0"){
      rate = new Container(
        padding: EdgeInsets.only(top: 2),
        alignment: Alignment.centerRight,
        child: new Icon(
          FontAwesomeIcons.solidCircle,
          color: Colors.red,
          size: 8.0,
        ),
      );
    }else{
      rate = new Container();
    }
    var sch = dt.date.toString() +" "+dt.close.toString();
    var schdate = DateTime.parse(sch);
    final dateFormatter = DateFormat('dd-MM-yyyy');
    final dateString = dateFormatter.format(schdate);

    return new Container(
      padding: EdgeInsets.only(top:10, bottom: 10, left: 16, right: 16),
      child:new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Stack(
            children: <Widget>[
              new Container(
                width: 80.0,
                height: 80.0,
                padding: EdgeInsets.all(1.0),
                decoration: new BoxDecoration(
                    borderRadius: new BorderRadius.all(new Radius.circular(100.0)),
                    color: Colors.grey[300]
                ),
                alignment: Alignment.center,
                child: CircleAvatar(
                  backgroundImage: NetworkImage(dt.image),
                  minRadius: 90,
                  maxRadius: 150,
                ),
              ),
              Positioned(
                  bottom: 0,
                child: new Container(
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                    color: Colors.red,
                    shape: BoxShape.circle,
                  ),
                  child: Text(
                    dt.noUrut,
                    style: TextStyle(color: Colors.white, fontSize: 12),
                  ),
                )
              )
            ],
          ),
          new Expanded(
            flex: 2,
            child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  new Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(left:10.0, right:10.0),
                    child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Container(
                            alignment: Alignment.topLeft,
                            child:new Text(
                              dt.gelardepan+dt.officer_name+dt.gelarbelakang,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              softWrap: false,
                              style: TextStyle(fontFamily: "NeoSansBold", fontSize: 14.0, color: Colors.grey[800], fontWeight: FontWeight.w700),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          new Container(
                            alignment: Alignment.topLeft,
                            child:new Text(
                              "No.: "+dt.rsv_no,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              softWrap: false,
                              style: TextStyle(fontFamily: "NeoSansBold", fontSize: 12.0, color: Colors.grey[800]),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          new Container(
                            alignment: Alignment.topLeft,
                            child: new Text(
                              dt.branch_name,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              softWrap: false,
                              style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          new Container(
                            alignment: Alignment.topLeft,
                            child: new Text(
                              dt.day +", "+ dateString,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              softWrap: false,
                              style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          new Container(
                            alignment: Alignment.topLeft,
                            child: new Text(
                              dt.timeopen,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              softWrap: false,
                              style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ]
                    ),
                  ),
                ]
            ),
          ),
          new Row(
            children: <Widget>[
              rate,
              new Container(
                alignment: Alignment.centerRight,
                child: new Icon(
                  FontAwesomeIcons.angleRight,
                  color: Colors.grey,
                  size: 18.0,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _nonactive(dt){
    Widget rate;
    if(dt.star == "" || dt.star == "0"){
      rate = new Container(
        padding: EdgeInsets.only(top: 2),
        alignment: Alignment.centerRight,
        child: new Icon(
          FontAwesomeIcons.solidCircle,
          color: Colors.red,
          size: 8.0,
        ),
      );
    }else{
      rate = new Container();
    }
    var sch = dt.date.toString() +" "+dt.close.toString();
    var schdate = DateTime.parse(sch);
    final dateFormatter = DateFormat('dd-MM-yyyy');
    final dateString = dateFormatter.format(schdate);
    return new Column(
      children: <Widget>[
        new Container(
          padding: EdgeInsets.only(top:10, bottom: 10, left: 16, right: 16),
          child:new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              new Stack(
                children: <Widget>[
                  new Container(
                    width: 80.0,
                    height: 80.0,
                    padding: EdgeInsets.all(1.0),
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      borderRadius: new BorderRadius.all(new Radius.circular(100.0)),
                      border: Border.all(width: 1, color: Colors.grey[200]),
                      image: new DecorationImage(
                        image: new NetworkImage(dt.image),
                        fit: BoxFit.cover,
                        colorFilter: new ColorFilter.mode(Colors.white.withOpacity(0.4), BlendMode.dstATop),
                      ),
                    ),
                    alignment: Alignment.center,
                  ),
                  Positioned(
                      bottom: 0,
                      child: new Container(
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          color: Colors.red[100],
                          shape: BoxShape.circle,
                        ),
                        child: Text(
                          dt.noUrut,
                          style: TextStyle(color: Colors.white, fontSize: 12),
                        ),
                      )
                  )
                ],
              ),
              new Expanded(
                flex: 2,
                child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      new Container(
                        alignment: Alignment.topLeft,
                        padding: EdgeInsets.only(left:10.0, right:10.0),
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                alignment: Alignment.topLeft,
                                child:new Text(
                                  dt.gelardepan+dt.officer_name+dt.gelarbelakang,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  softWrap: false,
                                  style: TextStyle(fontFamily: "NeoSansBold", fontSize: 14.0, color: Colors.grey[400], fontWeight: FontWeight.w500),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              new Container(
                                alignment: Alignment.topLeft,
                                child:new Text(
                                  "No.: "+dt.rsv_no,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  softWrap: false,
                                  style: TextStyle(fontFamily: "NeoSansBold", fontSize: 12.0, color: Colors.grey[400]),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              new Container(
                                alignment: Alignment.topLeft,
                                child: new Text(
                                  dt.branch_name,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  softWrap: false,
                                  style: TextStyle(color: Colors.grey[400], fontSize: 12.0),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              new Container(
                                alignment: Alignment.topLeft,
                                child: new Text(
                                  dt.day +", "+ dateString,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  softWrap: false,
                                  style: TextStyle(color: Colors.grey[400], fontSize: 12.0),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              new Container(
                                alignment: Alignment.topLeft,
                                child: new Text(
                                  dt.timeopen,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  softWrap: false,
                                  style: TextStyle(color: Colors.grey[400], fontSize: 12.0),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ]
                        ),
                      ),
                    ]
                ),
              ),
              new Row(
                children: <Widget>[
                  rate,
                  new Container(
                    alignment: Alignment.centerRight,
                    child: new Icon(
                      FontAwesomeIcons.angleRight,
                      color: Colors.grey[400],
                      size: 18.0,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
        new Container(
          margin: EdgeInsets.only(top: 5.0),
          width: double.infinity,
          height: 1.0,
          color: Colors.grey[200],
        ),
      ],
    );
  }

  Widget _searchHeader(){
    return new Container(
        color: Colors.white,
        child: new Column(
          children: <Widget>[
            new Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              child: TextField(
                autofocus: false,
                textInputAction: TextInputAction.done,
                //onTap:
                focusNode: _search,
                controller: _searchController,
                cursorColor: Colors.grey,
                style: TextStyle(color: Colors.grey,fontSize: 16),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(top: 14),
                  hintText: "Cari Reservasi",
                  border: InputBorder.none,
                  labelStyle: TextStyle(
                    color: Colors.grey,
                  ),
                  prefixIcon: Icon(
                    FontAwesomeIcons.search,
                    color: Colors.grey,
                    size: 16,
                  ),
                ),
              ),
            ),
            new Container(
              width: double.infinity,
              height: 1.0,
              color: Colors.grey[200],
            ),
          ],
        )
    );
  }

  String Apiresponse;
  bool _validasitrue = false;
  String _data;

  @override
  Future<String> fetchList() async {
    var api = new Http_Controller();
    if(!_validasitrue) {
      List<Map<String, dynamic>> dtNext = [];
      dtNext.add({
        "email": widget._emailval.toString(),
        "bod": widget._bodval.toString(),
      });
      var http_response = await api.postResponse("https://suryahusadha.com/api/rsvlist/", dtNext);
      _data = convert.jsonEncode(http_response);
      if(_mReservasiMdl.length == 0 && _mReservasiMdl.isEmpty){
        var decode = convert.jsonDecode(_data.toString());
        var decodeConvert = convert.jsonDecode(decode["responseBody"]);
        final dtkonten = decodeConvert["rsvlist"];
        final rsvlist = dtkonten[0]["data"];
        msg = dtkonten[0]["msg"];
        Apiresponse = dtkonten[0]["response"];
        int i = 0;
        rsvlist.forEach((dt) => {
          _mReservasiMdl.add(new Reservasi_Mdl(
            i,
            dt["branchcode"].toString(),
            dt["idrsv"].toString(),
            dt["rsv_no"].toString(),
            dt["qrcode"].toString(),
            dt["noUrut"].toString(),
            dt["bpjsid"].toString(),
            dt["NoRujukanBPJS"].toString(),
            dt["opHid"].toString(),
            dt["schdatetime"].toString(),
            dt["day"].toString(),
            dt["date"].toString(),
            dt["timeopen"].toString(),
            dt["close"].toString(),
            dt["nrm"].toString(),
            dt["userid"].toString(),
            dt["TanggalLahir"].toString(),
            dt["type_pasien"].toString(),
            dt["typepx_id"].toString(),
            dt["typepx_en"].toString(),
            dt["typepx_jpn"].toString(),
            dt["pasienbaru"].toString(),
            dt["namepx"].toString(),
            dt["mothersname"].toString(),
            dt["genderpx"].toString(),
            dt["emailpx"].toString(),
            dt["phonepx"].toString(),
            dt["idcardType"].toString(),
            dt["nik"].toString(),
            dt["enabled"].toString(),
            dt["status"].toString(),
            dt["rsv_time"].toString(),
            dt["note"].toString(),
            dt["star"].toString(),
            dt["comment"].toString(),
            dt["rate_d"].toString(),
            dt["rate_m"].toString(),
            dt["rate_y"].toString(),
            dt["randcheck"].toString(),
            dt["image"].toString(),
            dt["gelardepan"].toString(),
            dt["officer_name"].toString(),
            dt["officer_info"].toString(),
            dt["gelarbelakang"].toString(),
            dt["titlesp_en"].toString(),
            dt["titlesp_id"].toString(),
            dt["titlesp_jpn"].toString(),
            dt["branch_name"].toString(),
            dt["branch_address"].toString(),
            dt["branch_phone"].toString(),
            dt["advanceData"].toString(),
          )),
          i ++,
        });
      }
    }
    return _data;
  }

  Widget _validasi(BuildContext context){
    return FutureBuilder(
        future: fetchList(),
        builder: (context, snapshot){
          if(snapshot.hasData && _mReservasiMdl.length > 0 && _mReservasiMdl.isNotEmpty){
            if(Apiresponse == "1"){
              _validasitrue = true;
              widget._viewlist = true;
            }else{
              _validasitrue = false;
              widget._viewlist = false;
            }
            return Dialogue(context, msg, widget._color1, widget._color2, null, null);
          }else{
            return new Container(
                margin: EdgeInsets.only(top: 40, bottom: 10),
                child: Center(
                  child: SizedBox(
                      width: 80.0,
                      height: 80.0,
                      child: CircularProgressIndicator(
                        backgroundColor: Colors.grey[300],
                      )
                  ),
                )
            );
          }
        }
    );
  }

  showPickerDate(BuildContext context) {
    final f = new DateFormat('dd-MM-yyyy');
    DateTime _Selected;
    if(_tgllahirController.text != ""){
      int d = int.parse(_tgllahirController.text.split("-")[0]);
      int m = int.parse(_tgllahirController.text.split("-")[1]);
      int y = int.parse(_tgllahirController.text.split("-")[2]);
      _Selected = DateTime(y, m ,d);
    }else{
      _Selected = DateTime.now();
    }
    int _yNow = int.parse(DateFormat.y().format(_Selected));
    DateTime _max = DateTime(_yNow-100);

    Picker(
      hideHeader: true,
      changeToFirst: false,
      adapter: DateTimePickerAdapter(
        customColumnType: [0, 1, 2],
        value: _Selected,
        maxValue: DateTime.now(),
        minValue: _max,
      ),
      title: new Container(
        child: Column(children: <Widget>[
          Text("Pilih Tgl. Lahir:", style: TextStyle(color: widget.calselect),),
        ],),
      ),
      selectedTextStyle: TextStyle(color: widget.calselect),
      confirm: new InkWell(
        onTap: ()=>{
          Navigator.of(context).pop(),
          widget,_tgllahirController.text = f.format(_Selected).toString(),
        },
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(children: <Widget>[
            new Icon(
              FontAwesomeIcons.check,
              color: widget.calselect,
              size: 12,
            ),
            Container(width: 5,),
            Text("Pilih", style: TextStyle(color: widget.calselect),)
          ],),
        ),
      ),
      cancel: new InkWell(
        onTap: ()=>{
          Navigator.of(context).pop()
        },
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(children: <Widget>[
            new Icon(
              FontAwesomeIcons.times,
              color: widget.calselect,
              size: 12,
            ),
            Container(width: 2,),
            Text("Tutup", style: TextStyle(color: widget.calselect),)
          ],),
        ),
      ),
      onSelect: (Picker picker, int index, List<int> selecteds) {
        this.setState(() {
          int d = _Selected.day;
          int m = _Selected.month;
          int y = _Selected.year;
          if(index == 2){
            d = (picker.adapter as DateTimePickerAdapter).value.day;
            _Selected = DateTime(y, m, d);
          }
          if(index == 1){
            m = (picker.adapter as DateTimePickerAdapter).value.month;
            _Selected = DateTime(y, m, d);
          }
          if(index == 0){
            y = (picker.adapter as DateTimePickerAdapter).value.year;
            _Selected = DateTime(y, m, d);
          }

        });
      },
    ).showDialog(context);

  }


  @override
  Widget build(BuildContext context) {

    if(widget._emailval !=null && widget._bodval !=null){
      widget._viewlist = true;
    }else{
      widget._viewlist = false;
    }

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.white,
        systemNavigationBarDividerColor: Colors.grey[300],
        systemNavigationBarIconBrightness: Brightness.dark,// navigation bar color
        statusBarColor: widget._color2,
        statusBarIconBrightness: Brightness.dark// statu// status bar color
    ));

    Widget content;
    if(widget._viewlist){
      content = new Scaffold(
        //bottomNavigationBar: NavBottom(widget._cabang),
        key: _Scafoltkey,
        drawer: drawerController(widget._akun, widget._cabang, "ReservasiList"),
        appBar: new PreferredSize(
          preferredSize: Size.fromHeight(200.0),
          child: AppBarController(widget._cabang, _Scafoltkey, widget._akun, "ReservasiList"),
        ),
        body: new WillPopScope(
          onWillPop: _onWillPop,
          child: _buildcontent(context),
        ),
      );
    }else{
      content = new Scaffold(
        body: new WillPopScope(
          onWillPop: _onWillPop,
          child: _showDiaogue(context),
        ),
      );
    }
    return new SafeArea(
      child: content,
    );
  }
}