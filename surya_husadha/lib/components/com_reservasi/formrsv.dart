import 'dart:async';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:surya_husadha/components/com_reservasi/reservasi_models.dart';
import 'package:surya_husadha/helper/dialogue/customDialogue.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:masked_text_input_formatter/masked_text_input_formatter.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'dart:convert' as convert;

class Formrsv extends StatefulWidget {

  bool _pxBaru;
  String _rm;
  String _namapx;
  String _gender;
  String _tgllahir;
  String _email;
  String _mobile;
  String _alamat;
  int _selectedTipepx;
  String _typepx;
  String _typepx_id;
  String _typepx_en;
  String _nobpjs;
  String _norujukan;
  String _note;
  DateTime _currentDate2Select;
  int _clinicSelect;
  int _drSelect;
  String _Namacabang;
  String _BranchCode;
  String _cabang;
  Color _color1;
  Color _color2;
  Color _Selected_color;
  String _idsp;
  String _drimage;
  String _secid;
  String _dridapi;
  String _spc_id;
  String _spc_en;
  String _drname;
  String _titlesp_id;
  String _titlesp_en;
  String _dridweb;
  String _NamaDokterPengganti;
  String _DokterID;
  String _SectionID;
  String _Tanggal;
  int _WaktuID;
  String _Hari;
  int _JmlAntrian;
  bool _Realisasi;
  bool _Cancel;
  String _DokterPenggantiID;
  int _NoAntrianTerakhir;
  String _SectionName;
  String _FromJam;
  String _ToJam;
  String _time;
  String _WaktuFromJam;
  String _WaktuToJam;
  String _sch;
  bool _asgeriatri;
  Widget _geriatri;
  String _BPJS;
  String _imgbpjs;
  Widget _bpjslogo;
  int _closed;
  String _statussch;
  String _status_id;
  String _status_en;
  var _akun;

  String _nrmshh;
  String _nrmub;
  String _nrmnd;
  String _bod;
  String _userid;
  String _name;
  String _bpjs_id;

  Formrsv(List params, akun){
    this._akun = akun;
    if(akun != null){
      if(akun[0]["bpjs_id"] != null){
        this._bpjs_id = akun[0]["bpjs_id"].toString();
      }
      if(akun[0]["name"] != null){
        this._name = akun[0]["name"].toString();
      }
      if(akun[0]["id"] != null){
        this._userid = akun[0]["id"].toString();
      }
      if(akun[0]["bod"] != null){
        this._bod = akun[0]["bod"] ;
      }
      if(akun[0]["nrmshh"] != null){
        this._nrmshh = akun[0]["nrmshh"] ;
      }
      if(akun[0]["nrmub"] != null){
        this._nrmub = akun[0]["nrmub"] ;
      }
      if(akun[0]["nrmnd"] != null){
        this._nrmnd = akun[0]["nrmnd"] ;
      }
    }
    params.forEach((dt) => {
      if(dt["_pxBaru"] == null){
        this._pxBaru=false,
      }else{
        this._pxBaru=dt["_pxBaru"],
      },
      this._rm=dt["_rm"],
      this._namapx=dt["_namapx"],
      this._gender=dt["_gender"],
      this._tgllahir=dt["_tgllahir"],
      this._email=dt["_email"],
      this._mobile=dt["_mobile"],
      this._alamat=dt["_alamat"],
      if(dt["_bpjs_id"] != null){
        this._bpjs_id=dt["_bpjs_id"],
      },
      this._selectedTipepx= dt["_selectedTipepx"],
      this._typepx= dt["_typepx"],
      this._typepx_id= dt["_typepx_id"],
      this._typepx_en= dt["_typepx_en"],
      this._nobpjs= dt["_nobpjs"],
      this._norujukan=dt["_norujukan"],
      this._note=dt["_note"],
      this._currentDate2Select = dt["_currentDate2Select"],
      this._clinicSelect = dt["_clinicSelect"],
      this._drSelect = dt["_drSelect"],
      this._Namacabang = dt["_Namacabang"],
      this._BranchCode = dt["_BranchCode"],
      this._cabang = dt["_cabang"],
      this._color1 = dt["_color1"],
      this._color2 = dt["_color2"],
      this._Selected_color = dt["_Selected_color"],
      this._idsp = dt["_idsp"],
      this._drimage = dt["_drimage"],
      this._drname = dt["_drname"],
      this._titlesp_id = dt["_titlesp_id"],
      this._titlesp_en = dt["_titlesp_en"],
      this._dridweb = dt["_dridweb"],
      this._secid = dt["_secid"],
      this._dridapi = dt["_dridapi"],
      this._spc_id=dt["_spc_id"],
      this._spc_en=dt["_spc_en"],
      this._NamaDokterPengganti=dt["_NamaDokterPengganti"],
      this._DokterID=dt["_DokterID"],
      this._SectionID=dt["_SectionID"],
      this._Tanggal=dt["_Tanggal"],
      this._WaktuID=dt["_WaktuID"],
      this._Hari=dt["_Hari"],
      this._JmlAntrian=dt["_JmlAntrian"],
      this._Realisasi=dt["_Realisasi"],
      this._Cancel=dt["_Cancel"],
      this._DokterPenggantiID=dt["_DokterPenggantiID"],
      this._NoAntrianTerakhir=dt["_NoAntrianTerakhir"],
      this._SectionName=dt["_SectionName"],
      this._FromJam=dt["_FromJam"],
      this._ToJam=dt["_ToJam"],
      this._time=dt["_time"],
      this._WaktuFromJam=dt["_WaktuFromJam"],
      this._WaktuToJam=dt["_WaktuToJam"],
      this._sch=dt["_sch"],
      this._asgeriatri=dt["_asgeriatri"],
      this._geriatri=dt["_geriatri"],
      this._BPJS=dt["_BPJS"],
      this._imgbpjs=dt["_imgbpjs"],
      this._bpjslogo=dt["_bpjslogo"],
      this._closed=dt["_closed"],
      this._statussch=dt["_statussch"],
      this._status_id=dt["_status_id"],
      this._status_en = dt["_status_en"],
    });
  }

  @override
  _FormrsvController createState() => new _FormrsvController();
}

class _FormrsvController extends State<Formrsv> with SingleTickerProviderStateMixin, WidgetsBindingObserver{

  ScrollController listScrollController = ScrollController();
  final _ScafoltkeyFrm= GlobalKey<ScaffoldState>();
  bool selectYears = false;
  Color colorfill_lama;
  Color highlight_lama;
  Color colorfill_baru;
  Color highlight_baru;

  int _validasiPx = 0;
  //_rm
  bool _rmEna = true;
  bool _rmNull = false;
  FocusNode _rm = new FocusNode();
  TextEditingController _rmController = new TextEditingController();
  //_tgllahir
  bool _tgllahirEna = true;
  bool _tgllahirNull = false;
  FocusNode _tgllahir = new FocusNode();
  TextEditingController _tgllahirController = new TextEditingController();
  //_nama
  bool _namaEna = true;
  bool _namaNull = false;
  FocusNode _nama = new FocusNode();
  TextEditingController _namaController = new TextEditingController();
  //_email
  bool _emailNull = false;
  FocusNode _email = new FocusNode();
  TextEditingController _emailController = new TextEditingController();
  //_mobile
  bool _mobileEna = true;
  bool _mobileNull = false;
  FocusNode _mobile = new FocusNode();
  TextEditingController _mobileController = new TextEditingController();
  //_alamat
  bool _alamatEna = true;
  bool _alamatNull = false;
  FocusNode _alamat = new FocusNode();
  TextEditingController _alamatController = new TextEditingController();

  Color _fillColor = Colors.white.withOpacity(0.6);
  bool _genderEna = true;
  bool _genderNull = false;
  Gender selectedGender;
  List<Gender> gender = <Gender>[
      const Gender("", "Pilih Jenis Kelamin", FontAwesomeIcons.ban),
      const Gender("M", "Laki - Laki", FontAwesomeIcons.male),
      const Gender("F", "Perempuan", FontAwesomeIcons.female)
  ];

  double paddingtop;
  bool visibleDesc = true;
  bool _visible_verifikasi = true;
  bool _visible_lama = true;
  bool _visible_baru = false;
  double maxheight;
  Color _textColor = Colors.white;
  String _akunname;
  bool _vakunname = false;


  @override
  void initState(){
    if(widget._akun != null){
      if(widget._name != null){
        _akunname = widget._name;
        _vakunname = true;
      }
      if(widget._bod != null){
        _tgllahirController.text = widget._bod;
      }
    }

   if(widget._cabang == "denpasar"){
     _rmController.text = widget._nrmshh;
   }
    if(widget._cabang == "ubung"){
      _rmController.text = widget._nrmub;
    }
    if(widget._cabang == "nusadua"){
      _rmController.text = widget._nrmnd;
    }

    colorfill_lama=widget._color1;
    highlight_lama=widget._color2;
    colorfill_baru=Colors.grey[400];
    highlight_baru=Colors.grey[600];
    selectedGender=gender[0];
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose(){
    WidgetsBinding.instance.removeObserver(this);
    listScrollController.dispose();
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    final value = WidgetsBinding.instance.window.viewInsets.bottom;
    if (value > 0) {
      _onKeyboardChanged(true);
    } else {
      _onKeyboardChanged(false);
    }
  }

  _onKeyboardChanged(bool isVisible) {
    if (isVisible) {
      setState(() {
        paddingtop = 0;
        if(!widget._pxBaru){
          visibleDesc = false;
        }
      });
    } else {
      setState(() {
        if(!widget._pxBaru){
          visibleDesc = true;
        }
        paddingtop = maxheight * (1/7);
        if(paddingtop < 0){
          paddingtop = 0;
        }
        if(!widget._pxBaru && !_visible_verifikasi){
          paddingtop = 20;
        }
        if(widget._pxBaru){
          paddingtop = 20;
        }
      });
      FocusScope.of(context).requestFocus(new FocusNode());
      listScrollController.jumpTo(listScrollController.position.minScrollExtent);
    }
  }

  @override
  void didChangeDependencies() {
    setState((){
      maxheight = MediaQuery.of(context).size.height;
      if(_visible_verifikasi){
        paddingtop = maxheight * (1/7);
        if(paddingtop < 0){
          paddingtop = 0;
        }
      }else{
        paddingtop = 20;
      }
    });
    super.didChangeDependencies();
  }

  int response = 0;
  String msg = "";

  @override
  Future<String> _checknrm() async {
    var db = new Http_Controller();
    response = 0;
    if(_rmController.text != "" && _tgllahirController.text !="" && _validasiPx == 0){
      var http_response = await db.getResponse("https://suryahusadha.com/api/checknrm?BranchCode="+widget._BranchCode+"&nrm="+ _rmController.text+"&bodVal="+_tgllahirController.text);
      var _data = convert.jsonEncode(http_response);
      var decode = convert.jsonDecode(_data);
      var decodeConvert = convert.jsonDecode(decode["responseBody"]);
      var dataResponse = decodeConvert["datapx"];
      response = dataResponse[0]["response"];
      msg = dataResponse[0]["msg"];
      if (response == 1) {
        setState(() {
          _validasiPx = 1;
          _visible_baru = true;
          paddingtop = 20;
          _fillColor = Colors.grey.withOpacity(0.1);
          _visible_verifikasi = false;
          _genderEna = false;
          _rmEna = false;
          _tgllahirEna = false;
          _namaEna = false;
          _mobileEna = false;
          _alamatEna = false;
          _namaController.text=dataResponse[0]["data"]["NamaPasien"];
          _emailController.text=dataResponse[0]["data"]["Email"];
          _mobileController.text=dataResponse[0]["data"]["Phone"];
          _alamatController.text=dataResponse[0]["data"]["Alamat"];
          _alamatController.text=dataResponse[0]["data"]["Alamat"];
          if(dataResponse[0]["data"]["JenisKelamin"] == "M"){
            selectedGender = gender[1];
          };
          if(dataResponse[0]["data"]["JenisKelamin"] == "F"){
            selectedGender = gender[2];
          };
        });
      }
    }
    return response.toString();
  }

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> dtNext=[];
    dtNext.add({
      "_currentDate2Select": widget._currentDate2Select,
      "_clinicSelect": widget._clinicSelect,
      "_drSelect": widget._drSelect,
      "_Namacabang": widget._Namacabang,
      "_BranchCode": widget._BranchCode,
      "_cabang": widget._cabang,
      "_color1": widget._color1,
      "_color2": widget._color2,
      "_Selected_color": widget._Selected_color,
      "_idsp": widget._idsp,
      "_drimage": widget._drimage,
      "_drname": widget._drname,
      "_titlesp_id": widget._titlesp_id,
      "_titlesp_en": widget._titlesp_en,
      "_dridweb": widget._dridweb,
      "_secid": widget._secid,
      "_dridapi": widget._dridapi,

    });
    RouteHelper(context, "Schedules", dtNext);
  }

  @override
  Widget build(BuildContext context) {
    if(widget._pxBaru && _validasiPx == 1) {
      paddingtop = 20;
      _fillColor = Colors.white.withOpacity(0.6);
      _genderEna = true;
      _rmEna = true;
      _tgllahirEna = true;
      _namaEna = true;
      _mobileEna = true;
      _alamatEna = true;
    }

    if(widget._namapx != null){
      _rmController.text = widget._rm;
      _namaController.text = widget._namapx;
      if(widget._gender == "M"){
        selectedGender = gender[1];
      };
      if(widget._gender == "F"){
        selectedGender = gender[2];
      };
      _tgllahirController.text = widget._tgllahir;
      _emailController.text = widget._email;
      _mobileController.text = widget._mobile;
      _alamatController.text = widget._alamat;

      if(!widget._pxBaru){
        _validasiPx = 1;
        _visible_baru = true;
        _visible_lama = true;
        paddingtop = 20;
        _fillColor = Colors.grey.withOpacity(0.1);
        _visible_verifikasi = false;
        _genderEna = false;
        _rmEna = false;
        _tgllahirEna = false;
        _namaEna = false;
        _mobileEna = false;
        _alamatEna = false;
      }else{
        _visible_baru = false;
        _visible_lama = true;
        paddingtop = 20;
        _fillColor = Colors.white.withOpacity(0.6);
        _genderEna = true;
        _rmEna = true;
        _tgllahirEna = true;
        _namaEna = true;
        _mobileEna = true;
        _alamatEna = true;
      }
    }

    return new SafeArea(
        child: new Scaffold(
            key: _ScafoltkeyFrm,
            drawer: drawerController(widget._akun, widget._cabang, "Clinics"),
            appBar: new PreferredSize(
              preferredSize: Size.fromHeight(200.0),
              child: AppBarController(widget._cabang, _ScafoltkeyFrm,widget._akun, "Formrsv"),
            ),
            body: new WillPopScope(
              onWillPop: _onWillPop,
              child: new Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  new Container(
                    child: new Column(
                      children: <Widget>[
                        Expanded(
                          child: ListView(
                            controller: listScrollController,
                            scrollDirection: Axis.vertical,
                            children: <Widget>[
                              _buildheader(),
                              new Padding(padding: EdgeInsets.only(top: paddingtop)),
                              _buildForm(context),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            )
        )
    );
  }

  Widget _buildheader() {
    return new Padding(
      padding: const EdgeInsets.only(left: 24.0, right: 24.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Row(
              children: <Widget>[
                new Column(
                    children: <Widget>[
                      new Container(
                        width: 80.0,
                        height: 80.0,
                        decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          border: new Border.all(color: Colors.grey[300]),
                        ),
                        margin: const EdgeInsets.only(top: 16.0, right: 16.0),
                        padding: const EdgeInsets.all(3.0),
                        child: new ClipOval(
                          child: new Image.asset(
                              "assets/suri/suri2.png"
                          ),
                        ),
                      ),
                    ]
                ),
                new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 18.0),
                      ),
                      new Text(
                        "RESERVASI",
                        style: new TextStyle(
                          color: Colors.grey[800],
                          fontWeight: FontWeight.bold,
                          fontSize: 24.0,
                        ),
                      ),
                      new Text(
                        "Silahkan isi dan lengkapi formulir!",
                        style: new TextStyle(
                          color: Colors.grey[800],
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ]
                )
              ]
          ),
          new Container(
            color: Colors.grey[300],
            margin: const EdgeInsets.symmetric(vertical: 16.0),
            width: MediaQuery.of(context).size.width,
            height: 1.0,
          ),
          new Visibility(
            visible: visibleDesc,
            child: Text(
              "Untuk pasien lama atau member tentunya sudah mempunyai No. Rekam Medis yang tertera pada kartu identitas berobat Anda, silahkan masukkan No. Rekam Medis dan Tgl. Lahir untuk verifikasi data Anda, dan mempermudah aplikasi dalam membantu pengisian formulir reservasi ini.",
              style: new TextStyle(
                color: Colors.grey[800],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<String> _getDataKlg() async {
    var _data;
    var db = new Http_Controller();
    var http_response = await db.getResponse("https://suryahusadha.com/api/akun/getklg/"+widget._userid);
    _data = convert.jsonEncode(http_response);
    return _data;
  }

  Widget cardHeader(String title, IconData icon){
    return new Container(
      decoration: BoxDecoration(
        color: widget._color1,
        borderRadius: BorderRadius.only(topRight: Radius.circular(10),topLeft: Radius.circular(10))
      ),
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          new Container(
            padding: EdgeInsets.only(left: 16, right: 16, top: 18, bottom: 18),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Icon(
                      icon,
                      size: 16,
                      color: _textColor,
                    ),
                    new Container(
                      padding: EdgeInsets.only(left: 10),
                    ),
                    new Text(
                      title,
                      style: TextStyle(color: _textColor, fontSize: 16, fontWeight: FontWeight.w700),
                    ),
                  ],
                ),
              ],
            ),
          ),
          new Container(
            color: Colors.grey[300],
            width: MediaQuery.of(context).size.width,
            height: 1.0,
          ),
        ],
      ),
    );
  }

  Widget cardDt(BuildContext context, dt){
    Color _txtcolor;
    if( dt["enabled_klg"] == "0"){
      _txtcolor = Colors.grey[400];
    }else{
      _txtcolor = Colors.grey[800];
    }
    return new Container(
      color: Colors.white,
      margin: EdgeInsets.only(top: 5.0, left: 10, right: 10),
      child: InkWell(
          onTap: (){
            Navigator.of(context).pop(dt);
          },
          child: new Column(
            children: <Widget>[
              new Container(
                padding: EdgeInsets.only(top:5, bottom: 5, left: 16, right: 16),
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Container(
                      width: 40.0,
                      height: 40.0,
                      alignment: Alignment.center,
                      child: new Icon(
                        FontAwesomeIcons.handPointRight,
                        color: _txtcolor,
                        size: 24.0,
                      ),
                    ),
                    new Expanded(
                      flex: 2,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(left:10.0, right:10.0),
                              child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child:new Text(
                                        dt["nama_klg"],
                                        style: TextStyle(fontFamily: "NeoSansBold", fontSize: 14.0, color: _txtcolor),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        dt["hubungan_klg"],
                                        style: TextStyle(color: _txtcolor, fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                  ]
                              ),
                            ),
                          ]
                      ),
                    ),
                    new Container(
                      height: 40.0,
                      width: 40.0,
                      alignment: Alignment.centerRight,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.all(6.0),
                              alignment: Alignment.centerRight,
                              child: new Icon(
                                FontAwesomeIcons.angleRight,
                                color: Colors.grey,
                                size: 18.0,
                              ),
                            ),
                          ]
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                margin: EdgeInsets.only(top: 5.0),
                width: double.infinity,
                height: 1.0,
                color: Colors.grey[200],
              ),
            ],
          )
      ),
    );
  }

  Widget _createpage(akun){
    return new SafeArea(
      child: new Scaffold(
        backgroundColor: Colors.black.withOpacity(0.4),
        body: new Container(
          decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.only(topRight: Radius.circular(10),topLeft: Radius.circular(10))
          ),
          margin: EdgeInsets.all(40),
          padding: EdgeInsets.only(bottom: 30),
          child: Column(
            children: <Widget>[
              cardHeader("Pilih Akun Keluarga", FontAwesomeIcons.users),
              new FutureBuilder(
                  future: _getDataKlg(),
                  builder: (context, snapshot) {
                    if(snapshot.hasData && snapshot.connectionState == ConnectionState.done){
                      var decode = convert.jsonDecode(snapshot.data);
                      var decodeConvert = convert.jsonDecode(decode["responseBody"]);
                      final dtkonten = decodeConvert["getklg"];
                      var getdata = dtkonten[0]["data"];
                      return new Expanded(
                        child: ListView.builder(
                            itemCount: getdata.length,
                            itemBuilder:(BuildContext context, int index) {
                              return cardDt(context, getdata[index]);
                            }
                        ),
                      );
                    }else{
                      if(snapshot.connectionState == ConnectionState.waiting){
                        return Container(
                          margin: EdgeInsets.only(top: 40),
                          child: Column(
                            children: <Widget>[
                              Center(
                                child: SizedBox(
                                    width: 80.0,
                                    height: 80.0,
                                    child: const CircularProgressIndicator(
                                      backgroundColor: Colors.grey,
                                    )),
                              )
                            ],
                          ),
                        );
                      }else{
                        return Container(
                          margin: EdgeInsets.only(top: 40),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text("Data tidak ditemukan!")
                            ],
                          ),
                        );
                      }
                    }
                  }
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildForm(BuildContext context) {
    return new Container(
      padding: EdgeInsets.only(left:24, right:24),
      child: new Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Container(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                  margin: EdgeInsets.only(bottom: 20),
                  child: new Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: new Column(children: <Widget>[
                          new RawMaterialButton(
                              onPressed: ()=>{
                                setState(() {
                                  colorfill_lama=widget._color1;
                                  highlight_lama=widget._color2;
                                  colorfill_baru=Colors.grey[400];
                                  highlight_baru=Colors.grey[600];
                                  _visible_baru = false;
                                  _visible_verifikasi = true;
                                  _visible_lama = true;
                                  visibleDesc = true;
                                  paddingtop = maxheight * (1/7);
                                  widget._pxBaru = false;
                                  _namaEna = true;
                                  _namaNull = false;
                                  _namaController.text = "";
                                  _rmEna = true;
                                  _rmNull = false;
                                  _rmController.text = "";
                                  _tgllahirEna = true;
                                  _tgllahirNull = false;
                                  _tgllahirController.text = "";
                                  _genderEna = true;
                                  _genderNull = false;
                                  selectedGender = gender[0];
                                  _emailNull = false;
                                  _emailController.text = "";
                                  _mobileEna = true;
                                  _mobileNull = false;
                                  _mobileController.text = "";
                                  _alamatEna = true;
                                  _alamatNull = false;
                                  _alamatController.text = "";
                                  if(widget._akun != null){
                                    if(widget._name != null){
                                      _akunname = widget._name;
                                      _vakunname = true;
                                    }
                                    if(widget._bod != null){
                                      _tgllahirController.text = widget._bod;
                                    }
                                  }
                                  if(widget._cabang == "denpasar"){
                                    _rmController.text = widget._nrmshh;
                                  }
                                  if(widget._cabang == "ubung"){
                                    _rmController.text = widget._nrmub;
                                  }
                                  if(widget._cabang == "nusadua"){
                                    _rmController.text = widget._nrmnd;
                                  }
                                }),
                              },
                              elevation: 0,
                              padding: EdgeInsets.all(16),
                              animationDuration: Duration(milliseconds: 500),
                              fillColor: colorfill_lama,
                              highlightColor:  highlight_lama,
                              splashColor: Colors.red,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(10),
                                      topLeft: Radius.circular(10)
                                  )
                              ),
                              constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Icon(
                                    FontAwesomeIcons.check,
                                    color: Colors.white,
                                    size: 18,
                                  ),
                                  new Container(width: 5,),
                                  new Text("Pasien Lama", style: TextStyle(color: Colors.white, fontSize: 16),)
                              ],)
                          )
                        ],),
                      ),
                      Expanded(
                        flex: 1,
                        child: new Column(children: <Widget>[
                          new RawMaterialButton(
                              onPressed: ()=>{
                                setState(() {
                                  _validasiPx = 0;
                                  colorfill_baru=widget._color1;
                                  highlight_baru=widget._color2;
                                  colorfill_lama=Colors.grey[400];
                                  highlight_lama=Colors.grey[600];
                                  _visible_lama = false;
                                  _visible_baru = true;
                                  _visible_verifikasi = false;
                                  visibleDesc = false;
                                  paddingtop = 20;
                                  _fillColor = Colors.white.withOpacity(0.6);
                                  widget._namapx = null;
                                  widget._pxBaru = true;
                                  _namaEna = true;
                                  _namaNull = false;
                                  _namaController.text = "";
                                  _rmEna = true;
                                  _rmNull = false;
                                  _rmController.text = "";
                                  _tgllahirEna = true;
                                  _tgllahirNull = false;
                                  _tgllahirController.text = "";
                                  _genderEna = true;
                                  _genderNull = false;
                                  selectedGender = gender[0];
                                  _emailNull = false;
                                  _emailController.text = "";
                                  _mobileEna = true;
                                  _mobileNull = false;
                                  _mobileController.text = "";
                                  _alamatEna = true;
                                  _alamatNull = false;
                                  _alamatController.text = "";
                                }),
                              },
                              elevation: 0,
                              padding: EdgeInsets.all(16),
                              animationDuration: Duration(milliseconds: 500),
                              fillColor: colorfill_baru,
                              highlightColor: highlight_baru,
                              splashColor: Colors.red,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      bottomRight: Radius.circular(10),
                                      topRight: Radius.circular(10)
                                  )
                              ),
                              constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Icon(
                                    FontAwesomeIcons.times,
                                    color: Colors.white,
                                    size: 18,
                                  ),
                                  new Container(width: 5,),
                                  new Text("Pasien Baru", style: TextStyle(color: Colors.white, fontSize: 16),)
                              ],)
                          )
                        ],),
                      ),
                    ],
                  ),
                ),
                new Visibility(
                  visible: _vakunname,
                  child: new Container(
                    padding: EdgeInsets.only(bottom: 24),
                    child: new Row(
                      children: <Widget>[
                        new Icon(
                          FontAwesomeIcons.userCircle,
                          color: Colors.black,
                          size: 16,
                        ),
                        new Container(
                          padding: EdgeInsets.only(left: 10, top: 3),
                          child: new Row(
                            children: <Widget>[
                              Text(
                                "Info Akun: ",
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black
                                ),
                              ),
                             new Container(
                               child: new InkWell(
                                 onTap: (){
                                   setState(() {
                                     widget._pxBaru = true;
                                     if(widget._akun != null){
                                       widget._bpjs_id = widget._akun[0]["bpjs_id"];
                                       if(widget._name != null){
                                         _akunname = widget._name;
                                         _vakunname = true;
                                       }
                                       if(widget._cabang == "denpasar"){
                                         _rmController.text = widget._nrmshh;
                                         _textColor = Colors.grey[600];
                                       }
                                       if(widget._cabang == "ubung"){
                                         _rmController.text = widget._nrmub;
                                       }
                                       if(widget._cabang == "nusadua"){
                                         _rmController.text = widget._nrmnd;
                                       }
                                       if(widget._bod != null){
                                         _tgllahirController.text = widget._bod;
                                       }
                                     }
                                   });
                                 },
                                 child: new Row(
                                   children: <Widget>[
                                     new Container(
                                       padding: EdgeInsets.only(right: 10, left: 5),
                                       child: Text(
                                         _akunname != null ? _akunname : "",
                                         style: TextStyle(
                                             fontSize: 14,
                                             color: Colors.redAccent
                                         ),
                                       ),
                                     ),
                                     new Icon(
                                       FontAwesomeIcons.times,
                                       size: 16,
                                       color: Colors.redAccent,
                                     ),
                                   ],
                                 ),
                               )
                             )
                            ],
                          )
                        )
                      ],
                    ),
                  )
                ),
                new Visibility(
                    visible: _visible_lama,
                    child:Container(
                        child: TextField(
                          enabled: _rmEna,
                          inputFormatters: [
                            MaskedTextInputFormatter(
                              mask: 'xx.xx.xx',
                              separator: '.',
                            ),
                          ],
                          focusNode: _rm,
                          controller: _rmController,
                          textInputAction: TextInputAction.next,
                          onEditingComplete: () => {
                            _next(_tgllahir, "next"),
                            showPickerDate(context)
                          },
                          cursorColor: Colors.grey[800],
                          style: TextStyle(color: Colors.grey[800], fontSize: 14),
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: _fillColor,
                            labelStyle: TextStyle(
                              color: Colors.grey[800],
                            ),
                            labelText: 'No. Rekam Medis (xx.xx.xx)',
                            errorText: _rmNull ? "" : null,
                            prefixIcon: Icon(
                              FontAwesomeIcons.idBadge,
                              color: Colors.grey[800],
                            ),
                            suffixIcon: InkWell(
                              onTap: (){
                                FocusScope.of(context).requestFocus(new FocusNode());
                                showDialog(
                                  barrierDismissible: true,
                                  context: context,
                                  builder: (BuildContext context){
                                    return _createpage(widget._akun);
                                  },
                                ).then((dt){
                                  if(dt != null){
                                    setState(() {
                                      widget._bpjs_id = dt["bpjsid_klg"];
                                      _akunname = dt["nama_klg"];
                                      _tgllahirController.text = dt["tgllahir_klg"];
                                      _vakunname = true;
                                      String nrm;
                                      if(widget._cabang == "denpasar"){
                                        nrm = dt["nrmdps_klg"];
                                      }
                                      if(widget._cabang == "ubung"){
                                        nrm = dt["nrmub_klg"];
                                      }
                                      if(widget._cabang == "nusadua"){
                                        nrm = dt["nrmnd_klg"];
                                      }
                                      _rmController.text =nrm;
                                    });
                                  }
                                });
                              },
                              child: new Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("Pilih Akun Lain"),
                                  Icon(
                                    FontAwesomeIcons.caretDown,
                                    color: Colors.grey[800],
                                  ),
                                  Padding(padding: EdgeInsets.only(right: 10))
                                ],
                              )
                            ),
                            focusedBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                            ),
                            enabledBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                          ),
                        )
                    ),
                ),

                new Visibility(
                  visible: _visible_baru,
                  child: Column(children: <Widget>[
                    new Container(
                        margin: EdgeInsets.only(top:20),
                        child: TextField(
                          focusNode: _nama,
                          controller: _namaController,
                          enabled: _namaEna,
                          textInputAction: TextInputAction.next,
                          onEditingComplete: () => {
                            _next(_tgllahir, "next"),
                            showPickerDate(context)
                          },
                          cursorColor: Colors.grey[800],
                          style: TextStyle(color: Colors.grey[800], fontSize: 14),
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: _fillColor,
                            labelStyle: TextStyle(
                              color: Colors.grey[800],
                            ),
                            errorText: _namaNull ? "" : null,
                            labelText: 'Nama Lengkap',
                            prefixIcon: Icon(
                              FontAwesomeIcons.tag,
                              color: Colors.grey[800],
                            ),
                            focusedBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                            ),
                            enabledBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        )
                    ),
                  ]
                  ),
                ),

                new Container(
                    margin: EdgeInsets.only(top:20),
                    child: TextField(
                      enabled: _tgllahirEna,
                      onTap: (){
                        showPickerDate(context);
                        FocusScope.of(context).requestFocus(new FocusNode());
                      },
                      inputFormatters: [
                        MaskedTextInputFormatter(
                          mask: 'xx-xx-xxxx',
                          separator: '-',
                        ),
                      ],
                      focusNode: _tgllahir,
                      controller: _tgllahirController,
                      textInputAction: TextInputAction.next,
                      onEditingComplete: () => {
                        if( widget._pxBaru){
                          _next(_tgllahir, "next")
                        }
                      },
                      cursorColor: Colors.grey[800],
                      style: TextStyle(color: Colors.grey[800], fontSize: 14),
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: _fillColor,
                        labelStyle: TextStyle(
                          color: Colors.grey[800],
                        ),
                        labelText: 'Tgl Lahir (dd-mm-YYYY)',
                        errorText: _tgllahirNull ? "" : null,
                        prefixIcon: Icon(
                          FontAwesomeIcons.calendarCheck,
                          color: Colors.grey[800],
                        ),
                        focusedBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        enabledBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                          borderSide: BorderSide(
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    )
                ),

                new Visibility(
                    visible: _visible_baru,
                    child: Column(children: <Widget>[
                      new Container(
                          margin: EdgeInsets.only(top:20),
                          child: new DropdownButtonFormField(
                            value: selectedGender,
                            onChanged: (Gender newValue) {
                              if(_genderEna){
                                setState(() {
                                  if(newValue.val == ""){
                                    selectedGender = gender[0];
                                  }
                                  if(newValue.val == "M"){
                                    selectedGender = gender[1];
                                  }
                                  if(newValue.val == "F"){
                                    selectedGender = gender[2];
                                  }
                                });
                              }
                            },
                            items: gender.map((Gender gender){
                              return new DropdownMenuItem<Gender>(
                                  value: gender,
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      new Container(
                                        padding: EdgeInsets.only(top:3),
                                        child:new Text(
                                          gender.title,
                                          style: new TextStyle(color: Colors.black, fontSize: 14),
                                        ),
                                      ),
                                      new Container(width: 5,),
                                      new Icon(
                                        gender.icon,
                                        size: 16,
                                      ),
                                    ],
                                  )
                              );
                            }).toList(),
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: _fillColor,
                              labelStyle: TextStyle(
                                color: Colors.grey[800],
                              ),
                              labelText: 'Jenis Kelamin',
                              errorText: _genderNull ? "" : null,
                              prefixIcon: Icon(
                                FontAwesomeIcons.transgender,
                                color: Colors.grey[800],
                              ),
                              focusedBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                                borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                              ),
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                                borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ),
                      ),
                      new Container(
                          margin: EdgeInsets.only(top:20),
                          child: TextField(
                            focusNode: _email,
                            controller: _emailController,
                            textInputAction: TextInputAction.next,
                            onEditingComplete: () => _next(_mobile, "next"),
                            cursorColor: Colors.grey[800],
                            style: TextStyle(color: Colors.grey[800], fontSize: 14),
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white.withOpacity(0.6),
                              labelStyle: TextStyle(
                                color: Colors.grey[800],
                              ),
                              labelText: 'Email',
                              errorText: _emailNull ? "" : null,
                              prefixIcon: Icon(
                                FontAwesomeIcons.at,
                                color: Colors.grey[800],
                              ),
                              focusedBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                                borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                              ),
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                                borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          )
                      ),
                      new Container(
                          margin: EdgeInsets.only(top:20),
                          child: TextField(
                            enabled: _mobileEna,
                            focusNode: _mobile,
                            controller: _mobileController,
                            textInputAction: TextInputAction.next,
                            onEditingComplete: () => _next(_alamat, "next"),
                            cursorColor: Colors.grey[800],
                            style: TextStyle(color: Colors.grey[800], fontSize: 14),
                            keyboardType: TextInputType.phone,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: _fillColor,
                              labelStyle: TextStyle(
                                color: Colors.grey[800],
                              ),
                              labelText: 'Mobile Phone',
                              errorText: _mobileNull ? "" : null,
                              prefixIcon: Icon(
                                FontAwesomeIcons.mobileAlt,
                                color: Colors.grey[800],
                              ),
                              focusedBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                                borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                              ),
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                                borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          )
                      ),
                      new Container(
                          margin: EdgeInsets.only(top:20),
                          child: TextField(
                            enabled: _alamatEna,
                            focusNode: _alamat,
                            controller: _alamatController,
                            maxLines: null,
                            textInputAction: TextInputAction.done,
                            onEditingComplete: () => _next(_alamat, "done"),
                            cursorColor: Colors.grey[800],
                            style: TextStyle(color: Colors.grey[800], fontSize: 14),
                            keyboardType: TextInputType.multiline,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: _fillColor,
                              labelStyle: TextStyle(
                                color: Colors.grey[800],
                              ),
                              labelText: 'Alamat',
                              errorText: _alamatNull ? "" : null,
                              prefixIcon: Icon(
                                FontAwesomeIcons.home,
                                color: Colors.grey[800],
                              ),
                              focusedBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                                borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                              ),
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                                borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          )
                      ),
                    ],)
                )

              ],
            ),
          ),
          Visibility(
            visible: _visible_baru,
            child: new Container(
              margin: EdgeInsets.only(top:60),
              child: Column(
                  children: <Widget>[
                    new Align(
                      alignment: Alignment.bottomCenter,
                      child: RawMaterialButton(
                        onPressed: (){
                          var _Next = true;
                          setState(() {
                            if(widget._pxBaru){
                              _namaNull = false;
                              if(_namaController.text.isEmpty){
                                _namaNull = true;
                                _Next = false;
                              }
                              _tgllahirNull = false;
                              if(_tgllahirController.text.isEmpty){
                                _tgllahirNull = true;
                                _Next = false;
                              }
                              _genderNull = false;
                              if(selectedGender.val == ""){
                                _genderNull = true;
                                _Next = false;
                              }
                              _emailNull = false;
                              if(_emailController.text.isEmpty){
                                _emailNull = true;
                                _Next = false;
                              }
                              _mobileNull = false;
                              if(_mobileController.text.isEmpty){
                                _mobileNull = true;
                                _Next = false;
                              }
                              _alamatNull = false;
                              if(_alamatController.text.isEmpty){
                                _alamatNull = true;
                                _Next = false;
                              }
                            }else{
                              _namaNull = false;
                              if(_namaController.text.isEmpty){
                                _namaNull = true;
                                _Next = false;
                              }
                              _rmNull = false;
                              if(_rmController.text.isEmpty){
                                _rmNull = true;
                                _Next = false;
                              }
                              _tgllahirNull = false;
                              if(_tgllahirController.text.isEmpty){
                                _tgllahirNull = true;
                                _Next = false;
                              }
                              _genderNull = false;
                              if(selectedGender.val == ""){
                                _genderNull = true;
                              }
                              _emailNull = false;
                              if(_emailController.text.isEmpty){
                                _emailNull = true;
                                _Next = false;
                              }
                              _mobileNull = false;
                              if(_mobileController.text.isEmpty){
                                _mobileNull = true;
                                _Next = false;
                              }
                              _alamatNull = false;
                              if(_alamatController.text.isEmpty){
                                _alamatNull = true;
                                _Next = false;
                              }
                            }
                          });
                          if(_Next){
                            List<Map<String, dynamic>> dtNext=[];
                            dtNext.add({
                              "_pxBaru": widget._pxBaru,
                              "_rm": _rmController.text,
                              "_namapx": _namaController.text,
                              "_gender": selectedGender.val,
                              "_tgllahir": _tgllahirController.text,
                              "_email": _emailController.text,
                              "_mobile": _mobileController.text,
                              "_alamat": _alamatController.text,
                              "_bpjs_id": widget._bpjs_id,
                              "_selectedTipepx": widget._selectedTipepx,
                              "_typepx": widget._typepx,
                              "_typepx_id": widget._typepx_id,
                              "_typepx_en": widget._typepx_en,
                              "_nobpjs": widget._nobpjs,
                              "_norujukan": widget._norujukan,
                              "_note": widget._note,
                              "_currentDate2Select": widget._currentDate2Select,
                              "_clinicSelect": widget._clinicSelect,
                              "_drSelect": widget._drSelect,
                              "_Namacabang": widget._Namacabang,
                              "_BranchCode": widget._BranchCode,
                              "_cabang": widget._cabang,
                              "_color1": widget._color1,
                              "_color2": widget._color2,
                              "_Selected_color": widget._Selected_color,
                              "_idsp": widget._idsp,
                              "_drimage": widget._drimage,
                              "_drname": widget._drname,
                              "_titlesp_id": widget._titlesp_id,
                              "_titlesp_en": widget._titlesp_en,
                              "_dridweb": widget._dridweb,
                              "_secid": widget._secid,
                              "_dridapi": widget._dridapi,
                              "_spc_id": widget._spc_id,
                              "_spc_en": widget._spc_en,
                              "_drname": widget._drname,
                              "_NamaDokterPengganti": widget._NamaDokterPengganti,
                              "_DokterID": widget._DokterID,
                              "_SectionID": widget._SectionID,
                              "_Tanggal": widget._Tanggal,
                              "_WaktuID": widget._WaktuID,
                              "_Hari": widget._Hari,
                              "_JmlAntrian": widget._JmlAntrian,
                              "_Realisasi": widget._Realisasi,
                              "_Cancel": widget._Cancel,
                              "_DokterPenggantiID": widget._DokterPenggantiID,
                              "_NoAntrianTerakhir": widget._NoAntrianTerakhir,
                              "_SectionName": widget._SectionName,
                              "_FromJam": widget._FromJam,
                              "_ToJam": widget._ToJam,
                              "_time": widget._FromJam+"-"+widget._ToJam,
                              "_WaktuFromJam": widget._WaktuFromJam,
                              "_WaktuToJam": widget._WaktuToJam,
                              "_sch": widget._sch,
                              "_asgeriatri": widget._asgeriatri,
                              "_geriatri": widget._geriatri,
                              "_BPJS": widget._BPJS,
                              "_imgbpjs": widget._imgbpjs,
                              "_bpjslogo": widget._bpjslogo,
                              "_closed": widget._closed,
                              "_statussch": widget._statussch,
                              "_status_id": widget._status_id,
                              "_status_en": widget._status_en,
                            });
                            RouteHelper(context, "tipePx", dtNext);
                          }
                        },
                        elevation: 0,
                        padding: EdgeInsets.all(16),
                        animationDuration: Duration(milliseconds: 500),
                        fillColor: widget._color1,
                        splashColor: Colors.red,
                        highlightColor:widget._color2,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Expanded(
                              flex: 1,
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Column(children: <Widget>[
                                    Text(
                                      "Berikutnya",
                                      style: TextStyle(color: Colors.white, fontSize: 18),
                                    ),
                                  ],),
                                ],
                              ),
                            ),
                            Icon(
                              FontAwesomeIcons.arrowRight,
                              color: Colors.white,
                              size: 16,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ]
              ),
            ),
          ),
          Visibility(
            visible: _visible_verifikasi,
            child: new Container(
              margin: EdgeInsets.only(top:60),
              child: Column(
                  children: <Widget>[
                    new Align(
                      alignment: Alignment.bottomCenter,
                      child: RawMaterialButton(
                        onPressed: (){
                          setState(() {
                            _rmNull = false;
                            _tgllahirNull = false;
                            if(_rmController.text.isEmpty){
                              _rmNull = true;
                            }
                            if(_tgllahirController.text.isEmpty){
                              _tgllahirNull = true;
                            }
                          });
                          if(!_rmNull && !_tgllahirNull){
                            _validasiPx = 0;
                            _validasi(context);
                          }
                        },
                        elevation: 0,
                        padding: EdgeInsets.all(16),
                        animationDuration: Duration(milliseconds: 500),
                        fillColor: widget._color1,
                        splashColor: Colors.red,
                        highlightColor:widget._color2,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Expanded(
                              flex: 1,
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Column(children: <Widget>[
                                    Text(
                                      "Verifikasi",
                                      style: TextStyle(color: Colors.white, fontSize: 18),
                                    ),
                                  ],),
                                ],
                              ),
                            ),
                            Icon(
                              FontAwesomeIcons.arrowRight,
                              color: Colors.white,
                              size: 16,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ]
              ),
            ),
          ),
          new Padding(padding: EdgeInsets.only(bottom:40)),
        ],
      ),
    );
  }

  showPickerDate(BuildContext context) {
    final f = new DateFormat('dd-MM-yyyy');
    DateTime _Selected;
    if(_tgllahirController.text != ""){
      int d = int.parse(_tgllahirController.text.split("-")[0]);
      int m = int.parse(_tgllahirController.text.split("-")[1]);
      int y = int.parse(_tgllahirController.text.split("-")[2]);
      _Selected = DateTime(y, m ,d);
    }else{
      _Selected = DateTime.now();
    }
    int _yNow = int.parse(DateFormat.y().format(_Selected));
    DateTime _max = DateTime(_yNow-100);

    Picker(
      hideHeader: true,
      changeToFirst: false,
      adapter: DateTimePickerAdapter(
        customColumnType: [0, 1, 2],
        value: _Selected,
        maxValue: DateTime.now(),
        minValue: _max,
      ),
      title: new Container(
        child: Column(children: <Widget>[
          Text("Pilih Tgl. Lahir:", style: TextStyle(color: widget._color1),),
        ],),
      ),
      selectedTextStyle: TextStyle(color: widget._color1),
      confirm: new InkWell(
        onTap: ()=>{
          Navigator.of(context).pop(),
          widget,_tgllahirController.text = f.format(_Selected).toString(),
        },
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(children: <Widget>[
            new Icon(
              FontAwesomeIcons.check,
              color: widget._color1,
              size: 12,
            ),
            Container(width: 5,),
            Text("Pilih", style: TextStyle(color: widget._color1),)
          ],),
        ),
      ),
      cancel: new InkWell(
        onTap: ()=>{
          Navigator.of(context).pop()
        },
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(children: <Widget>[
            new Icon(
              FontAwesomeIcons.times,
              color: widget._color1,
              size: 12,
            ),
            Container(width: 2,),
            Text("Tutup", style: TextStyle(color: widget._color1),)
          ],),
        ),
      ),
      onSelect: (Picker picker, int index, List<int> selecteds) {
        this.setState(() {
          int d = _Selected.day;
          int m = _Selected.month;
          int y = _Selected.year;
          if(index == 2){
            d = (picker.adapter as DateTimePickerAdapter).value.day;
            _Selected = DateTime(y, m, d);
          }
          if(index == 1){
            m = (picker.adapter as DateTimePickerAdapter).value.month;
            _Selected = DateTime(y, m, d);
          }
          if(index == 0){
            y = (picker.adapter as DateTimePickerAdapter).value.year;
            _Selected = DateTime(y, m, d);
          }

        });
      },
    ).showDialog(context);

  }

  _validasi(BuildContext _context){
    showDialog(
        barrierDismissible: false,
        context: _context,
        builder: (BuildContext context) => validasiDilogue(_context),
    );
  }

  Widget validasiDilogue(BuildContext context){
    return FutureBuilder(
        future: _checknrm(),
        builder: (context, snapshot){
          if(snapshot.hasData && (snapshot.connectionState == ConnectionState.done)){
            if(snapshot.data == "0"){
              return Dialogue(context, msg, Colors.redAccent, widget._color2, null, null);
            }else{
              return Dialogue(context, msg, widget._color1, widget._color2, null, null);
            }
          }else{
            return new Container(
              width: MediaQuery.of(context).size.width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(
                    backgroundColor: Colors.grey,
                  ),
                ],
              ),
            );
          }
        }
    );
  }

  _next(FocusNode reqfocus, String s){
    FocusScope.of(context).requestFocus(reqfocus);
    if(s == "last"){
      listScrollController.jumpTo(listScrollController.position.maxScrollExtent);
    }
    if(s == "done"){
      listScrollController.jumpTo(listScrollController.position.minScrollExtent);
      FocusScope.of(context).requestFocus(new FocusNode());
    }
  }
}