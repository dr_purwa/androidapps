import 'package:flutter/src/widgets/icon_data.dart';

class Clinics_mdl{
  String idsp;
  String titleen;
  String titleid;
  String secid;
  String secid_ub;
  String secid_nd;

  Clinics_mdl({
    this.idsp,
    this.titleen,
    this.titleid,
    this.secid,
    this.secid_ub,
    this.secid_nd
  });
}

class Doctors_mdl{
  String BranchCode;
  String image;
  String nama;
  String dridapi;
  String secid;
  String officer_id;
  String titlesp_id;
  String titlesp_en;
  Doctors_mdl({
    this.BranchCode,
    this.image,
    this.nama,
    this.dridapi,
    this.secid,
    this.officer_id,
    this.titlesp_id,
    this.titlesp_en,
  });
}

class Gender{
  const Gender(this.val, this.title, this.icon);
  final String title;
  final String val;
  final IconData icon;
}

class TipePX{
  const TipePX(this.index,this.simrsId, this.advanceData, this.typepx_id, this.typepx_en);
  final int index;
  final String simrsId;
  final String advanceData;
  final String typepx_id;
  final String typepx_en;
}

class Reservasi_Mdl{
  int idmdl;
  String branchcode;
  String idrsv;
  String rsv_no;
  String qrcode;
  String noUrut;
  String bpjsid;
  String NoRujukanBPJS;
  String opHid;
  String schdatetime;
  String day;
  String date;
  String timeopen;
  String close;
  String nrm;
  String userid;
  String TanggalLahir;
  String type_pasien;
  String typepx_id;
  String typepx_en;
  String typepx_jpn;
  String pasienbaru;
  String namepx;
  String mothersname;
  String genderpx;
  String emailpx;
  String phonepx;
  String idcardType;
  String nik;
  String enabled;
  String status;
  String rsv_time;
  String note;
  String star;
  String comment;
  String rate_d;
  String rate_m;
  String rate_y;
  String randcheck;
  String image;
  String gelardepan;
  String officer_name;
  String officer_info;
  String gelarbelakang;
  String titlesp_en;
  String titlesp_id;
  String titlesp_jpn;
  String branch_name;
  String branch_address;
  String branch_phone;
  String advanceData;
  Reservasi_Mdl(
      this.idmdl,
      this.branchcode,
      this.idrsv,
      this.rsv_no,
      this.qrcode,
      this.noUrut,
      this.bpjsid,
      this.NoRujukanBPJS,
      this.opHid,
      this.schdatetime,
      this.day,
      this.date,
      this.timeopen,
      this.close,
      this.nrm,
      this.userid,
      this.TanggalLahir,
      this.type_pasien,
      this.typepx_id,
      this.typepx_en,
      this.typepx_jpn,
      this.pasienbaru,
      this.namepx,
      this.mothersname,
      this.genderpx,
      this.emailpx,
      this.phonepx,
      this.idcardType,
      this.nik,
      this.enabled,
      this.status,
      this.rsv_time,
      this.note,
      this.star,
      this.comment,
      this.rate_d,
      this.rate_m,
      this.rate_y,
      this.randcheck,
      this.image,
      this.gelardepan,
      this.officer_name,
      this.officer_info,
      this.gelarbelakang,
      this.titlesp_en,
      this.titlesp_id,
      this.titlesp_jpn,
      this.branch_name,
      this.branch_address,
      this.branch_phone,
      this.advanceData,
      );
}