import 'package:flutter/material.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:convert' as convert;

class AkunBerobatKartu extends StatefulWidget {
  var _akun;
  String _userid;
  String _cabang;
  Color _color1;
  Color _color2;
  AkunBerobatKartu(List params, akun){
    this._akun = akun;
    this._userid = akun[0]["id"].toString();
    params.forEach((dt){
      this._cabang = dt["_cabang"];
      if(_cabang=="group"){
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
      }
      if(_cabang=="denpasar"){
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
      }
      if(_cabang=="ubung"){
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
      }
      if(_cabang=="nusadua"){
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
      }
      if(_cabang=="kmc"){
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
      }
    });
  }
  @override
  _AkunBerobatKartu createState() => new _AkunBerobatKartu();
}

class _AkunBerobatKartu extends State<AkunBerobatKartu>{

  final _Scafoltkey = GlobalKey<ScaffoldState>();

  Future<bool> _onWillPop(){
    //Navigator.pop(context);
    List<Map<String, dynamic>> dtNext=[];
    dtNext.add({
      "_cabang": widget._cabang,
      "_page": "AkunBerobatKartu",
    });
    RouteHelper(context, widget._cabang, dtNext);
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(
        child: new Scaffold(
            body:new WillPopScope(
              onWillPop: _onWillPop,
              child: new Scaffold(
                  key: _Scafoltkey,
                  drawer: drawerController(widget._akun , widget._cabang, "AkunBerobatKartu"),
                  appBar: new PreferredSize(
                    preferredSize: Size.fromHeight(200.0),
                    child: AppBarController(widget._cabang, _Scafoltkey, widget._akun, "AkunBerobatKartu"),
                  ),
                  body: _createpage(widget._akun),
              ),
            ),
        )
    );
  }

  Future<String> _getData() async {
    var _data;
    var db = new Http_Controller();
    var http_response = await db.getResponse("https://suryahusadha.com/api/akun/getklg/"+widget._userid);
    _data = convert.jsonEncode(http_response);
    return _data;
  }

  Widget _createpage(akun){
    return new Container(
      padding: EdgeInsets.only(bottom: 30),
      child: Column(
        children: <Widget>[
          cardHeader("Kartu Berobat Keluarga", FontAwesomeIcons.users),
          new FutureBuilder(
            future: _getData(),
              builder: (context, snapshot) {
                if(snapshot.hasData && snapshot.connectionState == ConnectionState.done){
                  var decode = convert.jsonDecode(snapshot.data);
                  var decodeConvert = convert.jsonDecode(decode["responseBody"]);
                  final dtkonten = decodeConvert["getklg"];
                  var getdata = dtkonten[0]["data"];
                  return new Expanded(
                    child: ListView.builder(
                        itemCount: getdata.length,
                        itemBuilder:(BuildContext context, int index) {
                            return cardDt(context, getdata[index]);
                        }
                    ),
                  );
                }else{
                  if(snapshot.connectionState == ConnectionState.waiting){
                    return Container(
                      margin: EdgeInsets.only(top: 40),
                      child: Column(
                        children: <Widget>[
                          Center(
                            child: SizedBox(
                                width: 80.0,
                                height: 80.0,
                                child: const CircularProgressIndicator(
                                  backgroundColor: Colors.grey,
                                )),
                          )
                        ],
                      ),
                    );
                  }else{
                    return Container(
                      margin: EdgeInsets.only(top: 40),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text("Data tidak ditemukan!")
                        ],
                      ),
                    );
                  }
                }
              }
          )
        ],
      ),
    );
  }

  Widget cardHeader(String title, IconData icon){
    return new Container(
      color: Colors.white,
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          new Container(
            padding: EdgeInsets.only(left: 16, right: 16, top: 18, bottom: 18),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Icon(
                      icon,
                      size: 16,
                      color: Colors.grey[800],
                    ),
                    new Container(
                      padding: EdgeInsets.only(left: 10),
                    ),
                    new Text(
                      title,
                      style: TextStyle(color: Colors.grey[800], fontSize: 16, fontWeight: FontWeight.w700),
                    ),
                  ],
                ),
              ],
            ),
          ),
          new Container(
            color: Colors.grey[300],
            width: MediaQuery.of(context).size.width,
            height: 1.0,
          ),
        ],
      ),
    );
  }

  Widget ecardberobat(cabang, nrm, bod){
    print("https://suryahusadha.com/api/imgcard/berobat/?cabang="+cabang+"&nrm="+nrm+"&bodVal="+bod+"&dummy=${new DateTime.now()}");
    return new SafeArea(
      child: new Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Center(
          child: RotatedBox(
            quarterTurns: 5,
            child: Stack(
              children: <Widget>[
                new Container(
                  child: Image.network("https://suryahusadha.com/api/imgcard/berobat/?cabang="+cabang+"&nrm="+nrm+"&bodVal="+bod+"&dummy=${new DateTime.now()}"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _shocabang(dt){
    return new SafeArea(
      child: new Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: new Center(
          child: new Container(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Container(
                  child: new Card(
                      color: Colors.white,
                      child: new Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                        child: new Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text("Pilih Unit Bisnis / Cabang",style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 16
                            ), textAlign: TextAlign.center,),
                            Text("Akun: "+dt["nama_klg"], textAlign: TextAlign.center,),
                          ],
                        ),
                      )
                  ),
                  padding: EdgeInsets.only(left: 40, right: 40, bottom: 10),
                ),
                new Container(
                  child: new Card(
                    color: Colors.white,
                    child: InkWell(
                      onTap: (){
                        showDialog(
                          barrierDismissible: true,
                          context: context,
                          builder: (BuildContext context){
                            return ecardberobat("denpasar", dt["nrmdps_klg"].toString(), dt["tgllahir_klg"].toString());
                          },
                        );
                      },
                      child: new Container(
                        padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("RSU. Surya Husadha Denpasar"),
                            Icon(
                              FontAwesomeIcons.arrowRight,
                              size: 14,
                              color: Colors.grey[600],
                            )
                          ],
                        ),
                      ),
                    )
                  ),
                  padding: EdgeInsets.only(left: 40, right: 40, bottom: 0),
                ),
                new Container(
                  child: new Card(
                      color: Colors.white,
                      child: new InkWell(
                        onTap: (){
                          showDialog(
                            barrierDismissible: true,
                            context: context,
                            builder: (BuildContext context){
                              return ecardberobat("ubung", dt["nrmub_klg"].toString(), dt["tgllahir_klg"].toString());
                            },
                          );
                        },
                        child: new Container(
                          padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text("RSU. Surya Husadha Ubung"),
                              Icon(
                                FontAwesomeIcons.arrowRight,
                                size: 14,
                                color: Colors.grey[600],
                              )
                            ],
                          ),
                        ),
                      )
                  ),
                  padding: EdgeInsets.only(left: 40, right: 40, bottom: 0),
                ),
                new Container(
                  child: new Card(
                      color: Colors.white,
                      child: new InkWell(
                        onTap: (){
                          showDialog(
                            barrierDismissible: true,
                            context: context,
                            builder: (BuildContext context){
                              return ecardberobat("nusadua", dt["nrmnd_klg"].toString(), dt["tgllahir_klg"].toString());
                            },
                          );
                        },
                        child: new Container(
                          padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text("RSU. Surya Husadha Nusadua"),
                              Icon(
                                FontAwesomeIcons.arrowRight,
                                size: 14,
                                color: Colors.grey[600],
                              )
                            ],
                          ),
                        ),
                      )
                  ),
                  padding: EdgeInsets.only(left: 40, right: 40, bottom: 0),
                )
              ],
            ),
          ),
        )
      ),
    );
  }

  Widget cardDt(BuildContext context, dt){
    print(dt.toString());
    Color _txtcolor;
    if( dt["enabled_klg"] == "0"){
      _txtcolor = Colors.grey[400];
    }else{
      _txtcolor = Colors.grey[800];
    }

    return new Container(
      color: Colors.white,
      margin: EdgeInsets.only(top: 5.0),
      child: InkWell(
         onTap: (){
           showDialog(
             barrierDismissible: true,
             context: context,
             builder: (BuildContext context){
               return _shocabang(dt);
             },
           );
        },
        child: new Column(
          children: <Widget>[
            new Container(
              padding: EdgeInsets.only(top:5, bottom: 5, left: 16, right: 16),
              child:new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Container(
                    width: 40.0,
                    height: 40.0,
                    alignment: Alignment.center,
                    child: new Icon(
                      FontAwesomeIcons.handPointRight,
                      color: _txtcolor,
                      size: 24.0,
                    ),
                  ),
                  new Expanded(
                    flex: 2,
                    child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Container(
                            alignment: Alignment.topLeft,
                            padding: EdgeInsets.only(left:10.0, right:10.0),
                            child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    alignment: Alignment.topLeft,
                                    child:new Text(
                                      dt["nama_klg"],
                                      style: TextStyle(fontFamily: "NeoSansBold", fontSize: 14.0, color: _txtcolor),
                                      textAlign: TextAlign.left,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      softWrap: false,
                                    ),
                                  ),
                                  new Container(
                                    alignment: Alignment.topLeft,
                                    child: new Text(
                                      dt["hubungan_klg"],
                                      style: TextStyle(color: _txtcolor, fontSize: 12.0),
                                      textAlign: TextAlign.left,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      softWrap: false,
                                    ),
                                  ),
                                ]
                            ),
                          ),
                        ]
                    ),
                  ),
                  new Container(
                    height: 40.0,
                    width: 40.0,
                    alignment: Alignment.centerRight,
                    child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          new Container(
                            padding: EdgeInsets.all(6.0),
                            alignment: Alignment.centerRight,
                            child: new Icon(
                              FontAwesomeIcons.angleRight,
                              color: Colors.grey,
                              size: 18.0,
                            ),
                          ),
                        ]
                    ),
                  ),
                ],
              ),
            ),
            new Container(
              margin: EdgeInsets.only(top: 5.0),
              width: double.infinity,
              height: 1.0,
              color: Colors.grey[200],
            ),
          ],
        )
      ),
    );
  }
}