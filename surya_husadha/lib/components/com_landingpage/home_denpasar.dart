import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:surya_husadha/helper/animation/animationList.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:surya_husadha/modules/mod_fiturecontent/fiturecontent_controller.dart';
import 'package:surya_husadha/modules/mod_hcontent/hcontent_controller.dart';
import 'package:surya_husadha/modules/mod_mainmenu/mainmenu_controller.dart';
import 'package:surya_husadha/modules/mod_navbottom/navbottom_controller.dart';
import 'package:surya_husadha/modules/mod_slavemenu/slavemenu_controller.dart';
import 'package:surya_husadha/modules/mod_userstatus/userstatus_controller.dart';

class BerandaDps extends StatefulWidget {

  BerandaDps({
    Key key,
    @required this.dtakun,
    @required this.params,
  }) : super(key: key);

  List dtakun;
  var params;

  @override
  _BerandaDpsState createState() => new _BerandaDpsState();

}

class _BerandaDpsState extends State<BerandaDps> with SingleTickerProviderStateMixin{

  AnimationController _controllerDps;
  GlobalKey<ScaffoldState> scafolt_key = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
     _controllerDps = new AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );
    _controllerDps.forward();
  }

  @override
  void dispose() {
    _controllerDps.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.white,
        systemNavigationBarDividerColor: Colors.grey[300],
        systemNavigationBarIconBrightness: Brightness.dark,// navigation bar color
        statusBarColor: WarnaCabang.shh2,
        statusBarIconBrightness: Brightness.dark// statu// status bar color
    ));
    return AnimatedBuilder(
        animation: _controllerDps,
        builder: (BuildContext context, Widget child) {
          return new SafeArea(
            child: new Scaffold(
              key: scafolt_key,
              drawer: drawerController(widget.dtakun, "denpasar", "BerandaDps"),
              appBar: new PreferredSize(
                preferredSize: Size.fromHeight(200.0),
                child: AppBarController("denpasar", scafolt_key, widget.dtakun, "BerandaDps"),
              ),
              backgroundColor: Colors.grey[200],
              body: WillPopScope(
                onWillPop: (){
                  scafolt_key.currentState.removeCurrentSnackBar();
                  final snackBar = SnackBar(
                    content: Text('Apakah Anda akan keluar dari halaman ini?'),
                    action: SnackBarAction(
                      label: 'Ya',
                      onPressed: () {
                        SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                      },
                    ),
                  );
                  scafolt_key.currentState.showSnackBar(snackBar);
                },
                child: new Container(
                  child: new ListView(
                    physics: ClampingScrollPhysics(),
                    children: <Widget>[
                      new Container(
                        child: new Column(
                          children: <Widget>[
                            UserStatusController("denpasar", widget.dtakun, scafolt_key),
                          ],
                        ),
                      ),
                      new Container(
                          padding: EdgeInsets.only(left: 16.0, right: 16.0),
                          child: new Column(
                            children: <Widget>[
                              mainMenuController("denpasar"),
                              slaveMenuController("denpasar", widget.dtakun, scafolt_key),
                            ],
                          )
                      ),
                      new Container(
                        child: new Column(
                          children: <Widget>[
                            HctnController("PRODUK SPESIAL", "36", 5, 0, "denpasar", widget.dtakun),
                            HctnController("TODAY-INFO", "34", 5, 1, "denpasar", widget.dtakun),
                            HctnController("SURYA-TIPS", "68", 5, 1, "denpasar", widget.dtakun),
                            FitureContent("ARTIKEL", "18", 5, WarnaCabang.shh, Colors.white, "denpasar", widget.dtakun),
                            FitureContent("AKTIFITAS", "26", 5, WarnaCabang.shh, Colors.white, "denpasar", widget.dtakun),
                            FitureContent("INFORMASI KARIR", "27", 5, WarnaCabang.shh, Colors.white, "denpasar", widget.dtakun),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              bottomNavigationBar: NavBottom("denpasar"),
            ),
          );
        }
    );
  }
}