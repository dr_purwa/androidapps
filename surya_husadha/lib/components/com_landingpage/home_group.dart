import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:surya_husadha/modules/mod_fiturecontent/fiturecontent_controller.dart';
import 'package:surya_husadha/modules/mod_hcontent/hcontent_controller.dart';
import 'package:surya_husadha/modules/mod_slavemenu/slavemenu_controller.dart';
import 'package:surya_husadha/modules/mod_sliders/sliders_controller.dart';
import 'package:surya_husadha/modules/mod_userstatus/userstatus_controller.dart';
import 'package:surya_husadha/modules/mod_navbottom/navbottom_controller.dart';

class BerandaGr extends StatefulWidget {

   BerandaGr({
     Key key,
     @required this.dtakun,
     @required this.params,
   }) : super(key: key);

   List dtakun;
   var params;

  @override
  _BerandaGrState createState() => new _BerandaGrState();

}

class _BerandaGrState extends State<BerandaGr> with SingleTickerProviderStateMixin{

  AnimationController _controllerGr;
  GlobalKey<ScaffoldState> group_key = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _controllerGr = new AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );
    _controllerGr.forward();
  }

  @override
  void dispose() {
    _controllerGr.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.white,
        systemNavigationBarDividerColor: Colors.grey[300],
        systemNavigationBarIconBrightness: Brightness.dark,// navigation bar color
        statusBarColor: WarnaCabang.group2,
        statusBarIconBrightness: Brightness.dark// status bar color
    ));
    return AnimatedBuilder(
        animation: _controllerGr,
        builder: (BuildContext context, Widget child) {
          return new SafeArea(
            child: new Scaffold(
              key: group_key,
              drawer: drawerController(widget.dtakun, "group", "group"),
              appBar: new PreferredSize(
                preferredSize: Size.fromHeight(200.0),
                child: AppBarController("group", group_key, widget.dtakun, "BerandaGr"),
              ),
              backgroundColor: Colors.grey[200],
              body: new WillPopScope(
                onWillPop: (){
                  group_key.currentState.removeCurrentSnackBar();
                  final snackBar = SnackBar(
                    content: Text('Apakah Anda akan keluar dari halaman ini?'),
                    action: SnackBarAction(
                      label: 'Ya',
                      onPressed: () {
                        SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                      },
                    ),
                  );
                  group_key.currentState.showSnackBar(snackBar);
                },
                child: new Container(
                  child: new ListView(
                    physics: ClampingScrollPhysics(),
                    children: <Widget>[
                      new Container(
                        child: new Column(
                          children: <Widget>[
                            slidersController(),
                            UserStatusController("group", widget.dtakun, group_key),
                          ],
                        ),
                      ),
                      new Container(
                          padding: EdgeInsets.only(left: 16.0, right: 16.0),
                          child: new Column(
                            children: <Widget>[
                              slaveMenuController("group", widget.dtakun, group_key),
                            ],
                          )
                      ),
                      new Container(
                        child: new Column(
                          children: <Widget>[
                            HctnController("PRODUK SPESIAL", "36", 5, 0, "group", widget.dtakun),
                            HctnController("TODAY-INFO", "34", 5, 1, "group", widget.dtakun),
                            HctnController("SURYA-TIPS", "68", 5, 1, "group", widget.dtakun),
                            FitureContent("ARTIKEL", "18", 5, WarnaCabang.group, Colors.grey[600], "group", widget.dtakun),
                            FitureContent("AKTIFITAS", "26", 5, WarnaCabang.group, Colors.grey[600], "group", widget.dtakun),
                            FitureContent("INFORMASI KARIR", "27", 5, WarnaCabang.group, Colors.grey[600], "group", widget.dtakun),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              bottomNavigationBar: NavBottom("group"),
            ),
          );
        }
    );
  }
}