import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:surya_husadha/helper/animation/animationList.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:surya_husadha/modules/mod_fiturecontent/fiturecontent_controller.dart';
import 'package:surya_husadha/modules/mod_hcontent/hcontent_controller.dart';
import 'package:surya_husadha/modules/mod_mainmenu/mainmenu_controller.dart';
import 'package:surya_husadha/modules/mod_navbottom/navbottom_controller.dart';
import 'package:surya_husadha/modules/mod_slavemenu/slavemenu_controller.dart';
import 'package:surya_husadha/modules/mod_userstatus/userstatus_controller.dart';

class BerandaUb extends StatefulWidget {

  BerandaUb({
    Key key,
    @required this.dtakun,
    @required this.params,
  }) : super(key: key);

  List dtakun;
  var params;

  @override
  _BerandaUbState createState() => new _BerandaUbState();

}

class _BerandaUbState extends State<BerandaUb> with SingleTickerProviderStateMixin{

  AnimationList _animationUb;
  AnimationController _controllerUb;
  GlobalKey<ScaffoldState> scafolt_key = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _controllerUb = new AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );
    _controllerUb.forward();
  }

  @override
  void dispose() {
    _controllerUb.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _animationUb = new AnimationList(_controllerUb);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.white,
        systemNavigationBarDividerColor: Colors.grey[300],
        systemNavigationBarIconBrightness: Brightness.dark,// navigation bar color
        statusBarColor: WarnaCabang.ubung2,
        statusBarIconBrightness: Brightness.dark// statu// status bar color
    ));
    return AnimatedBuilder(
        animation: _controllerUb,
        builder: (BuildContext context, Widget child) {
          return new SafeArea(
            child: new Scaffold(
              key: scafolt_key,
              drawer: drawerController(widget.dtakun, "ubung", "BerandaUb"),
              appBar: new PreferredSize(
                preferredSize: Size.fromHeight(200.0),
                child: AppBarController("ubung", scafolt_key, widget.dtakun, "BerandaUb"),
              ),
              backgroundColor: Colors.grey[200],
              body: new WillPopScope(
                onWillPop: (){
                  scafolt_key.currentState.removeCurrentSnackBar();
                  final snackBar = SnackBar(
                    content: Text('Apakah Anda akan keluar dari halaman ini?'),
                    action: SnackBarAction(
                      label: 'Ya',
                      onPressed: () {
                        SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                      },
                    ),
                  );
                  scafolt_key.currentState.showSnackBar(snackBar);
                },
                child: new Container(
                  child: new ListView(
                    physics: ClampingScrollPhysics(),
                    children: <Widget>[
                      new Container(
                        child: new Column(
                          children: <Widget>[
                            UserStatusController("ubung", widget.dtakun, scafolt_key),
                          ],
                        ),
                      ),
                      new Container(
                          padding: EdgeInsets.only(left: 16.0, right: 16.0),
                          child: new Column(
                            children: <Widget>[
                              mainMenuController("ubung"),
                              slaveMenuController("ubung", widget.dtakun, scafolt_key),
                            ],
                          )
                      ),
                      new Container(
                        child: new Column(
                          children: <Widget>[
                            HctnController("PRODUK SPESIAL", "36", 5, 0, "ubung", widget.dtakun),
                            HctnController("TODAY-INFO", "34", 5, 1, "ubung", widget.dtakun),
                            HctnController("SURYA-TIPS", "68", 5, 1, "ubung", widget.dtakun),
                            FitureContent("ARTIKEL", "18", 5, WarnaCabang.ubung, Colors.white, "ubung", widget.dtakun),
                            FitureContent("AKTIFITAS", "26", 5, WarnaCabang.ubung, Colors.white, "ubung", widget.dtakun),
                            FitureContent("INFORMASI KARIR", "27", 5, WarnaCabang.ubung, Colors.white, "ubung", widget.dtakun),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              bottomNavigationBar: NavBottom("ubung"),
            ),
          );
        }
    );
  }
}