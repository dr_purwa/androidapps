import 'dart:async';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:surya_husadha/components/com_reservasi/reservasi_models.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/dialogue/customDialogue.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:masked_text_input_formatter/masked_text_input_formatter.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'dart:convert' as convert;

class EditformKlg extends StatefulWidget {

  var _akun;
  String _cabang;
  String _page;
  Color _color1;
  Color _color2;
  Color _textColor = Colors.white;
  Color _callendarColor;
  var _id;
  var _title;
  var _value;
  var _icon;
  var _column;
  var _anak_ke;
  var _nama;
  var _dt;
  var tgllahir_klg;
  List params;

  EditformKlg(List params, akun){
    this._akun = akun;
    this.params = params;
    params.forEach((dt){
      this._dt = dt["_dt"];
      this._page = dt["_page"];
      this._cabang = dt["_cabang"];
      this._nama = dt["_nama"];
      this._id = dt["_id"];
      this._title = dt["_title"];
      this._value = dt["_value"];
      this._icon = dt["_icon"];
      this._column = dt["_column"];
      this._anak_ke = dt["_anak_ke"];
      this.tgllahir_klg = dt["_tgllahir_klg"];
      if(_cabang=="group"){
        this._textColor = Colors.grey[600];
        this._callendarColor = WarnaCabang.shh;
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
      }
      if(_cabang=="denpasar"){
        this._callendarColor = WarnaCabang.shh;
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
      }
      if(_cabang=="ubung"){
        this._callendarColor = WarnaCabang.ubung;
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
      }
      if(_cabang=="nusadua"){
        this._callendarColor = WarnaCabang.nusadua;
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
      }
      if(_cabang=="kmc"){
        this._callendarColor = WarnaCabang.kmc;
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
      }
    });
  }

  @override
  _EditformKlg createState() => new _EditformKlg();
}

class Hubungan{
  const Hubungan(this.index, this.title);
  final String index;
  final String title;
}

enum ConfirmAction { CANCEL, ACCEPT }
Future<ConfirmAction> _asyncConfirmDialog(BuildContext context) async {
  return showDialog<ConfirmAction>(
    context: context,
    barrierDismissible: false, // user must tap button for close dialog!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Hapusa data ini?'),
        content: const Text(
            'Data keluarga ini akan dihapus dari akun Anda'),
        actions: <Widget>[
          FlatButton(
            child: const Text('Tidak', style: TextStyle(color: Colors.black),),
            onPressed: () {Navigator.of(context).pop(ConfirmAction.CANCEL);},
          ),
          FlatButton(
            child: const Text('Ya', style: TextStyle(color: Colors.black),),
            onPressed: () {Navigator.of(context).pop(ConfirmAction.ACCEPT);},
          )
        ],
      );
    },
  );
}

class _EditformKlg extends State<EditformKlg> with SingleTickerProviderStateMixin{
  var newakun;
  Color _fillColor = Colors.white.withOpacity(0.6);
  final _ScafoltkeyFrm= GlobalKey<ScaffoldState>();
  bool selectYears = false;
  //Hubungan
  bool _hubNull = false;
  Hubungan selectedhub;
  String _shortindex;
  List<Hubungan> hub = <Hubungan>[
    const Hubungan("0", "Pilih Hubungan Keluarga"),
    const Hubungan("1", "Kakek"),
    const Hubungan("2", "Nenek"),
    const Hubungan("3", "Bapak"),
    const Hubungan("4", "Ibu"),
    const Hubungan("5", "Suami"),
    const Hubungan("6", "Istri"),
    const Hubungan("7", "Kakak"),
    const Hubungan("8", "Adik"),
    const Hubungan("9", "Anak"),
  ];
  //_anakke
  bool _anakkeVisibility = false;
  bool _anakkeNull = false;
  FocusNode _anakke = new FocusNode();
  TextEditingController _anakkeController = new TextEditingController();
  //_nama
  bool _namaNull = false;
  FocusNode _nama = new FocusNode();
  TextEditingController _namaController = new TextEditingController();
  //_tgllahir
  bool _tgllahirNull = false;
  FocusNode _tgllahir = new FocusNode();
  TextEditingController _tgllahirController = new TextEditingController();
  //gender
  bool _genderNull = false;
  Gender selectedGender;
  List<Gender> gender = <Gender>[
    const Gender("", "Pilih Jenis Kelamin", FontAwesomeIcons.ban),
    const Gender("M", "Laki - Laki", FontAwesomeIcons.male),
    const Gender("F", "Perempuan", FontAwesomeIcons.female)
  ];
  //_mobile
  bool _mobileNull = false;
  FocusNode _mobile = new FocusNode();
  TextEditingController _mobileController = new TextEditingController();
  //_aktifkanakun
  int _aktifkanakun;
  Color aktif;
  Color nonaktif;
  Color delete;
  //_rmshh
  FocusNode _rmshh = new FocusNode();
  TextEditingController _rmshhController = new TextEditingController();
  //_rmub
  FocusNode _rmub = new FocusNode();
  TextEditingController _rmubController = new TextEditingController();
  //_rmnd
  FocusNode _rmnd = new FocusNode();
  TextEditingController _rmndController = new TextEditingController();
  //_bpjsid
  FocusNode _bpjsid = new FocusNode();
  TextEditingController _bpjsidController = new TextEditingController();

  @override
  void initState(){
    if(widget._column == "hubungan_klg"){
      if(widget._value != null){
        int _idx;
        if(widget._value == "Kakek"){_idx = 1;}
        if(widget._value == "Nenek"){_idx = 2;}
        if(widget._value == "Bapak"){_idx = 3;}
        if(widget._value == "Ibu"){_idx = 4;}
        if(widget._value == "Suami"){_idx = 5;}
        if(widget._value == "Istri"){_idx = 6;}
        if(widget._value == "Kakak"){_idx = 7;}
        if(widget._value == "Adik"){_idx = 8;}
        if(widget._value == "Anak"){_idx = 9;}
        selectedhub=hub[_idx];
        _shortindex=selectedhub.index;
        if(widget._value == "Anak"){
          _anakkeVisibility = true;
          _anakkeController.text = widget._anak_ke;
        }
      }else{
        selectedhub=hub[0];
        _shortindex=selectedhub.index;
      }
    }
    if(widget._column == "nama_klg"){
      _namaController.text = widget._value;
    }
    if(widget._column == "tgllahir_klg"){
      _tgllahirController.text = widget._value;
    }
    if(widget._column == "jeniskelamin_klg"){
      if(widget._value != null){
        if( widget._value =="Laki - Laki"){
          selectedGender = gender[1];
        }
        if( widget._value =="Perempuan"){
          selectedGender = gender[2];
        }
      }else{
        selectedGender=gender[0];
      }
    }
    if(widget._column == "mobile_klg"){
      _mobileController.text = widget._value;
    }
    if(widget._column == "nrmdps_klg"){
      _rmshhController.text = widget._value;
    }
    if(widget._column == "nrmub_klg"){
      _rmubController.text = widget._value;
    }
    if(widget._column == "nrmnd_klg"){
      _rmndController.text = widget._value;
    }
    if(widget._column == "bpjsid_klg"){
      _bpjsidController.text = widget._value;
    }
    if(widget._column == "enabled_klg"){
      if(widget._value == "Aktif"){
        _aktifkanakun = 1;
      }else{
        _aktifkanakun = 0;
      }
    }
    super.initState();
  }

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> params=[];
    params.add({
      "_cabang": widget._cabang,
      "_dt": widget._dt,
      "_page": "EditformKlg",
    });
    RouteHelper(context, "KeluargaDetail", widget.params);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context){
    if(newakun!=null){
      new Timer(const Duration(milliseconds: 500), () {
        Navigator.of(context).pop(newakun);
      });
      return new Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.grey,
        ),
      );
    }else{
      return new SafeArea(
          child: new Scaffold(
              key: _ScafoltkeyFrm,
              drawer: drawerController(widget._akun, widget._cabang, "TambahKlg"),
              appBar: new PreferredSize(
                preferredSize: Size.fromHeight(200.0),
                child: AppBarController(widget._cabang, _ScafoltkeyFrm,widget._akun, "TambahKlg"),
              ),
              body: new WillPopScope(
                  onWillPop: _onWillPop,
                  child: new Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      new Container(
                        child: new Column(
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                _header(),
                              ],
                            ),
                            _buildForm(context),
                            new Visibility(
                              visible: widget._column == "enabled_klg" ? true : false,
                              child: new Container(
                                padding: EdgeInsets.only(left: 24, right: 24),
                                margin: EdgeInsets.only(bottom:30, top: 60),
                                child: new Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: new Column(children: <Widget>[
                                        new RawMaterialButton(
                                            onPressed: ()=>{
                                              setState(() {
                                                _aktifkanakun = -1;
                                                List<Map<String, dynamic>> params=[];
                                                params.add({
                                                  "_id": widget._id.toString(),
                                                  "_column": "enabled_klg",
                                                  "_value": _aktifkanakun.toString(),
                                                });
                                                _asyncConfirmDialog(context).then((val){
                                                  if(val == ConfirmAction.ACCEPT){
                                                    showDialog(
                                                      barrierDismissible: false,
                                                      context: context,
                                                      builder: (BuildContext context) => showNotif(context, null, params),
                                                    );
                                                  }
                                                });
                                              }),
                                            },
                                            elevation: 0,
                                            padding: EdgeInsets.all(16),
                                            animationDuration: Duration(milliseconds: 500),
                                            fillColor: Colors.redAccent,
                                            //highlightColor:  highlight_lama,
                                            shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.only(
                                                  bottomLeft: Radius.circular(10),
                                                  topLeft: Radius.circular(10),
                                                  topRight: Radius.circular(10),
                                                  bottomRight: Radius.circular(10),
                                                )
                                            ),
                                            constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                            child: new Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: <Widget>[
                                                new Icon(
                                                  FontAwesomeIcons.trash,
                                                  color: Colors.white,
                                                  size: 18,
                                                ),
                                                new Container(width: 5,),
                                                new Text("Delete", style: TextStyle(color:  Colors.white, fontSize: 16),)
                                              ],)
                                        )
                                      ],),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            new Visibility(
                              visible: widget._column != "enabled_klg" ? true : false,
                              child: new Container(
                                padding: EdgeInsets.only(left: 24, right: 24),
                                margin: EdgeInsets.only(bottom:30, top: 60),
                                child: Column(
                                    children: <Widget>[
                                      new Align(
                                        alignment: Alignment.bottomCenter,
                                        child: RawMaterialButton(
                                          onPressed: (){
                                            bool next = true;
                                            setState(() {
                                              if(widget._column =="hubungan_klg"){
                                                _hubNull = false;
                                                if(selectedhub == hub[0]){
                                                  _hubNull = true;
                                                  next = false;
                                                }
                                                if(selectedhub.title == "Anak"){
                                                  _anakkeNull = false;
                                                  if(_anakkeController.text.isEmpty){
                                                    _anakkeNull = true;
                                                    next = false;
                                                  }
                                                }else{
                                                  _anakkeNull = false;
                                                  _anakkeController.text = null;
                                                }
                                              }
                                              if(widget._column =="nama_klg"){
                                                _namaNull = false;
                                                if(_namaController.text.isEmpty){
                                                  _namaNull = true;
                                                  next = false;
                                                }
                                              }
                                              if(widget._column =="tgllahir_klg"){
                                                _tgllahirNull = false;
                                                if(_tgllahirController.text.isEmpty){
                                                  _tgllahirNull = true;
                                                  next = false;
                                                }
                                              }
                                              if(widget._column =="jeniskelamin_klg"){
                                                _genderNull = false;
                                                if(selectedGender == gender[0] ){
                                                  _genderNull = true;
                                                  next = false;
                                                }
                                              }
                                            });
                                            if(next){
                                              List<Map<String, dynamic>> params=[];
                                              if(widget._column =="hubungan_klg") {
                                                params.add({
                                                  "_id": widget._id.toString(),
                                                  "_column": "hubungan_klg",
                                                  "_value": selectedhub.title,
                                                  "_anak_ke": _anakkeController.text,
                                                  "_shortindex": _shortindex,
                                                });
                                              }
                                              if(widget._column =="nama_klg"){
                                                params.add({
                                                  "_id": widget._id.toString(),
                                                  "_column": "nama_klg",
                                                  "_value": _namaController.text,
                                                });
                                              }
                                              if(widget._column =="tgllahir_klg"){
                                                params.add({
                                                  "_id": widget._id.toString(),
                                                  "_column": "tgllahir_klg",
                                                  "_value": _tgllahirController.text,
                                                });
                                              }
                                              if(widget._column =="jeniskelamin_klg"){
                                                params.add({
                                                  "_id": widget._id.toString(),
                                                  "_column": "jeniskelamin_klg",
                                                  "_value": selectedGender.val,
                                                });
                                              }
                                              if(widget._column =="mobile_klg"){
                                                params.add({
                                                  "_id": widget._id.toString(),
                                                  "_column": "mobile_klg",
                                                  "_value": _mobileController.text,
                                                });
                                              }
                                              if(widget._column =="nrmdps_klg"){
                                                params.add({
                                                  "_id": widget._id.toString(),
                                                  "_column": "nrmdps_klg",
                                                  "_value": _rmshhController.text,
                                                  "_bod": widget.tgllahir_klg.toString(),
                                                });
                                              }
                                              if(widget._column =="nrmub_klg"){
                                                params.add({
                                                  "_id": widget._id.toString(),
                                                  "_column": "nrmub_klg",
                                                  "_value": _rmubController.text,
                                                  "_bod": widget.tgllahir_klg.toString(),
                                                });
                                              }
                                              if(widget._column =="nrmnd_klg"){
                                                params.add({
                                                  "_id": widget._id.toString(),
                                                  "_column": "nrmnd_klg",
                                                  "_value": _rmndController.text,
                                                  "_bod": widget.tgllahir_klg.toString(),
                                                });
                                              }
                                              if(widget._column =="bpjsid_klg"){
                                                params.add({
                                                  "_id": widget._id.toString(),
                                                  "_column": "bpjsid_klg",
                                                  "_value": _bpjsidController.text,
                                                });
                                              }
                                              showDialog(
                                                barrierDismissible: false,
                                                context: context,
                                                builder: (BuildContext context) => showNotif(context, null, params),
                                              );
                                            }
                                          },
                                          elevation: 0,
                                          padding: EdgeInsets.all(16),
                                          animationDuration: Duration(milliseconds: 500),
                                          fillColor: widget._color1,
                                          splashColor: widget._color2,
                                          highlightColor: widget._color2,
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                              side: BorderSide(color: Colors.grey[300])
                                          ),
                                          constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                          child: new Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              new Expanded(
                                                flex: 1,
                                                child: new Row(
                                                  mainAxisSize: MainAxisSize.max,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    new Column(children: <Widget>[
                                                      Text(
                                                        "Simpan",
                                                        style: TextStyle(color: widget._textColor, fontSize: 18),
                                                      ),
                                                    ],),
                                                  ],
                                                ),
                                              ),
                                              Icon(
                                                FontAwesomeIcons.arrowRight,
                                                color: widget._textColor,
                                                size: 16,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ]
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  )
              )
          )
      );
    }
  }

  bool sendingData = true;
  @override
  Future<String> postData(context, List<Map<String, dynamic>> dtNext) async {
    var jsonResponse;
    if(sendingData){
      var api = new Http_Controller();
      var _data = await api.postResponse("https://suryahusadha.com/api/akun/editakunklg", dtNext);
      jsonResponse = convert.jsonEncode(_data);
      Navigator.pop(context);
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => showNotif(context, jsonResponse.toString(), null),
      ).then((val){
        setState(() {
          newakun = val;
        });
      });
    }
    return jsonResponse.toString();
  }

  Widget showNotif(context, _jsonResponse, List<Map<String, dynamic>> dtNext) {
    if (dtNext != null) {
      newakun = null;
      postData(context, dtNext);
      sendingData = false;
      return new Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.grey,
        ),
      );
    } else {
      if(_jsonResponse != "" || _jsonResponse != null) {
        sendingData = true;
        var decode = convert.jsonDecode(_jsonResponse);
        if(decode["statusCode"] == 200){
          var jsonResponse = convert.jsonDecode(decode["responseBody"]);
          String msg = jsonResponse["editakunklg"][0]["msg"];
          String response = jsonResponse["editakunklg"][0]["response"];
          var newdata = jsonResponse["editakunklg"][0]["data"];
          if (response == "1"){
            return Dialogue(context, msg, widget._color1, widget._color2, null, newdata);
          } else {
            return Dialogue(context, msg, Colors.red, Colors.red, null, null);
          }
        }else{
          return Dialogue(context,
              "Mohon maaf, terjadi kesalahan pengolahan data pada saat simpan data, silahkan ulangi beberapa saat lagi!",
              Colors.red, Colors.red, null, null);
        }
      } else {
        return Dialogue(context,
            "Mohon maaf, terjadi kesalahan koneksi pada saat simpan data, silahkan ulangi beberapa saat lagi!",
            Colors.red, Colors.red, null, null);
      }
    }
  }

  Widget _header(){
    return new Container(
      color: Colors.grey[100],
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 18, bottom: 18),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Icon(
                        FontAwesomeIcons.infoCircle,
                        size: 18,
                        color: Colors.grey[800],
                      ),
                      new Container(
                        padding: EdgeInsets.only(left: 10, top: 3),
                        child: new Text(
                          "Formulir Edit Data Keluarga",
                          style: TextStyle(
                              color: Colors.grey[800],
                              fontSize: 16,
                              fontWeight: FontWeight.w700
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  color: Colors.grey[300],
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildForm(BuildContext context) {
    Widget _openform;
    switch(widget._column){
      case "hubungan_klg":
        _openform = hubungan_klg();
        break;
      case "nama_klg":
        _openform = nama_klg();
        break;
      case "tgllahir_klg":
        _openform = tgllahir_klg();
        break;
      case "jeniskelamin_klg":
        _openform = jeniskelamin_klg();
        break;
      case "mobile_klg":
        _openform = mobile_klg();
        break;
      case "nrmdps_klg":
        _openform = nrmdps_klg();
        break;
      case "nrmub_klg":
        _openform = nrmub_klg();
        break;
      case "nrmnd_klg":
        _openform = nrmnd_klg();
        break;
      case "bpjsid_klg":
        _openform = bpjsid_klg();
        break;
     case "enabled_klg":
        _openform = enabled_klg();
        break;
      default:
        _openform = normal_form();
    }

    return Expanded(
      child: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          new Container(
            padding: EdgeInsets.all(24),
            child: new Row(
              children: <Widget>[
                new Icon(
                  FontAwesomeIcons.userCircle
                ),
                new Container(
                  padding: EdgeInsets.only(left: 10, top: 3),
                  child: new Text(
                      widget._nama
                  ),
                )
              ],
            ),
          ),
          new Container(
              padding: EdgeInsets.only(left:24, right:24),
              child: new Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                        child: new Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              _openform,
                            ]
                        )
                    )
                  ]
              )
          )
        ],
      ),
    );
  }

  Widget normal_form(){
    return Container();
  }

  Widget hubungan_klg(){
    return new Column(
      children: <Widget>[
        new Container(
          margin: EdgeInsets.only(top: 30),
          child: new DropdownButtonFormField(
            value: selectedhub,
            onChanged: (Hubungan newval){
              setState(() {
                selectedhub = newval;
                _shortindex=selectedhub.index;
                if(newval.title == "Anak"){
                  _anakkeVisibility = true;
                }else{
                  _anakkeVisibility = false;
                  _anakkeController.text = null;
                }
              });
            },
            items: hub.map((Hubungan hub){
              return new DropdownMenuItem<Hubungan>(
                  value: hub,
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.only(top:3),
                        child:new Text(
                          hub.title,
                          style: new TextStyle(color: Colors.black, fontSize: 14),
                        ),
                      ),
                    ],
                  )
              );
            }).toList(),
            decoration: InputDecoration(
              filled: true,
              fillColor: _fillColor,
              labelStyle: TextStyle(
                color: Colors.grey[800],
              ),
              labelText: 'Hubungan Keluarga',
              errorText: _hubNull ? "" : null,
              prefixIcon: Icon(
                FontAwesomeIcons.users,
                color: Colors.grey[800],
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
              ),
              enabledBorder: const OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                borderSide: BorderSide(
                  color: Colors.grey,
                ),
              ),
            ),
          ),
        ),
        new Visibility(
          visible: _anakkeVisibility,
          child: new Container(
              padding: EdgeInsets.only(top: 20),
              child: TextField(
                focusNode: _anakke,
                controller: _anakkeController,
                textInputAction: TextInputAction.done,
                cursorColor: Colors.grey[800],
                style: TextStyle(color: Colors.grey[800], fontSize: 14),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: _fillColor,
                  labelStyle: TextStyle(
                    color: Colors.grey[800],
                  ),
                  errorText: _anakkeNull ? "" : null,
                  labelText: 'Anak Ke ?',
                  prefixIcon: Icon(
                    FontAwesomeIcons.child,
                    color: Colors.grey[800],
                  ),
                  focusedBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                    borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                  ),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                    borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                    borderSide: BorderSide(
                      color: Colors.grey,
                    ),
                  ),
                ),
              )
          ),
        ),
      ],
    );
  }

  Widget nama_klg(){
    return new Container(
        child: TextField(
          focusNode: _nama,
          controller: _namaController,
          textInputAction: TextInputAction.done,
          cursorColor: Colors.grey[800],
          style: TextStyle(color: Colors.grey[800], fontSize: 14),
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            filled: true,
            fillColor: _fillColor,
            labelStyle: TextStyle(
              color: Colors.grey[800],
            ),
            errorText: _namaNull ? "" : null,
            labelText: 'Nama Lengkap',
            prefixIcon: Icon(
              FontAwesomeIcons.tag,
              color: Colors.grey[800],
            ),
            focusedBorder: const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.grey, width: 1.0),
              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
            ),
            enabledBorder: const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.grey, width: 0.0),
              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
          ),
        )
    );
  }

  Widget tgllahir_klg(){
    showPickerDate(BuildContext context) {
      final f = new DateFormat('dd-MM-yyyy');
      DateTime _Selected;
      if(_tgllahirController.text != ""){
        int d = int.parse(_tgllahirController.text.split("-")[0]);
        int m = int.parse(_tgllahirController.text.split("-")[1]);
        int y = int.parse(_tgllahirController.text.split("-")[2]);
        _Selected = DateTime(y, m ,d);
      }else{
        _Selected = DateTime.now();
      }
      int _yNow = int.parse(DateFormat.y().format(_Selected));
      DateTime _max = DateTime(_yNow-100);

      Picker(
        hideHeader: true,
        changeToFirst: false,
        adapter: DateTimePickerAdapter(
          customColumnType: [0, 1, 2],
          value: _Selected,
          maxValue: DateTime.now(),
          minValue: _max,
        ),
        title: new Container(
          child: Column(children: <Widget>[
            Text("Pilih Tgl. Lahir:", style: TextStyle(color: widget._callendarColor),),
          ],),
        ),
        selectedTextStyle: TextStyle(color: widget._callendarColor),
        confirm: new InkWell(
          onTap: ()=>{
            Navigator.of(context).pop(),
            widget,_tgllahirController.text = f.format(_Selected).toString(),
          },
          child: Container(
            padding: EdgeInsets.all(10),
            child: Row(children: <Widget>[
              new Icon(
                FontAwesomeIcons.check,
                color: widget._callendarColor,
                size: 12,
              ),
              Container(width: 5,),
              Text("Pilih", style: TextStyle(color: widget._callendarColor),)
            ],),
          ),
        ),
        cancel: new InkWell(
          onTap: ()=>{
            Navigator.of(context).pop()
          },
          child: Container(
            padding: EdgeInsets.all(10),
            child: Row(children: <Widget>[
              new Icon(
                FontAwesomeIcons.times,
                color: widget._callendarColor,
                size: 12,
              ),
              Container(width: 2,),
              Text("Tutup", style: TextStyle(color: widget._callendarColor),)
            ],),
          ),
        ),
        onSelect: (Picker picker, int index, List<int> selecteds) {
          this.setState(() {
            int d = _Selected.day;
            int m = _Selected.month;
            int y = _Selected.year;
            if(index == 2){
              d = (picker.adapter as DateTimePickerAdapter).value.day;
              _Selected = DateTime(y, m, d);
            }
            if(index == 1){
              m = (picker.adapter as DateTimePickerAdapter).value.month;
              _Selected = DateTime(y, m, d);
            }
            if(index == 0){
              y = (picker.adapter as DateTimePickerAdapter).value.year;
              _Selected = DateTime(y, m, d);
            }

          });
        },
      ).showDialog(context);

    }

    return new Container(
        margin: EdgeInsets.only(top:20),
        child: TextField(
          onTap: (){
            showPickerDate(context);
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          inputFormatters: [
            MaskedTextInputFormatter(
              mask: 'xx-xx-xxxx',
              separator: '-',
            ),
          ],
          focusNode: _tgllahir,
          controller: _tgllahirController,
          textInputAction: TextInputAction.done,
          cursorColor: Colors.grey[800],
          style: TextStyle(color: Colors.grey[800], fontSize: 14),
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            filled: true,
            fillColor: _fillColor,
            labelStyle: TextStyle(
              color: Colors.grey[800],
            ),
            labelText: 'Tgl Lahir (dd-mm-YYYY)',
            errorText: _tgllahirNull ? "" : null,
            prefixIcon: Icon(
              FontAwesomeIcons.calendarCheck,
              color: Colors.grey[800],
            ),
            focusedBorder: const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.grey, width: 1.0),
              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
            ),
            enabledBorder: const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.grey, width: 0.0),
              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
          ),
        )
    );
  }

  Widget jeniskelamin_klg(){
    return new Container(
      margin: EdgeInsets.only(top:20),
      child: new DropdownButtonFormField(
        value: selectedGender,
        onChanged: (Gender newValue) {
          setState(() {
            if(newValue.val == ""){
              selectedGender = gender[0];
            }
            if(newValue.val == "M"){
              selectedGender = gender[1];
            }
            if(newValue.val == "F"){
              selectedGender = gender[2];
            }
          });
        },
        items: gender.map((Gender gender){
          return new DropdownMenuItem<Gender>(
              value: gender,
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Container(
                    padding: EdgeInsets.only(top:3),
                    child:new Text(
                      gender.title,
                      style: new TextStyle(color: Colors.black, fontSize: 14),
                    ),
                  ),
                  new Container(width: 5,),
                  new Icon(
                    gender.icon,
                    size: 16,
                  ),
                ],
              )
          );
        }).toList(),
        decoration: InputDecoration(
          filled: true,
          fillColor: _fillColor,
          labelStyle: TextStyle(
            color: Colors.grey[800],
          ),
          labelText: 'Jenis Kelamin',
          errorText: _genderNull ? "" : null,
          prefixIcon: Icon(
            FontAwesomeIcons.transgender,
            color: Colors.grey[800],
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.grey, width: 1.0),
            borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
          ),
          enabledBorder: const OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.grey, width: 0.0),
            borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
            borderSide: BorderSide(
              color: Colors.grey,
            ),
          ),
        ),
      ),
    );
  }

  Widget mobile_klg(){
    return new Container(
        margin: EdgeInsets.only(top:20),
        child: TextField(
          focusNode: _mobile,
          controller: _mobileController,
          textInputAction: TextInputAction.done,
          cursorColor: Colors.grey[800],
          style: TextStyle(color: Colors.grey[800], fontSize: 14),
          keyboardType: TextInputType.phone,
          decoration: InputDecoration(
            filled: true,
            fillColor: _fillColor,
            labelStyle: TextStyle(
              color: Colors.grey[800],
            ),
            labelText: 'Mobile Phone',
            errorText: _mobileNull ? "" : null,
            prefixIcon: Icon(
              FontAwesomeIcons.mobileAlt,
              color: Colors.grey[800],
            ),
            focusedBorder: const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.grey, width: 1.0),
              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
            ),
            enabledBorder: const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.grey, width: 0.0),
              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
          ),
        )
    );
  }

  Widget nrmdps_klg(){
    return new Container(
      child: new Column(
        children: <Widget>[
          new Container(
              padding: EdgeInsets.only(top: 30, bottom: 20),
              child: new Row(
                children: <Widget>[
                  new Icon(
                    FontAwesomeIcons.mapMarkerAlt,
                    size: 14,
                  ),
                  new Container(
                    padding: EdgeInsets.only(left: 10, top: 4),
                    child: new Text(
                      "Akun Berobat Surya Husadha Hospital",
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                      maxLines: 1,
                    ),
                  )
                ],
              )
          ),
          new Container(
              child: TextField(
                inputFormatters: [
                  MaskedTextInputFormatter(
                    mask: 'xx.xx.xx',
                    separator: '.',
                  ),
                ],
                focusNode: _rmshh,
                controller: _rmshhController,
                textInputAction: TextInputAction.done,
                cursorColor: Colors.grey[800],
                style: TextStyle(color: Colors.grey[800], fontSize: 14),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: _fillColor,
                  labelStyle: TextStyle(
                    color: Colors.grey[800],
                  ),
                  labelText: 'No. Rekam Medis (xx.xx.xx)',
                  prefixIcon: Icon(
                    FontAwesomeIcons.idBadge,
                    color: Colors.grey[800],
                  ),
                  focusedBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                    borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                  ),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                    borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                    borderSide: BorderSide(color: Colors.grey),
                  ),
                ),
              )
          ),
        ],
      ),
    );
  }

  Widget nrmub_klg(){
    return Container(
      child: new Column(
        children: <Widget>[
          new Container(
              padding: EdgeInsets.only(top: 20, bottom: 20),
              child: new Row(
                children: <Widget>[
                  new Icon(
                    FontAwesomeIcons.mapMarkerAlt,
                    size: 14,
                  ),
                  new Container(
                    padding: EdgeInsets.only(left: 10, top: 4),
                    child: new Text(
                      "Akun Berobat Surya Husadha Ubung",
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                      maxLines: 1,
                    ),
                  )
                ],
              )
          ),
          new Container(
              child: TextField(
                inputFormatters: [
                  MaskedTextInputFormatter(
                    mask: 'xx.xx.xx',
                    separator: '.',
                  ),
                ],
                focusNode: _rmub,
                controller: _rmubController,
                textInputAction: TextInputAction.done,
                cursorColor: Colors.grey[800],
                style: TextStyle(color: Colors.grey[800], fontSize: 14),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: _fillColor,
                  labelStyle: TextStyle(
                    color: Colors.grey[800],
                  ),
                  labelText: 'No. Rekam Medis (xx.xx.xx)',
                  prefixIcon: Icon(
                    FontAwesomeIcons.idBadge,
                    color: Colors.grey[800],
                  ),
                  focusedBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                    borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                  ),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                    borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                    borderSide: BorderSide(color: Colors.grey),
                  ),
                ),
              )
          ),
        ],
      ),
    );
  }

  Widget nrmnd_klg(){
    return Container(
      child: new Column(
        children: <Widget>[
          new Container(
              padding: EdgeInsets.only(top: 20, bottom: 20),
              child: new Row(
                children: <Widget>[
                  new Icon(
                    FontAwesomeIcons.mapMarkerAlt,
                    size: 14,
                  ),
                  new Container(
                    padding: EdgeInsets.only(left: 10, top: 4),
                    child: new Text(
                      "Akun Berobat Surya Husadha Nusadua",
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                      maxLines: 1,
                    ),
                  )
                ],
              )
          ),
          new Container(
              child: TextField(
                inputFormatters: [
                  MaskedTextInputFormatter(
                    mask: 'xx.xx.xx',
                    separator: '.',
                  ),
                ],
                focusNode: _rmnd,
                controller: _rmndController,
                textInputAction: TextInputAction.done,
                cursorColor: Colors.grey[800],
                style: TextStyle(color: Colors.grey[800], fontSize: 14),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: _fillColor,
                  labelStyle: TextStyle(
                    color: Colors.grey[800],
                  ),
                  labelText: 'No. Rekam Medis (xx.xx.xx)',
                  prefixIcon: Icon(
                    FontAwesomeIcons.idBadge,
                    color: Colors.grey[800],
                  ),
                  focusedBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                    borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                  ),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                    borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                    borderSide: BorderSide(color: Colors.grey),
                  ),
                ),
              )
          ),
        ],
      ),
    );
  }

  Widget bpjsid_klg(){
    return new Column(
      children: <Widget>[
        new Container(
            padding: EdgeInsets.only(top: 0, bottom: 20),
            child: new Row(
              children: <Widget>[
                new Icon(
                  FontAwesomeIcons.idCard,
                  size: 14,
                ),
                new Container(
                  padding: EdgeInsets.only(left: 10, top: 4),
                  child: new Text(
                    "Nomor ID BPJS",
                    overflow: TextOverflow.ellipsis,
                    softWrap: true,
                    maxLines: 1,
                  ),
                )
              ],
            )
        ),
        new Container(
            child: TextField(
              focusNode: _bpjsid,
              controller: _bpjsidController,
              textInputAction: TextInputAction.done,
              cursorColor: Colors.grey[800],
              style: TextStyle(color: Colors.grey[800], fontSize: 14),
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                filled: true,
                fillColor: _fillColor,
                labelStyle: TextStyle(
                  color: Colors.grey[800],
                ),
                labelText: 'No. Kartu BPJS',
                prefixIcon: Icon(
                  FontAwesomeIcons.idBadge,
                  color: Colors.grey[800],
                ),
                focusedBorder: const OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                ),
                enabledBorder: const OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                  borderSide: BorderSide(color: Colors.grey),
                ),
              ),
            )
        )
      ],
    );
  }
  Widget enabled_klg(){
    if(_aktifkanakun == 1){
      aktif = widget._color1;
      nonaktif = Colors.grey[300];
    }
    if(_aktifkanakun == 0){
      aktif = Colors.grey[300];
      nonaktif = widget._color1;
    }
    return new Column(
      children: <Widget>[
        new Container(
          padding: EdgeInsets.only(bottom: 10),
          child: new Row(
            children: <Widget>[
              new Text(
                  "Update status akun"
              ),
            ],
          )
        ),
        new Container(
          child: new Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: new Column(children: <Widget>[
                  new RawMaterialButton(
                      onPressed: ()=>{
                        setState(() {
                          _aktifkanakun = 1;
                          List<Map<String, dynamic>> params=[];
                          params.add({
                            "_id": widget._id.toString(),
                            "_column": "enabled_klg",
                            "_value": _aktifkanakun.toString(),
                          });
                          showDialog(
                            barrierDismissible: false,
                            context: context,
                            builder: (BuildContext context) => showNotif(context, null, params),
                          );
                        }),
                      },
                      elevation: 0,
                      padding: EdgeInsets.all(16),
                      animationDuration: Duration(milliseconds: 500),
                      fillColor: aktif,
                      //highlightColor:  highlight_lama,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10),
                              topLeft: Radius.circular(10)
                          )
                      ),
                      constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Icon(
                            FontAwesomeIcons.check,
                            color: widget._textColor,
                            size: 18,
                          ),
                          new Container(width: 5,),
                          new Text("Aktif", style: TextStyle(color: widget._textColor, fontSize: 16),)
                        ],)
                  )
                ],),
              ),
              Expanded(
                flex: 1,
                child: new Column(children: <Widget>[
                  new RawMaterialButton(
                      onPressed: ()=>{
                        setState(() {
                          _aktifkanakun = 0;
                          List<Map<String, dynamic>> params=[];
                          params.add({
                            "_id": widget._id.toString(),
                            "_column": "enabled_klg",
                            "_value": _aktifkanakun.toString(),
                          });
                          showDialog(
                            barrierDismissible: false,
                            context: context,
                            builder: (BuildContext context) => showNotif(context, null, params),
                          );
                        }),
                      },
                      elevation: 0,
                      padding: EdgeInsets.all(16),
                      animationDuration: Duration(milliseconds: 500),
                      fillColor: nonaktif,
                      //highlightColor: highlight_baru,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(10),
                              topRight: Radius.circular(10)
                          )
                      ),
                      constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Icon(
                            FontAwesomeIcons.times,
                            color: widget._textColor,
                            size: 18,
                          ),
                          new Container(width: 5,),
                          new Text("Tidak Akitf", style: TextStyle(color: widget._textColor, fontSize: 16),)
                        ],)
                  )
                ],),
              ),
            ],
          ),
        ),
      ],
    );
  }
}