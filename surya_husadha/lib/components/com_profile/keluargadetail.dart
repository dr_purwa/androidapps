import 'dart:async';

import 'package:flutter/material.dart';
import 'package:surya_husadha/components/com_profile/editformklg.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/helper/transition/slide_route.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surya_husadha/pages/widgetpage.dart';


class KeluargaDetail extends StatefulWidget {
  var _akun;
  var _dt;
  String _cabang;
  String _page;
  Color _color1;
  Color _color2;
  KeluargaDetail(List params, akun){
    this._akun = akun;
    params.forEach((dt){
      this._page = dt["_page"];
      this._dt = dt["_dt"];
      this._cabang = dt["_cabang"];
      if(_cabang=="group"){
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
      }
      if(_cabang=="denpasar"){
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
      }
      if(_cabang=="ubung"){
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
      }
      if(_cabang=="nusadua"){
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
      }
      if(_cabang=="kmc"){
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
      }
    });
  }
  @override
  _KeluargaDetail createState() => new _KeluargaDetail();
}

class _KeluargaDetail extends State<KeluargaDetail>{
  var newakun;
  final _Scafoltkey = GlobalKey<ScaffoldState>();
  List newAkun;

  Future<bool> _onWillPop(){
    //Navigator.pop(context);
    List<Map<String, dynamic>> dtNext=[];
    dtNext.add({
      "_cabang": widget._cabang,
      "_dt": widget._dt,
      "_page": "KeluargaDetail",
    });
    RouteHelper(context, "AkunBerobatKeluarga", dtNext);
  }

  @override
  Widget build(BuildContext context){
    if(newakun!=null){
      widget._dt = newakun;
      if(newakun["enabled_klg"]== "-1"){
        new Timer(const Duration(milliseconds: 500), () {
          Navigator.of(context).pop(newakun);
        });
        return new Center(
          child: CircularProgressIndicator(
            backgroundColor: Colors.grey,
          ),
        );
      }
    }
    return new SafeArea(
        child: new Scaffold(
            body:new WillPopScope(
              onWillPop: _onWillPop,
              child: new Scaffold(
                  key: _Scafoltkey,
                  drawer: drawerController(widget._akun, widget._cabang, "KeluargaDetail"),
                  appBar: new PreferredSize(
                    preferredSize: Size.fromHeight(200.0),
                    child: AppBarController(widget._cabang, _Scafoltkey, widget._akun, "KeluargaDetail"),
                  ),
                  body: new Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      new Container(
                        child: new Column(
                          children: <Widget>[
                            new Expanded(
                              child: new ListView(
                                scrollDirection: Axis.vertical,
                                physics: ScrollPhysics(),
                                children: <Widget>[
                                  _detailakun(widget._dt),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  )
              ),
            )
        )
    );
  }

  Widget _detailakun(akunklg){
    var dt = akunklg;
    String _gender ="";
    if(dt["jeniskelamin_klg"] == "M"){
      _gender = "Laki - Laki";
    }
    if(dt["jeniskelamin_klg"] == "F"){
      _gender = "Perempuan";
    }

    Widget _aktif;
    if(dt["enabled_klg"] == "1"){
      _aktif = cardDt("Status Akun", "Aktif",  FontAwesomeIcons.check, "enabled_klg");
    }else{
      _aktif = cardDt("Status Akun", "Tidak Aktif",  FontAwesomeIcons.ban, "enabled_klg");
    }

    Widget _hub;
    if(dt["hubungan_klg"] == "Anak"){
      _hub = cardDt("Hubungan", dt["hubungan_klg"]+" ["+dt["anak_ke"]+"]",  FontAwesomeIcons.circle, "hubungan_klg");
    }else{
      _hub = cardDt("Hubungan", dt["hubungan_klg"],  FontAwesomeIcons.circle, "hubungan_klg");
    }

    return new Container(
      padding: EdgeInsets.only(bottom: 30),
      child: Column(
        children: <Widget>[
          cardHeader("Data Keluarga", FontAwesomeIcons.cog),
          _hub,
          cardDt("Nama", dt["nama_klg"],  FontAwesomeIcons.tag, "nama_klg"),
          cardDt("Tgl Lahir", dt["tgllahir_klg"],  FontAwesomeIcons.calendarCheck, "tgllahir_klg"),
          cardDt("Jenis Kelamin", _gender,  FontAwesomeIcons.transgender, "jeniskelamin_klg"),
          cardDt("No. Tlp", dt["mobile_klg"],  FontAwesomeIcons.mobileAlt, "mobile_klg"),
          cardHeader("Nomor Rekam Medik Unit Bisnis", FontAwesomeIcons.cog),
          cardDt("Surya Husahda Denpasar", dt["nrmdps_klg"],  FontAwesomeIcons.idBadge, "nrmdps_klg"),
          cardDt("Surya Husahda Ubung", dt["nrmub_klg"],  FontAwesomeIcons.idBadge, "nrmub_klg"),
          cardDt("Surya Husahda Nusadua", dt["nrmnd_klg"],  FontAwesomeIcons.idBadge, "nrmnd_klg"),
          cardHeader("Data BPJS", FontAwesomeIcons.cog),
          cardDt("BPJS ID", dt["bpjsid_klg"],  FontAwesomeIcons.idBadge, "bpjsid_klg"),
          cardHeader("Status Akun", FontAwesomeIcons.cog),
          _aktif,
        ],
      ),
    );
  }

  Widget cardHeader(String title, IconData icon){
    return new Container(
      color: Colors.grey[100],
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 18, bottom: 18),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Icon(
                        icon,
                        size: 16,
                        color: Colors.grey[800],
                      ),
                      new Container(
                        padding: EdgeInsets.only(left: 10, top: 2),
                        child: new Text(
                          title,
                          style: TextStyle(color: Colors.grey[800], fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  color: Colors.grey[300],
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget cardDt(String title, String _text, IconData icon, String _column){
    return new InkWell(
      onTap: (){
        newakun = null;
        List<Map<String, dynamic>> params=[];
        if(_column=="hubungan_klg"){
          params.add({
            "_cabang": widget._cabang,
            "_nama": widget._dt["nama_klg"],
            "_title": title,
            "_value": widget._dt["hubungan_klg"],
            "_icon": icon,
            "_column": _column,
            "_page": "KeluargaDetail",
            "_anak_ke": widget._dt["anak_ke"],
            "_id": widget._dt["id_klg"],
            "_dt": widget._dt,
          });
        }else{
          params.add({
            "_cabang": widget._cabang,
            "_nama": widget._dt["nama_klg"],
            "_title": title,
            "_value": _text,
            "_icon": icon,
            "_column": _column,
            "_page": "KeluargaDetail",
            "_id": widget._dt["id_klg"],
            "_tgllahir_klg": widget._dt["tgllahir_klg"],
            "_dt": widget._dt,
          });
        }
        Navigator.pushReplacement(context, SlideLeftRoute(page: Fullpage(widgetpage: EditformKlg(params, widget._akun),pagename: "KeluargaDetail",))).then((val){
          if(val != null){
            setState(() {
              newakun = val[0];
              print("KeluargaDetail: "+newakun.toString());
            });
          }
        });
      },
      child: new Container(
          color: Colors.white,
          child: new Row(
            children: <Widget>[
              new Expanded(
                  child: new Column(
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.only(left: 24, right: 16, top: 14, bottom: 18),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Row(
                              children: <Widget>[
                                new Icon(
                                  icon,
                                  size: 16,
                                  color: Colors.grey[800],
                                ),
                                new Container(
                                  padding: EdgeInsets.only(left: 5, top: 3),
                                  child: new Text(
                                    title,
                                    style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                  ),
                                ),

                              ],
                            ),
                            new Container(width: 10),
                            new Expanded(
                                flex: 1,
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    new Text(
                                      _text,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      softWrap: false,
                                      style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                    ),
                                    new Container(width: 10,),
                                    new Icon(
                                      FontAwesomeIcons.chevronRight,
                                      size: 12,
                                      color: Colors.grey[400],
                                    ),
                                  ],
                                )
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        color: Colors.grey[300],
                        width: MediaQuery.of(context).size.width,
                        height: 1.0,
                      ),
                    ],
                  )
              ),
            ],
          )
      )
    );
  }
}