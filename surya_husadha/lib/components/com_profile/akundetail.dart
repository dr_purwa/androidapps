import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:surya_husadha/components/com_profile/editform.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/database/akun/akun_db.dart';
import 'package:surya_husadha/helper/dialogue/customDialogue.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/helper/transition/slide_route.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surya_husadha/pages/widgetpage.dart';
import 'dart:convert' as convert;
import 'package:image_picker/image_picker.dart';


class AkunDetail extends StatefulWidget {
  var _akun;
  String _cabang;
  Color _color1;
  Color _color2;
  AkunDetail(List params, akun){
    this._akun = akun;
    params.forEach((dt){
      this._cabang = dt["_cabang"];
      if(_cabang=="group"){
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
      }
      if(_cabang=="denpasar"){
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
      }
      if(_cabang=="ubung"){
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
      }
      if(_cabang=="nusadua"){
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
      }
      if(_cabang=="kmc"){
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
      }
    });
  }
  @override
  _AkunDetail createState() => new _AkunDetail();
}

class _AkunDetail extends State<AkunDetail>{

  final _Scafoltkey = GlobalKey<ScaffoldState>();
  var navigationResult;
  List newAkun;
  File imageFile;

  Future<bool> _onWillPop(){
    //Navigator.pop(context);
    List<Map<String, dynamic>> dtNext=[];
    dtNext.add({"_cabang": widget._cabang});
    RouteHelper(context, "AkunMenu", dtNext);
  }

  @override
  Widget build(BuildContext context){
    if(navigationResult != null){
      var db = new akunDB();
      db.getCount().then((i) async {
        if (i > 0) {
          newAkun = await db.getAllAkun();
          setState(() {
            widget._akun = newAkun;
            navigationResult = null;
          });
        }
      });
    }
    return new SafeArea(
        child: new Scaffold(
            body:new WillPopScope(
              onWillPop: _onWillPop,
              child: new Scaffold(
                  key: _Scafoltkey,
                  drawer: drawerController(widget._akun, widget._cabang, "AkunDetail"),
                  appBar: new PreferredSize(
                    preferredSize: Size.fromHeight(200.0),
                    child: AppBarController(widget._cabang, _Scafoltkey, widget._akun, "AkunDetail"),
                  ),
                  body: new Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      new Container(
                        child: new Column(
                          children: <Widget>[
                            new Expanded(
                              child: new ListView(
                                scrollDirection: Axis.vertical,
                                physics: ScrollPhysics(),
                                children: <Widget>[
                                  _detailakun(widget._akun),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  )
              ),
            )
        )
    );
  }

  Future<Null> _pickImage() async {
    imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (imageFile != null) {
      setState(() {
        _cropImage();
      });
    }
  }

  Future<Null> _cropImage() async {
    File croppedFile = await ImageCropper.cropImage(
      sourcePath: imageFile.path,
      aspectRatio: CropAspectRatio(ratioX: 1.0, ratioY: 1.0),
      cropStyle: CropStyle.circle,
      maxWidth: 250,
      maxHeight: 250,
    );
    if (croppedFile != null) {
      imageFile = croppedFile;
      setState(() {
        _saveImage();
      });
    }
  }

  bool sendingData;
  Future<Null> _saveImage() async {
    sendingData = true;
    showNotif(context, null, true);
  }

  Future<Null> _clearImage()async {
    imageFile = null;
    navigationResult = "_clearImage";
  }

  @override
  Future<String> postData() async {
    var jsonResponse;
    if(sendingData){
      var api = new Http_Controller();
      List<Map<String, dynamic>> dtNext=[];
      dtNext.add({
        "_id": widget._akun[0]["id"].toString(),
        "_value": base64Encode(imageFile.readAsBytesSync()).toString(),
      });
      var _data = await api.postResponse("https://suryahusadha.com/api/akun/updatepp", dtNext);
      jsonResponse = convert.jsonEncode(_data);
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => showNotif(context, jsonResponse.toString(), false),
      );
    }
    return jsonResponse.toString();
  }

  Widget showNotif(context, _jsonResponse, send) {
    if (send) {
      postData();
      sendingData = false;
      return new Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.grey,
        ),
      );
    } else {
      if(_jsonResponse != "" || _jsonResponse != null) {
        var decode = convert.jsonDecode(_jsonResponse);
        if(decode["statusCode"] == 200){
          var jsonResponse = convert.jsonDecode(decode["responseBody"]);
          String msg = jsonResponse["editakun"][0]["msg"];
          String response = jsonResponse["editakun"][0]["response"];
          var data = jsonResponse["editakun"][0]["data"];
          if (response == "1"){
            var db = new akunDB();
            //updateColumn(id, column, dt)
            db.updateColumn(widget._akun[0]["id"].toString(), "pp", data[0]["_pp"]);
            _clearImage().whenComplete((){
              setState(() {

              });
            });
            return Dialogue(context, msg, widget._color1, widget._color2, null, null);
          } else {
            return Dialogue(context, msg, Colors.red, Colors.red, null, null);
          }
        }else{
          return Dialogue(context,
              "Mohon maaf, terjadi kesalahan pengolahan data pada saat simpan data, silahkan ulangi beberapa saat lagi!",
              Colors.red, Colors.red, null, null);
        }
      } else {
        return Dialogue(context,
            "Mohon maaf, terjadi kesalahan koneksi pada saat simpan data, silahkan ulangi beberapa saat lagi!",
            Colors.red, Colors.red, null, null);
      }
    }
  }

  Widget _detailakun(akun){
    var dt = akun[0];
    Widget _pp;
    if(dt["pp"] != ""){
        String img = SERVER.domain + dt["pp"] +'?dummy=${new DateTime.now()}';
        _pp = new InkWell(
          onTap: (){
            _pickImage();
          },
          child: new Container(
            height: 150.0,
            width: 150.0,
            padding: EdgeInsets.all(2),
            decoration: new BoxDecoration(
                borderRadius:
                new BorderRadius.all(new Radius.circular(100.0)),
                color: Colors.lightBlue
            ),
            alignment: Alignment.center,
            child: CircleAvatar(
              backgroundImage:NetworkImage(img),
              minRadius: 90,
              maxRadius: 150,
            ),
          ),
        );
      } else {
      _pp = new InkWell(
          onTap: (){
            _pickImage();
          },
          child: new Container(
            height: 150.0,
            width: 150.0,
            padding: EdgeInsets.all(6.0),
            decoration: new BoxDecoration(
                borderRadius:
                new BorderRadius.all(new Radius.circular(100.0)),
                color: Colors.lightBlue
            ),
            alignment: Alignment.center,
            child: new Icon(
              FontAwesomeIcons.user,
              color: Colors.white,
              size: 80.0,
            ),
          )
        );
      }

    return new Container(
      padding: EdgeInsets.only(bottom: 30),
      child: Column(
        children: <Widget>[
          new Container(
              color: Colors.white,
              child: new Column(
                children: <Widget>[
                  new Container(
                    color: Colors.grey[300],
                    width: MediaQuery.of(context).size.width,
                    height: 1.0,
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 24)),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      _pp,
                    ],
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 10, top: 10)),
                  new Container(
                    child: Column(
                      children: <Widget>[
                        new Text(
                          "Hi, "+dt["name"],
                          style: TextStyle(fontSize: 18, color: Colors.grey[800]),
                        ),
                      ],
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 24)),
                  new Container(
                    color: Colors.grey[300],
                    width: MediaQuery.of(context).size.width,
                    height: 1.0,
                  ),
                ],
              )
          ),
          cardHeader("Data Umum", FontAwesomeIcons.cog),
          cardDt("Nama", dt["name"],  FontAwesomeIcons.userCircle, true, "name"),
          cardDt("TTL", dt["bodcity"]+", "+dt["bod"],  FontAwesomeIcons.calendarAlt, true, "bodcity"),
          cardDt("Jenis Kelamin", dt["gender"],  FontAwesomeIcons.transgender, true, "gender"),
          cardDt("Status", dt["marital"],  FontAwesomeIcons.circle, true, "marital"),
          cardDtAlamat("Alamat", dt["address"], FontAwesomeIcons.home, true, "address"),
          cardHeader("Data Lainnya", FontAwesomeIcons.cog),
          cardDt("Kewarganegaraan", dt["nasionality"],  FontAwesomeIcons.flag, true, "nasionality"),
        ],
      ),
    );
  }

  Widget cardHeader(String title, IconData icon){
    return new Container(
      color: Colors.grey[100],
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 18, bottom: 18),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Icon(
                        icon,
                        size: 16,
                        color: Colors.grey[800],
                      ),
                      new Container(width: 5,),
                      new Text(
                        title,
                        style: TextStyle(color: Colors.grey[800], fontSize: 14),
                      ),
                    ],
                  ),
                ),
                new Container(
                  color: Colors.grey[300],
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget cardDt(String title, String dt, IconData icon, bool edit, String _column){
    Widget _edit;
    if(edit){
      _edit = new Icon(
        FontAwesomeIcons.pen,
        size: 10,
        color: Colors.grey.withOpacity(0.6),
      );
    }else{
      _edit = new Icon(
        FontAwesomeIcons.ban,
        size: 10,
        color: Colors.red.withOpacity(0.4),
      );
    }

    String _text ="";
    if(_column == "gender"){
      if(dt == "M"){
        _text = "Laki - Laki";
      }
      if(dt == "F"){
        _text = "Perempuan";
      }
    }else{
      _text = dt;
    }

    return new InkWell(
      onTap: () async {
        if(_column != "" && edit){
          List<Map<String, dynamic>> params=[];
          params.add({
            "_cabang": widget._cabang,
            "_title": title,
            "_dt": dt,
            "_icon": icon,
            "_column": _column,
            "_page": "AkunDetail",
          });
          Navigator.pushReplacement(context, SlideLeftRoute(page: Fullpage(widgetpage: EditForm(params, widget._akun), pagename: "EditForm",))).then((val){
            navigationResult = val.toString();
          });
        }
      },
      child: new Container(
          color: Colors.white,
          child: new Row(
            children: <Widget>[
              new Expanded(
                  child: new Column(
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.only(left: 24, right: 24, top: 18, bottom: 18),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Row(
                              children: <Widget>[
                                new Icon(
                                  icon,
                                  size: 16,
                                  color: Colors.grey[800],
                                ),
                                new Container(width: 5,),
                                new Text(
                                  title,
                                  style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                ),
                              ],
                            ),
                            new Container(width: 10),
                            new Expanded(
                                flex: 1,
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    new Text(
                                      _text,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      softWrap: false,
                                      style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                    ),
                                    new Container(width: 10,),
                                    _edit
                                  ],
                                )
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        color: Colors.grey[300],
                        width: MediaQuery.of(context).size.width,
                        height: 1.0,
                      ),
                    ],
                  )
              ),
            ],
          )
      ),
    );
  }

  Widget cardDtAlamat(String title, String dt, IconData icon, bool edit, String _column){
    Widget _edit;
    if(edit){
      _edit = new Icon(
        FontAwesomeIcons.pen,
        size: 10,
        color: Colors.grey.withOpacity(0.6),
      );
    }else{
      _edit = new Icon(
        FontAwesomeIcons.ban,
        size: 10,
        color: Colors.red.withOpacity(0.4),
      );
    }
    return new InkWell(
      onTap: () async {
        if(_column != "" && edit){
          List<Map<String, dynamic>> params=[];
          params.add({
            "_cabang": widget._cabang,
            "_title": title,
            "_dt": dt,
            "_icon": icon,
            "_column": _column,
            "_page": "AkunDetail",
          });
          Navigator.pushReplacement(context, SlideLeftRoute(page: Fullpage(widgetpage: EditForm(params, widget._akun), pagename: "EditForm",))).then((val){
            navigationResult = val.toString();
          });
        }
      },
      child: Container(
          color: Colors. white,
          child: new Row(
            children: <Widget>[
              new Expanded(
                  child: new Column(
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 16),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Row(
                                  children: <Widget>[
                                    new Icon(
                                      icon,
                                      size: 16,
                                      color: Colors.grey[800],
                                    ),
                                    new Container(width: 5,),
                                    new Text(
                                      title,
                                      style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                    ),
                                  ],
                                ),
                                new Expanded(
                                    flex: 1,
                                    child: Row(
                                      mainAxisSize: MainAxisSize.max,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        new Container(width: 10,),
                                        _edit
                                      ],
                                    )
                                ),
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 16),
                            ),
                            new Column(
                              children: <Widget>[
                                new Text(
                                  dt,
                                  style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        color: Colors.grey[300],
                        width: MediaQuery.of(context).size.width,
                        height: 1.0,
                      ),
                    ],
                  )
              )
            ],
          )
      ),
    );
  }
}