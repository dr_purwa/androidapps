import 'dart:async';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:surya_husadha/components/com_profile/tambahakunkeluarga_nrm.dart';
import 'package:surya_husadha/components/com_reservasi/reservasi_models.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/helper/transition/slide_route.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:masked_text_input_formatter/masked_text_input_formatter.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:surya_husadha/pages/widgetpage.dart';

class TambahKlg extends StatefulWidget {

  var _akun;
  String _cabang;
  String _page;
  Color _color1;
  Color _color2;
  Color _textColor = Colors.white;
  Color _callendarColor;

  TambahKlg(List params, akun){
    this._akun = akun;
    params.forEach((dt){
      this._cabang = dt["_cabang"];
      this._page = dt["_page"];
      if(_cabang=="group"){
        this._textColor = Colors.grey[600];
        this._callendarColor = WarnaCabang.shh;
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
      }
      if(_cabang=="denpasar"){
        this._callendarColor = WarnaCabang.shh;
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
      }
      if(_cabang=="ubung"){
        this._callendarColor = WarnaCabang.ubung;
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
      }
      if(_cabang=="nusadua"){
        this._callendarColor = WarnaCabang.nusadua;
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
      }
      if(_cabang=="kmc"){
        this._callendarColor = WarnaCabang.kmc;
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
      }
    });
  }

  @override
  _TambahKlg createState() => new _TambahKlg();
}

class Hubungan{
  const Hubungan(this.index, this.title);
  final String index;
  final String title;
}

class _TambahKlg extends State<TambahKlg> with SingleTickerProviderStateMixin{
  bool _close = false;
  Color _fillColor = Colors.white.withOpacity(0.6);
  ScrollController listScrollController = ScrollController();
  final _ScafoltkeyFrm= GlobalKey<ScaffoldState>();
  bool selectYears = false;
  //Hubungan
  bool _hubNull = false;
  Hubungan selectedhub;
  String _shortindex;
  List<Hubungan> hub = <Hubungan>[
    const Hubungan("0", "Pilih Hubungan Keluarga"),
    const Hubungan("1", "Kakek"),
    const Hubungan("2", "Nenek"),
    const Hubungan("3", "Bapak"),
    const Hubungan("4", "Ibu"),
    const Hubungan("5", "Suami"),
    const Hubungan("6", "Istri"),
    const Hubungan("7", "Kakak"),
    const Hubungan("8", "Adik"),
    const Hubungan("9", "Anak"),
  ];
  //_anakke
  bool _anakkeVisibility = false;
  bool _anakkeNull = false;
  FocusNode _anakke = new FocusNode();
  TextEditingController _anakkeController = new TextEditingController();
  //_nama
  bool _namaNull = false;
  FocusNode _nama = new FocusNode();
  TextEditingController _namaController = new TextEditingController();
  //_tgllahir
  bool _tgllahirNull = false;
  FocusNode _tgllahir = new FocusNode();
  TextEditingController _tgllahirController = new TextEditingController();
  //gender
  bool _genderNull = false;
  Gender selectedGender;
  List<Gender> gender = <Gender>[
    const Gender("", "Pilih Jenis Kelamin", FontAwesomeIcons.ban),
    const Gender("M", "Laki - Laki", FontAwesomeIcons.male),
    const Gender("F", "Perempuan", FontAwesomeIcons.female)
  ];
  //_mobile
  bool _mobileNull = false;
  FocusNode _mobile = new FocusNode();
  TextEditingController _mobileController = new TextEditingController();
  //_aktifkanakun
  bool _aktifkanakun = false;
  //akunberobat
  String _rmshhController;
  String _rmubController;
  String _rmndController;
  String _bpjsidController;

  @override
  void initState(){
    selectedGender=gender[0];
    selectedhub=hub[0];
    _shortindex=selectedhub.index;
    super.initState();
  }

  Future<bool> _onWillPop(){
    //Navigator.pop(context);
    List<Map<String, dynamic>> dtNext=[];
    if(widget._page == "LoyaltiPage"){
      dtNext.add({
        "_cabang": widget._cabang,
        "_page": "kidscard",
      });
      RouteHelper(context, widget._page, dtNext);
    }else{
      dtNext.add({
        "_cabang": widget._cabang
      });
      RouteHelper(context, widget._page, dtNext);
    }

  }

  @override
  Widget build(BuildContext context) {
    if(_close){
      new Timer(const Duration(milliseconds: 500), () {
        List<Map<String, dynamic>> paramsClose=[];
        paramsClose.add({
          "_close": "1",
        });
        print("params2:"+paramsClose.toString());
        Navigator.of(context).pop(paramsClose);
      });
      return new Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.grey,
        ),
      );
    }
    return new SafeArea(
        child: new Scaffold(
            key: _ScafoltkeyFrm,
            drawer: drawerController(widget._akun, widget._cabang, "TambahKlg"),
            appBar: new PreferredSize(
              preferredSize: Size.fromHeight(200.0),
              child: AppBarController(widget._cabang, _ScafoltkeyFrm,widget._akun, "TambahKlg"),
            ),
            body: new WillPopScope(
                onWillPop: _onWillPop,
                child: new Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    new Container(
                      child: new Column(
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              _header(),
                            ],
                          ),
                          Expanded(
                            child: ListView(
                              controller: listScrollController,
                              scrollDirection: Axis.vertical,
                              children: <Widget>[
                                _buildForm(context),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
            )
        )
    );
  }

  Widget _header(){
    return new Container(
      color: Colors.grey[100],
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 18, bottom: 18),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Icon(
                        FontAwesomeIcons.infoCircle,
                        size: 18,
                        color: Colors.grey[800],
                      ),
                      new Container(
                        padding: EdgeInsets.only(left: 10, top: 3),
                        child: new Text(
                          "Form Data Keluarga",
                          style: TextStyle(
                              color: Colors.grey[800],
                              fontSize: 18,
                              fontWeight: FontWeight.w700
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  color: Colors.grey[300],
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildForm(BuildContext context) {
    return new Container(
      padding: EdgeInsets.only(left:24, right:24),
      child: new Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Container(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                  margin: EdgeInsets.only(top: 30),
                  child: new DropdownButtonFormField(
                    value: selectedhub,
                    onChanged: (Hubungan newval){
                      setState(() {
                        selectedhub = newval;
                        _shortindex=selectedhub.index;
                        if(newval.title == "Anak"){
                          _anakkeVisibility = true;
                        }else{
                          _anakkeVisibility = false;
                          _anakkeController.text = null;
                        }
                      });
                    },
                    items: hub.map((Hubungan hub){
                      return new DropdownMenuItem<Hubungan>(
                          value: hub,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              new Container(
                                padding: EdgeInsets.only(top:3),
                                child:new Text(
                                  hub.title,
                                  style: new TextStyle(color: Colors.black, fontSize: 14),
                                ),
                              ),
                            ],
                          )
                      );
                    }).toList(),
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: _fillColor,
                      labelStyle: TextStyle(
                        color: Colors.grey[800],
                      ),
                      labelText: 'Hubungan Keluarga',
                      errorText: _hubNull ? "" : null,
                      prefixIcon: Icon(
                        FontAwesomeIcons.users,
                        color: Colors.grey[800],
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                        borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                      ),
                      enabledBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                        borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        borderSide: BorderSide(
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ),
                ),
                new Visibility(
                  visible: _anakkeVisibility,
                  child: new Container(
                      padding: EdgeInsets.only(top: 20),
                      child: TextField(
                        focusNode: _anakke,
                        controller: _anakkeController,
                        textInputAction: TextInputAction.next,
                        onEditingComplete: () => {
                          _next(_nama, "next"),
                          showPickerDate(context)
                        },
                        cursorColor: Colors.grey[800],
                        style: TextStyle(color: Colors.grey[800], fontSize: 14),
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: _fillColor,
                          labelStyle: TextStyle(
                            color: Colors.grey[800],
                          ),
                          errorText: _anakkeNull ? "" : null,
                          labelText: 'Anak Ke ?',
                          prefixIcon: Icon(
                            FontAwesomeIcons.child,
                            color: Colors.grey[800],
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                            borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                          ),
                          enabledBorder: const OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                            borderSide: BorderSide(
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      )
                  ),
                ),
                new Container(
                  margin: EdgeInsets.only(top: 30, bottom: 30),
                  height: 1,
                  color: Colors.grey[300],
                ),
                new Container(
                    child: TextField(
                      focusNode: _nama,
                      controller: _namaController,
                      textInputAction: TextInputAction.next,
                      onEditingComplete: () => {
                        _next(_tgllahir, "next"),
                        showPickerDate(context)
                      },
                      cursorColor: Colors.grey[800],
                      style: TextStyle(color: Colors.grey[800], fontSize: 14),
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: _fillColor,
                        labelStyle: TextStyle(
                          color: Colors.grey[800],
                        ),
                        errorText: _namaNull ? "" : null,
                        labelText: 'Nama Lengkap',
                        prefixIcon: Icon(
                          FontAwesomeIcons.tag,
                          color: Colors.grey[800],
                        ),
                        focusedBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        enabledBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                          borderSide: BorderSide(
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    )
                ),
                new Container(
                    margin: EdgeInsets.only(top:20),
                    child: TextField(
                      onTap: (){
                        showPickerDate(context);
                        FocusScope.of(context).requestFocus(new FocusNode());
                      },
                      inputFormatters: [
                        MaskedTextInputFormatter(
                          mask: 'xx-xx-xxxx',
                          separator: '-',
                        ),
                      ],
                      focusNode: _tgllahir,
                      controller: _tgllahirController,
                      textInputAction: TextInputAction.next,
                      onEditingComplete: () => {
                          _next(_tgllahir, "next")
                      },
                      cursorColor: Colors.grey[800],
                      style: TextStyle(color: Colors.grey[800], fontSize: 14),
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: _fillColor,
                        labelStyle: TextStyle(
                          color: Colors.grey[800],
                        ),
                        labelText: 'Tgl Lahir (dd-mm-YYYY)',
                        errorText: _tgllahirNull ? "" : null,
                        prefixIcon: Icon(
                          FontAwesomeIcons.calendarCheck,
                          color: Colors.grey[800],
                        ),
                        focusedBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        enabledBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                          borderSide: BorderSide(
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    )
                ),
                new Container(
                  margin: EdgeInsets.only(top:20),
                  child: new DropdownButtonFormField(
                    value: selectedGender,
                    onChanged: (Gender newValue) {
                      setState(() {
                        if(newValue.val == ""){
                          selectedGender = gender[0];
                        }
                        if(newValue.val == "M"){
                          selectedGender = gender[1];
                        }
                        if(newValue.val == "F"){
                          selectedGender = gender[2];
                        }
                      });
                    },
                    items: gender.map((Gender gender){
                      return new DropdownMenuItem<Gender>(
                          value: gender,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              new Container(
                                padding: EdgeInsets.only(top:3),
                                child:new Text(
                                  gender.title,
                                  style: new TextStyle(color: Colors.black, fontSize: 14),
                                ),
                              ),
                              new Container(width: 5,),
                              new Icon(
                                gender.icon,
                                size: 16,
                              ),
                            ],
                          )
                      );
                    }).toList(),
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: _fillColor,
                      labelStyle: TextStyle(
                        color: Colors.grey[800],
                      ),
                      labelText: 'Jenis Kelamin',
                      errorText: _genderNull ? "" : null,
                      prefixIcon: Icon(
                        FontAwesomeIcons.transgender,
                        color: Colors.grey[800],
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                        borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                      ),
                      enabledBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                        borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        borderSide: BorderSide(
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ),
                ),
                new Container(
                    margin: EdgeInsets.only(top:20),
                    child: TextField(
                      focusNode: _mobile,
                      controller: _mobileController,
                      textInputAction: TextInputAction.done,
                      cursorColor: Colors.grey[800],
                      style: TextStyle(color: Colors.grey[800], fontSize: 14),
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: _fillColor,
                        labelStyle: TextStyle(
                          color: Colors.grey[800],
                        ),
                        labelText: 'Mobile Phone',
                        errorText: _mobileNull ? "" : null,
                        prefixIcon: Icon(
                          FontAwesomeIcons.mobileAlt,
                          color: Colors.grey[800],
                        ),
                        focusedBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        enabledBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                          borderSide: BorderSide(
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    )
                ),
                new Container(
                  margin: EdgeInsets.only(top:20),
                  child: CheckboxListTile (
                    value: _aktifkanakun,
                    onChanged: (val){
                      setState(() {
                        if(val){
                          _aktifkanakun = true;
                        }else{
                          _aktifkanakun = false;
                        }
                      });
                    },
                    title: Text("Aktifkan Akun"),
                    activeColor: Colors.grey[600],
                  ),
                ),

                new Container(
                  margin: EdgeInsets.only(bottom:30, top: 60),
                  child: Column(
                      children: <Widget>[
                        new Align(
                          alignment: Alignment.bottomCenter,
                          child: RawMaterialButton(
                            onPressed: (){
                              bool next = true;
                              setState(() {
                                _hubNull = false;
                                if(selectedhub == hub[0]){
                                  _hubNull = true;
                                  next = false;
                                }
                                if(selectedhub.title == "Anak"){
                                  _anakkeNull = false;
                                  if(_anakkeController.text.isEmpty){
                                    _anakkeNull = true;
                                    next = false;
                                  }
                                }
                                _namaNull = false;
                                if(_namaController.text.isEmpty){
                                  _namaNull = true;
                                  next = false;
                                }
                                _tgllahirNull = false;
                                if(_tgllahirController.text.isEmpty){
                                  _tgllahirNull = true;
                                  next = false;
                                }
                                _genderNull = false;
                                if(selectedGender == gender[0] ){
                                  _genderNull = true;
                                  next = false;
                                }
                              });
                              if(next){
                                List<Map<String, dynamic>> params=[];
                                params.add({
                                  "_cabang": widget._cabang,
                                  "_hubungan_klg": selectedhub.title,
                                  "_anak_ke": _anakkeController.text,
                                  "_nama_klg": _namaController.text,
                                  "_tgllahir_klg": _tgllahirController.text,
                                  "_jeniskelamin_klg": selectedGender.val,
                                  "_mobile_klg": _mobileController.text,
                                  "_nrmdps_klg": _rmshhController,
                                  "_nrmub_klg": _rmubController,
                                  "_nrmnd_klg": _rmndController,
                                  "_bpjsid_klg": _bpjsidController,
                                  "_enabled_klg": _aktifkanakun.toString(),
                                  "_shortindex": _shortindex,
                                  "_page": "AkunBerobatKeluarga",
                                });
                                Navigator.pushReplacement(context, SlideLeftRoute(page: Fullpage(widgetpage: TambahKlgNrm(params, widget._akun,), pagename: "Tambah Keluarga",))).then((val){
                                  setState(() {
                                    if(val[0]["_close"].toString() == "1"){
                                      _close = true;
                                    }else{
                                      _rmshhController=val[0]["_nrmdps_klg"].toString();
                                      _rmubController=val[0]["_nrmub_klg"].toString();
                                      _rmndController=val[0]["_nrmnd_klg"].toString();
                                      _bpjsidController=val[0]["_bpjsid_klg"].toString();
                                    }
                                  });
                                });
                              }
                            },
                            elevation: 0,
                            padding: EdgeInsets.all(16),
                            animationDuration: Duration(milliseconds: 500),
                            fillColor: widget._color1,
                            splashColor: widget._color2,
                            highlightColor: widget._color2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                side: BorderSide(color: Colors.grey[300])
                            ),
                            constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Expanded(
                                  flex: 1,
                                  child: new Row(
                                    mainAxisSize: MainAxisSize.max,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new Column(children: <Widget>[
                                        Text(
                                          "Berikutnya",
                                          style: TextStyle(color: widget._textColor, fontSize: 18),
                                        ),
                                      ],),
                                    ],
                                  ),
                                ),
                                Icon(
                                  FontAwesomeIcons.arrowRight,
                                  color: widget._textColor,
                                  size: 16,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ]
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  showPickerDate(BuildContext context) {
    final f = new DateFormat('dd-MM-yyyy');
    DateTime _Selected;
    if(_tgllahirController.text != ""){
      int d = int.parse(_tgllahirController.text.split("-")[0]);
      int m = int.parse(_tgllahirController.text.split("-")[1]);
      int y = int.parse(_tgllahirController.text.split("-")[2]);
      _Selected = DateTime(y, m ,d);
    }else{
      _Selected = DateTime.now();
    }
    int _yNow = int.parse(DateFormat.y().format(_Selected));
    DateTime _max = DateTime(_yNow-100);

    Picker(
      hideHeader: true,
      changeToFirst: false,
      adapter: DateTimePickerAdapter(
        customColumnType: [0, 1, 2],
        value: _Selected,
        maxValue: DateTime.now(),
        minValue: _max,
      ),
      title: new Container(
        child: Column(children: <Widget>[
          Text("Pilih Tgl. Lahir:", style: TextStyle(color: widget._callendarColor),),
        ],),
      ),
      selectedTextStyle: TextStyle(color: widget._callendarColor),
      confirm: new InkWell(
        onTap: ()=>{
          Navigator.of(context).pop(),
          widget,_tgllahirController.text = f.format(_Selected).toString(),
        },
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(children: <Widget>[
            new Icon(
              FontAwesomeIcons.check,
              color: widget._callendarColor,
              size: 12,
            ),
            Container(width: 5,),
            Text("Pilih", style: TextStyle(color: widget._callendarColor),)
          ],),
        ),
      ),
      cancel: new InkWell(
        onTap: ()=>{
          Navigator.of(context).pop()
        },
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(children: <Widget>[
            new Icon(
              FontAwesomeIcons.times,
              color: widget._callendarColor,
              size: 12,
            ),
            Container(width: 2,),
            Text("Tutup", style: TextStyle(color: widget._callendarColor),)
          ],),
        ),
      ),
      onSelect: (Picker picker, int index, List<int> selecteds) {
        this.setState(() {
          int d = _Selected.day;
          int m = _Selected.month;
          int y = _Selected.year;
          if(index == 2){
            d = (picker.adapter as DateTimePickerAdapter).value.day;
            _Selected = DateTime(y, m, d);
          }
          if(index == 1){
            m = (picker.adapter as DateTimePickerAdapter).value.month;
            _Selected = DateTime(y, m, d);
          }
          if(index == 0){
            y = (picker.adapter as DateTimePickerAdapter).value.year;
            _Selected = DateTime(y, m, d);
          }

        });
      },
    ).showDialog(context);

  }

  _next(FocusNode reqfocus, String s){
    FocusScope.of(context).requestFocus(reqfocus);
    if(s == "last"){
      listScrollController.jumpTo(listScrollController.position.maxScrollExtent);
    }
    if(s == "done"){
      listScrollController.jumpTo(listScrollController.position.minScrollExtent);
      FocusScope.of(context).requestFocus(new FocusNode());
    }
  }
}