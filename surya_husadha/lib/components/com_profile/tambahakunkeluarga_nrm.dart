import 'dart:async';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/dialogue/customDialogue.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:masked_text_input_formatter/masked_text_input_formatter.dart';
import 'dart:convert' as convert;

class TambahKlgNrm extends StatefulWidget {

  var _akun;
  String _cabang;
  Color _color1;
  Color _color2;
  Color _textColor = Colors.white;
  String _userid;
  String _hubungan_klg;
  String _anak_ke;
  String _nama_klg;
  String _tgllahir_klg;
  String _jeniskelamin_klg;
  String _mobile_klg;
  String _nrmdps_klg;
  String _nrmub_klg;
  String _nrmnd_klg;
  String _bpjsid_klg;
  String _enabled_klg;
  String _shortindex;

  TambahKlgNrm(List params, akun){
    this._akun = akun;
    this._userid = akun[0]["id"].toString();
    print(_userid);
    params.forEach((dt){
      this._cabang = dt["_cabang"];
      this._hubungan_klg = dt["_hubungan_klg"];
      this._anak_ke = dt["_anak_ke"];
      this._nama_klg = dt["_nama_klg"];
      this._tgllahir_klg = dt["_tgllahir_klg"];
      this._jeniskelamin_klg = dt["_jeniskelamin_klg"];
      this._mobile_klg = dt["_mobile_klg"];
      this._nrmdps_klg = dt["_nrmdps_klg"];
      this._nrmub_klg = dt["_nrmub_klg"];
      this._nrmnd_klg = dt["_nrmnd_klg"];
      this._bpjsid_klg = dt["_bpjsid_klg"];
      this._enabled_klg = dt["_enabled_klg"];
      this._shortindex = dt["_shortindex"];
      if(_cabang=="group"){
        this._textColor = Colors.grey[600];
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
      }
      if(_cabang=="denpasar"){
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
      }
      if(_cabang=="ubung"){
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
      }
      if(_cabang=="nusadua"){
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
      }
      if(_cabang=="kmc"){
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
      }
    });
  }

  @override
  _TambahKlgNrm createState() => new _TambahKlgNrm();
}

class Hubungan{
  const Hubungan(this.title);
  final String title;
}

class _TambahKlgNrm extends State<TambahKlgNrm> with SingleTickerProviderStateMixin{

  var callback;
  ScrollController listScrollController = ScrollController();
  Color _fillColor = Colors.white.withOpacity(0.6);
  final _ScafoltkeyFrm= GlobalKey<ScaffoldState>();

  //_rmshh
  FocusNode _rmshh = new FocusNode();
  TextEditingController _rmshhController = new TextEditingController();
  //_rmub
  FocusNode _rmub = new FocusNode();
  TextEditingController _rmubController = new TextEditingController();
  //_rmnd
  FocusNode _rmnd = new FocusNode();
  TextEditingController _rmndController = new TextEditingController();
  //_bpjsid
  FocusNode _bpjsid = new FocusNode();
  TextEditingController _bpjsidController = new TextEditingController();

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> params=[];
    params.add({
      "_status": null,
      "_nrmdps_klg": _rmshhController.text,
      "_nrmub_klg": _rmubController.text,
      "_nrmnd_klg": _rmndController.text,
      "_bpjsid_klg": _bpjsidController.text,
    });
    Navigator.of(context).pop(params);
  }

  @override
  void initState() {
    _rmshhController.text = widget._nrmdps_klg;
    _rmubController.text = widget._nrmub_klg;
    _rmndController.text = widget._nrmnd_klg;
    _bpjsidController.text = widget._bpjsid_klg;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if(callback != null){
      new Timer(const Duration(milliseconds: 500), () {
        print("callback: "+callback.toString());
        List<Map<String, dynamic>> params=[];
        params.add({
          "_close": "1",
        });
        Navigator.of(context).pop(params);
      });
      return new Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.grey,
        ),
      );
    }else{
      return new SafeArea(
          child: new Scaffold(
              key: _ScafoltkeyFrm,
              drawer: drawerController(widget._akun, widget._cabang, "TambahKlgNrm"),
              appBar: new PreferredSize(
                preferredSize: Size.fromHeight(200.0),
                child: AppBarController(widget._cabang, _ScafoltkeyFrm,widget._akun, "TambahKlgNrm"),
              ),
              body: new WillPopScope(
                  onWillPop: _onWillPop,
                  child: new Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      new Container(
                        child: new Column(
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                _header(),
                              ],
                            ),
                            Expanded(
                              child: ListView(
                                controller: listScrollController,
                                scrollDirection: Axis.vertical,
                                children: <Widget>[
                                  _buildForm(context),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
              )
          )
      );
    }
  }

  Widget _header(){
    return new Container(
      color: Colors.grey[100],
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 14, bottom: 18),
                  child: new Column(
                    children: <Widget>[
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          new Expanded(
                            child: new Container(
                                padding: EdgeInsets.only(left: 10, top: 3),
                                child: new Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Row(
                                      children: <Widget>[
                                        new Icon(
                                          FontAwesomeIcons.infoCircle,
                                          size: 18,
                                          color: Colors.grey[800],
                                        ),
                                        new Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: <Widget>[
                                              new Container(
                                                padding: EdgeInsets.only(top: 4, left: 10),
                                                child: new Text(
                                                  "Data Akun Berobat",
                                                  overflow: TextOverflow.ellipsis,
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                      color: Colors.grey[800],
                                                      fontSize: 18,
                                                      fontWeight: FontWeight.w700
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                    new Padding(
                                      padding: EdgeInsets.only(top: 5),
                                    ),
                                    new Text(
                                      "Untuk mempermudah pemilihan akun untuk reservasi online, silahkan lengkapi akun berobat dibawah ini!",
                                      style: TextStyle(fontSize: 14),
                                    ),
                                  ],
                                )
                            ),
                          )
                        ],
                      ),
                    ],
                  )
                ),
                new Container(
                  color: Colors.grey[300],
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildForm(BuildContext context) {
    return new Container(
      padding: EdgeInsets.only(left:24, right:24),
      child: new Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Container(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                  margin: EdgeInsets.only(top: 30),
                  child: new Row(
                    children: <Widget>[
                      new Icon(
                        FontAwesomeIcons.userCircle,
                        color: Colors.grey[800],
                        size: 18,
                      ),
                      new Expanded(
                        child: new Container(
                          padding: EdgeInsets.only(left: 10, top: 3),
                          child: new Text(
                            widget._nama_klg,
                            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
                          ),
                        ),
                      )
                    ],
                  )
                ),
                new Container(
                  margin: EdgeInsets.only(top: 20, bottom: 0),
                  height: 1,
                  color: Colors.grey[300],
                ),
                new Container(
                  padding: EdgeInsets.only(top: 30, bottom: 20),
                  child: new Row(
                    children: <Widget>[
                      new Icon(
                        FontAwesomeIcons.mapMarkerAlt,
                        size: 14,
                      ),
                      new Container(
                        padding: EdgeInsets.only(left: 10, top: 4),
                        child: new Text(
                          "Akun Berobat Surya Husadha Hospital",
                          overflow: TextOverflow.ellipsis,
                          softWrap: true,
                          maxLines: 1,
                        ),
                      )
                    ],
                  )
                ),
                new Container(
                    child: TextField(
                      inputFormatters: [
                        MaskedTextInputFormatter(
                          mask: 'xx.xx.xx',
                          separator: '.',
                        ),
                      ],
                      focusNode: _rmshh,
                      controller: _rmshhController,
                      textInputAction: TextInputAction.next,
                      onEditingComplete: () => {
                        _next(_rmub, "next"),
                      },
                      cursorColor: Colors.grey[800],
                      style: TextStyle(color: Colors.grey[800], fontSize: 14),
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: _fillColor,
                        labelStyle: TextStyle(
                          color: Colors.grey[800],
                        ),
                        labelText: 'No. Rekam Medis (xx.xx.xx)',
                        prefixIcon: Icon(
                          FontAwesomeIcons.idBadge,
                          color: Colors.grey[800],
                        ),
                        focusedBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        enabledBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                          borderSide: BorderSide(color: Colors.grey),
                        ),
                      ),
                    )
                ),
                new Container(
                    padding: EdgeInsets.only(top: 20, bottom: 20),
                    child: new Row(
                      children: <Widget>[
                        new Icon(
                          FontAwesomeIcons.mapMarkerAlt,
                          size: 14,
                        ),
                        new Container(
                          padding: EdgeInsets.only(left: 10, top: 4),
                          child: new Text(
                            "Akun Berobat Surya Husadha Ubung",
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            maxLines: 1,
                          ),
                        )
                      ],
                    )
                ),
                new Container(
                    child: TextField(
                      inputFormatters: [
                        MaskedTextInputFormatter(
                          mask: 'xx.xx.xx',
                          separator: '.',
                        ),
                      ],
                      focusNode: _rmub,
                      controller: _rmubController,
                      textInputAction: TextInputAction.next,
                      onEditingComplete: () => {
                        _next(_rmnd, "next"),
                      },
                      cursorColor: Colors.grey[800],
                      style: TextStyle(color: Colors.grey[800], fontSize: 14),
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: _fillColor,
                        labelStyle: TextStyle(
                          color: Colors.grey[800],
                        ),
                        labelText: 'No. Rekam Medis (xx.xx.xx)',
                        prefixIcon: Icon(
                          FontAwesomeIcons.idBadge,
                          color: Colors.grey[800],
                        ),
                        focusedBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        enabledBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                          borderSide: BorderSide(color: Colors.grey),
                        ),
                      ),
                    )
                ),
                new Container(
                    padding: EdgeInsets.only(top: 20, bottom: 20),
                    child: new Row(
                      children: <Widget>[
                        new Icon(
                          FontAwesomeIcons.mapMarkerAlt,
                          size: 14,
                        ),
                        new Container(
                          padding: EdgeInsets.only(left: 10, top: 4),
                          child: new Text(
                            "Akun Berobat Surya Husadha Nusadua",
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            maxLines: 1,
                          ),
                        )
                      ],
                    )
                ),
                new Container(
                    child: TextField(
                      inputFormatters: [
                        MaskedTextInputFormatter(
                          mask: 'xx.xx.xx',
                          separator: '.',
                        ),
                      ],
                      focusNode: _rmnd,
                      controller: _rmndController,
                      textInputAction: TextInputAction.next,
                      onEditingComplete: () => {
                        _next(_bpjsid, "next"),
                      },
                      cursorColor: Colors.grey[800],
                      style: TextStyle(color: Colors.grey[800], fontSize: 14),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: _fillColor,
                        labelStyle: TextStyle(
                          color: Colors.grey[800],
                        ),
                        labelText: 'No. Rekam Medis (xx.xx.xx)',
                        prefixIcon: Icon(
                          FontAwesomeIcons.idBadge,
                          color: Colors.grey[800],
                        ),
                        focusedBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        enabledBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                          borderSide: BorderSide(color: Colors.grey),
                        ),
                      ),
                    )
                ),
                new Container(
                  margin: EdgeInsets.only(top: 40, bottom: 30),
                  height: 1,
                  color: Colors.grey[300],
                ),
                new Container(
                    padding: EdgeInsets.only(top: 0, bottom: 20),
                    child: new Row(
                      children: <Widget>[
                        new Icon(
                          FontAwesomeIcons.idCard,
                          size: 14,
                        ),
                        new Container(
                          padding: EdgeInsets.only(left: 10, top: 4),
                          child: new Text(
                            "Nomor ID BPJS",
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            maxLines: 1,
                          ),
                        )
                      ],
                    )
                ),
                new Container(
                    child: TextField(
                      focusNode: _bpjsid,
                      controller: _bpjsidController,
                      textInputAction: TextInputAction.done,
                      cursorColor: Colors.grey[800],
                      style: TextStyle(color: Colors.grey[800], fontSize: 14),
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: _fillColor,
                        labelStyle: TextStyle(
                          color: Colors.grey[800],
                        ),
                        labelText: 'No. Kartu BPJS',
                        prefixIcon: Icon(
                          FontAwesomeIcons.idBadge,
                          color: Colors.grey[800],
                        ),
                        focusedBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        enabledBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                          borderSide: BorderSide(color: Colors.grey),
                        ),
                      ),
                    )
                ),
              ],
            ),
          ),
          new Container(
            margin: EdgeInsets.only(top:60),
            child: Column(
                children: <Widget>[
                  new Align(
                    alignment: Alignment.bottomCenter,
                    child: RawMaterialButton(
                      onPressed: (){
                        callback = null;
                        List<Map<String, dynamic>> params=[];
                        params.add({
                          "mbr_id": widget._userid,
                          "_hubungan_klg": widget._hubungan_klg,
                          "_anak_ke": widget._anak_ke,
                          "_nama_klg": widget._nama_klg,
                          "_tgllahir_klg": widget._tgllahir_klg,
                          "_jeniskelamin_klg": widget._jeniskelamin_klg,
                          "_mobile_klg": widget._mobile_klg,
                          "_nrmdps_klg": _rmshhController.text,
                          "_nrmub_klg": _rmubController.text,
                          "_nrmnd_klg": _rmndController.text,
                          "_bpjsid_klg": _bpjsidController.text,
                          "_enabled_klg": widget._enabled_klg,
                          "_shortindex": widget._shortindex,
                        });
                        showDialog(
                          barrierDismissible: false,
                          context: context,
                          builder: (BuildContext context) => showNotif(context, null, params),
                        );
                      },
                      elevation: 0,
                      padding: EdgeInsets.all(16),
                      animationDuration: Duration(milliseconds: 500),
                      fillColor: widget._color1,
                      splashColor: widget._color2,
                      highlightColor: widget._color2,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                          side: BorderSide(color: Colors.grey[200])
                      ),
                      constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Expanded(
                            flex: 1,
                            child: new Row(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Column(children: <Widget>[
                                  Text(
                                    "Simpan",
                                    style: TextStyle(color: widget._textColor, fontSize: 18),
                                  ),
                                ],),
                              ],
                            ),
                          ),
                          Icon(
                            FontAwesomeIcons.arrowRight,
                            color: widget._textColor,
                            size: 16,
                          ),
                        ],
                      ),
                    ),
                  ),
                ]
            ),
          ),
          new Padding(padding: EdgeInsets.only(bottom:40)),
        ],
      ),
    );
  }

  _next(FocusNode reqfocus, String s){
    FocusScope.of(context).requestFocus(reqfocus);
    if(s == "last"){
      listScrollController.jumpTo(listScrollController.position.maxScrollExtent);
    }
    if(s == "done"){
      listScrollController.jumpTo(listScrollController.position.minScrollExtent);
      FocusScope.of(context).requestFocus(new FocusNode());
    }
  }

  bool sendingData = true;
  @override
  Future<String> postData(context, List<Map<String, dynamic>> dtNext) async {
    var jsonResponse;
    if(sendingData){
      var api = new Http_Controller();
      var _data = await api.postResponse("https://suryahusadha.com/api/akun/addklg", dtNext);
      jsonResponse = convert.jsonEncode(_data);
      Navigator.pop(context);
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => showNotif(context, jsonResponse.toString(), null),
      ).then((val){
        setState(() {
          callback = val;
        });
      });
    }
    return jsonResponse.toString();
  }

  Widget showNotif(context, _jsonResponse, List<Map<String, dynamic>> dtNext) {
    if (dtNext != null) {
      postData(context, dtNext);
      sendingData = false;
      return new Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.grey,
        ),
      );
    }else{
      sendingData = true;
      if(_jsonResponse != "" || _jsonResponse != null) {
        var decode = convert.jsonDecode(_jsonResponse);
        if(decode["statusCode"] == 200){
          var jsonResponse = convert.jsonDecode(decode["responseBody"]);
          String msg = jsonResponse["addklg"][0]["msg"];
          String response = jsonResponse["addklg"][0]["response"];
          var newdata = jsonResponse["addklg"][0]["data"];
          if (response == "1"){
            return Dialogue(context, msg, widget._color1, widget._color2, null, newdata);
          } else {
            return Dialogue(context, msg, Colors.red, Colors.red, null, null);
          }
        }else{
          return Dialogue(context,
              "Mohon maaf, terjadi kesalahan pengolahan data pada saat simpan data, silahkan ulangi beberapa saat lagi!",
              Colors.red, Colors.red, null, null);
        }
      } else {
        return Dialogue(context,
            "Mohon maaf, terjadi kesalahan koneksi pada saat simpan data, silahkan ulangi beberapa saat lagi!",
            Colors.red, Colors.red, null, null);
      }
    }
  }
}