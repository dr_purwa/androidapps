import 'package:country_pickers/country.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:masked_text_input_formatter/masked_text_input_formatter.dart';
import 'package:surya_husadha/components/com_reservasi/reservasi_models.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surya_husadha/helper/database/akun/akun_db.dart';
import 'package:surya_husadha/helper/dialogue/customDialogue.dart';
import 'package:country_pickers/country_pickers.dart';
import 'dart:ui' as ui;
import 'dart:convert' as convert;
import 'package:intl/intl.dart';

import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';

class EditForm extends StatefulWidget {
  var _akun;
  Color _textColor = Colors.white;
  String _cabang;
  IconData _icon;
  String _column;
  String _dt;
  String _title;
  String _page;
  Color _color1;
  Color _color2;
  Color calselect;

  EditForm(List params, akun){
    this._akun = akun;
    params.forEach((dt){
      this._cabang = dt["_cabang"];
      this._title = dt["_title"];
      this._dt = dt["_dt"];
      this._icon = dt["_icon"];
      this._column = dt["_column"];
      this._page = dt["_page"];
      if(_cabang=="group"){
        this.calselect = WarnaCabang.shh;
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
        this._textColor= Colors.grey[600];
      }
      if(_cabang=="denpasar"){
        this.calselect = WarnaCabang.shh;
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
      }
      if(_cabang=="ubung"){
        this.calselect = WarnaCabang.ubung;
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
      }
      if(_cabang=="nusadua"){
        this.calselect = WarnaCabang.nusadua;
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
      }
      if(_cabang=="kmc"){
        this.calselect = WarnaCabang.kmc;
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
      }
    });
  }

  @override
  _EditForm createState() => new _EditForm();
}

class _EditForm extends State<EditForm>{

  var inputfomater = null;

  Country _selectedDialogCountry;

  final _Scafoltkey = GlobalKey<ScaffoldState>();
  Widget _buttonUpdate;
  Widget _editForm;

  bool selectGender = false;
  bool _genderErr = false;
  Gender selectedGender;
  List<Gender> gender = <Gender>[
    const Gender("", "Pilih Jenis Kelamin", FontAwesomeIcons.ban),
    const Gender("M", "Laki - Laki", FontAwesomeIcons.male),
    const Gender("F", "Perempuan", FontAwesomeIcons.female)
  ];

  TextEditingController _editTextController = TextEditingController();
  bool _errorText = false;
  bool sendingData = false;
  bool loadfirst = true;

  //_tgllahir
  bool _tgllahirErr = false;
  FocusNode _tgllahir = new FocusNode();
  TextEditingController _tgllahirController = new TextEditingController();

  //_noIdCard
  bool _noIdCardErr = false;
  FocusNode _noIdCard = new FocusNode();
  TextEditingController _noIdCardController = new TextEditingController();
  bool selectCard = false;
  String selectedTipecard;
  List<String> _mTipecard = ['Pilih Jenis Identitas', 'KTP', 'SIM', 'PASSPORT'].toList();

  bool selectstts = false;
  String selectedstts;
  List<String> _mStts = ['Pilih Status', 'Single', 'Menikah'].toList();

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> params=[];
    params.add({
      "_cabang": widget._cabang,
    });
    //Navigator.of(context).pop(params);
    RouteHelper(context, widget._page, params);
  }

  @override
  void initState() {
    super.initState();
  }

  bool passwordVisible = true;
  bool _passwdErr = false;
  String _passwdErrtxt = "";
  String _passwdErrtxt1 = "";
  String _passwdErrtxt2 = "";
  TextEditingController _passwdController = new TextEditingController();
  TextEditingController _passwdController1 = new TextEditingController();
  TextEditingController _passwdController2 = new TextEditingController();

  bool visiblePaswdlama = true;

  @override
  Widget build(BuildContext context) {

    if(loadfirst) {
      _editTextController.text = widget._dt.toString();
    }
    _editForm = _normalForm();
    _buttonUpdate = _normalButton();

    if (widget._column == "nrmshh" || widget._column == "nrmub" || widget._column == "nrmnd") {
      inputfomater = [
        MaskedTextInputFormatter(
          mask: 'xx.xx.xx',
          separator: '.',
        ),
      ];
      if(widget._column == "nrmshh"){
        widget._title = "NRM Denpasar";
      }
      if(widget._column == "nrmub"){
        widget._title = "NRM Ubung";
      }
      if(widget._column == "nrmnd"){
        widget._title = "NRM Nusadua";
      }
      _editForm = _nrmForm();
      _buttonUpdate = _nrmButton();
    }
    if (widget._column == "address") {
      _editForm = _addressForm();
    }
    bool visibleUname = false;
    if(widget._column == "username"){
      visibleUname = true;
    }
    if (widget._column == "passwd") {
      if(loadfirst){
        _passwdController.text = "";
        _passwdController1.text = "";
        _passwdController2.text = "";
      }
      if(widget._dt == null){
        visiblePaswdlama = false;
      }
      _editForm = _passwdForm();
      _buttonUpdate = _passwdButton();
    }
    if (widget._column == "nasionality") {
      _editForm = _nasionalityForm();
    }
    if (widget._column == "bodcity") {
      if(loadfirst) {
        _editTextController.text = widget._dt.split(", ")[0].toString();
        _tgllahirController.text = widget._dt.split(", ")[1].toString();
      }
      _editForm = _bodForm();
      _buttonUpdate = _bodButton();
    }
    if (widget._column == "NoidCard") {
      if(!selectCard) {
        String idcard = widget._dt.split(", ")[0].toString();
        if(idcard == null || idcard == "" || idcard.isEmpty || idcard.length == 0){
          selectedTipecard=_mTipecard.elementAt(0);
        }else{
          selectedTipecard=idcard.toString();
        }
      }
      if(loadfirst) {
        _noIdCardController.text = widget._dt.split(", ")[1].toString();
      }
      _editForm = _idCardForm();
      _buttonUpdate = _idCardButton();
    }
    if (widget._column == "gender") {
      if(!selectGender) {
        if(widget._dt == "" || widget._dt == null){
          selectedGender=gender[0];
        };
        if(widget._dt == "M"){
          selectedGender = gender[1];
        };
        if(widget._dt == "F"){
          selectedGender = gender[2];
        };
      }
      _editForm = _genderForm();
      _buttonUpdate = _genderButton();
    }
    if (widget._column == "marital") {
      if(!selectstts) {
        String stts = widget._dt;
        if(stts == null || stts == "" || stts.isEmpty || stts.length == 0){
          selectedstts=_mStts.elementAt(0);
        }else{
          selectedstts=stts;
        }
      }
      _editForm = _maritalForm();
      _buttonUpdate = _maritalButton();
    }

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.white,
        systemNavigationBarDividerColor: Colors.grey[300],
        systemNavigationBarIconBrightness: Brightness.dark,// navigation bar color
        statusBarColor: widget._color2,
        statusBarIconBrightness: Brightness.dark// statu// status bar color
    ));
    return new SafeArea(
        child: new Scaffold(
          appBar: new PreferredSize(
              preferredSize: Size.fromHeight(200.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Container(
                    color: widget._color1,
                    height: 55,
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Flexible(
                          flex: 1,
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Container(
                                child: new InkWell(
                                  onTap: ()=>Navigator.pop(context),
                                  child: new Container(
                                    padding: EdgeInsets.only(left: 24, right: 24,),
                                    alignment: Alignment.center,
                                    child: new Icon(
                                      FontAwesomeIcons.arrowLeft,
                                      size: 24.0,
                                      color: widget._textColor,
                                    ),
                                  ),
                                ),
                              ),
                              new Expanded(
                                flex: 1,
                                child: new Text(
                                  "Perbarui Informasi Akun",
                                  overflow: TextOverflow.fade,
                                  softWrap: false,
                                  style: TextStyle(fontSize: 16, color: widget._textColor),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
          ),
            body:new WillPopScope(
              onWillPop: _onWillPop,
              child: new Scaffold(
                  key: _Scafoltkey,
                  body: new Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      new Container(
                        padding:EdgeInsets.only(top: 24, left: 24, right: 24),
                        child: Column(
                          children: <Widget>[
                            new Expanded(
                              child: ListView(
                                children: <Widget>[
                                  new Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "Update "+widget._title,
                                        softWrap: true,
                                        overflow: TextOverflow.visible,
                                        style: TextStyle(
                                            fontSize: 22,
                                            fontWeight: FontWeight.w900
                                        ),
                                      ),
                                    ],
                                  ),
                                  new Visibility(
                                    visible: visibleUname ? false : true,
                                    child: new Container(
                                      child: Text(
                                        "Silahkan perbarui informasi akun yang Anda gunakan, melalui formulir yang tersedia dibawah ini.",
                                        style: TextStyle(height: 1.2, color: Colors.grey[600]),
                                      ),
                                    ),
                                  ),
                                  new Visibility(
                                    visible: visibleUname,
                                    child: new Container(
                                      child: Text(
                                        "Untuk update username hanya diberikan 1x kesempatan, setelah berhasil melakukan perubahan Anda tidak dapat melakukan perubahan lagi.",
                                        style: TextStyle(height: 1.2, color: Colors.grey[600]),
                                      ),
                                    ),
                                  ),
                                  _editForm,
                                ],
                              ),
                            ),
                            _buttonUpdate,
                          ],
                        ),
                      )
                    ],
                  )
              ),
            )
        )
    );
  }

  @override
  Future<String> postData(context, List<Map<String, dynamic>> dtNext) async {
    var jsonResponse;
    if(sendingData){
      var api = new Http_Controller();
      var _data = await api.postResponse("https://suryahusadha.com/api/akun/editakun", dtNext);
      jsonResponse = convert.jsonEncode(_data);
      print("jsonResponse: "+jsonResponse.toString());
      Navigator.pop(context);
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => showNotif(context, jsonResponse.toString(), null),
      );
    }
    return jsonResponse.toString();
  }

  Widget showNotif(context, _jsonResponse, List<Map<String, dynamic>> dtNext) {
    if (dtNext != null) {
      postData(context, dtNext);
      sendingData = false;
      return new Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.grey,
        ),
      );
    } else {
      if(_jsonResponse != "" || _jsonResponse != null) {
        var decode = convert.jsonDecode(_jsonResponse);
        if(decode["statusCode"] == 200){
          var jsonResponse = convert.jsonDecode(decode["responseBody"]);
          String msg = jsonResponse["editakun"][0]["msg"];
          String response = jsonResponse["editakun"][0]["response"];
          if (response == "1"){
            String page;
            var db = new akunDB();
            //updateColumn(id, column, dt)
            if(widget._column == "bodcity"){
              db.updateColumn(widget._akun[0]["id"].toString(), "bod", _tgllahirController.text.toString());
              db.updateColumn(widget._akun[0]["id"].toString(), "bodcity", _editTextController.text.toString());
            }else{
              if(widget._column == "NoidCard"){
                db.updateColumn(widget._akun[0]["id"].toString(), "idCardtype", selectedTipecard.toString());
                db.updateColumn(widget._akun[0]["id"].toString(), "NoidCard", _noIdCardController.text.toString());
              }else{
                if(widget._column == "gender"){
                  db.updateColumn(widget._akun[0]["id"].toString(), widget._column.toString(), selectedGender.val);
                }else{
                  if(widget._column == "passwd"){
                    if(visiblePaswdlama){
                      var db = new akunDB();
                      db.deleteAkun();
                      page = 'group';
                    }
                  }else{
                    if(widget._column == "marital"){
                      db.updateColumn(widget._akun[0]["id"].toString(), widget._column.toString(), selectedstts);
                    }else{
                      db.updateColumn(widget._akun[0]["id"].toString(), widget._column.toString(), _editTextController.text.toString());
                    }
                  }
                }
              }
            }
            List<Map<String, dynamic>> params=[];
            params.add({
              "_cabang": widget._cabang,
            });
            return Dialogue(context, msg, widget._color1, widget._color2, page, params);
          } else {
            return Dialogue(context, msg, Colors.red, Colors.red, null, null);
          }
        }else{
          return Dialogue(context,
              "Mohon maaf, terjadi kesalahan pengolahan data pada saat simpan data, silahkan ulangi beberapa saat lagi!",
              Colors.red, Colors.red, null, null);
        }
      } else {
        return Dialogue(context,
            "Mohon maaf, terjadi kesalahan koneksi pada saat simpan data, silahkan ulangi beberapa saat lagi!",
            Colors.red, Colors.red, null, null);
      }
    }
  }

  //NORMAL
  Widget _nrmForm(){
    return new Container(
      padding:EdgeInsets.only(top: 60),
      child: new TextField(
        inputFormatters: inputfomater,
        style: TextStyle(
            color: Colors.grey[800]
        ),
        decoration: InputDecoration(
            contentPadding: EdgeInsets.only(top: 14, bottom: 14),
            prefix: new Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(right: 10),
                  child: Icon( widget._icon, size: 18, color: Colors.grey[800],),
                )
              ],
            ),
            errorBorder: UnderlineInputBorder(borderSide: BorderSide(
                color: Colors.redAccent
            )),
            focusedBorder: UnderlineInputBorder(borderSide: BorderSide(
                color: Colors.grey[400]
            ))
        ),
        controller: _editTextController,
      ),
    );
  }

  Widget _nrmButton(){
    return new Container(
      padding: EdgeInsets.only(bottom: 40),
      child: RawMaterialButton(
        onPressed: (){
          bool next = true;
          setState(() {
            loadfirst = false;
            _errorText = false;
            sendingData = true;
          });
          if(next){
            List<Map<String, dynamic>> dtNext=[];
            dtNext.add({
              "_bod": widget._akun[0]["bod"].toString(),
              "_id": widget._akun[0]["id"].toString(),
              "_column": widget._column.toString(),
              "_value": _editTextController.text.toString(),
            });
            showDialog(
              barrierDismissible: false,
              context: context,
              builder: (BuildContext context) => showNotif(context, null, dtNext),
            );
          }
        },
        elevation: 0,
        padding: EdgeInsets.all(16),
        animationDuration: Duration(milliseconds: 500),
        //fillColor: Colors.grey[300],
        fillColor: widget._color1,
        splashColor: widget._color2,
        highlightColor: widget._color2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
        ),
        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Expanded(
                child: new Column(
                  children: <Widget>[
                    Text(
                      "Simpan",
                      style: TextStyle(color: widget._textColor, fontSize: 18),
                    ),
                  ],
                )
            ),
            new Column(children: <Widget>[
              Icon(
                FontAwesomeIcons.arrowRight,
                color: widget._textColor,
                size: 16,
              )
            ],),
          ],
        ),
      ),
    );
  }

  //NORMAL
  Widget _passwdForm(){
    return new Column(
      children: <Widget>[
        new Visibility(
          visible: visiblePaswdlama,
          child: new Container(
            padding:EdgeInsets.only(top: 60),
            child: new TextField(
              style: TextStyle(
                  color: Colors.grey[800]
              ),
              obscureText: passwordVisible,
              decoration: InputDecoration(
                  hintText: "Password Lama",
                  suffixIcon: IconButton(
                    icon: Icon(
                      passwordVisible
                          ? Icons.visibility
                          : Icons.visibility_off,
                      color: Colors.grey,
                    ),
                    onPressed: () {
                      setState(() {
                        passwordVisible = !passwordVisible;
                      });
                    },
                  ),
                  errorText: _passwdErr ? _passwdErrtxt : null,
                  contentPadding: EdgeInsets.only(top: 14, bottom: 14),
                  prefix: new Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.only(right: 10),
                        child: Icon( widget._icon, size: 18, color: Colors.grey[800],),
                      )
                    ],
                  ),
                  errorBorder: UnderlineInputBorder(borderSide: BorderSide(
                      color: Colors.redAccent
                  )),
                  focusedBorder: UnderlineInputBorder(borderSide: BorderSide(
                      color: Colors.grey[400]
                  ))
              ),
              controller: _passwdController,
              onTap: ()=> loadfirst = false,
            ),
          ),
        ),
        new Container(
          padding:EdgeInsets.only(top: 20),
          child: new TextField(
            obscureText: passwordVisible,
            style: TextStyle(
                color: Colors.grey[800]
            ),
            decoration: InputDecoration(
                hintText: "Password Baru",
                suffixIcon: IconButton(
                  icon: Icon(
                    passwordVisible
                        ? Icons.visibility
                        : Icons.visibility_off,
                    color: Colors.grey,
                  ),
                  onPressed: () {
                    setState(() {
                      passwordVisible = !passwordVisible;
                    });
                  },
                ),
                errorText: _passwdErr ? _passwdErrtxt1 : null,
                contentPadding: EdgeInsets.only(top: 14, bottom: 14),
                prefix: new Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.only(right: 10),
                      child: Icon( widget._icon, size: 18, color: Colors.grey[800],),
                    )
                  ],
                ),
                errorBorder: UnderlineInputBorder(borderSide: BorderSide(
                    color: Colors.redAccent
                )),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(
                    color: Colors.grey[400]
                ))
            ),
            controller: _passwdController1,
            onTap: ()=> loadfirst = false,
          ),
        ),
        new Container(
          padding:EdgeInsets.only(top: 20),
          child: new TextField(
            obscureText: passwordVisible,
            style: TextStyle(
                color: Colors.grey[800]
            ),
            decoration: InputDecoration(
                hintText: "Ulangi Password Baru",
                suffixIcon: IconButton(
                  icon: Icon(
                    passwordVisible
                        ? Icons.visibility
                        : Icons.visibility_off,
                    color: Colors.grey,
                  ),
                  onPressed: () {
                    setState(() {
                      passwordVisible = !passwordVisible;
                    });
                  },
                ),
                errorText: _passwdErr ? _passwdErrtxt2 : null,
                contentPadding: EdgeInsets.only(top: 14, bottom: 14),
                prefix: new Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.only(right: 10),
                      child: Icon( widget._icon, size: 18, color: Colors.grey[800],),
                    )
                  ],
                ),
                errorBorder: UnderlineInputBorder(borderSide: BorderSide(
                    color: Colors.redAccent
                )),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(
                    color: Colors.grey[400]
                ))
            ),
            controller: _passwdController2,
            onTap: ()=> loadfirst = false,
          ),
        )
      ],
    );
  }

  Widget _passwdButton(){
    return new Container(
      padding: EdgeInsets.only(bottom: 40),
      child: RawMaterialButton(
        onPressed: (){
          bool next = true;
          setState(() {
            loadfirst = false;
            _passwdErr = false;
            sendingData = true;
            _passwdErrtxt = null;
            _passwdErrtxt1 = null;
            _passwdErrtxt2 = null;
            if(visiblePaswdlama){
              if(_passwdController.text.isEmpty){
                _passwdErr = true;
                _passwdErrtxt = "Formulir tidak boleh kosong!";
                next = false;
              }
            }
            if(_passwdController1.text.isEmpty){
              _passwdErr = true;
              _passwdErrtxt1 = "Formulir tidak boleh kosong!";
              next = false;
            }
            if(_passwdController2.text.isEmpty){
              _passwdErr = true;
              _passwdErrtxt2 = "Formulir tidak boleh kosong!";
              next = false;
            }
            if(_passwdController1.text != _passwdController2.text){
              _passwdErr = true;
              _passwdErrtxt2 = "Pasword baru tidak sama!";
              next = false;
            }
            if(_passwdController1.text.length < 8 ||  _passwdController2.text.length < 8){
              _passwdErr = true;
              _passwdErrtxt2 = "Minimal 8 character!";
              _passwdErrtxt1 = "Minimal 8 character!";
              next = false;
            }
          });
          if(next){
            List<Map<String, dynamic>> dtNext=[];
            dtNext.add({
              "_id": widget._akun[0]["id"].toString(),
              "_username": widget._akun[0]["username"].toString(),
              "_column": widget._column.toString(),
              "_paswdlama": _passwdController.text.toString(),
              "_paswdbaru": _passwdController2.text.toString(),
            });
            showDialog(
              barrierDismissible: false,
              context: context,
              builder: (BuildContext context) => showNotif(context, null, dtNext),
            );
          }
        },
        elevation: 0,
        padding: EdgeInsets.all(16),
        animationDuration: Duration(milliseconds: 500),
        //fillColor: Colors.grey[300],
        fillColor: widget._color1,
        splashColor: widget._color2,
        highlightColor: widget._color2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
        ),
        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Expanded(
                child: new Column(
                  children: <Widget>[
                    Text(
                      "Simpan",
                      style: TextStyle(color: widget._textColor, fontSize: 18),
                    ),
                  ],
                )
            ),
            new Column(children: <Widget>[
              Icon(
                FontAwesomeIcons.arrowRight,
                color: widget._textColor,
                size: 16,
              )
            ],),
          ],
        ),
      ),
    );
  }

  //NORMAL
  Widget _normalForm(){
    return new Container(
      padding:EdgeInsets.only(top: 60),
      child: new TextField(
        style: TextStyle(
          color: Colors.grey[800]
        ),
        decoration: InputDecoration(
            errorText: _errorText ? "" : null,
            contentPadding: EdgeInsets.only(top: 14, bottom: 14),
            prefix: new Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(right: 10),
                  child: Icon( widget._icon, size: 18, color: Colors.grey[800],),
                )
              ],
            ),
            errorBorder: UnderlineInputBorder(borderSide: BorderSide(
                color: Colors.redAccent
            )),
            focusedBorder: UnderlineInputBorder(borderSide: BorderSide(
                color: Colors.grey[400]
            ))
        ),
        controller: _editTextController,
      ),
    );
  }

  Widget _normalButton(){
    return new Container(
      padding: EdgeInsets.only(bottom: 40),
      child: RawMaterialButton(
        onPressed: (){
          bool next = true;
          setState(() {
            loadfirst = false;
            _errorText = false;
            sendingData = true;
            if(_editTextController.text.isEmpty){
              _errorText = true;
              next = false;
            }
          });
          if(next){
            List<Map<String, dynamic>> dtNext=[];
            dtNext.add({
              "_id": widget._akun[0]["id"].toString(),
              "_column": widget._column.toString(),
              "_value": _editTextController.text.toString(),
            });
            showDialog(
              barrierDismissible: false,
              context: context,
              builder: (BuildContext context) => showNotif(context, null, dtNext),
            );
          }
        },
        elevation: 0,
        padding: EdgeInsets.all(16),
        animationDuration: Duration(milliseconds: 500),
        fillColor: widget._color1,
        splashColor: widget._color2,
        highlightColor: widget._color2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
        ),
        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Expanded(
                child: new Column(
                  children: <Widget>[
                    Text(
                      "Simpan",
                      style: TextStyle(color: widget._textColor, fontSize: 18),
                    ),
                  ],
                )
            ),
            new Column(children: <Widget>[
              Icon(
                FontAwesomeIcons.arrowRight,
                color: widget._textColor,
                size: 16,
              )
            ],),
          ],
        ),
      ),
    );
  }

  //BOD
  Widget _bodForm(){
    return new Column(
      children: <Widget>[
        new Container(
          padding:EdgeInsets.only(top: 60),
          child: new TextField(
            onEditingComplete: () => FocusScope.of(context).requestFocus(_tgllahir),
            decoration: InputDecoration(
                errorText: _errorText ? "" : null,
                contentPadding: EdgeInsets.only(top: 14, bottom: 14),
                prefix: new Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Container(
                        padding: EdgeInsets.only(right: 10),
                        child: new Row(
                          children: <Widget>[
                            Icon( FontAwesomeIcons.mapMarkerAlt, size: 18,),
                          ],
                        )
                    )
                  ],
                ),
                suffix: new Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Container(
                        padding: EdgeInsets.only(left: 10),
                        child: new Row(
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.only(left: 5, top: 5),
                              child: Text(
                                "Kota",
                                style: TextStyle(
                                    color: Colors.grey[600]
                                ),
                              ),
                            )
                          ],
                        )
                    )
                  ],
                ),
                errorBorder:  UnderlineInputBorder(borderSide: BorderSide(
                    color: Colors.redAccent
                )),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(
                    color: Colors.grey[400]
                ))
            ),
            controller: _editTextController,
          ),
        ),
        //BOD
        new Container(
          padding:EdgeInsets.only(top: 20),
          child: new TextField(
            focusNode: _tgllahir,
            onTap: (){
              showPickerDate(context);
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            inputFormatters: [
              MaskedTextInputFormatter(
                mask: 'xx-xx-xxxx',
                separator: '-',
              ),
            ],
            decoration: InputDecoration(
                errorText: _tgllahirErr ? "" : null,
                contentPadding: EdgeInsets.only(top: 14, bottom: 14),
                prefix: new Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Container(
                        padding: EdgeInsets.only(right: 10),
                        child: new Row(
                          children: <Widget>[
                            Icon( FontAwesomeIcons.calendarAlt, size: 18,),
                          ],
                        )
                    )
                  ],
                ),
                suffix: new Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Container(
                        padding: EdgeInsets.only(left: 10),
                        child: new Row(
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.only(left: 5, top: 5),
                              child: Text(
                                "Tgl. Lahir",
                                style: TextStyle(
                                    color: Colors.grey[600]
                                ),
                              ),
                            )
                          ],
                        )
                    )
                  ],
                ),
                errorBorder:  UnderlineInputBorder(borderSide: BorderSide(
                    color: Colors.redAccent
                )),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(
                    color: Colors.grey[400]
                ))
            ),
            controller: _tgllahirController,
          ),
        ),
      ],
    );
  }

  Widget _bodButton(){
    return new Container(
      padding: EdgeInsets.only(bottom: 40),
      child: RawMaterialButton(
        onPressed: (){
          loadfirst = false;
          _errorText = false;
          _tgllahirErr = false;
          sendingData = true;
          bool next = true;
          if(_editTextController.text.isEmpty){
            _errorText = true;
            next = false;
          }
          if(_tgllahirController.text.isEmpty){
            _tgllahirErr = true;
            next = false;
          }
          if(next){
            List<Map<String, dynamic>> dtNext=[];
            dtNext.add({
              "_id": widget._akun[0]["id"].toString(),
              "_column": widget._column.toString(),
              "_kotavalue": _editTextController.text.toString(),
              "_datevalue": _tgllahirController.text.toString(),
            });
            showDialog(
              barrierDismissible: false,
              context: context,
              builder: (BuildContext context) => showNotif(context, null, dtNext),
            );
          }
        },
        elevation: 0,
        padding: EdgeInsets.all(16),
        animationDuration: Duration(milliseconds: 500),
        fillColor: widget._color1,
        splashColor: widget._color2,
        highlightColor: widget._color2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
        ),
        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Expanded(
                child: new Column(
                  children: <Widget>[
                    Text(
                      "Simpan",
                      style: TextStyle(color: widget._textColor, fontSize: 18),
                    ),
                  ],
                )
            ),
            new Column(children: <Widget>[
              Icon(
                FontAwesomeIcons.arrowRight,
                color: widget._textColor,
                size: 16,
              )
            ],),
          ],
        ),
      ),
    );
  }

  showPickerDate(BuildContext context) {
    final f = new DateFormat('dd-MM-yyyy');
    DateTime _Selected;
    if(_tgllahirController.text != ""){
      int d = int.parse(_tgllahirController.text.split("-")[0]);
      int m = int.parse(_tgllahirController.text.split("-")[1]);
      int y = int.parse(_tgllahirController.text.split("-")[2]);
      _Selected = DateTime(y, m ,d);
    }else{
      _Selected = DateTime.now();
    }
    int _yNow = int.parse(DateFormat.y().format(_Selected));
    DateTime _max = DateTime(_yNow-100);

    Picker(
      hideHeader: true,
      changeToFirst: false,
      adapter: DateTimePickerAdapter(
        customColumnType: [0, 1, 2],
        value: _Selected,
        maxValue: DateTime.now(),
        minValue: _max,
      ),
      title: new Container(
        child: Column(children: <Widget>[
          Text("Pilih Tgl. Lahir:", style: TextStyle(color: widget.calselect),),
        ],),
      ),
      selectedTextStyle: TextStyle(color: widget.calselect),
      confirm: new InkWell(
        onTap: ()=>{
          Navigator.of(context).pop(),
          widget,_tgllahirController.text = f.format(_Selected).toString(),
        },
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(children: <Widget>[
            new Icon(
              FontAwesomeIcons.check,
              color: widget.calselect,
              size: 12,
            ),
            Container(width: 5,),
            Text("Pilih", style: TextStyle(color: widget.calselect),)
          ],),
        ),
      ),
      cancel: new InkWell(
        onTap: ()=>{
          Navigator.of(context).pop()
        },
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(children: <Widget>[
            new Icon(
              FontAwesomeIcons.times,
              color: widget.calselect,
              size: 12,
            ),
            Container(width: 2,),
            Text("Tutup", style: TextStyle(color: widget.calselect),)
          ],),
        ),
      ),
      onSelect: (Picker picker, int index, List<int> selecteds) {
        this.setState(() {
          int d = _Selected.day;
          int m = _Selected.month;
          int y = _Selected.year;
          if(index == 2){
            d = (picker.adapter as DateTimePickerAdapter).value.day;
            _Selected = DateTime(y, m, d);
          }
          if(index == 1){
            m = (picker.adapter as DateTimePickerAdapter).value.month;
            _Selected = DateTime(y, m, d);
          }
          if(index == 0){
            y = (picker.adapter as DateTimePickerAdapter).value.year;
            _Selected = DateTime(y, m, d);
          }

        });
      },
    ).showDialog(context);

  }

  //IDCARD
  Widget _idCardForm(){
    return new Column(
      children: <Widget>[
        new Container(
          padding:EdgeInsets.only(top: 60),
          child: new DropdownButtonFormField(
            onChanged:  (s) {
              setState(() {
                if(s != null){
                  selectCard = true;
                  selectedTipecard = s;
                }
              });
            },
            value:selectedTipecard,
            items: _mTipecard.map((String item) =>
            new DropdownMenuItem<String>(
                value: item,
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.only(top:3),
                      child:new Text(
                        item,
                        style: new TextStyle(color: Colors.black, fontSize: 14),
                      ),
                    ),
                  ],
                )
            )
            ).toList(),
            decoration: InputDecoration(
                errorText: _errorText ? "" : null,
                contentPadding: EdgeInsets.only(top: 14, bottom: 14),
                prefix: new Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Container(
                        padding: EdgeInsets.only(right: 10),
                        child: new Row(
                          children: <Widget>[
                            Icon( FontAwesomeIcons.idCard, size: 18,),
                          ],
                        )
                    )
                  ],
                ),
                suffix: new Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Container(
                        padding: EdgeInsets.only(left: 10),
                        child: new Row(
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.only(left: 5, top: 5),
                              child: Text(
                                "Kartu",
                                style: TextStyle(
                                    color: Colors.grey[600]
                                ),
                              ),
                            )
                          ],
                        )
                    )
                  ],
                ),
                errorBorder:  UnderlineInputBorder(borderSide: BorderSide(
                    color: Colors.redAccent
                )),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(
                    color: Colors.grey[400]
                ))
            ),
          ),
        ),
        new Container(
          padding:EdgeInsets.only(top: 20),
          child: new TextField(
            focusNode: _noIdCard,
            decoration: InputDecoration(
                errorText: _noIdCardErr ? "" : null,
                contentPadding: EdgeInsets.only(top: 14, bottom: 14),
                prefix: new Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Container(
                        padding: EdgeInsets.only(right: 10),
                        child: new Row(
                          children: <Widget>[
                            Icon( FontAwesomeIcons.idBadge, size: 18,),
                          ],
                        )
                    )
                  ],
                ),
                suffix: new Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Container(
                        padding: EdgeInsets.only(left: 10),
                        child: new Row(
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.only(left: 5, top: 5),
                              child: Text(
                                "No. ID",
                                style: TextStyle(
                                    color: Colors.grey[600]
                                ),
                              ),
                            )
                          ],
                        )
                    )
                  ],
                ),
                errorBorder:  UnderlineInputBorder(borderSide: BorderSide(
                    color: Colors.redAccent
                )),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(
                    color: Colors.grey[400]
                ))
            ),
            controller: _noIdCardController,
          ),
        ),
      ],
    );
  }

  Widget _idCardButton(){
    return new Container(
      padding: EdgeInsets.only(bottom: 40),
      child: RawMaterialButton(
        onPressed: (){
          bool next = true;
          setState(() {
            loadfirst = false;
            _errorText = false;
            _noIdCardErr = false;
            sendingData = true;
            if(selectedTipecard.toString() == "Pilih Jenis Identitas"){
              _errorText = true;
              next = false;
            }
            if(_noIdCardController.text.isEmpty){
              _noIdCardErr = true;
              next = false;
            }
          });
          if(next){
            List<Map<String, dynamic>> dtNext=[];
            dtNext.add({
              "_id": widget._akun[0]["id"].toString(),
              "_column": widget._column.toString(),
              "_tipe": selectedTipecard.toString(),
              "_nomor": _noIdCardController.text.toString(),
            });
            showDialog(
              barrierDismissible: false,
              context: context,
              builder: (BuildContext context) => showNotif(context, null, dtNext),
            );
          }
        },
        elevation: 0,
        padding: EdgeInsets.all(16),
        animationDuration: Duration(milliseconds: 500),
        fillColor: widget._color1,
        splashColor: widget._color2,
        highlightColor: widget._color2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
        ),
        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Expanded(
                child: new Column(
                  children: <Widget>[
                    Text(
                      "Simpan",
                      style: TextStyle(color: widget._textColor, fontSize: 18),
                    ),
                  ],
                )
            ),
            new Column(children: <Widget>[
              Icon(
                FontAwesomeIcons.arrowRight,
                color: widget._textColor,
                size: 16,
              )
            ],),
          ],
        ),
      ),
    );
  }

  //GENDER
  Widget _genderForm(){
    return new Column(
      children: <Widget>[
        new Container(
          padding:EdgeInsets.only(top: 60),
          child: new DropdownButtonFormField(
            value:selectedGender,
            onChanged: (Gender newValue) {
              setState(() {
                selectGender = true;
                if(newValue.val == ""){
                  selectedGender = gender[0];
                }
                if(newValue.val == "M"){
                  selectedGender = gender[1];
                }
                if(newValue.val == "F"){
                  selectedGender = gender[2];
                }
              });
            },
            items: gender.map((Gender gender){
              return new DropdownMenuItem<Gender>(
                  value: gender,
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.only(top:3),
                        child:new Text(
                          gender.title,
                          style: new TextStyle(color: Colors.black, fontSize: 14),
                        ),
                      ),
                      new Container(width: 5,),
                      new Icon(
                        gender.icon,
                        size: 16,
                      ),
                    ],
                  )
              );
            }).toList(),
            decoration: InputDecoration(
                errorText: _genderErr ? "" : null,
                contentPadding: EdgeInsets.only(top: 14, bottom: 14),
                prefix: new Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Container(
                        padding: EdgeInsets.only(right: 10),
                        child: new Row(
                          children: <Widget>[
                            Icon( FontAwesomeIcons.idCard, size: 18,),
                          ],
                        )
                    )
                  ],
                ),
                errorBorder:  UnderlineInputBorder(borderSide: BorderSide(
                    color: Colors.redAccent
                )),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(
                    color: Colors.grey[400]
                ))
            ),
          ),
        ),
      ],
    );
  }

  Widget _genderButton(){
    return new Container(
      padding: EdgeInsets.only(bottom: 40),
      child: RawMaterialButton(
        onPressed: (){
          bool next = true;
          setState(() {
            loadfirst = false;
            _genderErr = false;
            sendingData = true;
            if(selectedGender == gender[0]){
              _genderErr = true;
              next = false;
            }
          });
          if(next){
            List<Map<String, dynamic>> dtNext=[];
            dtNext.add({
              "_id": widget._akun[0]["id"].toString(),
              "_column": widget._column.toString(),
              "_value": selectedGender.val,
            });
            showDialog(
              barrierDismissible: false,
              context: context,
              builder: (BuildContext context) => showNotif(context, null, dtNext),
            );
          }
        },
        elevation: 0,
        padding: EdgeInsets.all(16),
        animationDuration: Duration(milliseconds: 500),
        fillColor: widget._color1,
        splashColor: widget._color2,
        highlightColor: widget._color2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
        ),
        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Expanded(
                child: new Column(
                  children: <Widget>[
                    Text(
                      "Simpan",
                      style: TextStyle(color: widget._textColor, fontSize: 18),
                    ),
                  ],
                )
            ),
            new Column(children: <Widget>[
              Icon(
                FontAwesomeIcons.arrowRight,
                color: widget._textColor,
                size: 16,
              )
            ],),
          ],
        ),
      ),
    );
  }

  //MARITAL
  Widget _maritalForm(){
    return new Column(
      children: <Widget>[
        new Container(
          padding:EdgeInsets.only(top: 60),
          child: new DropdownButtonFormField(
            onChanged:  (s) {
              setState(() {
                if(s != null){
                  selectstts = true;
                  selectedstts = s;
                }
              });
            },
            value:selectedstts,
            items: _mStts.map((String item) =>
            new DropdownMenuItem<String>(
                value: item,
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.only(top:3),
                      child:new Text(
                        item,
                        style: new TextStyle(color: Colors.black, fontSize: 14),
                      ),
                    ),
                  ],
                )
            )
            ).toList(),
            decoration: InputDecoration(
                errorText: _errorText ? "" : null,
                contentPadding: EdgeInsets.only(top: 14, bottom: 14),
                prefix: new Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Container(
                        padding: EdgeInsets.only(right: 10),
                        child: new Row(
                          children: <Widget>[
                            Icon( FontAwesomeIcons.idCard, size: 18,),
                          ],
                        )
                    )
                  ],
                ),
                errorBorder:  UnderlineInputBorder(borderSide: BorderSide(
                    color: Colors.redAccent
                )),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(
                    color: Colors.grey[400]
                ))
            ),
          ),
        ),
      ],
    );
  }

  Widget _maritalButton(){
    return new Container(
      padding: EdgeInsets.only(bottom: 40),
      child: RawMaterialButton(
        onPressed: (){
          bool next = true;
          setState(() {
            loadfirst = false;
            _errorText = false;
            sendingData = true;
            if(selectedstts.toString() == "Pilih Status"){
              _errorText = true;
              next = false;
            }
          });
          if(next){
            List<Map<String, dynamic>> dtNext=[];
            dtNext.add({
              "_id": widget._akun[0]["id"].toString(),
              "_column": widget._column.toString(),
              "_value": selectedstts.toString(),
            });
            showDialog(
              barrierDismissible: false,
              context: context,
              builder: (BuildContext context) => showNotif(context, null, dtNext),
            );
          }
        },
        elevation: 0,
        padding: EdgeInsets.all(16),
        animationDuration: Duration(milliseconds: 500),
        fillColor: widget._color1,
        splashColor: widget._color2,
        highlightColor: widget._color2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
        ),
        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Expanded(
                child: new Column(
                  children: <Widget>[
                    Text(
                      "Simpan",
                      style: TextStyle(color: widget._textColor, fontSize: 18),
                    ),
                  ],
                )
            ),
            new Column(children: <Widget>[
              Icon(
                FontAwesomeIcons.arrowRight,
                color: widget._textColor,
                size: 16,
              )
            ],),
          ],
        ),
      ),
    );
  }

  //address
  Widget _addressForm(){
    return new Container(
      padding:EdgeInsets.only(top: 60),
      child: new TextField(
        style: TextStyle(
            color: Colors.grey[800]
        ),
        maxLines: null,
        keyboardType: TextInputType.multiline,
        decoration: InputDecoration(
            errorText: _errorText ? "" : null,
            contentPadding: EdgeInsets.only(top: 14, bottom: 14),
            prefix: new Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(right: 10),
                  child: Icon( widget._icon, size: 18, color: Colors.grey[800],),
                )
              ],
            ),
            errorBorder: UnderlineInputBorder(borderSide: BorderSide(
                color: Colors.redAccent
            )),
            focusedBorder: UnderlineInputBorder(borderSide: BorderSide(
                color: Colors.grey[400]
            ))
        ),
        controller: _editTextController,
      ),
    );
  }

  //address
  Widget _nasionalityForm(){
    return new Container(
      padding:EdgeInsets.only(top: 60),
      child: new TextField(
        onTap: ()=> _openCountryPickerDialog(),
        style: TextStyle(
            color: Colors.grey[800]
        ),
        decoration: InputDecoration(
            errorText: _errorText ? "" : null,
            contentPadding: EdgeInsets.only(top: 14, bottom: 14),
            prefix: new Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(right: 10),
                  child: Icon( widget._icon, size: 18, color: Colors.grey[800],),
                )
              ],
            ),
            errorBorder: UnderlineInputBorder(borderSide: BorderSide(
                color: Colors.redAccent
            )),
            focusedBorder: UnderlineInputBorder(borderSide: BorderSide(
                color: Colors.grey[400]
            ))
        ),
        controller: _editTextController,
      ),
    );
  }

  void _openCountryPickerDialog() => showDialog(
    context: context,
    builder: (context) => Theme(
        data: Theme.of(context).copyWith(primaryColor: widget._color1),
        child: CountryPickerDialog(
            titlePadding: EdgeInsets.all(8.0),
            searchCursorColor: Colors.pinkAccent,
            searchInputDecoration: InputDecoration(hintText: 'Search...'),
            isSearchable: true,
            title: Text('Pilih Nasionality'),
            onValuePicked: (Country country){
              setState((){
                loadfirst = false;
                _selectedDialogCountry = country;
                _editTextController.text = _selectedDialogCountry.name;
              });
            },
            itemBuilder: _buildDialogItem
        )
    ),
  );

  Widget _buildDialogItem(Country country) => Row(
    children: <Widget>[
      CountryPickerUtils.getDefaultFlagImage(country),
      SizedBox(width: 8.0),
      Flexible(child: Text(country.name))
    ],
  );


}