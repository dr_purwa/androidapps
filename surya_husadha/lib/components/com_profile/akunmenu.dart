import 'package:flutter/material.dart';
import 'package:surya_husadha/components/com_google_login/sign_in.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/database/akun/akun_db.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AkunMenu extends StatefulWidget {
  var _akun;
  String _cabang;
  Color _color1;
  Color _color2;
  Color _textColor = Colors.white;
  AkunMenu(List params, akun){
    this._akun = akun;
    params.forEach((dt){
      this._cabang = dt["_cabang"];
      if(_cabang=="group"){
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
        this._textColor= Colors.grey[600];
      }
      if(_cabang=="denpasar"){
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
      }
      if(_cabang=="ubung"){
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
      }
      if(_cabang=="nusadua"){
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
      }
      if(_cabang=="kmc"){
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
      }
    });
  }
  @override
  _AkunMenu createState() => new _AkunMenu();
}

class _AkunMenu extends State<AkunMenu>{

  final _Scafoltkey = GlobalKey<ScaffoldState>();

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> dtNext=[];
    dtNext.add({"_cabang": widget._cabang});
    RouteHelper(context, widget._cabang, dtNext);
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(
        child: new WillPopScope(
          onWillPop: _onWillPop,
          child: new Scaffold(
              key: _Scafoltkey,
              drawer: drawerController(widget._akun , widget._cabang, "AkunMenu"),
              appBar: new PreferredSize(
                preferredSize: Size.fromHeight(200.0),
                child: AppBarController(widget._cabang, _Scafoltkey, widget._akun, "AkunMenu"),
              ),
              body: new Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  new Container(
                    color: Colors.white,
                    child: new Column(
                      children: <Widget>[
                        new Expanded(
                          child: new ListView(
                            scrollDirection: Axis.vertical,
                            children: <Widget>[
                              _akunmenu(widget._akun),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              )
          ),
        ),
    );
  }

  Widget _akunmenu(akun){
    var dt = akun[0];
    Widget _pp;
    if(dt["pp"] != ""){
      String img = SERVER.domain + dt["pp"] +'?dummy=${new DateTime.now()}';
      _pp = new InkWell(
        child: new Container(
          height: 80.0,
          width: 80.0,
          padding: EdgeInsets.all(2),
          decoration: new BoxDecoration(
              borderRadius:
              new BorderRadius.all(new Radius.circular(100.0)),
              color: Colors.lightBlue
          ),
          alignment: Alignment.center,
          child: CircleAvatar(
            backgroundImage: NetworkImage(img),
            minRadius: 80,
            maxRadius: 150,
          ),
        ),
      );
    } else {
      _pp = new InkWell(
          child: new Container(
            height: 80.0,
            width: 80.0,
            padding: EdgeInsets.all(6.0),
            decoration: new BoxDecoration(
                borderRadius:
                new BorderRadius.all(new Radius.circular(100.0)),
                color: Colors.lightBlue
            ),
            alignment: Alignment.center,
            child: new Icon(
              FontAwesomeIcons.user,
              color: Colors.white,
              size: 80.0,
            ),
          )
      );
    }

    return new Container(
      color: Colors.white,
      child: new Column(
        children: <Widget>[
          new Container(
            padding: EdgeInsets.only(top: 24, bottom: 24, left: 24, right: 24),
            child: new Column(
              children: <Widget>[
                new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    _pp,
                    new Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          new Text(
                            "Hi, "+dt["name"],
                            style: TextStyle(fontSize: 18, color: Colors.grey[800], fontWeight: FontWeight.w700),
                          ),
                          new Text(
                            dt["email"],
                            style: TextStyle(fontSize: 14, color: Colors.grey[800]),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          new Container(
            color: Colors.grey[300],
            width: MediaQuery.of(context).size.width,
            height: 1.0,
          ),
          new Container(
            padding: EdgeInsets.only(bottom: 30),
            child: Column(children: <Widget>[
                cardHeader("Konfigurasi Akun", FontAwesomeIcons.cog),
                cardMenu("Kartu Identitas", FontAwesomeIcons.idCard, "KartuIdentitas"),
                cardMenu("Data Personal", FontAwesomeIcons.userCircle, "AkunDetail"),
                cardMenu("Akun Web & Apps", FontAwesomeIcons.key, "AkunWebApps"),
                cardMenu("Akun Berobat", FontAwesomeIcons.idCard, "AkunBerobat"),
                cardMenu("Akun Berobat Keluarga", FontAwesomeIcons.idCard, "AkunBerobatKeluarga"),
                cardHeader("Konfigurasi Lainnya", FontAwesomeIcons.cog),
                cardDt("Bahasa Apps", dt["defaultlang"],  FontAwesomeIcons.globe, false, "defaultlang"),
                cardDt("Sign Out", "",  FontAwesomeIcons.signOutAlt, true, "logout"),
              ],
            ),
          ),
        ],
      ),
    );

  }

  Widget cardHeader(String title, IconData icon){
    return new Container(
      color: Colors.grey[100],
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 18, bottom: 18),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Icon(
                        icon,
                        size: 16,
                        color: Colors.grey[800],
                      ),
                      new Container(width: 5,),
                      new Text(
                        title,
                        style: TextStyle(color: Colors.grey[800], fontSize: 14),
                      ),
                    ],
                  ),
                ),
                new Container(
                  color: Colors.grey[300],
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget cardMenu(String title, IconData icon, String openpage){
    return InkWell(
      onTap: (){
        List<Map<String, dynamic>> params=[];
        params.add({
          "_cabang": widget._cabang,
        });
        RouteHelper(context, openpage, params);
      },
      child: new Container(
        color: Colors.white,
        child: new Row(
          children: <Widget>[
            new Expanded(
              child: new Column(
                children: <Widget>[
                  new Container(
                    padding: EdgeInsets.only(left: 24, right: 24, top: 18, bottom: 18),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Expanded(
                            flex: 1,
                            child: new Row(
                              children: <Widget>[
                                new Icon(
                                  icon,
                                  size: 16,
                                  color: Colors.grey[800],
                                ),
                                new Container(width: 5,),
                                new Text(
                                  title,
                                  style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                ),
                              ],
                            )
                        ),
                        new Icon(
                          FontAwesomeIcons.chevronRight,
                          size: 14,
                          color: Colors.grey[400],
                        )
                      ],
                    ),
                  ),
                  new Container(
                    color: Colors.grey[300],
                    width: MediaQuery.of(context).size.width,
                    height: 1.0,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget cardDt(String title, String dt, IconData icon, bool edit, String _column){
    Widget _edit;
    if(edit){
      _edit = new Icon(
        FontAwesomeIcons.chevronRight,
        size: 14,
        color: Colors.grey.withOpacity(0.6),
      );
    }else{
      _edit = new Icon(
        FontAwesomeIcons.ban,
        size: 10,
        color: Colors.red.withOpacity(0.4),
      );
    }
    return new InkWell(
      onTap: (){
        if(_column == "defaultlang" && edit){
          List<Map<String, dynamic>> params=[];
          params.add({
            "_cabang": widget._cabang,
            "_title": title,
            "_dt": dt,
            "_icon": icon,
            "_column": _column,
          });
          RouteHelper(context, "EditForm", params);
        }
        if(_column == "logout" && edit){
          var db = new akunDB();
          db.deleteAkun();
          signOutGoogle();
          RouteHelper(context, null, null);
        }
      },
      child: new Container(
          color: Colors.white,
          child: new Row(
            children: <Widget>[
              new Expanded(
                  child: new Column(
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.only(left: 24, right: 24, top: 18, bottom: 18),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Row(
                              children: <Widget>[
                                new Icon(
                                  icon,
                                  size: 16,
                                  color: Colors.grey[800],
                                ),
                                new Container(width: 5,),
                                new Text(
                                  title,
                                  style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                ),
                              ],
                            ),
                            new Container(width: 10),
                            new Expanded(
                                flex: 1,
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    new Text(
                                      dt,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      softWrap: false,
                                      style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                    ),
                                    new Container(width: 10,),
                                    _edit
                                  ],
                                )
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        color: Colors.grey[300],
                        width: MediaQuery.of(context).size.width,
                        height: 1.0,
                      ),
                    ],
                  )
              ),
            ],
          )
      ),
    );
  }
}