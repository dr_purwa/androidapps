import 'package:flutter/material.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AkunWebApps extends StatefulWidget {
  var _akun;
  String _cabang;
  Color _color1;
  Color _color2;
  AkunWebApps(List params, akun){
    this._akun = akun;
    params.forEach((dt){
      this._cabang = dt["_cabang"];
      if(_cabang=="group"){
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
      }
      if(_cabang=="denpasar"){
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
      }
      if(_cabang=="ubung"){
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
      }
      if(_cabang=="nusadua"){
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
      }
      if(_cabang=="kmc"){
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
      }
    });
  }
  @override
  _AkunWebApps createState() => new _AkunWebApps();
}

class _AkunWebApps extends State<AkunWebApps>{

  final _Scafoltkey = GlobalKey<ScaffoldState>();

  Future<bool> _onWillPop(){
    //Navigator.pop(context);
    List<Map<String, dynamic>> dtNext=[];
    dtNext.add({"_cabang": widget._cabang});
    RouteHelper(context, "AkunMenu", dtNext);
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(
        child: new Scaffold(
            body:new WillPopScope(
              onWillPop: _onWillPop,
              child: new Scaffold(
                  key: _Scafoltkey,
                  drawer: drawerController(widget._akun , widget._cabang, "AkunWebApps"),
                  appBar: new PreferredSize(
                    preferredSize: Size.fromHeight(200.0),
                    child: AppBarController(widget._cabang, _Scafoltkey, widget._akun, "AkunWebApps"),
                  ),
                  body: new Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      new Container(
                        child: new Column(
                          children: <Widget>[
                            new Expanded(
                              child: new ListView(
                                scrollDirection: Axis.vertical,
                                physics: ScrollPhysics(),
                                children: <Widget>[
                                  _detailakun(widget._akun),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  )
              ),
            )
        )
    );
  }

  Widget _detailakun(akun){
    var dt = akun[0];
    bool Uname = false;
    if(dt["username"].toString().isEmpty){
        Uname = true;
    }

    return new Container(
      padding: EdgeInsets.only(bottom: 30),
      child: Column(
        children: <Widget>[
          cardHeader("Detail Akun", FontAwesomeIcons.thList),
          cardDt("Email", dt["email"], FontAwesomeIcons.at, false, ""),
          cardDt("Username", dt["username"], FontAwesomeIcons.user, Uname, "username"),
          cardDt("Password", dt["passwd"], FontAwesomeIcons.key, true, "passwd"),
        ],
      ),
    );
  }

  Widget cardHeader(String title, IconData icon){
    return new Container(
      color: Colors.grey[100],
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 18, bottom: 18),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Icon(
                        icon,
                        size: 16,
                        color: Colors.grey[800],
                      ),
                      new Container(width: 5,),
                      new Text(
                        title,
                        style: TextStyle(color: Colors.grey[800], fontSize: 14),
                      ),
                    ],
                  ),
                ),
                new Container(
                  color: Colors.grey[300],
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget cardDt(String title, String dt, IconData icon, bool edit, String _column){
    Widget _edit;
    if(edit){
      _edit = new Icon(
        FontAwesomeIcons.pen,
        size: 10,
        color: Colors.grey.withOpacity(0.6),
      );
    }else{
      _edit = new Icon(
        FontAwesomeIcons.ban,
        size: 10,
        color: Colors.red.withOpacity(0.4),
      );
    }

    var txtval = dt;
    if(_column == "passwd" ){
      txtval = "********";
    }

    return new InkWell(
      onTap: (){
        if(_column != "" && edit){
          List<Map<String, dynamic>> params=[];
          params.add({
            "_cabang": widget._cabang,
            "_title": title,
            "_dt": dt,
            "_icon": icon,
            "_column": _column,
            "_page": "AkunWebApps",
          });
          RouteHelper(context, "EditForm", params);
        }
      },
      child: new Container(
          color: Colors.white,
          child: new Row(
            children: <Widget>[
              new Expanded(
                  child: new Column(
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.only(left: 24, right: 24, top: 18, bottom: 18),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Row(
                              children: <Widget>[
                                new Icon(
                                  icon,
                                  size: 16,
                                  color: Colors.grey[800],
                                ),
                                new Container(width: 5,),
                                new Text(
                                  title,
                                  style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                ),
                              ],
                            ),
                            new Container(width: 10),
                            new Expanded(
                                flex: 1,
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    new Text(
                                      txtval,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      softWrap: false,
                                      style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                    ),
                                    new Container(width: 10,),
                                    _edit
                                  ],
                                )
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        color: Colors.grey[300],
                        width: MediaQuery.of(context).size.width,
                        height: 1.0,
                      ),
                    ],
                  )
              ),
            ],
          )
      ),
    );
  }
}