class Voucher_mdl{
  String shv_id;
  String shv_account;
  String shv_kode;
  String shv_end;
  String shv_createid;
  String shv_status;
  String shv_note;
  String shv_toname;
  String shv_toemail;
  String shv_d;
  String shv_m;
  String shv_y;
  String shv_point;

  Voucher_mdl({
    this.shv_id,
    this.shv_account,
    this.shv_kode,
    this.shv_end,
    this.shv_createid,
    this.shv_status,
    this.shv_note,
    this.shv_toname,
    this.shv_toemail,
    this.shv_d,
    this.shv_m,
    this.shv_y,
    this.shv_point,
  });
}

class Voucher1_mdl{
  String shv_id;
  String shv_account;
  String shv_kode;
  String shv_end;
  String shv_createid;
  String shv_status;
  String shv_note;
  String shv_toname;
  String shv_toemail;
  String shv_d;
  String shv_m;
  String shv_y;
  String shv_use_d;
  String shv_use_m;
  String shv_use_y;
  String shv_point;
  String shv_tagihan;

  Voucher1_mdl({
    this.shv_id,
    this.shv_account,
    this.shv_kode,
    this.shv_end,
    this.shv_createid,
    this.shv_status,
    this.shv_note,
    this.shv_toname,
    this.shv_toemail,
    this.shv_d,
    this.shv_m,
    this.shv_y,
    this.shv_use_d,
    this.shv_use_m,
    this.shv_use_y,
    this.shv_point,
    this.shv_tagihan,
  });
}

class Voucher2_mdl{
  String shv_id;
  String shv_account;
  String shv_kode;
  String shv_end;
  String shv_createid;
  String shv_status;
  String shv_note;
  String shv_toname;
  String shv_toemail;
  String shv_d;
  String shv_m;
  String shv_y;
  String shv_point;

  Voucher2_mdl({
    this.shv_id,
    this.shv_account,
    this.shv_kode,
    this.shv_end,
    this.shv_createid,
    this.shv_status,
    this.shv_note,
    this.shv_toname,
    this.shv_toemail,
    this.shv_d,
    this.shv_m,
    this.shv_y,
    this.shv_point,
  });
}

class TR_mdl{
  String tp_id;
  String tp_trid;
  String tp_cardid;
  String tp_mrcid;
  String tp_tipe;
  String tp_d;
  String tp_m;
  String tp_y;
  String tp_point;
  String tp_status;
  String tp_statustext;
  String tp_insertid;
  String tp_inserttime;
  String tp_updateid;
  String tp_updatetime;
  String tp_ambilname;
  String tp_note;
  String mrc_name;
  String mrc_point;
  String mrc_image;

  TR_mdl({
    this.tp_id,
    this.tp_trid,
    this.tp_cardid,
    this.tp_mrcid,
    this.tp_tipe,
    this.tp_d,
    this.tp_m,
    this.tp_y,
    this.tp_point,
    this.tp_status,
    this.tp_statustext,
    this.tp_insertid,
    this.tp_inserttime,
    this.tp_updateid,
    this.tp_updatetime,
    this.tp_ambilname,
    this.tp_note,
    this.mrc_name,
    this.mrc_point,
    this.mrc_image,
  });
}