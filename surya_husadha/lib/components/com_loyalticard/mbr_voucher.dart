import 'dart:async';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surya_husadha/components/com_loyalticard/voucher_model.dart';
import 'dart:convert' as convert;
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:intl/intl.dart' show DateFormat, NumberFormat;

class MbrVoucher extends StatefulWidget {

  var _akun;
  String _cabang;
  Color _color1;
  Color _color2;
  Color _textColor = Colors.white;
  Color _Selected_color;
  String _email;

  MbrVoucher(List params, List akun){
    this._akun = akun;
    if(akun!=null){
      this._email = akun[0]["email"];
    }
    params.forEach((dt){
      this._cabang = dt["_cabang"];
      if(_cabang=="group"){
        this._textColor = Colors.grey[600];
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
        this._Selected_color= WarnaCabang.group2.withOpacity(0.3);
      }
      if(_cabang=="denpasar"){
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
        this._Selected_color= WarnaCabang.shh2.withOpacity(0.3);
      }
      if(_cabang=="ubung"){
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
        this._Selected_color= WarnaCabang.ubung2.withOpacity(0.3);
      }
      if(_cabang=="nusadua"){
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
        this._Selected_color= WarnaCabang.nusadua2.withOpacity(0.3);
      }
      if(_cabang=="kmc"){
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
        this._Selected_color= WarnaCabang.kmc2.withOpacity(0.3);
      }
    });
  }

  @override
  _MbrVoucher createState() => _MbrVoucher();
}

class _MbrVoucher extends State<MbrVoucher> {

  GlobalKey<ScaffoldState> scafolt_key = GlobalKey<ScaffoldState>();

  int _active;
  bool isloading = true;
  bool isloading1 = true;
  bool isloading2 = true;

  List<Voucher_mdl> _mVoucher_mdl = [];
  List<Voucher1_mdl> _mVoucher1_mdl = [];
  List<Voucher2_mdl> _mVoucher2_mdl = [];

  ScrollController listviewCon;
  List showitem = [];
  int showitemCount = 0;

  ScrollController listviewCon1;
  List showitem1 = [];
  int showitemCount1 = 0;

  ScrollController listviewCon2;
  List showitem2 = [];
  int showitemCount2 = 0;

  var _tabswitch = 0;

  String filter;
  String filter1;
  String filter2;
  FocusNode _search  = FocusNode();
  FocusNode _search1  = FocusNode();
  FocusNode _search2  = FocusNode();
  TextEditingController _searchController = new TextEditingController();
  TextEditingController _searchController1 = new TextEditingController();
  TextEditingController _searchController2 = new TextEditingController();

  @override
  initState() {
    super.initState();
    _searchController.addListener(() {
      setState(() {
        filter = _searchController.text;
      });
    });
    _searchController1.addListener(() {
      setState(() {
        filter1 = _searchController1.text;
      });
    });
    _searchController2.addListener(() {
      setState(() {
        filter2 = _searchController2.text;
      });
    });
  }

  @override
  void dispose() {
    _searchController.removeListener((){
      filter = _searchController.text;
    });
    _searchController1.removeListener((){
      filter1 = _searchController1.text;
    });
    _searchController2.removeListener((){
      filter2 = _searchController2.text;
    });
    super.dispose();
  }

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> dtBack=[];
    dtBack.add({
      "_cabang": widget._cabang,
    });
    RouteHelper(context, widget._cabang, null);
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: new Scaffold(
          key: scafolt_key,
          drawer: drawerController(widget._akun, widget._cabang, "MbrVoucher"),
          appBar: new PreferredSize(
            preferredSize: Size.fromHeight(200.0),
            child: AppBarController(widget._cabang, scafolt_key, widget._akun, "MbrVoucher"),
          ),
          backgroundColor: Colors.grey[200],
          body: new WillPopScope(
              onWillPop: _onWillPop,
              child:new Container(
                  child: new ListView(
                      physics: ClampingScrollPhysics(),
                      children: <Widget>[
                        new Container(
                          color: Colors.white,
                          padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                          margin: EdgeInsets.only(top: 16, bottom: 16),
                          child: new Column(
                            children: <Widget>[
                             new Container(
                               margin: EdgeInsets.only(bottom: 10),
                               child: Text("Voucher Discount", style: TextStyle(
                                   fontWeight: FontWeight.w700,
                                   fontSize: 18
                               ),),
                             ),
                              Text("Gunakan voucher discount Anda pada saat berobat!\nUntuk informasi lebih lanjut mengenai voucher discount silahkan hubungi Customer Care kami!")
                            ],
                          ),
                        ),
                        _tabcontent(),
                      ]
                  )
              )
          )
      ),
    );
  }

  Widget _tabcontent(){

    Widget content;
    if(_tabswitch == 0){
      content = new Container(
        child: new Column(
          children: <Widget>[
            new Container(
              color: Colors.white,
              margin: EdgeInsets.only(top: 30, bottom: 5),
              padding: EdgeInsets.only(left: 16, right: 16, top: 25),
              child: TextField(
                textInputAction: TextInputAction.done,
                focusNode: _search,
                controller: _searchController,
                cursorColor: Colors.grey,
                style: TextStyle(color: Colors.grey, fontSize: 14),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(top: 14),
                  hintText: "Cari voucher berlaku",
                  border: InputBorder.none,
                  labelStyle: TextStyle(
                    color: Colors.grey,
                  ),
                  prefixIcon: Icon(
                    FontAwesomeIcons.search,
                    color: Colors.grey,
                  ),
                ),
              ),
            ),
            _voucherlist(),
          ],
        ),
      );
    };

    if(_tabswitch == 1){
      content = new Container(
        child: new Column(
          children: <Widget>[
            new Container(
              color: Colors.white,
              margin: EdgeInsets.only(top: 30, bottom: 5),
              padding: EdgeInsets.only(left: 16, right: 16, top: 25),
              child: TextField(
                textInputAction: TextInputAction.done,
                focusNode: _search1,
                controller: _searchController1,
                cursorColor: Colors.grey,
                style: TextStyle(color: Colors.grey, fontSize: 14),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(top: 14),
                  hintText: "Cari voucher sudah digunakan",
                  border: InputBorder.none,
                  labelStyle: TextStyle(
                    color: Colors.grey,
                  ),
                  prefixIcon: Icon(
                    FontAwesomeIcons.search,
                    color: Colors.grey,
                  ),
                ),
              ),
            ),
            _voucherlist1(),
          ],
        ),
      );
    }

    if(_tabswitch == 2){
      content = new Container(
        child: new Column(
          children: <Widget>[
            new Container(
              color: Colors.white,
              margin: EdgeInsets.only(top: 30, bottom: 5),
              padding: EdgeInsets.only(left: 16, right: 16, top: 25),
              child: TextField(
                textInputAction: TextInputAction.done,
                focusNode: _search2,
                controller: _searchController2,
                cursorColor: Colors.grey,
                style: TextStyle(color: Colors.grey, fontSize: 14),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(top: 14),
                  hintText: "Cari voucher expired",
                  border: InputBorder.none,
                  labelStyle: TextStyle(
                    color: Colors.grey,
                  ),
                  prefixIcon: Icon(
                    FontAwesomeIcons.search,
                    color: Colors.grey,
                  ),
                ),
              ),
            ),
            _voucherlist2(),
          ],
        ),
      );
    }

    return new Stack(
      children: <Widget>[
        content,
        new Container(
            child: new Column(
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(left: 32, right: 32),
                    padding: EdgeInsets.only(top: 16, bottom: 16),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey[300],style: BorderStyle.solid),
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        color: Colors.white
                    ),
                    child: new Container(
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          new InkWell(
                            onTap: (){
                              setState(() {
                                _tabswitch = 0;
                              });
                            },
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(FontAwesomeIcons.ticketAlt, size: 16,color: _tabswitch == 0 ? Colors.redAccent : Colors.grey[600]),
                                Container(
                                  padding: EdgeInsets.only(left: 10, top: 3),
                                  child: Text(
                                    "Berlaku",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        color: _tabswitch == 0 ? Colors.redAccent : Colors.grey[800]
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          new Container(
                              width: 10,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  new Text("|")
                                ],
                              )
                          ),
                          new InkWell(
                            onTap: (){
                              setState(() {
                                _tabswitch = 1;
                              });
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(FontAwesomeIcons.check, size: 16,color: _tabswitch == 1 ? Colors.redAccent : Colors.grey[600]),
                                Container(
                                  padding: EdgeInsets.only(left: 10, top: 3),
                                  child: Text("Digunakan",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        color: _tabswitch == 1 ? Colors.redAccent : Colors.grey[800]
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          new Container(
                              width: 10,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  new Text("|")
                                ],
                              )
                          ),
                          new InkWell(
                            onTap: (){
                              setState(() {
                                _tabswitch = 2;
                              });
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(FontAwesomeIcons.ban, size: 16,color: _tabswitch == 2 ? Colors.redAccent : Colors.grey[600]),
                                Container(
                                  padding: EdgeInsets.only(left: 10, top: 3),
                                  child: Text("Expired",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        color: _tabswitch == 2 ? Colors.redAccent : Colors.grey[800]
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                ),
              ],
            )
        ),
      ],
    );
  }

  //ACTIVE
  Future<String> _getDatavc0() async {
    var _data;
    if(isloading){
      _mVoucher_mdl.clear();
      var api = new Http_Controller();
      List<Map<String, dynamic>> dtNext=[];
      dtNext.add({
          "email": widget._email.toString(),
          "load": "active",
      });
      var http_response = await api.postResponse("https://suryahusadha.com/api/loyalti/mbr_voucher", dtNext);
      _data = convert.jsonEncode(http_response);
    }
    return _data;
  }

  Future<List<Voucher_mdl>> fetchVoucher() async {
    if(isloading) {
      _getDatavc0().then((String response){
        setState(() {
          var decode = convert.jsonDecode(response);
          var decodeConvert = convert.jsonDecode(decode["responseBody"]);
          final dtkonten = decodeConvert["mbr_voucher"];
          final dtmbr_voucher = dtkonten[0]["data"];
          if(dtmbr_voucher.length > 0) {
            isloading = false;
            dtmbr_voucher.forEach((dt) => {
              _mVoucher_mdl.add(new Voucher_mdl(
                shv_id: dt["shv_id"],
                shv_account: dt["shv_account"],
                shv_kode: dt["shv_kode"],
                shv_end: dt["shv_end"],
                shv_createid: dt["shv_createid"],
                shv_status: dt["shv_status"],
                shv_note: dt["shv_note"],
                shv_toname: dt["shv_toname"],
                shv_toemail: dt["shv_toemail"],
                shv_d: dt["shv_d"],
                shv_m: dt["shv_m"],
                shv_y: dt["shv_y"],
                shv_point: dt["shv_point"],
              )),
            });
          }else{
            isloading = false;
          }
        });
      });
    }
    return _mVoucher_mdl;
  }

  Widget _voucherlist(){
    return new FutureBuilder(
        future: fetchVoucher(),
        builder: (context, snapshot){
          if (snapshot.hasData && _mVoucher_mdl.length > 0 && _mVoucher_mdl.isNotEmpty){
            isloading = false;
            return new ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                controller: listviewCon,
                itemCount: snapshot.data.length,
                itemBuilder:(BuildContext context, int index) {
                  if(filter == null || filter == ""){
                    showitemCount = _mVoucher_mdl.length;
                    return _voucher(index);
                  }else{
                    if(_mVoucher_mdl.elementAt(index).shv_toname.toLowerCase().contains(filter.toLowerCase()) ||
                        _mVoucher_mdl.elementAt(index).shv_toemail.toLowerCase().contains(filter.toLowerCase()) ||
                        _mVoucher_mdl.elementAt(index).shv_kode.toLowerCase().contains(filter.toLowerCase())
                    ){
                      showitem.add(index);
                      showitemCount = showitem.length;
                      return _voucher(index);
                    }else{
                      showitemCount = 0;
                      return new Container();
                    }
                  }
                }
            );
          }else{
            return new Container();
          }
        }
    );
  }

  //USED
  Future<String> _getDatavc1() async {
    var _data;
    if(isloading1){
      _mVoucher1_mdl.clear();
      var api = new Http_Controller();
      List<Map<String, dynamic>> dtNext=[];
      dtNext.add({
        "email": widget._email.toString(),
        "load": "used",
      });
      var http_response = await api.postResponse("https://suryahusadha.com/api/loyalti/mbr_voucher", dtNext);
      _data = convert.jsonEncode(http_response);
      print("used: "+ _data.toString());
    }
    return _data;
  }

  Future<List<Voucher1_mdl>> fetchVoucher1() async {
    if(isloading1) {
      _getDatavc1().then((String response){
        setState(() {
          var decode = convert.jsonDecode(response);
          var decodeConvert = convert.jsonDecode(decode["responseBody"]);
          final dtkonten = decodeConvert["mbr_voucher"];
          final dtmbr_voucher = dtkonten[0]["data"];
          if(dtmbr_voucher.length > 0) {
            isloading1 = false;
            dtmbr_voucher.forEach((dt) => {
              _mVoucher1_mdl.add(new Voucher1_mdl(
                shv_id: dt["shv_id"],
                shv_account: dt["shv_account"],
                shv_kode: dt["shv_kode"],
                shv_end: dt["shv_end"],
                shv_createid: dt["shv_createid"],
                shv_status: dt["shv_status"],
                shv_note: dt["shv_note"],
                shv_toname: dt["shv_toname"],
                shv_toemail: dt["shv_toemail"],
                shv_d: dt["shv_d"],
                shv_m: dt["shv_m"],
                shv_y: dt["shv_y"],
                shv_use_d: dt["shv_use_d"],
                shv_use_m: dt["shv_use_m"],
                shv_use_y: dt["shv_use_y"],
                shv_point: dt["shv_point"],
                shv_tagihan: dt["shv_tagihan"],
              )),
            });
          }else{
            isloading1 = false;
          }
        });
      });
    }
    return _mVoucher1_mdl;
  }

  Widget _voucherlist1(){
    return new FutureBuilder(
        future: fetchVoucher1(),
        builder: (context, snapshot){
          if (snapshot.hasData && _mVoucher1_mdl.length > 0 && _mVoucher1_mdl.isNotEmpty){
            isloading1 = false;
            return new ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                controller: listviewCon1,
                itemCount: snapshot.data.length,
                itemBuilder:(BuildContext context, int index) {
                  if(filter1 == null || filter1 == ""){
                    showitemCount1 = _mVoucher1_mdl.length;
                    return _voucher1(index);
                  }else{
                    if(_mVoucher1_mdl.elementAt(index).shv_toname.toLowerCase().contains(filter1.toLowerCase()) ||
                        _mVoucher1_mdl.elementAt(index).shv_toemail.toLowerCase().contains(filter1.toLowerCase()) ||
                        _mVoucher1_mdl.elementAt(index).shv_kode.toLowerCase().contains(filter1.toLowerCase())
                    ){
                      showitem1.add(index);
                      showitemCount1 = showitem1.length;
                      return _voucher1(index);
                    }else{
                      showitemCount1 = 0;
                      return new Container();
                    }
                  }
                }
            );
          }else{
            return new Container();
          }
        }
    );
  }

  //EXPIRED
  Future<String> _getDatavc2() async {
    var _data;
    if(isloading2){
      _mVoucher2_mdl.clear();
      var api = new Http_Controller();
      List<Map<String, dynamic>> dtNext=[];
      dtNext.add({
        "email": widget._email.toString(),
        "load": "inactive",
      });
      var http_response = await api.postResponse("https://suryahusadha.com/api/loyalti/mbr_voucher", dtNext);
      _data = convert.jsonEncode(http_response);
    }
    return _data;
  }

  Future<List<Voucher2_mdl>> fetchVoucher2() async {
    if(isloading2) {
      _getDatavc2().then((String response){
        setState(() {
          var decode = convert.jsonDecode(response);
          var decodeConvert = convert.jsonDecode(decode["responseBody"]);
          final dtkonten = decodeConvert["mbr_voucher"];
          final dtmbr_voucher = dtkonten[0]["data"];
          if(dtmbr_voucher.length > 0) {
            isloading2 = false;
            dtmbr_voucher.forEach((dt) => {
              _mVoucher2_mdl.add(new Voucher2_mdl(
                shv_id: dt["shv_id"],
                shv_account: dt["shv_account"],
                shv_kode: dt["shv_kode"],
                shv_end: dt["shv_end"],
                shv_createid: dt["shv_createid"],
                shv_status: dt["shv_status"],
                shv_note: dt["shv_note"],
                shv_toname: dt["shv_toname"],
                shv_toemail: dt["shv_toemail"],
                shv_d: dt["shv_d"],
                shv_m: dt["shv_m"],
                shv_y: dt["shv_y"],
                shv_point: dt["shv_point"],
              )),
            });
          }else{
            isloading2 = false;
          }
        });
      });
    }
    return _mVoucher2_mdl;
  }

  Widget _voucherlist2(){
    return new FutureBuilder(
        future: fetchVoucher2(),
        builder: (context, snapshot){
          if (snapshot.hasData && _mVoucher2_mdl.length > 0 && _mVoucher2_mdl.isNotEmpty){
            isloading2 = false;
            return new ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                controller: listviewCon2,
                itemCount: snapshot.data.length,
                itemBuilder:(BuildContext context, int index) {
                  if(filter2 == null || filter2 == ""){
                    showitemCount2 = _mVoucher2_mdl.length;
                    return _voucher2(index);
                  }else{
                    if(_mVoucher2_mdl.elementAt(index).shv_toname.toLowerCase().contains(filter2.toLowerCase()) ||
                        _mVoucher2_mdl.elementAt(index).shv_toemail.toLowerCase().contains(filter2.toLowerCase()) ||
                        _mVoucher2_mdl.elementAt(index).shv_kode.toLowerCase().contains(filter2.toLowerCase())
                    ){
                      showitem2.add(index);
                      showitemCount2 = showitem2.length;
                      return _voucher2(index);
                    }else{
                      showitemCount2 = 0;
                      return new Container();
                    }
                  }
                }
            );
          }else{
            return new Container();
          }
        }
    );
  }

  Widget _voucher(int index){

    var mformat = new DateFormat('MMM');
    DateTime date = new DateTime(int.parse(_mVoucher_mdl.elementAt(index).shv_y),int.parse(_mVoucher_mdl.elementAt(index).shv_m),int.parse(_mVoucher_mdl.elementAt(index).shv_d));

    Color ActivColor;
    if(_active == index){
      ActivColor = widget._Selected_color;
    }else{
      ActivColor = Colors.white;
    }
    return new Container(
      margin: EdgeInsets.only(bottom: 5),
      color: ActivColor,
      child: new InkWell(
          onTap: (){
            showDialog(
              barrierDismissible: true,
              context: context,
              builder: (BuildContext context){
                return voucherpop(_mVoucher_mdl.elementAt(index).shv_kode, _mVoucher_mdl.elementAt(index).shv_y, _mVoucher_mdl.elementAt(index).shv_m);
              },
            );
          },
          child: new Column(
            children: <Widget>[
              new Container(
                padding: EdgeInsets.only(top:5, bottom: 5, left: 16, right: 16),
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Container(
                      width: 40,
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text(
                            _mVoucher_mdl.elementAt(index).shv_d,
                            style: TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.w700
                            ),
                          ),
                          new Text(
                            mformat.format(date),
                            style: TextStyle(
                                fontSize: 10
                            ),
                          ),
                          new Text(
                            _mVoucher_mdl.elementAt(index).shv_y,
                            style: TextStyle(
                                fontSize: 10
                            ),
                          )
                        ],
                      ),
                    ),
                    new Expanded(
                      flex: 2,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(left:10.0, right:10.0),
                              child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Nama: "+_mVoucher_mdl.elementAt(index).shv_toname,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child:new Text(
                                        "Kode Voucher: "+_mVoucher_mdl.elementAt(index).shv_kode,
                                        style: TextStyle(fontWeight: FontWeight.w700,fontFamily: "NeoSansBold", fontSize: 14.0, color: Colors.grey[800]),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Email: "+_mVoucher_mdl.elementAt(index).shv_toemail,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Expired Date: "+_mVoucher_mdl.elementAt(index).shv_end,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                  ]
                              ),
                            ),
                          ]
                      ),
                    ),
                    new Container(
                      height: 40.0,
                      width: 40.0,
                      alignment: Alignment.centerRight,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.all(6.0),
                              alignment: Alignment.centerRight,
                              child: new Icon(
                                FontAwesomeIcons.angleRight,
                                color: Colors.grey,
                                size: 18.0,
                              ),
                            ),
                          ]
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                margin: EdgeInsets.only(top: 5.0),
                width: double.infinity,
                height: 1.0,
                color: Colors.grey[200],
              ),
            ],
          )
      ),
    );
  }

  Widget _voucher1(int index){

    var mformat = new DateFormat('MMM');
    DateTime date = new DateTime(int.parse(_mVoucher1_mdl.elementAt(index).shv_use_y),int.parse(_mVoucher1_mdl.elementAt(index).shv_use_m),int.parse(_mVoucher1_mdl.elementAt(index).shv_use_d));

    final idrFormat = new NumberFormat("#,###");

    return new Container(
      margin: EdgeInsets.only(bottom: 5),
      color: Colors.white,
      child: new InkWell(
          child: new Column(
            children: <Widget>[
              new Container(
                padding: EdgeInsets.only(top:5, bottom: 5, left: 16, right: 16),
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Container(
                      width: 40,
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text(
                            _mVoucher1_mdl.elementAt(index).shv_use_d,
                            style: TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.w700
                            ),
                          ),
                          new Text(
                            mformat.format(date),
                            style: TextStyle(
                                fontSize: 10
                            ),
                          ),
                          new Text(
                            _mVoucher1_mdl.elementAt(index).shv_use_y,
                            style: TextStyle(
                                fontSize: 10
                            ),
                          )
                        ],
                      ),
                    ),
                    new Expanded(
                      flex: 2,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(left:10.0, right:10.0),
                              child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Nama: "+_mVoucher1_mdl.elementAt(index).shv_toname,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child:new Text(
                                        "Kode Voucher: "+_mVoucher1_mdl.elementAt(index).shv_kode,
                                        style: TextStyle(fontWeight: FontWeight.w700,fontFamily: "NeoSansBold", fontSize: 14.0, color: Colors.grey[800]),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Email: "+_mVoucher1_mdl.elementAt(index).shv_toemail,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Tagihan: IDR "+idrFormat.format(int.parse(_mVoucher1_mdl.elementAt(index).shv_tagihan))+",-",
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                  ]
                              ),
                            ),
                          ]
                      ),
                    ),
                    new Container(
                      height: 40.0,
                      width: 40.0,
                      alignment: Alignment.centerRight,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.all(6.0),
                              alignment: Alignment.centerRight,
                              child: new Icon(
                                FontAwesomeIcons.ban,
                                color: Colors.grey,
                                size: 18.0,
                              ),
                            ),
                          ]
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                margin: EdgeInsets.only(top: 5.0),
                width: double.infinity,
                height: 1.0,
                color: Colors.grey[200],
              ),
            ],
          )
      ),
    );
  }

  Widget _voucher2(int index){

    var mformat = new DateFormat('MMM');
    DateTime date = new DateTime(int.parse(_mVoucher2_mdl.elementAt(index).shv_y),int.parse(_mVoucher2_mdl.elementAt(index).shv_m),int.parse(_mVoucher2_mdl.elementAt(index).shv_d));

    Color ActivColor;
    if(_active == index){
      ActivColor = widget._Selected_color;
    }else{
      ActivColor = Colors.white;
    }
    return new Container(
      margin: EdgeInsets.only(bottom: 5),
      color: ActivColor,
      child: new InkWell(
          child: new Column(
            children: <Widget>[
              new Container(
                padding: EdgeInsets.only(top:5, bottom: 5, left: 16, right: 16),
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Container(
                      width: 40,
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text(
                            _mVoucher2_mdl.elementAt(index).shv_d,
                            style: TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.w700
                            ),
                          ),
                          new Text(
                            mformat.format(date),
                            style: TextStyle(
                                fontSize: 10
                            ),
                          ),
                          new Text(
                            _mVoucher2_mdl.elementAt(index).shv_y,
                            style: TextStyle(
                                fontSize: 10
                            ),
                          )
                        ],
                      ),
                    ),
                    new Expanded(
                      flex: 2,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(left:10.0, right:10.0),
                              child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Nama: "+_mVoucher2_mdl.elementAt(index).shv_toname,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child:new Text(
                                        "Kode Voucher: "+_mVoucher2_mdl.elementAt(index).shv_kode,
                                        style: TextStyle(fontWeight: FontWeight.w700,fontFamily: "NeoSansBold", fontSize: 14.0, color: Colors.grey[800]),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Email: "+_mVoucher2_mdl.elementAt(index).shv_toemail,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Expired Date: "+_mVoucher2_mdl.elementAt(index).shv_end,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                  ]
                              ),
                            ),
                          ]
                      ),
                    ),
                    new Container(
                      height: 40.0,
                      width: 40.0,
                      alignment: Alignment.centerRight,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.all(6.0),
                              alignment: Alignment.centerRight,
                              child: new Icon(
                                FontAwesomeIcons.ban,
                                color: Colors.grey,
                                size: 18.0,
                              ),
                            ),
                          ]
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                margin: EdgeInsets.only(top: 5.0),
                width: double.infinity,
                height: 1.0,
                color: Colors.grey[200],
              ),
            ],
          )
      ),
    );
  }

  Widget voucherpop(vc_code, year, month){
    return new SafeArea(
      child: new Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Center(
          child: RotatedBox(
            quarterTurns: 5,
            child: Stack(
              children: <Widget>[
                new Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage("https://suryahusadha.com/images/voucher/"+year+"/"+month+"/"+vc_code+".png?dummy=${new DateTime.now()}"),
                      )
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

}