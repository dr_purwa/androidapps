import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surya_husadha/components/com_loyalticard/voucher_model.dart';
import 'package:surya_husadha/components/com_profile/tambahkeluarga.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/helper/transition/slide_route.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:surya_husadha/pages/widgetpage.dart';
import 'dart:convert' as convert;

class TukarPoint extends StatefulWidget {

  var _akun;
  String _cabang;
  String _page;
  Color _color1;
  Color _color2;
  Color _textColor = Colors.white;
  Color _Selected_color;
  String _userid;
  String _rm;
  String _email;
  String _username;
  var _kidsParam;
  int _point = 0;

  TukarPoint(List params, akun){
    this._akun = akun;
    if(akun!=null){
      this._userid = akun[0]["id"].toString();
      this._email = akun[0]["email"];
      this._username = akun[0]["username"];
    }
    params.forEach((dt){
      this._cabang = dt["_cabang"];
      this._page = dt["_page"];
      if(dt["_page"] == "kidscard"){
        _kidsParam = dt["_kidsParam"];
        _point = _kidsParam["point"];
      }
      if(_cabang=="group"){
        this._textColor = Colors.grey[600];
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
        this._Selected_color= WarnaCabang.group2.withOpacity(0.3);
      }
      if(_cabang=="denpasar"){
        if(akun!=null){
          this._rm = akun[0]["nrmshh"];
        }
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
        this._Selected_color= WarnaCabang.shh2.withOpacity(0.3);
      }
      if(_cabang=="ubung"){
        if(akun!=null){
          this._rm = akun[0]["nrmub"];
        }
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
        this._Selected_color= WarnaCabang.ubung2.withOpacity(0.3);
      }
      if(_cabang=="nusadua"){
        if(akun!=null){
          this._rm = akun[0]["nrmnd"];
        }
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
        this._Selected_color= WarnaCabang.nusadua2.withOpacity(0.3);
      }
      if(_cabang=="kmc"){
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
        this._Selected_color= WarnaCabang.kmc2.withOpacity(0.3);
      }
    });
  }

  @override
  _TukarPoint createState() => new _TukarPoint();

}

class _TukarPoint extends State<TukarPoint> with SingleTickerProviderStateMixin, WidgetsBindingObserver{

  var getmerchant;
  bool isloading2 = true;
  var dt_tr;
  var dtkids;
  var getmember;
  bool isloadtr = true;
  bool isloadingMbr = true;
  var maintenance = 0;
  var _tabswitch = 0;
  String msg_maintnance;
  GlobalKey<ScaffoldState> scafolt_key = GlobalKey<ScaffoldState>();

  String filter;
  FocusNode _search  = FocusNode();
  TextEditingController _searchController = new TextEditingController();

  ScrollController listviewCon;
  List<TR_mdl> _mTR_mdl = [];
  List showitem = [];
  int showitemCount = 0;

  Future<String> _get_tr() async {
    _mTR_mdl.clear();
    var _datatr;
    var db = new Http_Controller();
    var http_response = await db.getResponse("https://suryahusadha.com/api/loyalti/get_transaksi/" + widget._email);
    _datatr = convert.jsonEncode(http_response);
    return _datatr;
  }

  Future<String> _get_trkids() async {
    _mTR_mdl.clear();
    var _datatr;
    var db = new Http_Controller();
    var http_response = await db.getResponse("https://suryahusadha.com/api/loyalti/get_transaksikids/" + widget._kidsParam["kc_id"]);
    _datatr = convert.jsonEncode(http_response);
    return _datatr;
  }

  Future<String> _getKlg() async {
    var _dataklg;
    var db = new Http_Controller();
    var http_response = await db.getResponse("https://suryahusadha.com/api/loyalti/getkidsprevilage/" + widget._userid);
    _dataklg = convert.jsonEncode(http_response);
    return _dataklg;
  }

  @override
  initState() {
    super.initState();
    _tabswitch = 0;
    _searchController.addListener(() {
      setState(() {
        filter = _searchController.text;
      });
    });
    _getKlg().then((String response){
      var decode = convert.jsonDecode(response);
      var decodeConvert = convert.jsonDecode(decode["responseBody"]);
      final dtkonten = decodeConvert["getklg"];
      dtkids = dtkonten[0]["data"];
    });
    if(widget._page == "kidscard"){
      _get_trkids().then((String response){
        var decode = convert.jsonDecode(response);
        var decodeConvert = convert.jsonDecode(decode["responseBody"]);
        final dtkonten = decodeConvert["get_transaksi"];
        dt_tr = dtkonten[0]["data"];
        dt_tr.forEach((dt) => {
          _mTR_mdl.add(new TR_mdl(
            tp_id: dt["tp_id"],
            tp_trid: dt["tp_trid"],
            tp_cardid: dt["tp_cardid"],
            tp_mrcid: dt["tp_mrcid"],
            tp_tipe: dt["tp_tipe"],
            tp_d: dt["tp_d"],
            tp_m: dt["tp_m"],
            tp_y: dt["tp_y"],
            tp_point: dt["tp_point"],
            tp_status: dt["tp_status"],
            tp_statustext: dt["tp_statustext"],
            tp_insertid: dt["tp_insertid"],
            tp_inserttime: dt["tp_inserttime"],
            tp_updateid: dt["tp_updateid"],
            tp_updatetime: dt["tp_updatetime"],
            tp_ambilname: dt["tp_ambilname"],
            tp_note: dt["tp_note"],
            mrc_name: dt["mrc_name"],
            mrc_point: dt["mrc_point"],
            mrc_image: dt["mrc_image"],
          )),
        });
      });
    }else{
      _get_tr().then((String response){
        var decode = convert.jsonDecode(response);
        var decodeConvert = convert.jsonDecode(decode["responseBody"]);
        final dtkonten = decodeConvert["get_transaksi"];
        dt_tr = dtkonten[0]["data"];
        dt_tr.forEach((dt) => {
          _mTR_mdl.add(new TR_mdl(
            tp_id: dt["tp_id"],
            tp_trid: dt["tp_trid"],
            tp_cardid: dt["tp_cardid"],
            tp_mrcid: dt["tp_mrcid"],
            tp_tipe: dt["tp_tipe"],
            tp_d: dt["tp_d"],
            tp_m: dt["tp_m"],
            tp_y: dt["tp_y"],
            tp_point: dt["tp_point"],
            tp_status: dt["tp_status"],
            tp_statustext: dt["tp_statustext"],
            tp_insertid: dt["tp_insertid"],
            tp_inserttime: dt["tp_inserttime"],
            tp_updateid: dt["tp_updateid"],
            tp_updatetime: dt["tp_updatetime"],
            tp_ambilname: dt["tp_ambilname"],
            tp_note: dt["tp_note"],
            mrc_name: dt["mrc_name"],
            mrc_point: dt["mrc_point"],
            mrc_image: dt["mrc_image"],
          )),
        });
      });
    }

  }

  @override
  void dispose() {
    _searchController.removeListener((){
      filter = _searchController.text;
    });
    super.dispose();
  }

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> dtBack=[];
    dtBack.add({
      "_cabang": widget._cabang,
    });
    RouteHelper(context, widget._cabang, dtBack);
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: new Scaffold(
          key: scafolt_key,
          drawer: drawerController(widget._akun, widget._cabang, "TukarPoint"),
          appBar: new PreferredSize(
            preferredSize: Size.fromHeight(200.0),
            child: AppBarController(widget._cabang, scafolt_key, widget._akun, "TukarPoint"),
          ),
          backgroundColor: Colors.grey[200],
          body: new WillPopScope(
              onWillPop: _onWillPop,
              child: new Container(
                child: ListView(
                  children: <Widget>[
                    widget._page == "kidscard" ? _kidsStatus(widget._kidsParam) : _LoyaltiStatus(),
                    _tabcontent(),
                  ],
                ),
              )
          )
      ),
    );
  }

  Widget _tabcontent(){

    Widget content;
    if(_tabswitch == 0){
      content = new Column(
        children: <Widget>[
          _mercanthead(),
          _merchantcard(),
        ],
      );
    }else{
      content = new Container(
        child: new Column(
          children: <Widget>[
            new Container(
              color: Colors.white,
              margin: EdgeInsets.only(top: 30, bottom: 5),
              padding: EdgeInsets.only(left: 16, right: 16, top: 25),
              child: TextField(
                textInputAction: TextInputAction.done,
                focusNode: _search,
                controller: _searchController,
                cursorColor: Colors.grey,
                style: TextStyle(color: Colors.grey, fontSize: 14),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(top: 14),
                  hintText: "Cari transaksi tukar Anda",
                  border: InputBorder.none,
                  labelStyle: TextStyle(
                    color: Colors.grey,
                  ),
                  prefixIcon: Icon(
                    FontAwesomeIcons.search,
                    color: Colors.grey,
                  ),
                ),
              ),
            ),
            _transaksicard(),
          ],
        ),
      );
    };

    return new Stack(
      children: <Widget>[
        content,
        new Container(
            child: new Column(
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(left: 32, right: 32),
                    padding: EdgeInsets.only(top: 16, bottom: 16),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey[300],style: BorderStyle.solid),
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        color: Colors.white
                    ),
                    child: new Container(
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          new InkWell(
                            onTap: (){
                              setState(() {
                                _tabswitch = 0;
                              });
                            },
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(FontAwesomeIcons.shoppingBag, size: 16,color: _tabswitch == 0 ? Colors.redAccent : Colors.grey[600]),
                                Container(
                                  padding: EdgeInsets.only(left: 10, top: 3),
                                  child: Text(
                                    "Merchant",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        color: _tabswitch == 0 ? Colors.redAccent : Colors.grey[800]
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          new Container(
                              width: 10,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  new Text("|")
                                ],
                              )
                          ),
                          new InkWell(
                            onTap: (){
                              setState(() {
                                _tabswitch = 1;
                              });
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(FontAwesomeIcons.exchangeAlt, size: 16,color: _tabswitch == 1 ? Colors.redAccent : Colors.grey[600]),
                                Container(
                                  padding: EdgeInsets.only(left: 10, top: 3),
                                  child: Text("Transaksi Tukar",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        color: _tabswitch == 1 ? Colors.redAccent : Colors.grey[800]
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                ),
              ],
            )
        ),
      ],
    );
  }

    Widget _tritem(int index){

      return new Container(
        margin: EdgeInsets.only(bottom: 5),
        color: Colors.white,
        child: new InkWell(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(top:5, bottom: 5, left: 16, right: 16),
                  child:new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      new Container(
                        height: 100.0,
                        width: 100.0,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey[300])
                        ),
                        child: Image.network(SERVER.domain + _mTR_mdl.elementAt(index).mrc_image +'?dummy=${new DateTime.now()}')
                      ),
                      new Expanded(
                        flex: 2,
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                alignment: Alignment.topLeft,
                                padding: EdgeInsets.only(left:10.0, right:10.0),
                                child: new Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      new Container(
                                        alignment: Alignment.topLeft,
                                        child: new Text(
                                          "ID: "+_mTR_mdl.elementAt(index).tp_trid,
                                          style: TextStyle(color: Colors.grey[600], fontSize: 14.0),
                                          textAlign: TextAlign.left,
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 1,
                                          softWrap: false,
                                        ),
                                      ),
                                      new Container(
                                        alignment: Alignment.topLeft,
                                        child: new Text(
                                          _mTR_mdl.elementAt(index).mrc_name,
                                          style: TextStyle(color: Colors.grey[800], fontSize: 16.0, fontWeight: FontWeight.w700),
                                          textAlign: TextAlign.left,
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 1,
                                          softWrap: false,
                                        ),
                                      ),
                                      new Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: <Widget>[
                                          new Container(
                                            margin: EdgeInsets.only(top: 10),
                                            padding: EdgeInsets.only(left: 10, top: 5, bottom: 5, right: 10),
                                            decoration: BoxDecoration(
                                                color: Colors.red,
                                                shape: BoxShape.rectangle,
                                                borderRadius: BorderRadius.circular(10)
                                            ),
                                            child: Text(
                                              _mTR_mdl.elementAt(index).mrc_point+" Pts",
                                              style: TextStyle(color: Colors.white, fontSize: 12),
                                            ),
                                          ),
                                          new Container(
                                            margin: EdgeInsets.only(top: 10, left: 5),
                                            padding: EdgeInsets.only(left: 10, top: 5, bottom: 5, right: 10),
                                            decoration: BoxDecoration(
                                                color: _mTR_mdl.elementAt(index).tp_status == "1" ? Colors.green : Colors.red,
                                                shape: BoxShape.rectangle,
                                                borderRadius: BorderRadius.circular(10)
                                            ),
                                            child: new Row(
                                              children: <Widget>[
                                                new Container(
                                                  child: _mTR_mdl.elementAt(index).tp_status == "1" ? new Container(
                                                      child: new Row(
                                                        children: <Widget>[
                                                          new Icon(
                                                            FontAwesomeIcons.check, size: 12, color: Colors.white,
                                                          ),
                                                          new Container(
                                                            padding: EdgeInsets.only(left: 5),
                                                            child: new Text(
                                                              _mTR_mdl.elementAt(index).tp_statustext,
                                                              style: TextStyle(color: Colors.white, fontSize: 12),
                                                              textAlign: TextAlign.left,
                                                              overflow: TextOverflow.ellipsis,
                                                              maxLines: 1,
                                                              softWrap: false,
                                                            ),
                                                          )
                                                        ],
                                                      )
                                                  ) : new Container(
                                                      child: new Row(
                                                        children: <Widget>[
                                                          new Icon(
                                                            FontAwesomeIcons.ban, size: 12, color: Colors.white,
                                                          ),
                                                          new Container(
                                                            padding: EdgeInsets.only(left: 5),
                                                            child: new Text(
                                                              _mTR_mdl.elementAt(index).tp_statustext,
                                                              style: TextStyle(color: Colors.white, fontSize: 12),
                                                              textAlign: TextAlign.left,
                                                              overflow: TextOverflow.ellipsis,
                                                              maxLines: 1,
                                                              softWrap: false,
                                                            ),
                                                          )
                                                        ],
                                                      )
                                                  )
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                                    ]
                                ),
                              ),
                            ]
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  margin: EdgeInsets.only(top: 5.0),
                  width: double.infinity,
                  height: 1.0,
                  color: Colors.grey[200],
                ),
              ],
            )
        ),
      );
    }

  _transaksicard(){
    return new ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        controller: listviewCon,
        itemCount: _mTR_mdl.length,
        itemBuilder:(BuildContext context, int index) {
          if(filter == null || filter == ""){
            showitemCount = _mTR_mdl.length;
            return _tritem(index);
          }else{
            if(_mTR_mdl.elementAt(index).tp_trid.toLowerCase().contains(filter.toLowerCase()) ||
                _mTR_mdl.elementAt(index).tp_cardid.toLowerCase().contains(filter.toLowerCase()) ||
                _mTR_mdl.elementAt(index).tp_ambilname.toLowerCase().contains(filter.toLowerCase())
            ){
              showitem.add(index);
              showitemCount = showitem.length;
              return _tritem(index);
            }else{
              showitemCount = 0;
              return new Container();
            }
          }
        }
    );
  }

  Widget _kidsStatus(dt){

     if(dt != ""){
        String pointKids;
        if(dt["point"].toString() != "" || dt["point"].toString() != null){
          pointKids = dt["point"].toString();
        }else{
          pointKids = "0";
        }

        return new Container(
          color: Colors.white,
          margin: EdgeInsets.only(bottom:16.0, top:16.0),
          padding: EdgeInsets.only(top:16.0, bottom:16.0, left: 16.0, right: 16.0),
          child: new Column(
            children: <Widget>[
              new Container(
                padding: EdgeInsets.only(top:5, bottom: 5),
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                          height: 55.0,
                          width: 55.0,
                          padding: EdgeInsets.all(6.0),
                          decoration: new BoxDecoration(
                              borderRadius:
                              new BorderRadius.all(new Radius.circular(100.0)),
                              color: Colors.lightBlue
                          ),
                          alignment: Alignment.center,
                          child: new Icon(
                            FontAwesomeIcons.child,
                            color: Colors.white,
                            size: 24.0,
                          ),
                        )
                      ],
                    ),
                    new Expanded(
                      flex: 2,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.only(left:10.0, right:10.0),
                              alignment: Alignment.topLeft,
                              child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      margin: EdgeInsets.only(bottom: 5),
                                      child:new Text(
                                        dt["kc_name"],
                                        style: TextStyle(fontWeight: FontWeight.w700, fontFamily: "NeoSansBold", fontSize: 14.0, color: Colors.grey[800]),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Row(
                                      children: <Widget>[
                                        new Container(
                                          margin: EdgeInsets.only(right: 5),
                                          padding: EdgeInsets.only(top: 3, bottom: 5, right: 10, left: 10),
                                          decoration: new BoxDecoration(
                                              borderRadius:
                                              new BorderRadius.all(new Radius.circular(5.0)),
                                              color: Color(0x50FFD180)
                                          ),
                                          child: new Row(
                                            children: <Widget>[
                                              new Container(
                                                alignment: Alignment.center,
                                                child: new Icon(
                                                  FontAwesomeIcons.creditCard,
                                                  size: 14.0,
                                                ),
                                              ),
                                              new Container(
                                                alignment: Alignment.center,
                                                padding: EdgeInsets.only(top:2.0, left: 5),
                                                child: new Text(
                                                  pointKids +" Poin",
                                                  style: TextStyle(fontSize: 12.0),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        new Container(
                                          padding: EdgeInsets.only(top: 3, bottom: 5, right: 10, left: 10),
                                          decoration: new BoxDecoration(
                                              borderRadius:
                                              new BorderRadius.all(new Radius.circular(5.0)),
                                              color: Color(0x50FFD180)
                                          ),
                                          child: new Row(
                                            children: <Widget>[
                                              new Container(
                                                alignment: Alignment.center,
                                                child: new Icon(
                                                  FontAwesomeIcons.idBadge,
                                                  size: 14.0,
                                                ),
                                              ),
                                              new Container(
                                                padding: EdgeInsets.only(top: 2, left: 1),
                                                alignment: Alignment.center,
                                                child: new Text(
                                                  dt["kc_number"],
                                                  style: TextStyle(color: Colors.grey[800], fontSize: 12.0),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.ellipsis,
                                                  maxLines: 1,
                                                  softWrap: false,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    )
                                  ]
                              ),
                            ),
                          ]
                      ),
                    ),
                    new InkWell(
                      onTap: (){
                        List<Map<String, dynamic>> params=[];
                        params.add({
                          "_cabang": widget._cabang,
                          "_page": "shcard",
                        });
                        RouteHelper(context, "TukarPoint", params);
                      },
                      child: new Container(
                        height: 40.0,
                        width: 40.0,
                        alignment: Alignment.centerRight,
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Container(
                                alignment: Alignment.centerRight,
                                child: new Icon(
                                  FontAwesomeIcons.times,
                                  color: Colors.grey,
                                  size: 24.0,
                                ),
                              ),
                            ]
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          )
        );
      }else{
       return new Container();
     }
  }

  Widget imgUser;
  String namaUser = "";
  Widget _LoyaltiStatus(){
    if(int.parse(DateTime.now().hour.toString()) > 00){
      namaUser = "Selamat pagi!";
    }
    if(int.parse(DateTime.now().hour.toString()) > 11){
      namaUser = "Selamat siang!";
    }
    if(int.parse(DateTime.now().hour.toString()) > 14){
      namaUser = "Selamat sore!";
    }
    if(int.parse(DateTime.now().hour.toString()) > 18){
      namaUser = "Selamat malam!";
    }
    if( widget._akun != null) {
      namaUser = "Hi, "+widget._akun[0]["name"];
      String img = SERVER.domain + widget._akun[0]["pp"] +'?dummy=${new DateTime.now()}';
      imgUser = new Container(
        height: 55.0,
        width: 55.0,
        padding: EdgeInsets.all(2.0),
        decoration: new BoxDecoration(
            borderRadius:
            new BorderRadius.all(new Radius.circular(100.0)),
            color: Colors.lightBlue
        ),
        alignment: Alignment.center,
        child: CircleAvatar(
          backgroundImage: NetworkImage(img),
          minRadius: 90,
          maxRadius: 150,
        ),
      );
    } else {
      imgUser = new Container(
        height: 55.0,
        width: 55.0,
        padding: EdgeInsets.all(6.0),
        decoration: new BoxDecoration(
            borderRadius:
            new BorderRadius.all(new Radius.circular(100.0)),
            color: Colors.lightBlue
        ),
        alignment: Alignment.center,
        child: new Icon(
          FontAwesomeIcons.user,
          color: Colors.white,
          size: 24.0,
        ),
      );
    }

    return new Container(
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
              color: Colors.grey[300]
          )
      ),
      margin: EdgeInsets.only(bottom:16.0, top:16.0),
      padding: EdgeInsets.only(top:16.0, bottom:16.0, left: 16.0, right: 16.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          new Container(
            child: new Row(
              children: <Widget>[
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    imgUser,
                  ],
                ),
                new Column(
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.only(left:10.0, right:10.0),
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                child:new Text(
                                  namaUser,
                                  style: TextStyle(fontFamily: "NeoSansBold", fontSize: 16.0),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              new Padding(
                                padding: EdgeInsets.only(top: 5.0),
                              ),
                              _getStatus(),
                            ]
                        ),
                      ),
                    ]
                ),

              ],
            ),
          )
        ],
      ),
    );
  }

  Future<String> get_member() async {
    var _datashcard;
    if(isloadingMbr){
      var api = new Http_Controller();
      List<Map<String, dynamic>> dtNext=[];
      dtNext.add({
        "email": widget._email.toString(),
        "username": widget._username.toString(),
        "card":"shcard",
      });
      var http_response = await api.postResponse("https://suryahusadha.com/api/loyalti/getmember", dtNext);
      _datashcard = convert.jsonEncode(http_response);
      var decode = convert.jsonDecode(_datashcard);
      var decodeConvert = convert.jsonDecode(decode["responseBody"]);
      final dtkonten = decodeConvert["getmember"];
      getmember = dtkonten[0]["msg"];
      widget._point = dtkonten[0]["point"];
    }
    return _datashcard;
  }

  Widget _getStatus(){
    return new FutureBuilder(
        future: get_member(),
        builder: (context, snapshot){
          if(getmember != null){
            isloadingMbr = false;
            if( getmember == "active"){
              return new Row(
                children: <Widget>[
                  new InkWell(
                    child: new Container(
                      decoration: new BoxDecoration(
                          borderRadius:
                          new BorderRadius.all(new Radius.circular(5.0)),
                          color: Color(0x50FFD180)
                      ),
                      child: new Row(
                        children: <Widget>[
                          new Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(left:10.0),
                            child: new Icon(
                              FontAwesomeIcons.creditCard,
                              size: 16.0,
                            ),
                          ),
                          new Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.all(6.0),
                            child: new Text(
                              widget._point.toString()+" Poin",
                              style: TextStyle(fontSize: 12.0),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  new Container(
                      margin: EdgeInsets.only(left: 10.0),
                      decoration: new BoxDecoration(
                          borderRadius:
                          new BorderRadius.all(new Radius.circular(5.0)),
                          color: Color(0x50FFD180)
                      ),
                      child: dtkids != null ? new InkWell(
                        onTap: (){
                          showDialog(
                            barrierDismissible: true,
                            context: context,
                            builder: (BuildContext context){
                              return _kidsmember();
                            },
                          );
                        },
                        child: new Row(
                          children: <Widget>[
                            new Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(left:10.0),
                              child:  new Icon(
                                FontAwesomeIcons.child,
                                size: 16.0,
                              ),
                            ),
                            new Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.all(6.0),
                              child: new Text(
                                "Kids Privilege",
                                style: TextStyle(fontSize: 12.0),
                              ),
                            ),
                            new Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(left:10.0, right: 10),
                              child:  new Icon(
                                FontAwesomeIcons.arrowRight,
                                size: 12.0,
                              ),
                            ),
                          ],
                        ),
                      ) : new Container(),
                  )
                ],
              );
            }else{
              return new Row(
                children: <Widget>[
                  new Container(
                    margin: EdgeInsets.only(left: 10.0),
                    decoration: new BoxDecoration(
                        borderRadius:
                        new BorderRadius.all(new Radius.circular(5.0)),
                        color: Color(0x50FFD180)
                    ),
                    child: new Row(
                      children: <Widget>[
                        new Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(left:10.0),
                          child:  new Icon(
                            FontAwesomeIcons.idCard,
                            size: 16.0,
                          ),
                        ),
                        new Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.all(6.0),
                          child: new Text(
                            getmember.toString(),
                            style: TextStyle(fontSize: 12.0),
                          ),
                        ),
                        new Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(right:10.0),
                          child:  new Icon(
                            FontAwesomeIcons.arrowRight,
                            size: 10.0,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              );
            }
          }else{
            return new Container();
          }
        }
    );
  }

  Widget _memberkidsHeader(){
    return new Container(
      color: Colors.white,
      padding: EdgeInsets.all(10),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Row(
            children: <Widget>[
              new Icon(
                FontAwesomeIcons.child,
                size: 18,
              ),
              new Container(
                padding: EdgeInsets.only(left: 10, top: 3),
                child: new Text(
                  "Akun Kids Privilege",
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                  ),
                ),
              )
            ],
          ),
          InkWell(
            onTap: (){
              List<Map<String, dynamic>> params=[];
              params.add({
                "_cabang": widget._cabang,
              });
              Navigator.pushReplacement(context, SlideLeftRoute(page: Fullpage(widgetpage: TambahKlg(params, widget._akun),pagename: "TambahKeluarga",))).then((val){
                //print("AkunBerobatKeluarga: "+val.toString());
              });
            },
            child: new Container(
              decoration: new BoxDecoration(
                borderRadius:
                new BorderRadius.all(new Radius.circular(5.0)),
                color: Colors.redAccent,
              ),
              alignment: Alignment.topLeft,
              child: new Container(
                padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Icon(
                      FontAwesomeIcons.plus,
                      size: 12.0,
                      color: Colors.white,
                    ),
                    new Padding(padding: EdgeInsets.only(left: 5)),
                    new Text(
                      "Kids",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 12
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _kidsmember(){
    return new SafeArea(
      child: new Scaffold(
        backgroundColor: Colors.grey[400],
        body: new Container(
          margin: EdgeInsets.only(top: 100, left: 20, right: 20, bottom: 40),
          child: Column(
            children: <Widget>[
              _memberkidsHeader(),
              dtkids != null ? ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: dtkids.length,
                  itemBuilder:(BuildContext context, int index) {
                    return cardmemberKids(context, dtkids[index]);
                  }
              ) : new Container(),
              new Container(
                width: double.infinity,
                height: 1.0,
                color: Colors.grey[200],
              ),
            ],
          ),
        ),
      )
    );
  }

  Widget cardmemberKids(BuildContext context, dt){

    String pointKids;
    if(dt["point"].toString() != "" || dt["point"].toString() != null){
      pointKids = dt["point"].toString();
    }else{
      pointKids = "0";
    }

    return new Container(
      color: Colors.white,
      padding: EdgeInsets.only(top:16.0, bottom:16.0, left: 16.0, right: 16.0),
      child: InkWell(
          onTap: (){
            List<Map<String, dynamic>> params=[];
            params.add({
              "_cabang": widget._cabang,
              "_page": "kidscard",
              "_kidsParam": dt,
            });
            RouteHelper(context, "TukarPoint", params);
          },
          child: new Column(
            children: <Widget>[
              new Container(
                padding: EdgeInsets.only(top:5, bottom: 5),
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                          height: 55.0,
                          width: 55.0,
                          padding: EdgeInsets.all(6.0),
                          decoration: new BoxDecoration(
                              borderRadius:
                              new BorderRadius.all(new Radius.circular(100.0)),
                              color: Colors.lightBlue
                          ),
                          alignment: Alignment.center,
                          child: new Icon(
                            FontAwesomeIcons.child,
                            color: Colors.white,
                            size: 24.0,
                          ),
                        )
                      ],
                    ),
                    new Expanded(
                      flex: 2,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.only(left:10.0, right:10.0),
                              alignment: Alignment.topLeft,
                              child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      margin: EdgeInsets.only(bottom: 5),
                                      child:new Text(
                                        dt["kc_name"],
                                        style: TextStyle(fontWeight: FontWeight.w700, fontFamily: "NeoSansBold", fontSize: 14.0, color: Colors.grey[800]),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Row(
                                      children: <Widget>[
                                        new Container(
                                          margin: EdgeInsets.only(right: 5),
                                          padding: EdgeInsets.only(top: 3, bottom: 5, right: 10, left: 10),
                                          decoration: new BoxDecoration(
                                              borderRadius:
                                              new BorderRadius.all(new Radius.circular(5.0)),
                                              color: Color(0x50FFD180)
                                          ),
                                          child: new Row(
                                            children: <Widget>[
                                              new Container(
                                                alignment: Alignment.center,
                                                child: new Icon(
                                                  FontAwesomeIcons.creditCard,
                                                  size: 14.0,
                                                ),
                                              ),
                                              new Container(
                                                alignment: Alignment.center,
                                                padding: EdgeInsets.only(top:2.0, left: 5),
                                                child: new Text(
                                                  pointKids +" Poin",
                                                  style: TextStyle(fontSize: 12.0),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        new Container(
                                          padding: EdgeInsets.only(top: 3, bottom: 5, right: 10, left: 10),
                                          decoration: new BoxDecoration(
                                              borderRadius:
                                              new BorderRadius.all(new Radius.circular(5.0)),
                                              color: Color(0x50FFD180)
                                          ),
                                          child: new Row(
                                            children: <Widget>[
                                              new Container(
                                                alignment: Alignment.center,
                                                child: new Icon(
                                                  FontAwesomeIcons.idBadge,
                                                  size: 14.0,
                                                ),
                                              ),
                                              new Container(
                                                padding: EdgeInsets.only(top: 2, left: 1),
                                                alignment: Alignment.center,
                                                child: new Text(
                                                  dt["kc_number"],
                                                  style: TextStyle(color: Colors.grey[800], fontSize: 12.0),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.ellipsis,
                                                  maxLines: 1,
                                                  softWrap: false,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    )
                                  ]
                              ),
                            ),
                          ]
                      ),
                    ),
                    new Container(
                      height: 40.0,
                      width: 40.0,
                      alignment: Alignment.centerRight,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Container(
                              alignment: Alignment.centerRight,
                              child: new Icon(
                                FontAwesomeIcons.angleRight,
                                color: Colors.grey,
                                size: 18.0,
                              ),
                            ),
                          ]
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )
      ),
    );
  }

  //MERCHANT
  Widget _mercanthead(){
    return new Container(
        child: new Column(
          children: <Widget>[
            new Container(
                margin: EdgeInsets.only(top: 30, bottom: 10),
                padding: EdgeInsets.only(top: 40, bottom: 20),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey[300],style: BorderStyle.solid),
                    color: Colors.white
                ),
                child: new Container(
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                        child: Text(
                          "MERCHANT KERJASAMA",
                          style: TextStyle(
                              color: Colors.grey[800],
                              fontSize: 16,
                              fontWeight: FontWeight.w700
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 10, right: 10),
                        child: Text(
                          "Tingkatkan point Anda dan segera tukarkan dengan merchant kerjasama kami!",
                          style: TextStyle(
                            color: Colors.grey[600],
                            fontSize: 14,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                )
            ),
          ],
        )
    );
  }

  Future<String> _getmerchantdt() async {
    var _datamrc;
    if(isloading2){
      isloading2 = false;
      var api = new Http_Controller();
      var http_response = await api.getResponse("https://suryahusadha.com/api/loyalti/getmerchant");
      _datamrc = convert.jsonEncode(http_response);
      var decode = convert.jsonDecode(_datamrc);
      var decodeConvert = convert.jsonDecode(decode["responseBody"]);
      final dtkonten = decodeConvert["getmerchant"];
      getmerchant = dtkonten[0]["data"];
      maintenance = dtkonten[0]["maintenance"];
      msg_maintnance = dtkonten[0]["msg_maintnance"];
      if(maintenance == 1 && !isloading2){
          showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) => _maintenance(msg_maintnance),
          );
      }
    }
    return _datamrc;
  }

  Widget _maintenance(msg){
    return new SafeArea(
      child: Scaffold(
          backgroundColor: Colors.black,
          body: new WillPopScope(
            onWillPop:(){
              if(widget._akun[0]["group_id"] == "7"){
                Navigator.pop(context);
              }else{
                RouteHelper(context, widget._cabang, null);
              }
            },
            child: new Center(
              child: new Container(
                height: 300,
                width: 300,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20))
                ),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 16),
                    ),
                    new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Icon(
                          FontAwesomeIcons.infoCircle,
                          size: 24,
                          color: Colors.grey[600],
                        ),
                        new Container(
                          margin: EdgeInsets.only(left: 10),
                          child: new Text(
                            "Informasi!",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 18,
                                fontWeight: FontWeight.w700
                            ),
                          ),
                        )
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 16),
                    ),
                    new Container(
                      height: 1,
                      color: Colors.grey[300],
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                      ),
                    ),
                    new Expanded(
                      child: new Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 16),
                          ),
                          new Container(
                            padding: EdgeInsets.only(left: 24, right: 24),
                            child: new Text(
                              msg,
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                    ),
                    new Container(
                      child: new Text("Terima Kasih"),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                    ),
                    new Container(
                      height: 1,
                      color: Colors.grey[300],
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                      ),
                    ),
                    InkWell(
                      onTap: (){
                        if(widget._akun != null && widget._akun[0]["group_id"] == "7"){
                          Navigator.pop(context);
                        }else{
                          RouteHelper(context, widget._cabang, null);
                        }
                      },
                      child: new Container(
                        padding: EdgeInsets.all(16),
                        child: new Row(
                          children: <Widget>[
                            new Expanded(
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Text(
                                      "Tutup",
                                      style: TextStyle(color: Colors.black),
                                    ),
                                  ],
                                )
                            ),
                            new Container(
                              padding: EdgeInsets.only(right: 10),
                              child: new Icon(
                                FontAwesomeIcons.arrowRight,
                                size: 12,
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
      ),
    );
  }

  Widget _merchantcard(){
    return new FutureBuilder(
        future: _getmerchantdt(),
        builder: (context, snapshot){
          if(getmerchant != null){
            isloading2 = false;
            return GridView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: getmerchant.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,childAspectRatio: 1,
              ),
              itemBuilder: (contxt, indx){
                return _rowmercant(getmerchant[indx]);
              },
            );
          }else{
            return new Container();
          }
        }
    );
  }
  Widget _rowmercant(dt){
    return new Card(
      shape: Border.all(color: Colors.grey[300]),
      child: InkWell(
        onTap: (){
          if(widget._page == "kidscard"){
            List<Map<String, dynamic>> dtNext=[];
            dtNext.add({"_cabang": widget._cabang});
            dtNext.add({"_mrcdt": dt});
            dtNext.add({"_page": "kidscard"});
            dtNext.add({"_kidsParam" : widget._kidsParam});
            RouteHelper(context, "DetailMerchant", dtNext);
          }else{
            List<Map<String, dynamic>> dtNext=[];
            dtNext.add({"_cabang": widget._cabang});
            dtNext.add({"_mrcdt": dt});
            dtNext.add({"_page": "shcard"});
            RouteHelper(context, "DetailMerchant", dtNext);
          }
        },
        child: new Column(
          children: <Widget>[
            new Expanded(
                child: new Stack(
                  children: <Widget>[
                    new Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(SERVER.domain + dt["mrc_image"]),
                              fit: BoxFit.cover
                          )
                      ),
                      width: double.infinity,
                      height: double.infinity,
                    ),
                    new Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Text(dt["mrc_point"] + "Pts",textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                      decoration: BoxDecoration(
                          color: Colors.redAccent,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(8),
                              bottomRight: Radius.circular(8)
                          )
                      ),
                      padding: EdgeInsets.all(5),
                    ),
                  ],
                )
            ),
            new Center(
              child: Padding(
                padding: const EdgeInsets.all(5),
                child: Text(dt["mrc_name"],textAlign: TextAlign.center,),
              ),
            ),
          ],
        ),
      )
    );
  }

}
