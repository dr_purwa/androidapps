import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surya_husadha/components/com_loyalticard/voucher_model.dart';
import 'package:surya_husadha/components/com_profile/tambahkeluarga.dart';
import 'dart:convert' as convert;
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/helper/transition/slide_route.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:surya_husadha/pages/widgetpage.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:intl/intl.dart' show DateFormat, NumberFormat;

class LoyaltiPage extends StatefulWidget {

  var _akun;
  String _userid;
  String _cabang;
  String _page;
  Color _color1;
  Color _color2;
  Color _textColor = Colors.white;
  Color _Selected_color;
  String _rm;
  String _email;
  String _username;

  LoyaltiPage(List params, List akun){
    this._akun = akun;
    this._userid = akun[0]["id"].toString();
    if(akun!=null){
      this._email = akun[0]["email"];
      this._username = akun[0]["username"];
    }
    params.forEach((dt){
      this._cabang = dt["_cabang"];
      this._page = dt["_page"];
      if(_cabang=="group"){
        this._textColor = Colors.grey[600];
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
        this._Selected_color= WarnaCabang.group2.withOpacity(0.3);
      }
      if(_cabang=="denpasar"){
        if(akun!=null){
          this._rm = akun[0]["nrmshh"];
        }
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
        this._Selected_color= WarnaCabang.shh2.withOpacity(0.3);
      }
      if(_cabang=="ubung"){
        if(akun!=null){
          this._rm = akun[0]["nrmub"];
        }
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
        this._Selected_color= WarnaCabang.ubung2.withOpacity(0.3);
      }
      if(_cabang=="nusadua"){
        if(akun!=null){
          this._rm = akun[0]["nrmnd"];
        }
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
        this._Selected_color= WarnaCabang.nusadua2.withOpacity(0.3);
      }
      if(_cabang=="kmc"){
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
        this._Selected_color= WarnaCabang.kmc2.withOpacity(0.3);
      }
    });
  }

  @override
  _LoyaltiPage createState() => _LoyaltiPage();
}

class _LoyaltiPage extends State<LoyaltiPage> {

  GlobalKey<ScaffoldState> scafolt_key = GlobalKey<ScaffoldState>();

  String maintenance = "0";
  bool isloadmtn = true;
  String msg_maintnance = "";
  var getconfig;

  int _tabswitch = 0;
  bool isloading = true;
  int point = 0;
  bool isloadingMbr = true;

  List<Voucher_mdl> _mVoucher_mdl = [];
  List<Voucher1_mdl> _mVoucher1_mdl = [];
  List<Voucher2_mdl> _mVoucher2_mdl = [];
  int _active;

  ScrollController listviewCon;
  List showitem = [];
  int showitemCount = 0;

  ScrollController listviewCon1;
  List showitem1 = [];
  int showitemCount1 = 0;

  ScrollController listviewCon2;
  List showitem2 = [];
  int showitemCount2 = 0;

  String filter;
  bool isloadingvc = true;
  int _tabvoucher = 0;
  FocusNode _search  = FocusNode();
  TextEditingController _searchController = new TextEditingController();

  Future<String> _getDatamtn() async {
    var _data;
    if(isloadmtn){
      var api = new Http_Controller();
      var http_response = await api.getResponse("https://suryahusadha.com/api/loyalti/getconfig/"+widget._page);
      _data = convert.jsonEncode(http_response);
      isloadmtn = false;
    }
    return _data;
  }

  var dtkonten;

  @override
  initState() {
    super.initState();
    _searchController.addListener(() {
      setState(() {
        filter = _searchController.text;
      });
    });
    _getDatamtn().then((String response){
      var decode = convert.jsonDecode(response);
      var decodeConvert = convert.jsonDecode(decode["responseBody"]);
      dtkonten = decodeConvert["getconfig"];
    }).whenComplete((){
      setState(() {
        getconfig = dtkonten[0]["data"];
        maintenance = dtkonten[0]["maintenance"].toString();
        msg_maintnance = dtkonten[0]["msg"];
      });
      if(maintenance == "1"){
        showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) => _maintenance(msg_maintnance),
        );
      }
    });
  }

  @override
  void dispose() {
    _searchController.removeListener((){
      filter = _searchController.text;
    });
    super.dispose();
  }

  Future<bool> _onWillPop(){
    RouteHelper(context, widget._cabang, null);
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: new Scaffold(
          key: scafolt_key,
          drawer: drawerController(widget._akun, widget._cabang, "LoyaltiPage"),
          appBar: new PreferredSize(
            preferredSize: Size.fromHeight(200.0),
            child: AppBarController(widget._cabang, scafolt_key, widget._akun, "LoyaltiPage"),
          ),
          backgroundColor: Colors.grey[200],
          body: new WillPopScope(
              onWillPop: _onWillPop,
              child:new Container(
                  child: new ListView(
                      physics: ClampingScrollPhysics(),
                      children: <Widget>[
                        widget._page == "shcard" && getconfig != null ?  new Container(
                            child: _LoyaltiStatus(),
                        ) : new Container(
                          margin: EdgeInsets.only(top: 10),
                        ),
                        _tab(widget._page),
                        widget._page == "shcard" && getconfig != null ?  _vouchertab() : new Container(),
                        Visibility(
                          visible: widget._page == "shcard" && _tabvoucher == 0 && getconfig != null ? true : false,
                          child: widget._page == "shcard" ? _voucherlist() : new Container(),
                        ),
                        Visibility(
                          visible: widget._page == "shcard" && _tabvoucher == 1 && getconfig != null ? true : false,
                          child:widget._page == "shcard" ? _voucherlist1() : new Container(),
                        ),
                        Visibility(
                          visible: widget._page == "shcard" && _tabvoucher == 2 && getconfig != null ? true : false,
                          child: widget._page == "shcard" ? _voucherlist2() : new Container(),
                        ),
                        Visibility(
                          visible: widget._page == "kidscard" && getconfig != null ? true : false,
                          child: widget._page == "kidscard" ? _memberkids(widget._akun) : new Container(),
                        ),
                      ]
                  )
              )
          )
      ),
    );
  }

  Widget _maintenance(msg){
    return new SafeArea(
      child: Scaffold(
          backgroundColor: Colors.black,
          body: new WillPopScope(
            onWillPop:(){
              if(widget._akun[0]["group_id"] == "7"){
                Navigator.pop(context);
              }else{
                RouteHelper(context, widget._cabang, null);
              }
            },
            child: new Center(
              child: new Container(
                height: 300,
                width: 300,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20))
                ),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 16),
                    ),
                    new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Icon(
                          FontAwesomeIcons.infoCircle,
                          size: 24,
                          color: Colors.grey[600],
                        ),
                        new Container(
                          margin: EdgeInsets.only(left: 10),
                          child: new Text(
                            "Informasi!",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 18,
                                fontWeight: FontWeight.w700
                            ),
                          ),
                        )
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 16),
                    ),
                    new Container(
                      height: 1,
                      color: Colors.grey[300],
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                      ),
                    ),
                    new Expanded(
                      child: new Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 16),
                          ),
                          new Container(
                            padding: EdgeInsets.only(left: 24, right: 24),
                            child: new Text(
                              msg,
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                    ),
                    new Container(
                      child: new Text("Terima Kasih"),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                    ),
                    new Container(
                      height: 1,
                      color: Colors.grey[300],
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                      ),
                    ),
                    InkWell(
                      onTap: (){
                        if(widget._akun != null && widget._akun[0]["group_id"] == "7"){
                          Navigator.pop(context);
                        }else{
                          RouteHelper(context, widget._cabang, null);
                        }
                      },
                      child: new Container(
                        padding: EdgeInsets.all(16),
                        child: new Row(
                          children: <Widget>[
                            new Expanded(
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Text(
                                      "Tutup",
                                      style: TextStyle(color: Colors.black),
                                    ),
                                  ],
                                )
                            ),
                            new Container(
                              padding: EdgeInsets.only(right: 10),
                              child: new Icon(
                                FontAwesomeIcons.arrowRight,
                                size: 12,
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
      ),
    );
  }

  Future<String> _getKlg() async {
    var _data;
    var db = new Http_Controller();
    var http_response = await db.getResponse("https://suryahusadha.com/api/loyalti/getkidsprevilage/" + widget._userid);
    _data = convert.jsonEncode(http_response);
    print("getkidsprevilage: "+_data.toString());
    return _data;
  }

  Widget _memberkidsHeader(){
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        new Row(
          children: <Widget>[
            new Icon(
              FontAwesomeIcons.child,
              size: 18,
            ),
            new Container(
              padding: EdgeInsets.only(left: 10, top: 3),
              child: new Text(
                "Akun Kids Privilege",
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                ),
              ),
            )
          ],
        ),
        InkWell(
          onTap: (){
            List<Map<String, dynamic>> params=[];
            params.add({
              "_cabang": widget._cabang,
              "_page": "LoyaltiPage",
            });
            Navigator.pushReplacement(context, SlideLeftRoute(page: Fullpage(widgetpage: TambahKlg(params, widget._akun),pagename: "TambahKeluarga",))).then((val){
              //print("AkunBerobatKeluarga: "+val.toString());
            });
          },
          child: new Container(
            decoration: new BoxDecoration(
              borderRadius:
              new BorderRadius.all(new Radius.circular(5.0)),
              color: Colors.redAccent,
            ),
            alignment: Alignment.topLeft,
            child: new Container(
              padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    FontAwesomeIcons.plus,
                    size: 12.0,
                    color: Colors.white,
                  ),
                  new Padding(padding: EdgeInsets.only(left: 5)),
                  new Text(
                    "Kids",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 12
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
  Widget _memberkids(akun){
    return new Container(
      color: Colors.white,
      padding: EdgeInsets.only(bottom: 16, left: 16, right: 16, top: 10),
      child: Column(
        children: <Widget>[
          _memberkidsHeader(),
          new FutureBuilder(
              future: _getKlg(),
              builder: (context, snapshot) {
                if(snapshot.hasData){
                  var decode = convert.jsonDecode(snapshot.data);
                  var decodeConvert = convert.jsonDecode(decode["responseBody"]);
                  final dtkonten = decodeConvert["getklg"];
                  var getdata = dtkonten[0]["data"];
                  return ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: getdata.length,
                      itemBuilder:(BuildContext context, int index) {
                        return cardmemberKids(context, getdata[index]);
                      }
                  );
                }else{
                  if(snapshot.connectionState == ConnectionState.waiting){
                    return Container(
                      child: Column(
                        children: <Widget>[
                          new Container(
                            margin: EdgeInsets.only(top: 5.0),
                            width: double.infinity,
                            height: 1.0,
                            color: Colors.grey[200],
                          ),
                          new Container(
                            margin: EdgeInsets.only(top: 40, bottom: 10),
                            child: Center(
                              child: SizedBox(
                                  width: 80.0,
                                  height: 80.0,
                                  child: CircularProgressIndicator(
                                    backgroundColor: Colors.grey[300],
                                  )
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  }else{
                    return Container(
                      margin: EdgeInsets.only(top: 40),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text("Akun Kids Privilege tidak ditemukan !")
                        ],
                      ),
                    );
                  }
                }
              }
          ),
          new Container(
            margin: EdgeInsets.only(top: 5.0),
            width: double.infinity,
            height: 1.0,
            color: Colors.grey[200],
          ),
        ],
      ),
    );
  }

  Widget cardHeader(String title, IconData icon){
    return new Container(
      color: Colors.white,
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          new Container(
            margin: EdgeInsets.only(top: 20),
            color: Colors.grey[300],
            width: MediaQuery.of(context).size.width,
            height: 1.0,
          ),
          new Container(
            padding: EdgeInsets.only(top: 5, bottom: 5),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Icon(
                      icon,
                      size: 16,
                      color: Colors.grey[800],
                    ),
                    new Container(
                      padding: EdgeInsets.only(left: 10),
                    ),
                    new Text(
                      title,
                      style: TextStyle(color: Colors.grey[800], fontSize: 16, fontWeight: FontWeight.w700),
                    ),
                  ],
                ),
                InkWell(
                  onTap: (){
                    List<Map<String, dynamic>> params=[];
                    params.add({
                      "_cabang": widget._cabang,
                    });
                    RouteHelper(context, "AkunBerobatKeluarga", params);
                  },
                  child: new Container(
                    decoration: new BoxDecoration(
                      borderRadius:
                      new BorderRadius.all(new Radius.circular(5.0)),
                      color: Colors.redAccent,
                    ),
                    alignment: Alignment.topLeft,
                    child: new Container(
                      padding: EdgeInsets.all(6),
                      child: Row(
                        children: <Widget>[
                          new Icon(
                            FontAwesomeIcons.plus,
                            size: 14.0,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          new Container(
            color: Colors.grey[300],
            width: MediaQuery.of(context).size.width,
            height: 1.0,
          ),
        ],
      ),
    );
  }

  Widget cardmemberKids(BuildContext context, dt){

    String pointKids;
    if(dt["point"].toString() != "" || dt["point"].toString() != null){
      pointKids = dt["point"].toString();
    }else{
      pointKids = "0";
    }

    return new Container(
      color: Colors.white,
      margin: EdgeInsets.only(top: 5.0),
      child: InkWell(
          onTap: (){
            showDialog(
              barrierDismissible: true,
              context: context,
              builder: (BuildContext context){
                return ekidscard(dt["kc_id"]);
              },
            );
          },
          child: new Column(
            children: <Widget>[
              new Container(
                margin: EdgeInsets.only(top: 5.0, bottom: 5),
                width: double.infinity,
                height: 1.0,
                color: Colors.grey[200],
              ),
              new Container(
                padding: EdgeInsets.only(top:5, bottom: 5),
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                          height: 55.0,
                          width: 55.0,
                          padding: EdgeInsets.all(6.0),
                          decoration: new BoxDecoration(
                              borderRadius:
                              new BorderRadius.all(new Radius.circular(100.0)),
                              color: Colors.lightBlue
                          ),
                          alignment: Alignment.center,
                          child: new Icon(
                            FontAwesomeIcons.child,
                            color: Colors.white,
                            size: 24.0,
                          ),
                        )
                      ],
                    ),
                    new Expanded(
                      flex: 2,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.only(left:10.0, right:10.0),
                              alignment: Alignment.topLeft,
                              child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      margin: EdgeInsets.only(bottom: 5),
                                      child:new Text(
                                        dt["kc_name"],
                                        style: TextStyle(fontWeight: FontWeight.w700, fontFamily: "NeoSansBold", fontSize: 14.0, color: Colors.grey[800]),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Row(
                                      children: <Widget>[
                                        new Container(
                                          margin: EdgeInsets.only(right: 5),
                                          padding: EdgeInsets.only(top: 3, bottom: 5, right: 10, left: 10),
                                          decoration: new BoxDecoration(
                                              borderRadius:
                                              new BorderRadius.all(new Radius.circular(5.0)),
                                              color: Color(0x50FFD180)
                                          ),
                                          child: new Row(
                                            children: <Widget>[
                                              new Container(
                                                alignment: Alignment.center,
                                                child: new Icon(
                                                  FontAwesomeIcons.creditCard,
                                                  size: 14.0,
                                                ),
                                              ),
                                              new Container(
                                                alignment: Alignment.center,
                                                padding: EdgeInsets.only(top:2.0, left: 5),
                                                child: new Text(
                                                  pointKids +" Poin",
                                                  style: TextStyle(fontSize: 12.0),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        new Container(
                                          padding: EdgeInsets.only(top: 3, bottom: 5, right: 10, left: 10),
                                          decoration: new BoxDecoration(
                                              borderRadius:
                                              new BorderRadius.all(new Radius.circular(5.0)),
                                              color: Color(0x50FFD180)
                                          ),
                                          child: new Row(
                                            children: <Widget>[
                                              new Container(
                                                alignment: Alignment.center,
                                                child: new Icon(
                                                  FontAwesomeIcons.idBadge,
                                                  size: 14.0,
                                                ),
                                              ),
                                              new Container(
                                                padding: EdgeInsets.only(top: 2, left: 1),
                                                alignment: Alignment.center,
                                                child: new Text(
                                                  dt["kc_number"],
                                                  style: TextStyle(color: Colors.grey[800], fontSize: 12.0),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.ellipsis,
                                                  maxLines: 1,
                                                  softWrap: false,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    )
                                  ]
                              ),
                            ),
                          ]
                      ),
                    ),
                    new Container(
                      height: 40.0,
                      width: 40.0,
                      alignment: Alignment.centerRight,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Container(
                              alignment: Alignment.centerRight,
                              child: new Icon(
                                FontAwesomeIcons.angleRight,
                                color: Colors.grey,
                                size: 18.0,
                              ),
                            ),
                          ]
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )
      ),
    );
  }

  Widget ekidscard(inDx){
    return new SafeArea(
      child: new Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Center(
          child: RotatedBox(
            quarterTurns: 5,
            child: Stack(
              children: <Widget>[
                new Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage("https://suryahusadha.com/api/loyalti/kidscard/"+inDx+"?dummy=${new DateTime.now()}"),
                      )
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _tab(page){
    if (getconfig != null) {
      isloading = false;
      switch(_tabswitch) {
        case 0:
          return _tabcontent(getconfig, page);
          break;
        case 1:
          return _tabcontent(getconfig, page);
          break;
        default :
          return new Container();
          break;
      }
    }else{
      return new Container(
        margin: EdgeInsets.only(top: 40, bottom: 10),
        child: Center(
            child: SizedBox(
                width: 80.0,
                height: 80.0,
                child: CircularProgressIndicator(
                  backgroundColor: Colors.grey[300],
                )
            ),
          )
      );
    }
  }

  _launchURL(url) async {
    await launch(url);
  }

  Widget _tabcontent(dtconfig, page){

    Widget content;
    var dt;
    var tabTitle;
    var iconTitle;
    if(page == "kidscard"){
      dt = dtconfig[1];
      content = new HtmlWidget(
        dt["full_desc"],
        textStyle: TextStyle(
            fontSize: 16,
            height: 1,
            fontFamily: "NeoSansBold",
            color: Colors.grey[800],
            wordSpacing: 2.0
        ),
        onTapUrl: (url){
          _launchURL(url);
        },
        bodyPadding: EdgeInsets.all(0),
      );
      tabTitle = "Kids Privilege";
      iconTitle = FontAwesomeIcons.child;
    }else{
      dt = dtconfig[0];
      content = new HtmlWidget(
        dt["full_desc"],
        textStyle: TextStyle(
            fontSize: 16,
            height: 1,
            fontFamily: "NeoSansBold",
            color: Colors.grey[800],
            wordSpacing: 2.0
        ),
        onTapUrl: (url){
          _launchURL(url);
        },
        bodyPadding: EdgeInsets.all(0),
      );
      tabTitle = "SH Loyalti";
      iconTitle = FontAwesomeIcons.thumbsUp;
    };

    return new Stack(
      children: <Widget>[
        new Container(
          margin: EdgeInsets.only(top: 30, bottom: 10),
          padding: EdgeInsets.only(bottom: 16, left: 10, right: 10),
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.grey[300],style: BorderStyle.solid)
          ),
          child: new Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              new Container(
                  padding: EdgeInsets.only(top: 30, left: 10, right: 10),
                  child: new Column(
                    children: <Widget>[
                      content,
                    ],
                  )
              )
            ],
          ),
        ),
        new Container(
            child: new Column(
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(left: 32, right: 32),
                    padding: EdgeInsets.only(top: 16, bottom: 16),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey[300],style: BorderStyle.solid),
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        color: Colors.white
                    ),
                    child: new Container(
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: widget._page == "shcard" ? MainAxisAlignment.spaceAround : MainAxisAlignment.center,
                        children: <Widget>[
                          new InkWell(
                            onTap: (){
                              setState(() {
                                _tabswitch = 0;
                              });
                            },
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(iconTitle, size: 16,color: _tabswitch == 0 ? Colors.redAccent : Colors.grey[600]),
                                Container(
                                  padding: EdgeInsets.only(left: 10, top: 3),
                                  child: Text(
                                    tabTitle,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        color: _tabswitch == 0 ? Colors.redAccent : Colors.grey[800]
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          widget._page == "shcard" ? new Container(
                              width: 10,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  new Text("|")
                                ],
                              )
                          ) : new Container(),
                          widget._page == "shcard" ? new InkWell(
                            onTap: (){
                              setState(() {
                                if(page == "kidscard"){
                                  _tabswitch = 1;
                                }else{
                                  showDialog(
                                    barrierDismissible: true,
                                    context: context,
                                    builder: (BuildContext context){
                                      return eshcard();
                                    },
                                  );
                                }
                              });
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(FontAwesomeIcons.idCard, size: 16,color: _tabswitch == 1 ? Colors.redAccent : Colors.grey[600]),
                                Container(
                                  padding: EdgeInsets.only(left: 10, top: 3),
                                  child: Text("E-SHCARD",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        color: _tabswitch == 1 ? Colors.redAccent : Colors.grey[800]
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ) : new Container(),
                        ],
                      ),
                    )
                ),
              ],
            )
        ),
      ],
    );
  }

  Widget _LoyaltiStatus(){

    Widget imgUser;
    String namaUser = "";
    if(int.parse(DateTime.now().hour.toString()) > 00){
      namaUser = "Selamat pagi!";
    }
    if(int.parse(DateTime.now().hour.toString()) > 11){
      namaUser = "Selamat siang!";
    }
    if(int.parse(DateTime.now().hour.toString()) > 14){
      namaUser = "Selamat sore!";
    }
    if(int.parse(DateTime.now().hour.toString()) > 18){
      namaUser = "Selamat malam!";
    }

    if( widget._akun != null) {
      namaUser = "Hi, "+widget._akun[0]["name"];
      String img = SERVER.domain + widget._akun[0]["pp"] +'?dummy=${new DateTime.now()}';
      imgUser = new Container(
        height: 55.0,
        width: 55.0,
        padding: EdgeInsets.all(2.0),
        decoration: new BoxDecoration(
            borderRadius:
            new BorderRadius.all(new Radius.circular(100.0)),
            color: Colors.lightBlue
        ),
        alignment: Alignment.center,
        child: CircleAvatar(
          backgroundImage: NetworkImage(img),
          minRadius: 90,
          maxRadius: 150,
        ),
      );
    } else {
      imgUser = new Container(
        height: 55.0,
        width: 55.0,
        padding: EdgeInsets.all(6.0),
        decoration: new BoxDecoration(
            borderRadius:
            new BorderRadius.all(new Radius.circular(100.0)),
            color: Colors.lightBlue
        ),
        alignment: Alignment.center,
        child: new Icon(
          FontAwesomeIcons.user,
          color: Colors.white,
          size: 24.0,
        ),
      );
    }
    return new Container(
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
              color: Colors.grey[300]
          )
      ),
      margin: EdgeInsets.only(bottom:16.0, top:16.0),
      padding: EdgeInsets.only(top:16.0, bottom:16.0, left: 16.0, right: 16.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          new Container(
            child: new Row(
              children: <Widget>[
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    imgUser,
                  ],
                ),
                new Column(
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.only(left:10.0, right:10.0),
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                child:new Text(
                                  namaUser,
                                  style: TextStyle(fontFamily: "NeoSansBold", fontSize: 16.0),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              new Padding(
                                padding: EdgeInsets.only(top: 5.0),
                              ),
                              _getStatus(),
                            ]
                        ),
                      ),

                    ]
                ),

              ],
            ),
          )
        ],
      ),
    );
  }

  var getmember;
  var _data;
  Future<String> get_member() async {
    if(isloadingMbr){
      var api = new Http_Controller();
      List<Map<String, dynamic>> dtNext=[];
      dtNext.add({
        "email": widget._email.toString(),
        "username": widget._username.toString(),
        "card":"shcard",
      });
      var http_response = await api.postResponse("https://suryahusadha.com/api/loyalti/getmember", dtNext);
      _data = convert.jsonEncode(http_response);
    }
    return _data;
  }

  Widget _getStatus(){
    return new FutureBuilder(
        future: get_member(),
        builder: (context, snapshotMbr){
          if(snapshotMbr.hasData){
            var decode = convert.jsonDecode(_data);
            var decodeConvert = convert.jsonDecode(decode["responseBody"]);
            final dtkonten = decodeConvert["getmember"];
            getmember = dtkonten[0]["msg"];
            print("getmember: "+getmember.toString());
            point = int.parse(dtkonten[0]["point"]);
            isloadingMbr = false;
            if(getmember == "active"){
              return new Row(
                children: <Widget>[
                  new InkWell(
                    child: new Container(
                      decoration: new BoxDecoration(
                          borderRadius:
                          new BorderRadius.all(new Radius.circular(5.0)),
                          color: Color(0x50FFD180)
                      ),
                      child: new Row(
                        children: <Widget>[
                          new Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(left:10.0),
                            child: new Icon(
                              FontAwesomeIcons.creditCard,
                              size: 16.0,
                            ),
                          ),
                          new Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.all(6.0),
                            child: new Text(
                              point.toString()+" Poin",
                              style: TextStyle(fontSize: 12.0),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  new Container(
                      margin: EdgeInsets.only(left: 10.0),
                      decoration: new BoxDecoration(
                          borderRadius:
                          new BorderRadius.all(new Radius.circular(5.0)),
                          color: Color(0x50FFD180)
                      ),
                      child: new InkWell(
                        onTap: (){
                          List<Map<String, dynamic>> params=[];
                          params.add({
                            "_cabang": widget._cabang,
                          });
                          RouteHelper(context, "AddVoucher", params);
                        },
                        child: new Row(
                          children: <Widget>[
                            new Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(left:10.0),
                              child:  new Icon(
                                FontAwesomeIcons.plus,
                                size: 16.0,
                              ),
                            ),
                            new Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.all(6.0),
                              child: new Text(
                                "Voucher Baru",
                                style: TextStyle(fontSize: 12.0),
                              ),
                            ),
                            new Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(right:10.0),
                              child:  new Icon(
                                FontAwesomeIcons.arrowRight,
                                size: 10.0,
                              ),
                            ),
                          ],
                        ),
                      )
                  )
                ],
              );
            }else{
              return new Row(
                children: <Widget>[
                  new Container(
                    decoration: new BoxDecoration(
                        borderRadius:
                        new BorderRadius.all(new Radius.circular(5.0)),
                        color: Color(0x50FFD180)
                    ),
                    child: new Row(
                      children: <Widget>[
                        new Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(left:10.0),
                          child:  new Icon(
                            FontAwesomeIcons.idCard,
                            size: 16.0,
                          ),
                        ),
                        new Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.all(6.0),
                          child: new Text(
                            getmember.toString(),
                            style: TextStyle(fontSize: 12.0),
                          ),
                        ),
                        new Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(right:10.0),
                          child:  new Icon(
                            FontAwesomeIcons.arrowRight,
                            size: 10.0,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              );
            }
          }else{
            return new Container();
          }
        }
    );
  }

  Widget eshcard(){
    return new SafeArea(
      child: new Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Center(
          child: RotatedBox(
            quarterTurns: 5,
            child: Stack(
              children: <Widget>[
                new Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage("https://suryahusadha.com/api/loyalti/shcard/?email="+widget._email.toString()+"&username="+widget._username.toString()+"&dummy=${new DateTime.now()}"),
                      )
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _vouchertab(){
    return new Stack(
      children: <Widget>[
        new Container(
          margin: EdgeInsets.only(top: 30, bottom: 5),
          padding: EdgeInsets.only(left: 10, right: 10),
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.grey[300],style: BorderStyle.solid)
          ),
          child: new Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              _tabvc(),
            ],
          ),
        ),
        new Container(
            child: new Column(
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(left: 16, right: 16),
                    padding: EdgeInsets.only(top: 16, bottom: 16),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey[300],style: BorderStyle.solid),
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        color: Colors.white
                    ),
                    child: new Container(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new InkWell(
                            onTap: (){
                              setState(() {
                                _tabvoucher = 0;
                                isloadingvc = false;
                              });
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Padding(padding: EdgeInsets.only(left: 10),),
                                Icon(
                                    FontAwesomeIcons.ticketAlt,
                                    size: 16,
                                    color: _tabvoucher == 0 ? Colors.redAccent : Colors.grey[600]
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 10, top: 3),
                                  child: Text(
                                    "Rilis",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        color: _tabvoucher == 0 ? Colors.redAccent : Colors.grey[800]
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          new Container(
                              width: 10,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  new Text("|")
                                ],
                              )
                          ),
                          new InkWell(
                            onTap: (){
                              setState(() {
                                _tabvoucher = 1;
                                isloadingvc = false;
                              });
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                    FontAwesomeIcons.check,
                                    size: 16,color: _tabvoucher == 1 ? Colors.redAccent : Colors.grey[600]
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 10, top: 3),
                                  child: Text("Digunakan",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        color: _tabvoucher == 1 ? Colors.redAccent : Colors.grey[800]
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          new Container(
                              width: 10,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  new Text("|")
                                ],
                              )
                          ),
                          new InkWell(
                            onTap: (){
                              setState(() {
                                _tabvoucher = 2;
                                isloadingvc = false;
                              });
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                    FontAwesomeIcons.ban,
                                    size: 16,color: _tabvoucher == 2 ? Colors.redAccent : Colors.grey[600]
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 10, top: 3, right: 10),
                                  child: Text("Non Aktif",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        color: _tabvoucher == 2 ? Colors.redAccent : Colors.grey[800]
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                ),
              ],
            )
        ),
      ],
    );
  }

  Widget _tabvc(){
    return new Container(
      padding: EdgeInsets.only(top: 25),
      child: new Container(
        color: Colors.white,
        child: new Column(
            children: <Widget>[
              new Visibility(
                visible: true,
                child: new Container(
                  child: TextField(
                    textInputAction: TextInputAction.done,
                    focusNode: _search,
                    controller: _searchController,
                    cursorColor: Colors.grey,
                    style: TextStyle(color: Colors.grey, fontSize: 14),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(top: 14),
                      hintText: "Cari Voucher SHCARD Anda",
                      border: InputBorder.none,
                      labelStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      prefixIcon: Icon(
                        FontAwesomeIcons.search,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
              ),
            ]
        ),
      ),
    );
  }


  Future<String> _getDatavc0() async {
    var _data;
    if(isloading){
      _mVoucher_mdl.clear();
      var api = new Http_Controller();
      var http_response = await api.getResponse("https://suryahusadha.com/api/loyalti/voucherlist/0/"+widget._email.toString()+"?dummy=${new DateTime.now()}");
      _data = convert.jsonEncode(http_response);
    }
    return _data;
  }
  Future<String> _getDatavc1() async {
    var _data;
    if(isloading){
      _mVoucher_mdl.clear();
      var api = new Http_Controller();
      var http_response = await api.getResponse("https://suryahusadha.com/api/loyalti/voucherlist/1/"+widget._email.toString()+"?dummy=${new DateTime.now()}");
      _data = convert.jsonEncode(http_response);
    }
    return _data;
  }
  Future<String> _getDatavc2() async {
    var _data;
    if(isloading){
      _mVoucher_mdl.clear();
      var api = new Http_Controller();
      var http_response = await api.getResponse("https://suryahusadha.com/api/loyalti/voucherlist/2/"+widget._email.toString()+"?dummy=${new DateTime.now()}");
      _data = convert.jsonEncode(http_response);
    }
    return _data;
  }

  Future<List<Voucher_mdl>> fetchVoucher() async {
    if(isloading) {
      _getDatavc0().then((String response){
        setState(() {
          var decode = convert.jsonDecode(response);
          var decodeConvert = convert.jsonDecode(decode["responseBody"]);
          final dtkonten = decodeConvert["voucherlist"];
          final clinic = dtkonten[0]["data"];
          if(clinic.length > 0) {
            isloading = false;
            clinic.forEach((dt) => {
              _mVoucher_mdl.add(new Voucher_mdl(
                shv_id: dt["shv_id"],
                shv_account: dt["shv_account"],
                shv_kode: dt["shv_kode"],
                shv_end: dt["shv_end"],
                shv_createid: dt["shv_createid"],
                shv_status: dt["shv_status"],
                shv_note: dt["shv_note"],
                shv_toname: dt["shv_toname"],
                shv_toemail: dt["shv_toemail"],
                shv_d: dt["shv_d"],
                shv_m: dt["shv_m"],
                shv_y: dt["shv_y"],
                shv_point: dt["shv_point"],
              )),
            });
          }else{
            isloading = false;
          }
        });
      });
    }
    return _mVoucher_mdl;
  }
  Future<List<Voucher1_mdl>> fetchVoucher1() async {
    if(isloading) {
      _getDatavc1().then((String response){
        setState(() {
          var decode = convert.jsonDecode(response);
          var decodeConvert = convert.jsonDecode(decode["responseBody"]);
          final dtkonten = decodeConvert["voucherlist"];
          final clinic = dtkonten[0]["data"];
          if(clinic.length > 0) {
            isloading = false;
            clinic.forEach((dt) => {
              _mVoucher1_mdl.add(new Voucher1_mdl(
                shv_id: dt["shv_id"],
                shv_account: dt["shv_account"],
                shv_kode: dt["shv_kode"],
                shv_end: dt["shv_end"],
                shv_createid: dt["shv_createid"],
                shv_status: dt["shv_status"],
                shv_note: dt["shv_note"],
                shv_toname: dt["shv_toname"],
                shv_toemail: dt["shv_toemail"],
                shv_d: dt["shv_d"],
                shv_m: dt["shv_m"],
                shv_y: dt["shv_y"],
                shv_use_d: dt["shv_use_d"],
                shv_use_m: dt["shv_use_m"],
                shv_use_y: dt["shv_use_y"],
                shv_point: dt["shv_point"],
                shv_tagihan: dt["shv_tagihan"],
              )),
            });
          }else{
            isloading = false;
          }
        });
      });
    }
    return _mVoucher1_mdl;
  }
  Future<List<Voucher2_mdl>> fetchVoucher2() async {
    if(isloading) {
      _getDatavc2().then((String response){
        setState(() {
          var decode = convert.jsonDecode(response);
          var decodeConvert = convert.jsonDecode(decode["responseBody"]);
          final dtkonten = decodeConvert["voucherlist"];
          final clinic = dtkonten[0]["data"];
          if(clinic.length > 0) {
            isloading = false;
            clinic.forEach((dt) => {
              _mVoucher2_mdl.add(new Voucher2_mdl(
                shv_id: dt["shv_id"],
                shv_account: dt["shv_account"],
                shv_kode: dt["shv_kode"],
                shv_end: dt["shv_end"],
                shv_createid: dt["shv_createid"],
                shv_status: dt["shv_status"],
                shv_note: dt["shv_note"],
                shv_toname: dt["shv_toname"],
                shv_toemail: dt["shv_toemail"],
                shv_d: dt["shv_d"],
                shv_m: dt["shv_m"],
                shv_y: dt["shv_y"],
                shv_point: dt["shv_point"],
              )),
            });
          }else{
            isloading = false;
          }
        });
      });
    }
    return _mVoucher2_mdl;
  }

  Widget _voucherlist(){
    return new FutureBuilder(
        future: fetchVoucher(),
        builder: (context, snapshot){
          if (snapshot.hasData && _mVoucher_mdl.length > 0 && _mVoucher_mdl.isNotEmpty){
            isloading = false;
            return new ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                controller: listviewCon,
                itemCount: snapshot.data.length,
                itemBuilder:(BuildContext context, int index) {
                  if(filter == null || filter == ""){
                    showitemCount = _mVoucher_mdl.length;
                    return _voucher(index);
                  }else{
                    if(_mVoucher_mdl.elementAt(index).shv_toname.toLowerCase().contains(filter.toLowerCase()) ||
                        _mVoucher_mdl.elementAt(index).shv_toemail.toLowerCase().contains(filter.toLowerCase()) ||
                        _mVoucher_mdl.elementAt(index).shv_kode.toLowerCase().contains(filter.toLowerCase())
                    ){
                      showitem.add(index);
                      showitemCount = showitem.length;
                      return _voucher(index);
                    }else{
                      showitemCount = 0;
                      return new Container();
                    }
                  }
                }
            );
          }else{
            return new Container();
          }
        }
    );
  }

  Widget _voucherlist1(){
    return new FutureBuilder(
        future: fetchVoucher1(),
        builder: (context, snapshot){
          if (snapshot.hasData && _mVoucher1_mdl.length > 0 && _mVoucher1_mdl.isNotEmpty){
            isloading = false;
            return new ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                controller: listviewCon1,
                itemCount: snapshot.data.length,
                itemBuilder:(BuildContext context, int index) {
                  if(filter == null || filter == ""){
                    showitemCount1 = _mVoucher1_mdl.length;
                    return _voucher1(index);
                  }else{
                    if(_mVoucher1_mdl.elementAt(index).shv_toname.toLowerCase().contains(filter.toLowerCase()) ||
                        _mVoucher1_mdl.elementAt(index).shv_toemail.toLowerCase().contains(filter.toLowerCase()) ||
                        _mVoucher1_mdl.elementAt(index).shv_kode.toLowerCase().contains(filter.toLowerCase())
                    ){
                      showitem1.add(index);
                      showitemCount1 = showitem.length;
                      return _voucher1(index);
                    }else{
                      showitemCount1 = 0;
                      return new Container();
                    }
                  }
                }
            );
          }else{
            return new Container();
          }
        }
    );
  }

  Widget _voucherlist2(){
    return new FutureBuilder(
        future: fetchVoucher2(),
        builder: (context, snapshot){
          if (snapshot.hasData && _mVoucher2_mdl.length > 0 && _mVoucher2_mdl.isNotEmpty){
            isloading = false;
            return new ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                controller: listviewCon2,
                itemCount: snapshot.data.length,
                itemBuilder:(BuildContext context, int index) {
                  if(filter == null || filter == ""){
                    showitemCount2 = _mVoucher2_mdl.length;
                    return _voucher2(index);
                  }else{
                    if(_mVoucher2_mdl.elementAt(index).shv_toname.toLowerCase().contains(filter.toLowerCase()) ||
                        _mVoucher2_mdl.elementAt(index).shv_toemail.toLowerCase().contains(filter.toLowerCase()) ||
                        _mVoucher2_mdl.elementAt(index).shv_kode.toLowerCase().contains(filter.toLowerCase())
                    ){
                      showitem2.add(index);
                      showitemCount2 = showitem.length;
                      return _voucher2(index);
                    }else{
                      showitemCount2 = 0;
                      return new Container();
                    }
                  }
                }
            );
          }else{
            return new Container();
          }
        }
    );
  }

  Widget _voucher(int index){

    var mformat = new DateFormat('MMM');
    DateTime date = new DateTime(int.parse(_mVoucher_mdl.elementAt(index).shv_y),int.parse(_mVoucher_mdl.elementAt(index).shv_m),int.parse(_mVoucher_mdl.elementAt(index).shv_d));


    Color ActivColor;
    if(_active == index){
      ActivColor = widget._Selected_color;
    }else{
      ActivColor = Colors.white;
    }
    return new Container(
      margin: EdgeInsets.only(bottom: 5),
      color: ActivColor,
      child: new InkWell(
          onTap: (){
            showDialog(
              barrierDismissible: true,
              context: context,
              builder: (BuildContext context){
                return voucherpop(_mVoucher_mdl.elementAt(index).shv_kode, _mVoucher_mdl.elementAt(index).shv_y, _mVoucher_mdl.elementAt(index).shv_m);
              },
            );
          },
          child: new Column(
            children: <Widget>[
              new Container(
                padding: EdgeInsets.only(top:5, bottom: 5, left: 16, right: 16),
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Container(
                      width: 40,
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text(
                            _mVoucher_mdl.elementAt(index).shv_d,
                            style: TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.w700
                            ),
                          ),
                          new Text(
                            mformat.format(date),
                            style: TextStyle(
                                fontSize: 10
                            ),
                          ),
                          new Text(
                            _mVoucher_mdl.elementAt(index).shv_y,
                            style: TextStyle(
                                fontSize: 10
                            ),
                          )
                        ],
                      ),
                    ),
                    new Expanded(
                      flex: 2,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(left:10.0, right:10.0),
                              child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Nama: "+_mVoucher_mdl.elementAt(index).shv_toname,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child:new Text(
                                        "Kode Voucher: "+_mVoucher_mdl.elementAt(index).shv_kode,
                                        style: TextStyle(fontWeight: FontWeight.w700,fontFamily: "NeoSansBold", fontSize: 14.0, color: Colors.grey[800]),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Email: "+_mVoucher_mdl.elementAt(index).shv_toemail,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Expired Date: "+_mVoucher_mdl.elementAt(index).shv_end,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                  ]
                              ),
                            ),
                          ]
                      ),
                    ),
                    new Container(
                      height: 40.0,
                      width: 40.0,
                      alignment: Alignment.centerRight,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.all(6.0),
                              alignment: Alignment.centerRight,
                              child: new Icon(
                                FontAwesomeIcons.angleRight,
                                color: Colors.grey,
                                size: 18.0,
                              ),
                            ),
                          ]
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                margin: EdgeInsets.only(top: 5.0),
                width: double.infinity,
                height: 1.0,
                color: Colors.grey[200],
              ),
            ],
          )
      ),
    );
  }

  Widget voucherpop(vc_code, year, month){
    return new SafeArea(
      child: new Scaffold(
        backgroundColor: Colors.black.withOpacity(0.8),
        body: Center(
          child: RotatedBox(
            quarterTurns: 5,
            child: Stack(
              children: <Widget>[
                new Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage("https://suryahusadha.com/images/voucher/"+year+"/"+month+"/"+vc_code+".png?dummy=${new DateTime.now()}"),
                      )
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _voucher1(int index){

    var mformat = new DateFormat('MMM');
    DateTime date = new DateTime(int.parse(_mVoucher1_mdl.elementAt(index).shv_use_y),int.parse(_mVoucher1_mdl.elementAt(index).shv_use_m),int.parse(_mVoucher1_mdl.elementAt(index).shv_use_d));

    final idrFormat = new NumberFormat("#,###");

    return new Container(
      margin: EdgeInsets.only(bottom: 5),
      color: Colors.white,
      child: new InkWell(
          child: new Column(
            children: <Widget>[
              new Container(
                padding: EdgeInsets.only(top:5, bottom: 5, left: 16, right: 16),
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Container(
                      width: 40,
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text(
                            _mVoucher1_mdl.elementAt(index).shv_use_d,
                            style: TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.w700
                            ),
                          ),
                          new Text(
                            mformat.format(date),
                            style: TextStyle(
                                fontSize: 10
                            ),
                          ),
                          new Text(
                            _mVoucher1_mdl.elementAt(index).shv_use_y,
                            style: TextStyle(
                                fontSize: 10
                            ),
                          )
                        ],
                      ),
                    ),
                    new Expanded(
                      flex: 2,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(left:10.0, right:10.0),
                              child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Nama: "+_mVoucher1_mdl.elementAt(index).shv_toname,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child:new Text(
                                        "Kode Voucher: "+_mVoucher1_mdl.elementAt(index).shv_kode,
                                        style: TextStyle(fontWeight: FontWeight.w700,fontFamily: "NeoSansBold", fontSize: 14.0, color: Colors.grey[800]),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Email: "+_mVoucher1_mdl.elementAt(index).shv_toemail,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Tagihan: IDR "+idrFormat.format(int.parse(_mVoucher1_mdl.elementAt(index).shv_tagihan))+",-",
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                  ]
                              ),
                            ),
                          ]
                      ),
                    ),
                    new Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                          padding: EdgeInsets.only(left: 10, top: 5, bottom: 5, right: 10),
                          decoration: BoxDecoration(
                              color: Colors.red,
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(10)
                          ),
                          child: Text(
                            _mVoucher1_mdl.elementAt(index).shv_point+" Pts",
                            style: TextStyle(color: Colors.white, fontSize: 12),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              new Container(
                margin: EdgeInsets.only(top: 5.0),
                width: double.infinity,
                height: 1.0,
                color: Colors.grey[200],
              ),
            ],
          )
      ),
    );
  }

  Widget _voucher2(int index){
    return new Container(
      color: Colors.white,
      margin: EdgeInsets.only(bottom: 5),
      child: new InkWell(
          onTap: (){
            showDialog(
              barrierDismissible: true,
              context: context,
              builder: (BuildContext context){
                return voucherpop(_mVoucher2_mdl.elementAt(index).shv_kode, _mVoucher2_mdl.elementAt(index).shv_y, _mVoucher2_mdl.elementAt(index).shv_m);
              },
            );
          },
          child: new Column(
            children: <Widget>[
              new Container(
                padding: EdgeInsets.only(top:5, bottom: 5, left: 16, right: 16),
                child:new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Container(
                      width: 40.0,
                      height: 40.0,
                      alignment: Alignment.center,
                      child: new Icon(
                        FontAwesomeIcons.ban,
                        color: Colors.red[600],
                        size: 24.0,
                      ),
                    ),
                    new Expanded(
                      flex: 2,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(left:10.0, right:10.0),
                              child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Nama: "+_mVoucher2_mdl.elementAt(index).shv_toname,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child:new Text(
                                        "Kode Voucher: "+_mVoucher2_mdl.elementAt(index).shv_kode,
                                        style: TextStyle(fontWeight: FontWeight.w700,fontFamily: "NeoSansBold", fontSize: 14.0, color: Colors.grey[800]),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Email: "+_mVoucher2_mdl.elementAt(index).shv_toemail,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.topLeft,
                                      child: new Text(
                                        "Expired Date: "+_mVoucher2_mdl.elementAt(index).shv_end,
                                        style: TextStyle(color: Colors.grey[600], fontSize: 12.0),
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        softWrap: false,
                                      ),
                                    ),
                                  ]
                              ),
                            ),
                          ]
                      ),
                    ),
                    new Container(
                      height: 40.0,
                      width: 40.0,
                      alignment: Alignment.centerRight,
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.all(6.0),
                              alignment: Alignment.centerRight,
                              child: new Icon(
                                FontAwesomeIcons.angleRight,
                                color: Colors.grey,
                                size: 18.0,
                              ),
                            ),
                          ]
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                margin: EdgeInsets.only(top: 5.0),
                width: double.infinity,
                height: 1.0,
                color: Colors.grey[200],
              ),
            ],
          )
      ),
    );
  }

}