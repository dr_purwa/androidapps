import 'dart:async';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/dialogue/customDialogue.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:convert' as convert;

class AddVoucher extends StatefulWidget {

  var _akun;
  String _cabang;
  Color _color1;
  Color _color2;

  AddVoucher(List params, akun){
    this._akun = akun;
    params.forEach((dt){
      this._cabang = dt["_cabang"];
      if(_cabang=="group"){
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
      }
      if(_cabang=="denpasar"){
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
      }
      if(_cabang=="ubung"){
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
      }
      if(_cabang=="nusadua"){
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
      }
      if(_cabang=="kmc"){
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
      }
    });
  }

  @override
  _AddVoucher createState() => new _AddVoucher();
}

class _AddVoucher extends State<AddVoucher> with SingleTickerProviderStateMixin{

  GlobalKey<ScaffoldState> scafolt_key = GlobalKey<ScaffoldState>();

  ScrollController listviewCon;
  List showitem = [];
  int showitemCount = 0;

  ScrollController listviewCon1;
  List showitem1 = [];
  int showitemCount1 = 0;

  ScrollController listviewCon2;
  List showitem2 = [];
  int showitemCount2 = 0;

  String filter;
  bool isloading = true;
  int _tabswitch = 0;
  FocusNode _search  = FocusNode();
  TextEditingController editingController = new TextEditingController();

  FocusNode _name = new FocusNode();

  bool _nameerr = false;
  String _nameerrtxt = null;
  bool _emailerr = false;
  String _emailerrtxt = null;
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> dtBack=[];
    dtBack.add({
      "_cabang": widget._cabang,
      "_page": "shcard",
    });
    RouteHelper(context, "LoyaltiPage", dtBack);
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: new Scaffold(
        backgroundColor: Colors.black.withOpacity(0.6),
        body: new WillPopScope(
          onWillPop: _onWillPop,
          child: new Stack(
              children: <Widget>[
                Positioned(
                  top: 80.0,
                  left: 24.0,
                  right: 24.0,
                  child: Column(
                    children: <Widget>[
                      new Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(20.0)),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 10.0,
                              offset: const Offset(0.0, 10.0),
                            ),
                          ],
                        ),
                        padding: EdgeInsets.only(left: 16, right: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Padding(
                              padding: EdgeInsets.only(top: 90),
                            ),
                            new Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Container(
                                  child: new Text(
                                    "Tambah Voucher",
                                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700,color: Colors.grey[600]),
                                  ),
                                ),
                              ],
                            ),
                            new Padding(
                              padding: EdgeInsets.only(top:20),
                            ),
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                new Expanded(
                                  flex: 1,
                                  child: new Container(
                                    child: new Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        new Container(
                                          width: double.infinity,
                                          height: 1.0,
                                          color: Colors.grey[200],
                                        ),
                                        new Container(
                                            margin: EdgeInsets.only(top:20),
                                            child: TextField(
                                              enabled: true,
                                              focusNode: _name,
                                              controller: _nameController,
                                              textInputAction: TextInputAction.next,
                                              cursorColor: Colors.grey[800],
                                              style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                              keyboardType: TextInputType.text,
                                              decoration: InputDecoration(
                                                filled: true,
                                                labelStyle: TextStyle(
                                                  color: Colors.grey[800],
                                                ),
                                                labelText: 'Nama Penerima Voucher',
                                                errorText: _nameerr ? _nameerrtxt : null,
                                                prefixIcon: Icon(
                                                  FontAwesomeIcons.user,
                                                  color: Colors.grey[800],
                                                ),
                                                focusedBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                enabledBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                  borderSide: BorderSide(
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                              ),
                                            )
                                        ),
                                        new Container(
                                            margin: EdgeInsets.only(top:20),
                                            child: TextField(
                                              enabled: true,
                                              controller: _emailController,
                                              textInputAction: TextInputAction.next,
                                              cursorColor: Colors.grey[800],
                                              style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                              keyboardType: TextInputType.emailAddress,
                                              decoration: InputDecoration(
                                                filled: true,
                                                labelStyle: TextStyle(
                                                  color: Colors.grey[800],
                                                ),
                                                labelText: 'Email Penerima Voucher',
                                                errorText: _emailerr ? _emailerrtxt : null,
                                                prefixIcon: Icon(
                                                  FontAwesomeIcons.at,
                                                  color: Colors.grey[800],
                                                ),
                                                focusedBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                enabledBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                  borderSide: BorderSide(
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                              ),
                                            )
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(top: 20),
                                          width: double.infinity,
                                          height: 1.0,
                                          color: Colors.grey[200],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            new Container(
                              margin: EdgeInsets.only(bottom:40, top: 20),
                              child: Column(
                                  children: <Widget>[
                                    new Align(
                                      alignment: Alignment.bottomCenter,
                                      child: RawMaterialButton(
                                        onPressed: (){
                                          bool next = true;
                                          setState(() {
                                            if(_nameController.text.isEmpty){
                                              _nameerr = true;
                                              _nameerrtxt = "Nama tidak boleh kosoneg";
                                              next = false;
                                            }
                                            if(_emailController.text.isEmpty){
                                              _emailerr = true;
                                              _emailerrtxt = "Email tidak boleh kosoneg";
                                              next = false;
                                            }
                                            if(next){
                                              List<Map<String, dynamic>> dtNext=[];
                                              dtNext.add({
                                                "shv_toname": _nameController.text.toString(),
                                                "shv_toemail": _emailController.text.toString(),
                                                "insertid": widget._akun[0]["id"].toString(),
                                                "mailid": widget._akun[0]["email"].toString(),
                                                "nrmshh": widget._akun[0]["nrmshh"],
                                                "nrmub": widget._akun[0]["nrmub"],
                                                "nrmnd": widget._akun[0]["nrmnd"],
                                                "bodVal": widget._akun[0]["bod"],
                                              });
                                              showDialog(
                                                barrierDismissible: false,
                                                context: context,
                                                builder: (BuildContext context) => showNotif("", dtNext),
                                              );
                                            }
                                          });
                                        },
                                        elevation: 0,
                                        padding: EdgeInsets.all(16),
                                        animationDuration: Duration(milliseconds: 500),
                                        fillColor: Colors.red[600],
                                        splashColor: Colors.orange,
                                        highlightColor: Colors.red,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                        ),
                                        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                        child: new Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            new Column(children: <Widget>[
                                              Text(
                                                "Simpan & Kirim",
                                                style: TextStyle(color: Colors.white, fontSize: 18),
                                              ),
                                            ],),
                                            new Padding(padding: EdgeInsets.only(left: 20)),
                                            new Column(children: <Widget>[
                                              Icon(
                                                FontAwesomeIcons.arrowRight,
                                                color: Colors.white,
                                                size: 16,
                                              )
                                            ],),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ]
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: 20,
                  width: MediaQuery.of(context).size.width,
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Container(
                        width: 140.0,
                        height: 140.0,
                        decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white, width: 5),
                          color: widget._color1,
                        ),
                        padding: EdgeInsets.all(5),
                        child: new ClipOval(
                          child: new Image.asset("assets/suri/suri2.png"),
                        ),
                      ),
                    ],
                  ),
                ),
              ]
          ),
        )
      ),
    );
  }

  @override
  Future<String> postData(List<Map<String, dynamic>> dtNext) async {
    var _data;
    var api = new Http_Controller();
    _data = await api.postResponse("https://suryahusadha.com/api/loyalti/addvoucher", dtNext);
    var jsonResponse = convert.jsonEncode(_data);
    Navigator.pop(context);
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) => showNotif(jsonResponse.toString(), null),
    );
    return jsonResponse.toString();
  }

  Widget showNotif(_jsonResponse, List<Map<String, dynamic>> dtNext){
    if(dtNext != null){
      postData(dtNext);
      return new Center(
        child:  CircularProgressIndicator(
          backgroundColor: Colors.grey,
        ),
      );
    }else{
      if(_jsonResponse != "" || _jsonResponse != null ){
        var decode = convert.jsonDecode(_jsonResponse);
        var jsonResponse = convert.jsonDecode(decode["responseBody"]);
        String msg = jsonResponse["addvoucher"][0]["msg"];
        String response = jsonResponse["addvoucher"][0]["response"];
        if(response.toString() == "1"){
          List<Map<String, dynamic>> params=[];
          params.add({
            "_cabang": widget._cabang,
            "_page": "shcard",
          });
          return Dialogue(context, msg, widget._color1, widget._color2, "LoyaltiPage", params);
        }else{
          return Dialogue(context, msg, Colors.red, Colors.red, null, null);
        }
      }else{
        return Dialogue(context, "Mohon maaf, terjadi kesalahan koneksi pada saat daftar, silahkan ulangi beberapa saat lagi!", Colors.red, Colors.red, null, null);
      }
    }
  }


}