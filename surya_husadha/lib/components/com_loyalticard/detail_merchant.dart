import 'dart:async';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/dialogue/customDialogue.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:convert' as convert;

class DetailMerchant extends StatefulWidget {

  String _cabang;
  var _akun;
  var _kidsParam;
  var _mrcdt;
  String _page;
  String _email;
  String _username;
  int _point = 0;

  DetailMerchant(List params, akun){
    this._akun = akun;
    if(akun!=null){
      this._email = akun[0]["email"];
      this._username = akun[0]["username"];
    }
    if(params != null){
      params.forEach((dt){
        if(dt["_page"] != null){
          this._page= dt["_page"];
        }
        if(dt["_cabang"] != null){
          this._cabang = dt["_cabang"];
        }
        if(dt["_mrcdt"] != null){
          this._mrcdt = dt["_mrcdt"];
        }
        if(_page == "kidscard"){
          if(dt["_kidsParam"] != null){
            _kidsParam = dt["_kidsParam"];
            _point = _kidsParam["point"];
          }
        }
      });
    }
  }
  @override
  _DetailMerchant createState() => new _DetailMerchant();
}

class _DetailMerchant extends State<DetailMerchant> {

  var getmember;
  bool isloadingMbr = true;

  @override
  initState() {
    super.initState();
    if(widget._page == "kidscard") {
      isloadingMbr = false;
    }
  }

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> tkrpointt=[];
    if(widget._page == "kidscard"){
      tkrpointt.add({
        "_cabang": widget._cabang,
        "_page": "kidscard",
        "_kidsParam": widget._kidsParam,
      });
    }else{
      tkrpointt.add({
        "_cabang": widget._cabang,
        "_page": "shcard",
      });
    }
    RouteHelper(context, "TukarPoint", tkrpointt);
  }

  @override
  Widget build(BuildContext context) {

    return new SafeArea(
      child: Scaffold(
          body: new WillPopScope(
            onWillPop: _onWillPop,
            child:new Container(
              child: new ListView(
                children: <Widget>[
                  widget._page == "kidscard" ? _kidsStatus(widget._kidsParam) : _LoyaltiStatus(),
                  new Container(
                    child: new Column(
                      children: <Widget>[
                        new Container(
                          height: 1,
                          color: Colors.grey[300],
                        ),
                        new Image.network(
                          SERVER.domain + widget._mrcdt["mrc_image"],
                          width: double.infinity,
                          fit: BoxFit.cover,
                        ),
                        new Container(
                          height: 1,
                          color: Colors.grey[300],
                        ),
                      ],
                    ),
                  ),

                  new Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.only(top: 10.0),
                    color: Colors.white,
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Container(
                          height: 1,
                          color: Colors.grey[300],
                        ),
                        new Container(
                          padding: EdgeInsets.all(16),
                          child: new Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                widget._mrcdt["mrc_name"],
                                softWrap: true,
                                style: new TextStyle(
                                    color: Colors.grey[1000],
                                    fontFamily: "NeoSansBold",
                                    fontSize: 22.0,
                                    fontWeight: FontWeight.w900
                                ),
                              ),
                            ],
                          ),
                        ),
                        new Container(
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: new Column(
                            children: <Widget>[
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                        flex: 1,
                                        child: new Row(
                                          children: <Widget>[
                                            new Icon(
                                              FontAwesomeIcons.solidStar,
                                              size: 12,
                                            ),
                                            new Container(
                                              padding: EdgeInsets.only(left: 5, top: 2),
                                              child: new Text(
                                                  "Point : "+widget._mrcdt["mrc_point"]
                                              ),
                                            ),
                                          ],
                                        )
                                    ),
                                  ],
                                ),
                              ),
                              new Padding(
                                padding: EdgeInsets.only(top: 10.0),
                              ),
                            ],
                          ),
                        ),
                        new Container(
                          height: 1,
                          color: Colors.grey[300],
                        ),
                      ],
                    ),
                  ),
                  widget._mrcdt["mrc_desc"] != "" ? new Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(top: 10),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Container(
                          height: 1,
                          color: Colors.grey[300],
                        ),
                        new Padding(
                          padding: EdgeInsets.only(top: 20.0),
                        ),
                        new Container(
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: new HtmlWidget(
                            widget._mrcdt["mrc_desc"],
                            textStyle: TextStyle(
                                fontSize: 16,
                                height: 1.4,
                                fontFamily: "NeoSansBold",
                                color: Colors.grey[800],
                                wordSpacing: 2.0
                            ),
                            onTapUrl: (url){
                              _launchURL(url);
                            },
                            bodyPadding: EdgeInsets.all(0),
                          ),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(bottom: 40),
                        ),
                        new Container(
                          height: 1,
                          color: Colors.grey[300],
                        ),
                      ],
                    ),
                  ): new Container(),
                  isloadingMbr ? Container(
                    color: Colors.white,
                    margin: EdgeInsets.only(top: 20),
                    padding: EdgeInsets.all(16),
                    child: new Column(
                      children: <Widget>[
                        Text("Mohon menunggu...")
                      ],
                    ),
                  ) : new Container(
                    child: int.parse(widget._point.toString()) < int.parse(widget._mrcdt["mrc_point"])
                        ? new Container(
                        color: Colors.white,
                        margin: EdgeInsets.only(top: 10),
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Container(
                              height: 1,
                              color: Colors.grey[300],
                            ),
                            new Container(
                              padding: EdgeInsets.all(16),
                              child:new Text("Point tidak mencukupi!", style: TextStyle(fontSize: 18, color: Colors.red, fontWeight: FontWeight.w700),),
                            ),
                            new Container(
                              height: 1,
                              color: Colors.grey[300],
                            ),
                          ],
                        )
                    )
                        : new Column(
                      children: <Widget>[
                        new Container(
                          margin: EdgeInsets.only(top: 10),
                          height: 1,
                          color: Colors.grey[300],
                        ),
                        new Container(
                          color: Colors.white,
                          padding: EdgeInsets.only(top: 40, bottom: 40, left: 24, right: 24),
                          child: Column(
                              children: <Widget>[
                                new Container(
                                  height: 1,
                                  color: Colors.grey[200],
                                ),
                                new Align(
                                  alignment: Alignment.bottomCenter,
                                  child: RawMaterialButton(
                                    onPressed: (){
                                      List<Map<String, dynamic>> dtNext=[];
                                      if(widget._page == "kidscard"){
                                        dtNext.add({
                                          "cabang": widget._cabang,
                                          "mrcid": widget._mrcdt["mrc_id"].toString(),
                                          "page": "kidscard",
                                          "cardid" : widget._kidsParam["kc_id"].toString(),
                                          "email" : widget._akun[0]["email"].toString(),
                                          "userid" : widget._akun[0]["id"].toString(),
                                        });
                                        print(dtNext.toString());
                                      }else{
                                        dtNext.add({
                                          "cabang": widget._cabang,
                                          "mrcid": widget._mrcdt["mrc_id"].toString(),
                                          "page": "shcard",
                                          "email" : widget._akun[0]["email"].toString(),
                                          "userid" : widget._akun[0]["id"].toString(),
                                        });
                                      }
                                      showDialog(
                                        barrierDismissible: false,
                                        context: context,
                                        builder: (BuildContext context) => showNotif("", dtNext),
                                      );
                                    },
                                    elevation: 0,
                                    padding: EdgeInsets.all(16),
                                    animationDuration: Duration(milliseconds: 500),
                                    fillColor: Colors.red,
                                    splashColor: Colors.red[300],
                                    highlightColor: Colors.red[500],
                                    highlightElevation: 0,
                                    shape: RoundedRectangleBorder(
                                      side: BorderSide(color: Colors.grey[400]),
                                      borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                    ),
                                    constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                                    child: new Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        new Expanded(
                                          flex: 1,
                                          child: new Row(
                                            mainAxisSize: MainAxisSize.max,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              new Column(children: <Widget>[
                                                Text(
                                                  "Tukar Sekarang ",
                                                  style: TextStyle(color: Colors.white, fontSize: 18),
                                                ),
                                              ],),
                                            ],
                                          ),
                                        ),
                                        Icon(
                                          FontAwesomeIcons.arrowRight,
                                          color: Colors.white,
                                          size: 16,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                new Container(
                                  height: 1,
                                  color: Colors.grey[300],
                                ),
                              ]
                          ),
                        ),
                        new Container(
                          height: 1,
                          color: Colors.grey[300],
                        ),
                      ],
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.only(bottom: 60),
                  ),
                ],
              ),
            ),
          )
      ),
    );
  }

  Widget _kidsStatus(dt){
    if(dt != ""){
      return new Container(
        margin: EdgeInsets.only(bottom:16.0, top:16.0),
        child: new Column(
          children: <Widget>[
            new Container(
              height: 1,
              color: Colors.grey[300],
            ),
            new Container(
                color: Colors.white,
                padding: EdgeInsets.only(top:16.0, bottom:16.0, left: 16.0, right: 16.0),
                child: new Column(
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.only(top:5, bottom: 5),
                      child:new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          new Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              new Container(
                                height: 55.0,
                                width: 55.0,
                                padding: EdgeInsets.all(6.0),
                                decoration: new BoxDecoration(
                                    borderRadius:
                                    new BorderRadius.all(new Radius.circular(100.0)),
                                    color: Colors.lightBlue
                                ),
                                alignment: Alignment.center,
                                child: new Icon(
                                  FontAwesomeIcons.child,
                                  color: Colors.white,
                                  size: 24.0,
                                ),
                              )
                            ],
                          ),
                          new Expanded(
                            flex: 2,
                            child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    padding: EdgeInsets.only(left:10.0, right:10.0),
                                    alignment: Alignment.topLeft,
                                    child: new Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          new Container(
                                            alignment: Alignment.topLeft,
                                            margin: EdgeInsets.only(bottom: 5),
                                            child:new Text(
                                              dt["kc_name"],
                                              style: TextStyle(fontWeight: FontWeight.w700, fontFamily: "NeoSansBold", fontSize: 14.0, color: Colors.grey[800]),
                                              textAlign: TextAlign.left,
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                              softWrap: false,
                                            ),
                                          ),
                                          new Row(
                                            children: <Widget>[
                                              new Container(
                                                margin: EdgeInsets.only(right: 5),
                                                padding: EdgeInsets.only(top: 3, bottom: 5, right: 10, left: 10),
                                                decoration: new BoxDecoration(
                                                    borderRadius:
                                                    new BorderRadius.all(new Radius.circular(5.0)),
                                                    color: Color(0x50FFD180)
                                                ),
                                                child: new Row(
                                                  children: <Widget>[
                                                    new Container(
                                                      alignment: Alignment.center,
                                                      child: new Icon(
                                                        FontAwesomeIcons.creditCard,
                                                        size: 14.0,
                                                      ),
                                                    ),
                                                    new Container(
                                                      alignment: Alignment.center,
                                                      padding: EdgeInsets.only(top:2.0, left: 5),
                                                      child: new Text(
                                                        widget._point.toString() +" Poin",
                                                        style: TextStyle(fontSize: 12.0),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                              new Container(
                                                padding: EdgeInsets.only(top: 3, bottom: 5, right: 10, left: 10),
                                                decoration: new BoxDecoration(
                                                    borderRadius:
                                                    new BorderRadius.all(new Radius.circular(5.0)),
                                                    color: Color(0x50FFD180)
                                                ),
                                                child: new Row(
                                                  children: <Widget>[
                                                    new Container(
                                                      alignment: Alignment.center,
                                                      child: new Icon(
                                                        FontAwesomeIcons.idBadge,
                                                        size: 14.0,
                                                      ),
                                                    ),
                                                    new Container(
                                                      padding: EdgeInsets.only(top: 2, left: 1),
                                                      alignment: Alignment.center,
                                                      child: new Text(
                                                        dt["kc_number"],
                                                        style: TextStyle(color: Colors.grey[800], fontSize: 12.0),
                                                        textAlign: TextAlign.left,
                                                        overflow: TextOverflow.ellipsis,
                                                        maxLines: 1,
                                                        softWrap: false,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ],
                                          )
                                        ]
                                    ),
                                  ),
                                ]
                            ),
                          ),
                          new InkWell(
                            onTap: (){
                              List<Map<String, dynamic>> params=[];
                              params.add({
                                "_cabang": widget._cabang,
                                "_page": "shcard",
                              });
                              RouteHelper(context, "TukarPoint", params);
                            },
                            child: new Container(
                              height: 40.0,
                              width: 40.0,
                              alignment: Alignment.centerRight,
                              child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.centerRight,
                                      child: new Icon(
                                        FontAwesomeIcons.times,
                                        color: Colors.grey,
                                        size: 24.0,
                                      ),
                                    ),
                                  ]
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                )
            ),
            new Container(
              height: 1,
              color: Colors.grey[300],
            ),
          ],
        ),
      );
    }else{
      return new Container();
    }
  }

  Widget imgUser;
  String namaUser = "";
  Widget _LoyaltiStatus(){
    if(int.parse(DateTime.now().hour.toString()) > 00){
      namaUser = "Selamat pagi!";
    }
    if(int.parse(DateTime.now().hour.toString()) > 11){
      namaUser = "Selamat siang!";
    }
    if(int.parse(DateTime.now().hour.toString()) > 14){
      namaUser = "Selamat sore!";
    }
    if(int.parse(DateTime.now().hour.toString()) > 18){
      namaUser = "Selamat malam!";
    }
    if( widget._akun != null) {
      namaUser = "Hi, "+widget._akun[0]["name"];
      String img = SERVER.domain + widget._akun[0]["pp"] +'?dummy=${new DateTime.now()}';
      imgUser = new Container(
        height: 55.0,
        width: 55.0,
        padding: EdgeInsets.all(2.0),
        decoration: new BoxDecoration(
            borderRadius:
            new BorderRadius.all(new Radius.circular(100.0)),
            color: Colors.lightBlue
        ),
        alignment: Alignment.center,
        child: CircleAvatar(
          backgroundImage: NetworkImage(img),
          minRadius: 90,
          maxRadius: 150,
        ),
      );
    } else {
      imgUser = new Container(
        height: 55.0,
        width: 55.0,
        padding: EdgeInsets.all(6.0),
        decoration: new BoxDecoration(
            borderRadius:
            new BorderRadius.all(new Radius.circular(100.0)),
            color: Colors.lightBlue
        ),
        alignment: Alignment.center,
        child: new Icon(
          FontAwesomeIcons.user,
          color: Colors.white,
          size: 24.0,
        ),
      );
    }

    return new Container(
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
              color: Colors.grey[300]
          )
      ),
      margin: EdgeInsets.only(bottom:16.0, top:16.0),
      padding: EdgeInsets.only(top:16.0, bottom:16.0, left: 16.0, right: 16.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          new Container(
            child: new Row(
              children: <Widget>[
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    imgUser,
                  ],
                ),
                new Column(
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.only(left:10.0, right:10.0),
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                child:new Text(
                                  namaUser,
                                  style: TextStyle(fontFamily: "NeoSansBold", fontSize: 16.0),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              new Padding(
                                padding: EdgeInsets.only(top: 5.0),
                              ),
                              _getStatus(),
                            ]
                        ),
                      ),
                    ]
                ),

              ],
            ),
          )
        ],
      ),
    );
  }

  Future<String> get_member() async {
    var _datashcard;
    if(isloadingMbr){
      var api = new Http_Controller();
      List<Map<String, dynamic>> dtNext=[];
      dtNext.add({
        "email": widget._email.toString(),
        "username": widget._username.toString(),
        "card":"shcard",
      });
      var http_response = await api.postResponse("https://suryahusadha.com/api/loyalti/getmember", dtNext);
      _datashcard = convert.jsonEncode(http_response);
      var decode = convert.jsonDecode(_datashcard);
      var decodeConvert = convert.jsonDecode(decode["responseBody"]);
      final dtkonten = decodeConvert["getmember"];
      getmember = dtkonten[0]["msg"];
      if(widget._page == "shcard") {
        setState(() {
          widget._point = dtkonten[0]["point"];
        });
      }
    }
    return _datashcard;
  }

  Widget _getStatus(){
    return new FutureBuilder(
        future: get_member(),
        builder: (context, snapshot){
          if(getmember != null){
            isloadingMbr = false;
            if( getmember == "active"){
              return new Row(
                children: <Widget>[
                  new InkWell(
                    child: new Container(
                      decoration: new BoxDecoration(
                          borderRadius:
                          new BorderRadius.all(new Radius.circular(5.0)),
                          color: Color(0x50FFD180)
                      ),
                      child: new Row(
                        children: <Widget>[
                          new Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(left:10.0),
                            child: new Icon(
                              FontAwesomeIcons.creditCard,
                              size: 16.0,
                            ),
                          ),
                          new Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.all(6.0),
                            child: new Text(
                              widget._point.toString()+" Poin",
                              style: TextStyle(fontSize: 12.0),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(left: 10.0),
                    decoration: new BoxDecoration(
                        borderRadius:
                        new BorderRadius.all(new Radius.circular(5.0)),
                        color: Color(0x50FFD180)
                    ),
                  )
                ],
              );
            }else{
              return new Row(
                children: <Widget>[
                  new Container(
                    margin: EdgeInsets.only(left: 10.0),
                    decoration: new BoxDecoration(
                        borderRadius:
                        new BorderRadius.all(new Radius.circular(5.0)),
                        color: Color(0x50FFD180)
                    ),
                    child: new Row(
                      children: <Widget>[
                        new Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(left:10.0),
                          child:  new Icon(
                            FontAwesomeIcons.idCard,
                            size: 16.0,
                          ),
                        ),
                        new Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.all(6.0),
                          child: new Text(
                            getmember.toString(),
                            style: TextStyle(fontSize: 12.0),
                          ),
                        ),
                        new Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(right:10.0),
                          child:  new Icon(
                            FontAwesomeIcons.arrowRight,
                            size: 10.0,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              );
            }
          }else{
            return new Container();
          }
        }
    );
  }

  _launchURL(url) async {
    await launch(url);
  }

  @override
  Future<String> postData(List<Map<String, dynamic>> dtNext) async {
    var _data;
    var api = new Http_Controller();
    _data = await api.postResponse("https://suryahusadha.com/api/loyalti/tukarpoint", dtNext);
    print("_data: "+_data.toString());
    var jsonResponse = convert.jsonEncode(_data);
    Navigator.pop(context);
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) => showNotif(jsonResponse.toString(), null),
    );
    return jsonResponse.toString();
  }

  Widget showNotif(_jsonResponse, List<Map<String, dynamic>> dtNext){
    if(dtNext != null){
      postData(dtNext);
      return new Center(
        child:  CircularProgressIndicator(
          backgroundColor: Colors.grey,
        ),
      );
    }else{
      if(_jsonResponse != "" || _jsonResponse != null ){
        var decode = convert.jsonDecode(_jsonResponse);
        var jsonResponse = convert.jsonDecode(decode["responseBody"]);
        print("jsonResponse: "+jsonResponse.toString());
        String msg = jsonResponse["tukarpoint"][0]["msg"];
        String response = jsonResponse["tukarpoint"][0]["response"];
        if(response.toString() == "1"){
          List<Map<String, dynamic>> params=[];
          params.add({
            "_cabang": widget._cabang,
            "_page": "shcard",
          });
          return Dialogue(context, msg, WarnaCabang.shh, WarnaCabang.shh2, "group", null);
        }else{
          return Dialogue(context, msg, Colors.red, Colors.red, null, null);
        }
      }else{
        return Dialogue(context, "Mohon maaf, terjadi kesalahan koneksi pada saat daftar, silahkan ulangi beberapa saat lagi!", Colors.red, Colors.red, null, null);
      }
    }
  }

}