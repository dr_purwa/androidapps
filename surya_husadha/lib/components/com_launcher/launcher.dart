import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/database/konten/konten_db.dart';
import 'package:surya_husadha/helper/database/konten/konten_models.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'dart:convert' as convert;

class Launcher extends StatefulWidget {
  @override
  _LauncherState createState() => new _LauncherState();
}

class _LauncherState extends State<Launcher> {

  String infotext;
  var db = new kontenDB();
  var http = new Http_Controller();
  var data=[];
  int dataCount = 0;

  bool inet = false;
  bool isloading = true;

  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  var response;
  var lastupdate;
  var msg;

  @override
  initState(){
    super.initState();
    _initPackageInfo();
    infotext = "Memeriksa konkesi internet...";
    _checkinet().then((responseDt){
      var decode = convert.jsonDecode(responseDt);
      var decodeConvert = convert.jsonDecode(decode["responseBody"]);
      final dtkonten = decodeConvert["konten"];
      response = dtkonten[0]["response"].toString();
      lastupdate = dtkonten[0]["lastupdate"].toString();
      msg = dtkonten[0]["msg"].toString();
      //setState(() {
        infotext = msg;
        inet = true;
      //});
      data = dtkonten[0]["data"];
    }).whenComplete((){
      db.getCount().then((dtCount){
        if(dtCount > 0){
          if(response == "1") {
              if (dtCount != data.length) {
                data.forEach((dt){
                  var Aid = db.getkonten(int.parse(dt["Aid"]));
                  Aid.then((ktn){
                    if(ktn == null){
                      _savedt(db, dt, lastupdate);
                      //setState(() {
                        infotext = "Simpan "+dt["Aid"].toString()+" ke database...";
                        print("ktn: "+ktn.toString());
                      //});
                    }else{
                      //if (dt["Aid"].toString() != ktn.id.toString()) {
                        //_savedt(db, dt, lastupdate);
                        //setState(() {
                          //infotext = "Simpan "+dt["Aid"].toString()+" ke database...";
                          //print(dt["Aid"].toString() +"!="+ ktn.id.toString());
                        //});
                      //}
                      if (dt["Aid"].toString() == ktn.id.toString()) {
                        _updatedt(db, dt, lastupdate);
                        //setState(() {
                          infotext = "Update "+dt["Aid"].toString()+" ke database...";
                          print(dt["Aid"].toString() +"=="+ ktn.id.toString());
                        //});
                      }
                    }
                  });
                  print("dataCount: "+data.length.toString() +" == "+dataCount.toString());
                  if(dataCount == data.length){
                    startLaunching();
                  }
                  dataCount++;
                });
              }else{
                startLaunching();
              }
          }
        }else{
          if(response == "1"){
            data.forEach((dt){
              _savedt(db, dt, lastupdate);
              //setState(() {
                infotext = "Status loading "+dataCount.toString()+"...";
                print("dataCount: "+dataCount.toString() +" == "+ data.length.toString());
              //});
              if(dataCount == data.length){
                startLaunching();
              }
              dataCount++;
            });
          }
        }
      });
    });
  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }


  @override
  Future<String> _checkinet() async {
    var jsonResponse;
    if(isloading){
      isloading = false;
      try {
        //setState(() {
          infotext = "Inisialisasi koneksi ke server...";
        //});
        final result = await InternetAddress.lookup(SERVER.host);
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          var http_response = await http.getResponse("https://suryahusadha.com/api/konten/get_konten");
          jsonResponse = convert.jsonEncode(http_response);
        }
      } on SocketException catch (_){
        //setState(() {
          infotext = 'gagal konkesi ke server...';
        //});
      }
    }
    return jsonResponse;
  }

  _updatedt(db, dt, lastupdate) async {
    await db.saveKonten(new Konten(
      int.parse(dt["Aid"]),
      dt["cat_id"],
      dt["cat_name_id"],
      dt["cat_name_en"],
      dt["image_default"],
      dt["galeries"],
      dt["enabled_en"],
      dt["enabled_id"],
      dt["title_en"],
      dt["title_id"],
      dt["short_content_en"],
      dt["short_content_id"],
      dt["full_content_en"],
      dt["full_content_id"],
      dt["start_date"],
      dt["end_date"],
      dt["metakey_en"],
      dt["metakey_id"],
      dt["created_time"],
      dt["edited_time"],
      dt["name"],
      dt["pp"],
      lastupdate,
    ));
  }

  _savedt(db, dt, lastupdate) async {
    await db.saveKonten(new Konten(
      int.parse(dt["Aid"]),
      dt["cat_id"],
      dt["cat_name_id"],
      dt["cat_name_en"],
      dt["image_default"],
      dt["galeries"],
      dt["enabled_en"],
      dt["enabled_id"],
      dt["title_en"],
      dt["title_id"],
      dt["short_content_en"],
      dt["short_content_id"],
      dt["full_content_en"],
      dt["full_content_id"],
      dt["start_date"],
      dt["end_date"],
      dt["metakey_en"],
      dt["metakey_id"],
      dt["created_time"],
      dt["edited_time"],
      dt["name"],
      dt["pp"],
      lastupdate,
    ));
  }

  startLaunching() async {
    var duration = const Duration(seconds: 1);
    return new Timer(duration, () {
      RouteHelper(context, "group", null);
    });
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(
          body: _loading(),
      ),
    );
  }

  Widget _loading(){
    return Stack(
      children: <Widget>[
        new Center(
          child: new Image.asset(
            "assets/logo/icon.png",
            height: 100.0,
            width: 200.0,
          ),
        ),
        new Positioned(
            bottom: 40,
            child: new Container(
              width: MediaQuery.of(context).size.width,
              child: new Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text("Versi: "+_packageInfo.version, style: TextStyle(fontSize: 12, color: Colors.grey),),
                  Padding(padding: EdgeInsets.only(top: 10),),
                  new Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Center(
                            child: SizedBox(
                                width: 10.0,
                                height: 10.0,
                                child: const CircularProgressIndicator(
                                  backgroundColor: Colors.grey,
                                  strokeWidth: 2,
                                )),
                          ),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(right: 5),),
                      Container(
                        child: Text(
                          infotext,
                          overflow: TextOverflow.ellipsis,
                          softWrap: true,
                          maxLines: 1,
                          style: TextStyle(fontSize: 12, color: Colors.grey),
                        ),
                      ),

                    ],
                  ),
                ],
              ),
            )
        ),
      ],
    );
  }

}