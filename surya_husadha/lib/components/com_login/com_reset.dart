import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/dialogue/customDialogue.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'dart:convert' as convert;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'com_login_anim.dart';

class resetController extends StatefulWidget {

  @override
  _resetController createState() => new _resetController();
}

class _resetController extends State<resetController> with SingleTickerProviderStateMixin, WidgetsBindingObserver{

  AnimLoginList _AnimationList;
  AnimationController _controllerReset;

  String _txterr;
  FocusNode _mail = new FocusNode();
  bool _mailNull = false;
  TextEditingController _mailContr = new TextEditingController();

  FocusNode _mailkonfirm = new FocusNode();
  bool _mailkonfirmNull = false;
  TextEditingController _mailkonfirmContr = new TextEditingController();

  ScrollController listScrollController = ScrollController();
  var isKeyboardOpen = false;
  bool passwordVisible;
  double paddingtop;
  bool _visible;
  double maxheight;
  double maxheight_min = 565;


  @override
  void initState() {
    super.initState();
    paddingtop = 0;
    _visible = true;
    WidgetsBinding.instance.addObserver(this);
    _controllerReset = new AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
    new Future.delayed(const Duration(milliseconds: 200), () {
      _controllerReset.forward();
    });
    passwordVisible = true;
  }

  @override
  void dispose() {
    _controllerReset.dispose();
    WidgetsBinding.instance.removeObserver(this);
    listScrollController.dispose();
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    final value = WidgetsBinding.instance.window.viewInsets.bottom;
    if (value > 0) {
      _onKeyboardChanged(true);
    } else {
      _onKeyboardChanged(false);
    }
  }

  _onKeyboardChanged(bool isVisible) {
    if (isVisible) {
      isKeyboardOpen = true;
      setState(() {
        paddingtop = 50;
        _visible = false;
      });
    } else {
      isKeyboardOpen = false;
      setState(() {
        _visible = true;
        paddingtop = maxheight - maxheight_min;
        if(paddingtop < 0){
          paddingtop = 0;
        }
      });
      listScrollController.jumpTo(listScrollController.position.minScrollExtent);
      FocusScope.of(context).requestFocus(new FocusNode());
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    maxheight = MediaQuery.of(context).size.height;
    setState(() {
      paddingtop = maxheight - maxheight_min;
      if(paddingtop < 0){
        paddingtop = 0;
      }
    });
    _AnimationList = new AnimLoginList(_controllerReset);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new AnimatedBuilder(
        animation: _AnimationList.controller,
        builder: _buildAnimation,
      ),
    );
  }

  Future<bool> _onWillPop(){
    Navigator.pop(context);
  }

  Widget _buildAnimation(BuildContext context, Widget child) {
    return new WillPopScope(
      onWillPop: _onWillPop,
      child: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Image.asset(
              "assets/bg/bg1.png",
              fit: BoxFit.cover,
          ),
          new BackdropFilter(
            filter: new ui.ImageFilter.blur(
              sigmaX: _AnimationList.backdropBlur.value,
              sigmaY: _AnimationList.backdropBlur.value,
            ),
            child: new Container(
              color: Colors.black.withOpacity(0.3),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: ListView(
                      controller: listScrollController,
                      scrollDirection: Axis.vertical,
                      children: <Widget>[
                        _buildInfo(),
                        new Padding(padding: EdgeInsets.only(top: paddingtop)),
                        _buildForm(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }


  Widget _buildAvatar() {
    return new Transform(
      transform: new Matrix4.diagonal3Values(
        _AnimationList.suriSize.value,
        _AnimationList.suriSize.value,
        1.0,
      ),
      alignment: Alignment.center,
      child: new Container(
        width: 80.0,
        height: 80.0,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
          border: new Border.all(color: Colors.white30),
        ),
        margin: const EdgeInsets.only(top: 32.0, right: 16.0),
        padding: const EdgeInsets.all(3.0),
        child: new ClipOval(
          child: new Image.asset(
              "assets/suri/suri1.png"
          ),
        ),
      ),
    );
  }

  Widget _buildInfo() {
    return new Container(
      padding: EdgeInsets.only(left: 24, right: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Row(
              children: <Widget>[
                new Column(
                    children: <Widget>[
                      _buildAvatar(),
                    ]
                ),
                new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 24.0),
                      ),
                      new Text(
                        "RESET PASSWORD",
                        style: new TextStyle(
                          color: Colors.white.withOpacity(_AnimationList.LoginTitle.value),
                          fontWeight: FontWeight.bold,
                          fontSize: 24.0,
                        ),
                      ),
                      new Text(
                        "Silahkan isi formulir dibawah ini!",
                        style: new TextStyle(
                          color: Colors.white.withOpacity(_AnimationList.subTitle.value),
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ]
                )
              ]
          ),

          new Container(
            color: Colors.white.withOpacity(0.85),
            margin: const EdgeInsets.symmetric(vertical: 16.0),
            width: _AnimationList.dividerWidth.value,
            height: 1.0,
          ),
          new Visibility(
            visible: _visible,
            child: Text(
              "Untuk melakukan reset password silahkan masukkan akun email Anda untuk permohonan reset password akun Anda!",
              style: new TextStyle(
                color: Colors.white.withOpacity(_AnimationList.desc.value),
                height: 1.4,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildForm() {
    return new Padding(
      padding: const EdgeInsets.only(left: 24.0, right: 24.0),
      child: new Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FractionalTranslation(
            translation: _AnimationList.Translation_1.value,
            child: new Container(
                margin: EdgeInsets.only(top:20),
                child: TextField(
                  textInputAction: TextInputAction.next,
                  onEditingComplete: () => FocusScope.of(context).requestFocus(_mailkonfirm),
                  cursorColor: Colors.white,
                  obscureText: false,
                  style: TextStyle(color: Colors.white),
                  keyboardType: TextInputType.emailAddress,
                  controller: _mailContr,
                  decoration: InputDecoration(
                    errorText: _mailNull ? _txterr : null,
                    filled: true,
                    fillColor: Colors.black.withOpacity(0.6),
                    labelStyle: TextStyle(
                      color: Colors.white,
                    ),
                    labelText: 'Email',
                    prefixIcon: Icon(
                      FontAwesomeIcons.at,
                      color: Colors.white,
                    ),
                    enabledBorder: const OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                      borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                      borderSide: BorderSide(
                        color: Colors.white,
                      ),
                    ),
                  ),
                )
            ),
          ),
          FractionalTranslation(
            translation: _AnimationList.Translation_2.value,
            child: new Container(
                margin: EdgeInsets.only(top:20),
                child: TextField(
                  focusNode: _mailkonfirm,
                  cursorColor: Colors.white,
                  obscureText: false,
                  style: TextStyle(color: Colors.white),
                  keyboardType: TextInputType.emailAddress,
                  controller: _mailkonfirmContr,
                  decoration: InputDecoration(
                    errorText: _mailkonfirmNull ? _txterr : null,
                    filled: true,
                    fillColor: Colors.black.withOpacity(0.6),
                    labelStyle: TextStyle(
                      color: Colors.white,
                    ),
                    labelText: 'Konfirmasi Email',
                    prefixIcon: Icon(
                      FontAwesomeIcons.at,
                      color: Colors.white,
                    ),
                    enabledBorder: const OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                      borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                      borderSide: BorderSide(
                        color: Colors.white,
                      ),
                    ),
                    //hintText: 'Enter your password',
                    hintStyle: TextStyle(color: Colors.white),
                  ),
                )
            ),
          ),
          new Padding(padding: EdgeInsets.only(top: 60)),
          FractionalTranslation(
            translation: _AnimationList.Translation_4.value,
            child: new Container(
              child: Column(
                  children: <Widget>[
                    new Align(
                      alignment: Alignment.bottomCenter,
                      child: RawMaterialButton(
                        onPressed: (){
                          bool reset = true;
                          setState(() {
                            _mailNull = false;
                            _mailkonfirmNull = false;
                            if(_mailContr.text.isEmpty){
                              reset = false;
                              _mailNull = true;
                            }
                            if(_mailkonfirmContr.text.isEmpty){
                              reset = false;
                              _mailkonfirmNull = true;
                            }
                            if(_mailContr.text.isNotEmpty && _mailkonfirmContr.text.isNotEmpty ){
                              if(_mailContr.text != _mailkonfirmContr.text){
                                reset = false;
                                _mailNull = true;
                                _mailkonfirmNull = true;
                                _txterr = "*Email dan Konfirmasi Email tidak sama!";
                              }
                            }
                            if(reset){
                              List<Map<String, dynamic>> dtNext=[];
                              dtNext.add({
                                "email": _mailContr.text.toString(),
                              });
                              showDialog(
                                barrierDismissible: false,
                                context: context,
                                builder: (BuildContext context) => showNotif(context, dtNext),
                              );
                            }
                          });
                        },
                        elevation: 16,
                        padding: EdgeInsets.all(16),
                        animationDuration: Duration(milliseconds: 500),
                        fillColor: Colors.red[600],
                        splashColor: Colors.orange,
                        highlightColor: Colors.red,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Column(children: <Widget>[
                              Text(
                                "Reset Sekarang",
                                style: TextStyle(color: Colors.white, fontSize: 18),
                              ),
                            ],),
                            new Padding(padding: EdgeInsets.only(left: 20)),
                            new Column(children: <Widget>[
                              Icon(
                                FontAwesomeIcons.arrowRight,
                                color: Colors.white,
                                size: 16,
                              )
                            ],),
                          ],
                        ),
                      ),
                    ),
                  ]
              ),
            ),
          ),
          new Padding(padding: EdgeInsets.only(bottom:20)),
        ],
      ),
    );
  }

  @override
  Future<String> _postData(List<Map<String, dynamic>> dtNext) async {
    var _data;
    var api = new Http_Controller();
    _data = await api.postResponse("https://suryahusadha.com/api/akun/forgot", dtNext);
    var jsonResponse = convert.jsonEncode(_data);
    return jsonResponse.toString();
  }

  Widget showNotif(BuildContext context, List<Map<String, dynamic>> dtNext){
    return FutureBuilder(
        future: _postData(dtNext),
        builder: (context, snapshot){
          if(snapshot.hasData && (snapshot.connectionState == ConnectionState.done)){
            print(snapshot.data.toString());
            var decode = convert.jsonDecode(snapshot.data);
            var jsonResponse = convert.jsonDecode(decode["responseBody"]);
            String msg = jsonResponse["forgot"][0]["msg"];
            String response = jsonResponse["forgot"][0]["response"];
            if(response == "1"){
              return Dialogue(context, msg, WarnaCabang.shh, WarnaCabang.shh2, "group", null);
            }else{
              return Dialogue(context, msg, Colors.red, Colors.red, null, null);
            }
          }else{
            return new Container(
              width: MediaQuery.of(context).size.width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(
                    backgroundColor: Colors.grey,
                  ),
                ],
              ),
            );
          }
        }
    );
  }
}