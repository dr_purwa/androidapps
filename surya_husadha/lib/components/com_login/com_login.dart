import 'dart:io';
import 'dart:ui' as ui;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:surya_husadha/components/com_login/com_daftarakun.dart';
import 'package:surya_husadha/components/com_login/com_reset.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/database/akun/akun_db.dart';
import 'package:surya_husadha/helper/database/akun/akun_models.dart';
import 'package:surya_husadha/helper/dialogue/customDialogue.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/helper/transition/slide_route.dart';
import 'package:surya_husadha/pages/widgetpage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:convert' as convert;
import 'com_login_anim.dart';

class LoginController extends StatefulWidget {

  String _cabang;
  LoginController(Cabang){
      this._cabang = Cabang;
  }

  @override
  _LoginController createState() => new _LoginController();
}

class _LoginController extends State<LoginController> with SingleTickerProviderStateMixin, WidgetsBindingObserver{

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();

  String name;
  String email;
  String imageUrl;

  FocusNode _paswd = new FocusNode();
  bool next = true;
  bool _paswdNull = false;
  bool _userNull = false;
  TextEditingController _userController = new TextEditingController();
  TextEditingController _paswdController = new TextEditingController();
  AnimLoginList animationList;
  AnimationController _controllerlogin;

  ScrollController listScrollController = ScrollController();
  bool passwordVisible;
  double paddingtop;
  bool _visible;
  double maxheight;

  @override
  void initState() {
    super.initState();
    paddingtop = 0;
    _visible = true;
    WidgetsBinding.instance.addObserver(this);
    _controllerlogin = new AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
    new Future.delayed(const Duration(milliseconds: 200), () {
      _controllerlogin.forward();
    });
    passwordVisible = true;
  }

  @override
  void dispose() {
    _controllerlogin.dispose();
    WidgetsBinding.instance.removeObserver(this);
    listScrollController.dispose();
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    final value = WidgetsBinding.instance.window.viewInsets.bottom;
    if (value > 0) {
      _onKeyboardChanged(true);
    } else {
      _onKeyboardChanged(false);
    }
  }

  _onKeyboardChanged(bool isVisible) {
    if (isVisible) {
      setState(() {
        paddingtop = 0;
        _visible = false;
      });
    } else {
      setState(() {
        _visible = true;
        if(next){
          paddingtop = maxheight * (1/11);
        }else{
          paddingtop = maxheight * (1/11);
        }
        if(paddingtop < 0){
          paddingtop = 0;
        }
      });
      listScrollController.jumpTo(listScrollController.position.minScrollExtent);
      FocusScope.of(context).requestFocus(new FocusNode());
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    animationList = new AnimLoginList(_controllerlogin);
    maxheight = MediaQuery.of(context).size.height;
    setState(() {
      paddingtop = maxheight * (1/11);
      if(paddingtop < 0){
        paddingtop = 0;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new AnimatedBuilder(
        animation: animationList.controller,
        builder: _buildAnimation,
      ),
    );
  }

  Future<bool> _onWillPop(){
    List<Map<String, dynamic>> dtNext=[];
    dtNext.add({"_cabang": widget._cabang});
    RouteHelper(context, widget._cabang, dtNext);
  }

  Widget _buildAnimation(BuildContext context, Widget child) {
    return new WillPopScope(
      onWillPop: _onWillPop,
      child: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Image.asset(
              "assets/bg/bg1.png",
              fit: BoxFit.cover,
          ),
          new BackdropFilter(
            filter: new ui.ImageFilter.blur(
              sigmaX: animationList.backdropBlur.value,
              sigmaY: animationList.backdropBlur.value,
            ),
            child: new Container(
              color: Colors.black.withOpacity(0.3),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: ListView(
                      controller: listScrollController,
                      scrollDirection: Axis.vertical,
                      children: <Widget>[
                        _buildInfo(),
                        new Padding(padding: EdgeInsets.only(top: paddingtop)),
                        _buildForm(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }


  Widget _buildAvatar() {
    return new Transform(
      transform: new Matrix4.diagonal3Values(
        animationList.suriSize.value,
        animationList.suriSize.value,
        1.0,
      ),
      alignment: Alignment.center,
      child: new Container(
        width: 80.0,
        height: 80.0,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
          border: new Border.all(color: Colors.white30),
        ),
        margin: const EdgeInsets.only(top: 32.0, right: 16.0),
        padding: const EdgeInsets.all(3.0),
        child: new ClipOval(
          child: new Image.asset(
              "assets/suri/suri2.png"
          ),
        ),
      ),
    );
  }

  Widget _buildInfo() {
    return new Padding(
      padding: const EdgeInsets.only(left: 24.0, right: 24.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Row(
              children: <Widget>[
                new Column(
                    children: <Widget>[
                      _buildAvatar(),
                    ]
                ),
                new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 24.0),
                      ),
                      new Text(
                        "LOGIN AKUN",
                        style: new TextStyle(
                          color: Colors.white.withOpacity(animationList.LoginTitle.value),
                          fontWeight: FontWeight.bold,
                          fontSize: 24.0,
                        ),
                      ),
                      new Text(
                        "Silahkan login ke profil akun Anda!",
                        style: new TextStyle(
                          color: Colors.white.withOpacity(animationList.subTitle.value),
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ]
                )
              ]
          ),

          new Container(
            color: Colors.white.withOpacity(0.85),
            margin: const EdgeInsets.symmetric(vertical: 16.0),
            width: animationList.dividerWidth.value,
            height: 1.0,
          ),
          new Visibility(
            visible: _visible,
            child:  new Text(
              "Untuk memudahkan Anda dalam pengunaan Aplikasi Surya Husadha Hospital, silahkan login ke akun profil Anda, dan nikmati segala kemudahan dalam menggunakannya!",
              style: new TextStyle(
                color: Colors.white.withOpacity(animationList.desc.value),
                height: 1.4,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildForm() {
    return new Padding(
      padding: const EdgeInsets.only(left: 24.0, right: 24.0),
      child: new Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FractionalTranslation(
            translation: animationList.Translation_1.value,
            child: new Container(
                margin: EdgeInsets.only(top:20),
                child: TextField(
                  keyboardType: TextInputType.emailAddress,
                  textInputAction: TextInputAction.next,
                  controller: _userController,
                  onEditingComplete: () => FocusScope.of(context).requestFocus(_paswd),
                  cursorColor: Colors.white,
                  obscureText: false,
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.black.withOpacity(0.6),
                    labelStyle: TextStyle(
                      color: Colors.white,
                    ),
                    labelText: 'Username / Email',
                    errorText: _userNull ? "" : null,
                    prefixIcon: Icon(
                      FontAwesomeIcons.user,
                      color: Colors.white,
                    ),
                    enabledBorder: const OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                      borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                      borderSide: BorderSide(
                        color: Colors.white,
                      ),
                    ),
                  ),
                )
            ),
          ),
          FractionalTranslation(
            translation: animationList.Translation_2.value,
            child: new Container(
                margin: EdgeInsets.only(top:20),
                child: TextField(
                  focusNode: _paswd,
                  controller: _paswdController,
                  cursorColor: Colors.white,
                  obscureText: passwordVisible,
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.black.withOpacity(0.6),
                    labelStyle: TextStyle(
                      color: Colors.white,
                    ),
                    labelText: 'Password',
                    errorText: _paswdNull ? "" : null,
                    prefixIcon: Icon(
                      FontAwesomeIcons.key,
                      color: Colors.white,
                    ),
                    enabledBorder: const OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                      borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                      borderSide: BorderSide(
                        color: Colors.white,
                      ),
                    ),
                    //hintText: 'Enter your password',
                    hintStyle: TextStyle(color: Colors.white),
                    suffixIcon: IconButton(
                      icon: Icon(
                        passwordVisible
                            ? Icons.visibility
                            : Icons.visibility_off,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        setState(() {
                          passwordVisible = !passwordVisible;
                        });
                      },
                    ),
                  ),
                )
            ),
          ),
          new Padding(padding: EdgeInsets.only(top:20)),
          FractionalTranslation(
            translation: animationList.Translation_3.value,
            child: new Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Align(
                      alignment: Alignment.centerLeft,
                      child: RawMaterialButton(
                        onPressed: (){
                          onCardTapped(context, resetController());
                        },
                        //elevation: 16,
                        animationDuration: Duration(milliseconds: 500),
                        splashColor: Colors.orange,
                        child: new Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                          new Column(children: <Widget>[
                            new Icon(
                              FontAwesomeIcons.handPointRight,
                              color: Colors.white,
                              size: 12,
                            )
                          ]),
                          new Padding(padding: EdgeInsets.only(right: 10)),
                          new Column(children: <Widget>[
                            Text(
                              "Lupa Password? Reset sekarang!",
                              style: TextStyle(color: Colors.white, fontSize: 12),
                            ),
                          ]),
                        ]),

                      ),
                    ),
                  ]
              ),
            ),
          ),
          new Padding(padding: EdgeInsets.only(top:20)),
          FractionalTranslation(
            translation: animationList.Translation_4.value,
            child: new Container(
              child: Column(
                  children: <Widget>[
                    new Align(
                      alignment: Alignment.bottomCenter,
                      child: RawMaterialButton(
                        onPressed: (){
                          setState(() {
                            if(_userController.text.isEmpty){
                              _userNull = true;
                              next = false;
                            }
                            if(_paswdController.text.isEmpty){
                              _paswdNull = true;
                              next = false;
                            }
                          });
                          if(next){
                            List<Map<String, dynamic>> dtNext=[];
                            dtNext.add({
                              "username": _userController.text.toString(),
                              "password": _paswdController.text.toString(),
                            });
                            showDialog(
                              barrierDismissible: false,
                              context: context,
                              builder: (BuildContext context) => showNotif("", dtNext),
                            );
                          }else{
                            paddingtop = maxheight * (1/11);
                          }
                        },
                        elevation: 16,
                        padding: EdgeInsets.all(16),
                        animationDuration: Duration(milliseconds: 500),
                        fillColor: Colors.red[600],
                        splashColor: Colors.orange,
                        highlightColor: Colors.red,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Column(children: <Widget>[
                              Text(
                                "Login Sekarang",
                                style: TextStyle(color: Colors.white, fontSize: 18),
                              ),
                            ],),
                            new Padding(padding: EdgeInsets.only(left: 20)),
                            new Column(children: <Widget>[
                              Icon(
                                FontAwesomeIcons.arrowRight,
                                color: Colors.white,
                                size: 16,
                              )
                            ],),
                          ],
                        ),
                      ),
                    ),
                  ]
              ),
            ),
          ),
          new Padding(padding: EdgeInsets.only(top:20)),
          new FractionalTranslation(
            translation: animationList.Translation_3.value,
            child:Center(
              child: Text(
                "Atau",
                style: TextStyle(
                    color: Colors.white
                ),
              ),
            ),
          ),
          new Padding(padding: EdgeInsets.only(top:20)),
          _googlebutton(),
          FractionalTranslation(
            translation: animationList.Translation_4.value,
            child: new Container(
              child: Column(
                  children: <Widget>[
                    new Align(
                      alignment: Alignment.bottomCenter,
                      child: RawMaterialButton(
                        onPressed: (){
                          onCardTapped(context, DaftarAkun());
                        },
                        elevation: 16,
                        animationDuration: Duration(milliseconds: 500),
                        splashColor: Colors.orange,
                        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                        child: Text(
                            "Tidak punya akun? Daftar sekarang!",
                          style: TextStyle(color: Colors.white, fontSize: 12),
                        ),
                      ),
                    ),
                  ]
              ),
            ),
          ),
          new Padding(padding: EdgeInsets.only(bottom:20)),
        ],
      ),
    );
  }

  var eror;

  @override
  Future<String> postData(List<Map<String, dynamic>> dtNext) async {
    var jsonResponse;
    eror = false;
    try {
      final result = await InternetAddress.lookup(SERVER.host);
      //print("result: "+result.toString());
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        var api = new Http_Controller();
        var _data = await api.postResponse("https://suryahusadha.com/api/akun/login", dtNext);
        jsonResponse = convert.jsonEncode(_data);
        Navigator.pop(context);
        showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) => showNotif(jsonResponse.toString(), null),
        );
      }else{
        eror = true;
        jsonResponse = "Gagal koneksi dengan server";
      }
    } on SocketException catch (e){
      eror = true;
      jsonResponse =  e.toString();
    }
    return jsonResponse.toString();
  }

  Widget showNotif(_jsonResponse, List<Map<String, dynamic>> dtNext){
    if(dtNext != null){
      postData(dtNext);
      return new Center(
        child:  CircularProgressIndicator(
          backgroundColor: Colors.grey,
        ),
      );
    }else{
      //print("eror:"+eror.toString());
      //print("_jsonResponse:"+_jsonResponse.toString());
      if(eror){
        return Dialogue(context, _jsonResponse, Colors.red, Colors.red, null, null);
      }else{
        if(_jsonResponse != "" || _jsonResponse != null ){
          var decode = convert.jsonDecode(_jsonResponse);
          var jsonResponse = convert.jsonDecode(decode["responseBody"]);
          String msg = jsonResponse["akun"][0]["msg"];
          String response = jsonResponse["akun"][0]["response"];
          if(response == "1"){
            var data = jsonResponse["akun"][0]["data"];
            var db = new akunDB();
            db.deleteAkun();
            data.forEach((dt) async {
              var passwd = dt["passwd"].toString().length;
              await db.saveAkun(new Akun(
                int.parse(dt["id"]),
                dt["group_id"],
                dt["pp"],
                dt["name"],
                dt["bod"],
                dt["bodcity"],
                dt["bpjs_id"],
                dt["nrmshh"],
                dt["nrmub"],
                dt["nrmnd"],
                dt["nrmkmc"],
                dt["nik"],
                dt["username"],
                passwd.toString(),
                dt["email"],
                dt["phone"],
                dt["address"],
                dt["agama"],
                dt["pekerjaan"],
                dt["pendidikan"],
                dt["ibu"],
                dt["idCardtype"],
                dt["NoidCard"],
                dt["nasionality"],
                dt["reg_time"],
                dt["marital"],
                dt["gender"],
                dt["enabled"],
                dt["activasi_link"],
                dt["nonactive_link"],
                dt["reset_link"],
                dt["note"],
                dt["defaultlang"],
                dt["edittime"],
                dt["isfirsttime"],
              ));
            });
            return Dialogue(context, msg, WarnaCabang.shh, WarnaCabang.shh2, "group", null);
          }else{
            return Dialogue(context, msg, Colors.red, Colors.red, null, null);
          }
        }else{
          return Dialogue(context, "Mohon maaf, terjadi kesalahan koneksi pada saat login, silahkan ulangi beberapa saat lagi!", Colors.red, Colors.red, null, null);
        }
      }
    }
  }

  onCardTapped(context, Widget widget) {
    return Navigator.push(context, SlideRightRoute(page: Fullpage(widgetpage: widget, pagename: "LoginPage",)));//ok
  }

  signInWithGoogle() async {
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
    await googleSignInAccount.authentication;
    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );
    //final FirebaseUser user = await _auth.signInWithCredential(credential) as FirebaseUser;
    final FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
    assert(user.email != null);
    assert(user.displayName != null);
    assert(user.photoUrl != null);
    name = user.displayName;
    email = user.email;
    imageUrl = user.photoUrl;
    if (name.contains(" ")) {
      name = name.substring(0, name.indexOf(" "));
    }
    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);
    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);
    //print('currentUser: $currentUser');
    if (currentUser != null){
      List<Map<String, dynamic>> dtNext=[];
      dtNext.add({
        "name": name.toString(),
        "username": email.toString(),
        "uid": currentUser.uid,
      });
      //print("dtNext:" +dtNext.toString());
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => showNotif("", dtNext),
      );
    }
  }

  Widget _googlebutton() {
    return FractionalTranslation(
      translation: animationList.Translation_4.value,
      child: RawMaterialButton(
        fillColor: Colors.white,
        splashColor: Colors.grey,
        onPressed: () {
          signOutGoogle();
          signInWithGoogle();
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        highlightElevation: 0,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image(image: AssetImage("assets/logo/glogo.png"), height: 35.0),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  'Sign in with Google',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.grey,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void signOutGoogle() async{
    await googleSignIn.signOut();
  }

}