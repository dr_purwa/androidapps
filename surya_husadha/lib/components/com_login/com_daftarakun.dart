import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/database/konten/konten_db.dart';
import 'package:surya_husadha/helper/database/konten/konten_models.dart';
import 'package:surya_husadha/helper/dialogue/customDialogue.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'com_login_anim.dart';
import 'dart:convert' as convert;

class DaftarAkun extends StatefulWidget {
  @override
  _DaftarAkun createState() => new _DaftarAkun();
}

class _DaftarAkun extends State<DaftarAkun> with SingleTickerProviderStateMixin, WidgetsBindingObserver{

  AnimLoginList _animationList;
  AnimationController _controllerDaftar;

  FocusNode _nama = new FocusNode();
  bool _namaNull = false;
  TextEditingController _namaContr = new TextEditingController();

  FocusNode _mail = new FocusNode();
  bool _mailNull = false;
  TextEditingController _mailContr = new TextEditingController();

  FocusNode _username = new FocusNode();
  bool _usernameNull = false;
  TextEditingController _usernameContr = new TextEditingController();

  FocusNode _paswd = new FocusNode();
  bool _paswdNull = false;
  String _paswderr = "";
  TextEditingController _paswdContr = new TextEditingController();

  FocusNode _paswdkonfirm = new FocusNode();
  bool _paswdkonfirmNull = false;
  TextEditingController _paswdkonfirmContr = new TextEditingController();

  bool _setuju = false;
  bool _setujuErr = false;

  ScrollController listScrollController = ScrollController();
  var isKeyboardOpen = false;
  bool passwordVisible;
  double paddingtop;
  bool _visible;
  double maxheight;
  double maxheight_min = 800;

  @override
  void initState() {
    paddingtop = 0;
    _visible = true;
    WidgetsBinding.instance.addObserver(this);
    passwordVisible = true;
    _controllerDaftar = new AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
    new Future.delayed(const Duration(milliseconds: 200), () {
      _controllerDaftar.forward();
    });
    super.initState();
  }

  @override
  void dispose(){
    WidgetsBinding.instance.removeObserver(this);
    _controllerDaftar.dispose();
    listScrollController.dispose();
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    final value = WidgetsBinding.instance.window.viewInsets.bottom;
    if (value > 0) {
        _onKeyboardChanged(true);
    } else {
      _onKeyboardChanged(false);
    }
  }

  _onKeyboardChanged(bool isVisible) {
    if (isVisible) {
      isKeyboardOpen = true;
      setState(() {
        paddingtop = 0;
        _visible = false;
      });
    } else {
      isKeyboardOpen = false;
      setState(() {
        _visible = true;
        paddingtop = maxheight - maxheight_min;
        if(paddingtop < 0){
          paddingtop = 0;
        }
      });
      listScrollController.jumpTo(listScrollController.position.minScrollExtent);
      FocusScope.of(context).requestFocus(new FocusNode());
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    maxheight = MediaQuery.of(context).size.height;
    setState(() {
      paddingtop = maxheight - maxheight_min;
      if(paddingtop < 0){
        paddingtop = 0;
      }
    });
    _animationList = new AnimLoginList(_controllerDaftar);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new AnimatedBuilder(
        animation: _animationList.controller,
        builder: _buildAnimation,
      ),
    );
  }

  Future<bool> _onWillPop(){
    Navigator.pop(context);
  }

  Widget _buildAnimation(BuildContext context, Widget child) {
    return new WillPopScope(
      onWillPop: _onWillPop,
      child: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Image.asset(
            "assets/bg/bg1.png",
            fit: BoxFit.cover,
          ),
          new BackdropFilter(
            filter: new ui.ImageFilter.blur(
              sigmaX: _animationList.backdropBlur.value,
              sigmaY: _animationList.backdropBlur.value,
            ),
            child: new Container(
              color: Colors.black.withOpacity(0.3),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: ListView(
                      controller: listScrollController,
                      scrollDirection: Axis.vertical,
                      children: <Widget>[
                        _buildInfo(),
                        new Padding(padding: EdgeInsets.only(top: paddingtop)),
                        _buildForm(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAvatar() {
    return new Transform(
      transform: new Matrix4.diagonal3Values(
        _animationList.suriSize.value,
        _animationList.suriSize.value,
        1.0,
      ),
      alignment: Alignment.center,
      child: new Container(
        width: 80.0,
        height: 80.0,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
          border: new Border.all(color: Colors.white30),
        ),
        margin: const EdgeInsets.only(top: 32.0, right: 16.0),
        padding: const EdgeInsets.all(3.0),
        child: new ClipOval(
          child: new Image.asset(
              "assets/suri/suri1.png"
          ),
        ),
      ),
    );
  }

  Widget _buildInfo() {
    return new Padding(
      padding: const EdgeInsets.only(left: 24.0, right: 24.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Row(
              children: <Widget>[
                new Column(
                    children: <Widget>[
                      _buildAvatar(),
                    ]
                ),
                new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 24.0),
                      ),
                      new Text(
                        "DAFTAR AKUN",
                        style: new TextStyle(
                          color: Colors.white.withOpacity(_animationList.LoginTitle.value),
                          fontWeight: FontWeight.bold,
                          fontSize: 24.0,
                        ),
                      ),
                      new Text(
                        "Silahkan isi dan lengkapi formulir!",
                        style: new TextStyle(
                          color: Colors.white.withOpacity(_animationList.subTitle.value),
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ]
                )
              ]
          ),
          new Container(
            color: Colors.white.withOpacity(0.85),
            margin: const EdgeInsets.symmetric(vertical: 16.0),
            width: _animationList.dividerWidth.value,
            height: 1.0,
          ),
          new Visibility(
            visible: _visible,
            child: Text(
              "Menjadi member dalam aplikasi kami memudahkan Anda dalam mendapatkan layanan dan fasilitas online pada aplikasi ini!",
              style: new TextStyle(
                color: Colors.white.withOpacity(_animationList.desc.value),
                height: 1.4,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildForm() {
    return new Padding(
      padding: const EdgeInsets.only(left: 24.0, right: 24.0),
      child: new Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FractionalTranslation(
              translation: _animationList.Translation_1.value,
              child: new Container(
                child: new Column(
                  children: <Widget>[
                    new Container(
                        child: TextField(
                          textInputAction: TextInputAction.next,
                          onEditingComplete: () => _next(_mail, ""),
                          cursorColor: Colors.white,
                          obscureText: false,
                          style: TextStyle(color: Colors.white),
                          controller: _namaContr,
                          decoration: InputDecoration(
                            errorText: _namaNull ? "" : null,
                            filled: true,
                            fillColor: Colors.black.withOpacity(0.6),
                            labelStyle: TextStyle(
                              color: Colors.white,
                            ),
                            labelText: 'Nama Lengkap',
                            prefixIcon: Icon(
                              FontAwesomeIcons.idBadge,
                              color: Colors.white,
                            ),
                            enabledBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                              borderSide: BorderSide(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        )
                    ),
                    new Container(
                        margin: EdgeInsets.only(top:20),
                        child: TextField(
                          focusNode: _mail,
                          keyboardType: TextInputType.emailAddress,
                          textInputAction: TextInputAction.next,
                          onEditingComplete: () => _next(_username, ""),
                          cursorColor: Colors.white,
                          obscureText: false,
                          style: TextStyle(color: Colors.white),
                          controller: _mailContr,
                          decoration: InputDecoration(
                            errorText: _mailNull ? "" : null,
                            filled: true,
                            fillColor: Colors.black.withOpacity(0.6),
                            labelStyle: TextStyle(
                              color: Colors.white,
                            ),
                            labelText: 'Email',
                            prefixIcon: Icon(
                              FontAwesomeIcons.at,
                              color: Colors.white,
                            ),
                            enabledBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                              borderSide: BorderSide(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        )
                    ),
                    new Container(
                        margin: EdgeInsets.only(top:20),
                        child: TextField(
                          focusNode: _username,
                          textInputAction: TextInputAction.next,
                          onEditingComplete: () => _next(_paswd, ""),
                          cursorColor: Colors.white,
                          obscureText: false,
                          style: TextStyle(color: Colors.white),
                          controller: _usernameContr,
                          decoration: InputDecoration(
                            errorText: _usernameNull ? "" : null,
                            filled: true,
                            fillColor: Colors.black.withOpacity(0.6),
                            labelStyle: TextStyle(
                              color: Colors.white,
                            ),
                            labelText: 'Username',
                            prefixIcon: Icon(
                              FontAwesomeIcons.user,
                              color: Colors.white,
                            ),
                            enabledBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                              borderSide: BorderSide(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        )
                    ),
                    new Container(
                        margin: EdgeInsets.only(top:20),
                        child: TextField(
                          focusNode: _paswd,
                          textInputAction: TextInputAction.next,
                          onEditingComplete: () => _next(_paswdkonfirm, "last"),
                          cursorColor: Colors.white,
                          obscureText: passwordVisible,
                          style: TextStyle(color: Colors.white),
                          controller: _paswdContr,
                          decoration: InputDecoration(
                            errorText: _paswdNull ? _paswderr : null,
                            errorStyle: TextStyle(color: Colors.white),
                            filled: true,
                            fillColor: Colors.black.withOpacity(0.6),
                            labelStyle: TextStyle(
                              color: Colors.white,
                            ),
                            labelText: 'Password',
                            prefixIcon: Icon(
                              FontAwesomeIcons.key,
                              color: Colors.white,
                            ),
                            enabledBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                              borderSide: BorderSide(
                                color: Colors.white,
                              ),
                            ),
                            //hintText: 'Enter your password',
                            hintStyle: TextStyle(color: Colors.white),
                            suffixIcon: IconButton(
                              icon: Icon(
                                passwordVisible
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                setState(() {
                                  passwordVisible = !passwordVisible;
                                });
                              },
                            ),
                          ),
                        )
                    ),
                    new Container(
                        margin: EdgeInsets.only(top:20),
                        child: TextField(
                          focusNode: _paswdkonfirm,
                          textInputAction: TextInputAction.done,
                          onEditingComplete: () => _next(_paswdkonfirm, "done"),
                          cursorColor: Colors.white,
                          obscureText: passwordVisible,
                          style: TextStyle(color: Colors.white),
                          controller: _paswdkonfirmContr,
                          decoration: InputDecoration(
                            errorText: _paswdkonfirmNull ? _paswderr : null,
                            errorStyle: TextStyle(color: Colors.white),
                            filled: true,
                            fillColor: Colors.black.withOpacity(0.6),
                            labelStyle: TextStyle(
                              color: Colors.white,
                            ),
                            labelText: 'Konfirmasi Password',
                            prefixIcon: Icon(
                              FontAwesomeIcons.key,
                              color: Colors.white,
                            ),
                            enabledBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                              borderSide: BorderSide(
                                color: Colors.white,
                              ),
                            ),
                            //hintText: 'Enter your password',
                            hintStyle: TextStyle(color: Colors.white),
                            suffixIcon: IconButton(
                              icon: Icon(
                                passwordVisible
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                setState(() {
                                  passwordVisible = !passwordVisible;
                                });
                              },
                            ),
                          ),
                        )
                    ),
                  ],
                ),
              ),
          ),

          FractionalTranslation(
              translation: _animationList.Translation_4.value,
              child: new InkWell(
                onTap: (){
                  setState(() {
                    if(!_setuju){
                      _setuju = true;
                      _setujuErr = false;
                    }else{
                      _setuju = false;
                    }
                  });
                },
                child: new Container(
                    margin: EdgeInsets.only(top:20),
                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Icon(
                          _setuju ? FontAwesomeIcons.checkSquare : FontAwesomeIcons.square,
                          color: _setujuErr ? Colors.red : Colors.white,
                          size: 28,
                        ),
                        Padding(padding: EdgeInsets.only(left: 10)),
                        new Expanded(
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                "Dengan ini menyutujui segala kebijakan dan ketentuan penggunaan aplikasi yang berlaku.",
                                overflow: TextOverflow.clip,
                                maxLines: 2,
                                style: TextStyle(color:Colors.white,fontSize: 11),
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                ),
              )
          ),

          FractionalTranslation(
            translation: _animationList.Translation_4.value,
            child: new Container(
              margin: EdgeInsets.only(top: 10),
              child: new InkWell(
                  onTap:(){
                    showDialog(
                      barrierDismissible: true,
                      context: context,
                      builder: (BuildContext context){
                        return terms();
                      },
                    );
                  },
                child: new Text(
                  "Baca Kebijakan dan Ketentuan",
                  style: TextStyle(color: Colors.white, fontSize: 11),
                ),
              ),
            ),
          ),

          FractionalTranslation(
            translation: _animationList.Translation_4.value,
            child: new Container(
              margin: EdgeInsets.only(top:40),
              child: Column(
                  children: <Widget>[
                    new Align(
                      alignment: Alignment.bottomCenter,
                      child: RawMaterialButton(
                        onPressed: (){
                          bool kirim = true;
                          _paswderr = "";
                          _namaNull = false;
                          _usernameNull = false;
                          _mailNull = false;
                          _paswdNull = false;
                          _paswdkonfirmNull = false;
                          _setujuErr = false;
                          setState(() {
                            if(_namaContr.text.isEmpty){
                              kirim = false;
                              _namaNull = true;
                            }
                            if(_usernameContr.text.isEmpty){
                              kirim = false;
                              _usernameNull = true;
                            }
                            if(_mailContr.text.isEmpty){
                              kirim = false;
                              _mailNull = true;
                            }
                            if(_paswdContr.text.isEmpty){
                              kirim = false;
                              _paswdNull = true;
                            }
                            if(_paswdkonfirmContr.text.isEmpty){
                              kirim = false;
                              _paswdkonfirmNull = true;
                            }
                            if(_paswdContr.text.isNotEmpty && _paswdkonfirmContr.text.isNotEmpty ){
                              if(_paswdContr.text.length < 8 || _paswdkonfirmContr.text.length<8){
                                kirim = false;
                                _paswdNull = true;
                                _paswdkonfirmNull = true;
                                _paswderr = "*Password minimal 8 karakter!";
                              }
                              if(_paswdContr.text != _paswdkonfirmContr.text){
                                kirim = false;
                                _paswdNull = true;
                                _paswdkonfirmNull = true;
                                _paswderr = "*Password dan Konfirmasi Password tidak sama!";
                              }
                            }
                            if(!_setuju){
                              _setujuErr = true;
                              kirim = false;
                            }
                            if(kirim){
                              List<Map<String, dynamic>> dtNext=[];
                              dtNext.add({
                                "nama": _namaContr.text.toString(),
                                "email": _mailContr.text.toString(),
                                "username": _usernameContr.text.toString(),
                                "password": _paswdContr.text.toString(),
                              });
                              showDialog(
                                barrierDismissible: false,
                                context: context,
                                builder: (BuildContext context) => showNotif("", dtNext),
                              );
                            }
                          });


                        },
                        elevation: 16,
                        padding: EdgeInsets.all(16),
                        animationDuration: Duration(milliseconds: 500),
                        fillColor: Colors.red[600],
                        splashColor: Colors.orange,
                        highlightColor: Colors.red,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                        ),
                        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Column(children: <Widget>[
                              Text(
                                "Daftar Sekarang",
                                style: TextStyle(color: Colors.white, fontSize: 18),
                              ),
                            ],),
                            new Padding(padding: EdgeInsets.only(left: 20)),
                            new Column(children: <Widget>[
                              Icon(
                                FontAwesomeIcons.arrowRight,
                                color: Colors.white,
                                size: 16,
                              )
                            ],),
                          ],
                        ),
                      ),
                    ),
                  ]
              ),
            ),
          ),
          new Padding(padding: EdgeInsets.only(bottom:20)),
        ],
      ),
    );
  }

  @override
  Future<Konten> _getData() async {
    var db = new kontenDB();
    var _data = await db.getkonten(224);
    print(_data.toString());
    return _data;
  }

  Widget terms(){
    return new SafeArea(
        child: new Scaffold(
            backgroundColor: Colors.black.withOpacity(0.8),
            body: new FutureBuilder(
                future: _getData(),
                builder: (context, snapshot) {
                  if (snapshot.hasData && (snapshot.connectionState == ConnectionState.done)) {
                    if(snapshot.data != null){
                      return ListView(
                        children: <Widget>[
                          new Container(
                            padding: EdgeInsets.all(24),
                            color: Colors.white,
                            child: new Column(
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.only(bottom: 28),
                                  child: Text(
                                    "Syarat dan Ketentuan Penggunaan Aplikasi",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700,
                                        color: Colors.grey[800]
                                    ),
                                  ),
                                ),
                                new HtmlWidget(
                                  snapshot.data.full_content_id,
                                  textStyle: TextStyle(
                                      fontSize: 16,
                                      height: 1.4,
                                      fontFamily: "NeoSansBold",
                                      color: Colors.grey[800],
                                      wordSpacing: 2.0
                                  ),
                                  bodyPadding: EdgeInsets.all(0),
                                ),
                              ],
                            ),
                          )
                        ],
                      );
                    }else{
                      return new Stack(
                        children: <Widget>[
                          Center(
                            child: CircularProgressIndicator(),
                          ),
                        ],
                      );
                    }
                  }else{
                    return new Stack(
                      children: <Widget>[
                        Center(
                          child: CircularProgressIndicator(),
                        ),
                      ],
                    );
                  }
                }
            )
        )
    );
  }

  @override
  Future<String> postData(List<Map<String, dynamic>> dtNext) async {
    var _data;
    var api = new Http_Controller();
    _data = await api.postResponse("https://suryahusadha.com/api/akun/register", dtNext);
    var jsonResponse = convert.jsonEncode(_data);
    Navigator.pop(context);
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) => showNotif(jsonResponse.toString(), null),
    );
    return jsonResponse.toString();
  }

  Widget showNotif(_jsonResponse, List<Map<String, dynamic>> dtNext){
    if(dtNext != null){
      postData(dtNext);
      return new Center(
        child:  CircularProgressIndicator(
          backgroundColor: Colors.grey,
        ),
      );
    }else{
      if(_jsonResponse != "" || _jsonResponse != null ){
        var decode = convert.jsonDecode(_jsonResponse);
        var jsonResponse = convert.jsonDecode(decode["responseBody"]);
        String msg = jsonResponse["register"][0]["msg"];
        String response = jsonResponse["register"][0]["response"];
        if(response == "1"){
          return Dialogue(context, msg, WarnaCabang.shh, WarnaCabang.shh2, "group", null);
        }else{
          return Dialogue(context, msg, Colors.red, Colors.red, null, null);
        }
      }else{
        return Dialogue(context, "Mohon maaf, terjadi kesalahan koneksi pada saat daftar, silahkan ulangi beberapa saat lagi!", Colors.red, Colors.red, null, null);
      }
    }
  }

  _next(FocusNode reqfocus, String s){
    print("reqfocus: " +s);
    FocusScope.of(context).requestFocus(reqfocus);
    if(s == "last"){
      listScrollController.jumpTo(listScrollController.position.maxScrollExtent);
    }
    if(s == "done"){
      listScrollController.jumpTo(listScrollController.position.minScrollExtent);
      FocusScope.of(context).requestFocus(new FocusNode());
    }
  }

}