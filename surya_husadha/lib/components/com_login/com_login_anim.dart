import 'package:flutter/material.dart';
class AnimLoginList {
  AnimLoginList(this.controller) :
        backdropBlur = new Tween(begin: 0.0, end: 2.0).animate(
          new CurvedAnimation(
            parent: controller,
            curve: new Interval(
              0.000,
              0.800,
              curve: Curves.ease,
            ),
          ),
        ),
        backdropOpacity = new Tween(begin: 0.2, end: 1.0).animate(
          new CurvedAnimation(
            parent: controller,
            curve: new Interval(
              0.000,
              0.800,
              curve: Curves.ease,
            ),
          ),
        ),
        suriSize = new Tween(begin: 0.0, end: 1.0).animate(
          new CurvedAnimation(
            parent: controller,
            curve: new Interval(
              0.100,
              0.800,
              curve: Curves.elasticOut,
            ),
          ),
        ),
        LoginTitle = new Tween(begin: 0.0, end: 1.0).animate(
          new CurvedAnimation(
            parent: controller,
            curve: new Interval(
              0.350,
              0.800,
              curve: Curves.easeIn,
            ),
          ),
        ),
        subTitle = new Tween(begin: 0.0, end: 0.85).animate(
          new CurvedAnimation(
            parent: controller,
            curve: new Interval(
              0.500,
              0.800,
              curve: Curves.easeIn,
            ),
          ),
        ),
        dividerWidth = new Tween(begin: 0.0, end: 350.0).animate(
          new CurvedAnimation(
            parent: controller,
            curve: new Interval(
              0.650,
              0.800,
              curve: Curves.fastOutSlowIn,
            ),
          ),
        ),
        desc = new Tween(begin: 0.0, end: 0.85).animate(
          new CurvedAnimation(
            parent: controller,
            curve: new Interval(
              0.500,
              0.800,
              curve: Curves.easeIn,
            ),
          ),
        ),
        Translation_1 = new Tween(begin: Offset(-2.0, 0.0), end: Offset(0.0, 0.0)).animate(
          CurvedAnimation(
            parent: controller,
            curve: new Interval(0.100, 0.800, curve: Curves.bounceIn),
          ),
        ),
        Translation_2 = new Tween(begin: Offset(-2.0, 0.0), end: Offset(0.0, 0.0)).animate(
          CurvedAnimation(
            parent: controller,
            curve: new Interval(0.100, 0.800, curve: Curves.bounceIn),
          ),
        ),
        Translation_3 = new Tween(begin: Offset(0.0, 100.0), end: Offset(0.0, 0.0)).animate(
          CurvedAnimation(
            parent: controller,
            curve: new Interval(0.100, 0.800, curve: Curves.bounceIn),
          ),
        ),
        Translation_4 = new Tween(begin: Offset(0.0, 10.0), end: Offset(0.0, 0.0)).animate(
          CurvedAnimation(
            parent: controller,
            curve: new Interval(0.100, 0.800, curve: Curves.bounceIn),
          ),
        );

  final AnimationController controller;
  final Animation<double> backdropBlur;
  final Animation<double> backdropOpacity;
  final Animation<double> suriSize;
  final Animation<double> LoginTitle;
  final Animation<double> subTitle;
  final Animation<double> dividerWidth;
  final Animation<double> desc;
  final Animation<Offset> Translation_1;
  final Animation<Offset> Translation_2;
  final Animation<Offset> Translation_3;
  final Animation<Offset> Translation_4;
}
