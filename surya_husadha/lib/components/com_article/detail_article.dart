import 'package:flutter/material.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/database/konten/konten_db.dart';
import 'package:surya_husadha/helper/database/konten/konten_models.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DetaiArticle extends StatefulWidget {

  String _cabang;
  int _ctntid;
  Color _color1;
  Color _color2;
  var _akun;

  DetaiArticle(List params, akun){
    params.forEach((dt){
      this._cabang = dt["_cabang"];
      this._ctntid = dt["_ctntid"];
      if(_cabang=="denpasar"){
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
      }
      if(_cabang=="ubung"){
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
      }
      if(_cabang=="nusadua"){
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
      }
      if(_cabang=="kmc"){
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
      }
    });
  }

  @override
  _DetaiArticleController createState() => new _DetaiArticleController();
}

class _DetaiArticleController extends State<DetaiArticle> {

  GlobalKey<ScaffoldState> scafolt_key = GlobalKey<ScaffoldState>();
  var _data;

  @override
  Future<Konten> _getData() async {
    var db = new kontenDB();
    _data = await db.getkonten(widget._ctntid);
    print(_data.toString());
    return _data;
  }

  Future<bool> _onWillPop(){
    Navigator.pop(context);
  }

  @override
  Widget _buildcontent(BuildContext context) {
    return new SafeArea(
      child: Scaffold(
        body: new WillPopScope(
          onWillPop: _onWillPop,
          child: new FutureBuilder(
              future: _getData(),
              builder: (context, snapshot) {
                if (snapshot.hasData && (snapshot.connectionState == ConnectionState.done)) {
                  if(_data != null){
                    return ListView(
                      children: <Widget>[
                        buildCtn(_data),
                      ],
                    );
                  }else{
                    return new Stack(
                      children: <Widget>[
                        Center(
                          child: CircularProgressIndicator(),
                        ),
                      ],
                    );
                  }
                }else{
                  return new Stack(
                    children: <Widget>[
                      Center(
                        child: CircularProgressIndicator(),
                      ),
                    ],
                  );
                }
              }
          ),
        )
      ),
    );
  }

  _launchURL(url) async {
    await launch(url);
  }

  Widget buildCtn(Konten dt){
    Widget _circleAvatar;
    if(dt.pp !=null){
      _circleAvatar = CircleAvatar(
        backgroundImage: NetworkImage(SERVER.domain + dt.pp),
        minRadius: 30,
        maxRadius: 50,
      );
    }else{
      _circleAvatar = new Container(
        height: 50.0,
        width: 50.0,
        padding: EdgeInsets.all(6.0),
        decoration: new BoxDecoration(
            borderRadius:
            new BorderRadius.all(new Radius.circular(100.0)),
            color: Colors.lightBlue
        ),
        alignment: Alignment.center,
        child: new Icon(
          FontAwesomeIcons.user,
          color: Colors.white,
          size: 24.0,
        ),
      );
    }

    String Editor;
    if(dt.name!= null){
      Editor=dt.name;
    }else{
      Editor="Administrator";
    }

    return new Container(
      color: Colors.white,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Image.network(
            SERVER.domain + dt.image_default,
            width: double.infinity,
            fit: BoxFit.cover,
          ),
          new Container(
            color: Colors.white,
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.all(16),
                  color: Colors.white,
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        dt.title_id,
                        softWrap: true,
                        style: new TextStyle(
                            color: Colors.grey[1000],
                            fontFamily: "NeoSansBold",
                            fontSize: 22.0,
                            fontWeight: FontWeight.w900
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  padding: EdgeInsets.only(left: 16, right: 16),
                  child: new Column(
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                flex: 1,
                                child: new Row(
                                  children: <Widget>[
                                    new Icon(
                                      FontAwesomeIcons.folder,
                                      size: 12,
                                    ),
                                    new Container(
                                      padding: EdgeInsets.only(left: 5, top: 2),
                                      child: new Text(
                                          dt.cat_name_id
                                      ),
                                    ),
                                  ],
                                )
                            ),
                          ],
                        ),
                      ),
                      new Padding(
                        padding: EdgeInsets.only(top: 10.0),
                      ),
                      new Container(
                        height: 1,
                        color: Colors.grey[200],
                      ),
                      new Padding(
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      new HtmlWidget(
                        dt.full_content_id,
                        textStyle: TextStyle(
                            fontSize: 16,
                            height: 1.4,
                            fontFamily: "NeoSansBold",
                            color: Colors.grey[800],
                            wordSpacing: 2.0
                        ),
                        onTapUrl: (url){
                          _launchURL(url);
                        },
                        bodyPadding: EdgeInsets.all(0),
                      ),
                      new Padding(
                        padding: EdgeInsets.only(bottom: 40),
                      ),
                    ],
                  ),
                ),

                new Container(
                  width: double.infinity,
                  height: 2.0,
                  color: Colors.grey[200],
                ),
                new Container(
                  color: Colors.grey[50],
                  padding: EdgeInsets.only(
                    left: 16,
                      right: 16,
                    top: 20,
                    bottom: 20
                  ),
                  child: Row(
                    children: <Widget>[
                      new Container(
                        height: 55.0,
                        width: 55.0,
                        padding: EdgeInsets.all(2.0),
                        decoration: new BoxDecoration(
                            borderRadius:
                            new BorderRadius.all(new Radius.circular(100.0)),
                            color: Colors.lightBlue
                        ),
                        alignment: Alignment.center,
                        child:_circleAvatar,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 5),
                      ),
                      new Text(
                          "Oleh "+Editor
                      ),
                    ],
                  ),
                ),
                new Container(
                  width: double.infinity,
                  height: 2.0,
                  color: Colors.grey[200],
                ),
                new Padding(
                  padding: EdgeInsets.only(bottom: 60),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildcontent(context);
  }

}