import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/database/konten/konten_db.dart';
import 'package:surya_husadha/helper/routehelper/routehelper.dart';
import 'package:surya_husadha/modules/mod_appbar/appbar_controller.dart';
import 'package:surya_husadha/modules/mod_drawer/drawer_controller.dart';
import 'package:surya_husadha/modules/mod_navbottom/navbottom_controller.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'detail_article.dart';

class CategoryPage extends StatefulWidget {

  var _dtakun;
  String _cabang;
  String _ctntid;
  Color _color1;
  Color _color2;
  Color _btnText;

  CategoryPage(List params, akun){
    _dtakun = akun;
    params.forEach((dt){
      this._cabang = dt["_cabang"];
      this._ctntid = dt["_ctntid"];
      if(_cabang=="group"){
        this._btnText = Colors.grey;
        this._color1 = WarnaCabang.group;
        this._color2= WarnaCabang.group2;
      }
      if(_cabang=="denpasar"){
        this._btnText = WarnaCabang.shh;
        this._color1 = WarnaCabang.shh;
        this._color2= WarnaCabang.shh2;
      }
      if(_cabang=="ubung"){
        this._btnText = WarnaCabang.ubung;
        this._color1 = WarnaCabang.ubung;
        this._color2= WarnaCabang.ubung2;
      }
      if(_cabang=="nusadua"){
        this._btnText =  WarnaCabang.nusadua;
        this._color1 = WarnaCabang.nusadua;
        this._color2= WarnaCabang.nusadua2;
      }
      if(_cabang=="kmc"){
        this._btnText = WarnaCabang.kmc;
        this._color1 = WarnaCabang.kmc;
        this._color2= WarnaCabang.kmc2;
      }
    });
  }

  @override
  _CategoryPage createState() => new _CategoryPage();

}

class _CategoryPage extends State<CategoryPage> with SingleTickerProviderStateMixin{

  GlobalKey<ScaffoldState> scafolt_key = GlobalKey<ScaffoldState>();
  String headerttitle = "Kategori Blog";


  @override
  Future<List> _getData() async {
    var db = new kontenDB();
    List _data;
    _data = await db.getCatKonten(widget._ctntid);
    return _data;
  }

  Future<bool> _onWillPop(){
    //Navigator.pop(context);
    List<Map<String, dynamic>> dtNext=[];
    dtNext.add({"_cabang": widget._cabang});
    RouteHelper(context, widget._cabang, dtNext);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.white,
        systemNavigationBarDividerColor: Colors.grey[300],
        systemNavigationBarIconBrightness: Brightness.dark,// navigation bar color
        statusBarColor: widget._color2,
        statusBarIconBrightness: Brightness.dark// statu// status bar color
    ));
    return new SafeArea(
      child: new Scaffold(
        key: scafolt_key,
        drawer: drawerController(widget._dtakun, widget._cabang, "CategoryPage"),
        appBar: new PreferredSize(
          preferredSize: Size.fromHeight(200.0),
          child: AppBarController(widget._cabang, scafolt_key, widget._dtakun, "CategoryPage"),
        ),
        backgroundColor: Colors.grey[200],
        body: new WillPopScope(
          onWillPop: _onWillPop,
          child: new Container(
            child: new ListView(
              physics: ClampingScrollPhysics(),
              children: <Widget>[
                new Container(
                  child: new Column(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      new FutureBuilder<List>(
                          future: _getData(),
                          builder: (context, snapshot){
                            if(snapshot.hasData && (snapshot.connectionState == ConnectionState.done)){
                              if(snapshot.data.length > 0){
                                headerttitle = snapshot.data[0]["cat_name_id"].toString();
                                return new Column(
                                  children: <Widget>[
                                    new Container(
                                        padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 20),
                                        color: Colors.white,
                                        child: new Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.max,
                                          children: <Widget>[
                                            Icon(
                                              FontAwesomeIcons.folder,
                                              size: 24,
                                            ),
                                            new Container(
                                              padding: EdgeInsets.only(left: 5, top: 5),
                                              child: new Text(
                                                headerttitle,
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 1,
                                                softWrap: true,
                                                style: new TextStyle(
                                                  color: Colors.grey[1000],
                                                  fontFamily: "NeoSansBold",
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 24,
                                                ),
                                              ),
                                            )
                                          ],
                                        )
                                    ),
                                    new Container(
                                      width: double.infinity,
                                      height: 1.0,
                                      color: Colors.grey[200],
                                    ),
                                    ListView.builder(
                                      physics: NeverScrollableScrollPhysics(),
                                      itemCount: snapshot.data.length,
                                      scrollDirection: Axis.vertical,
                                      shrinkWrap: true,
                                      itemBuilder:(context, index){
                                        if(index <= snapshot.data.length){
                                          return _fitures(snapshot.data, index);
                                        }else{
                                          return Container();
                                        }
                                      },
                                    ),
                                  ],
                                );
                              }else{
                                return Container(
                                  margin: EdgeInsets.only(top: 10),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Text("Konnten tidak ditemukan!", style: TextStyle(color: Colors.grey[600]),),
                                    ],
                                  ),
                                );
                              }
                            }else{
                              return Container();
                            }
                          }
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        bottomNavigationBar: NavBottom(widget._cabang),
      ),
    );
  }

  Widget _fitures(dt, index) {
    return new Container(
        padding: EdgeInsets.all(16),
        color: Colors.white,
        child: new InkWell(
          onTap: (){
            List<Map<String, dynamic>> dtNext=[];
            dtNext.add({"_cabang": widget._cabang});
            dtNext.add({"_ctntid": dt[index]["id"]});
            showDialog(
              barrierDismissible: true,
              context: context,
              builder: (BuildContext context){
                return new DetaiArticle(dtNext, widget._dtakun);
              },
            );
          },
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new ClipRRect(
                borderRadius: new BorderRadius.circular(8.0),
                child: new Image.network(
                  SERVER.domain + dt[index]["image_default"],
                  height: 200.0,
                  width: double.infinity,
                  fit: BoxFit.cover,
                ),
              ),
              new Padding(
                padding: EdgeInsets.only(top: 16.0),
              ),
              new Container(
                padding: EdgeInsets.only(bottom: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      dt[index]["title_id"],
                      style: new TextStyle(color: Colors.grey[800],fontFamily: "NeoSansBold", fontSize: 18.0, fontWeight: FontWeight.w800),
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 8.0),
                    ),
                    new Text(
                      dt[index]["short_content_id"],
                      maxLines: 4,
                      softWrap: true,
                      style: new TextStyle(color: Colors.grey[600], fontFamily: "NeoSans", fontSize: 14.0, height: 1.2),
                    ),
                    new Container(
                      child: Row(
                        children: <Widget>[
                          new Expanded(
                              flex: 1,
                              child: new Row(
                                children: <Widget>[
                                  new Icon(
                                    FontAwesomeIcons.folder,
                                    size: 12,
                                  ),
                                  new Container(
                                    padding: EdgeInsets.only(left: 5, top: 2),
                                    child: new Text(
                                        dt[index]["cat_name_id"],
                                    ),
                                  ),
                                ],
                              )
                          ),
                          new Container(
                            margin: EdgeInsets.only(top:10.0),
                            alignment: Alignment.centerRight,
                            child: new FlatButton(
                              shape: Border.all(width: 1, color: widget._color2),
                              onPressed: (){
                                List<Map<String, dynamic>> dtNext=[];
                                dtNext.add({"_cabang": widget._cabang});
                                dtNext.add({"_ctntid": dt[index]["id"]});
                                showDialog(
                                  barrierDismissible: true,
                                  context: context,
                                  builder: (BuildContext context){
                                    return new DetaiArticle(dtNext, widget._dtakun);
                                  },
                                );
                              },
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Text(
                                    "Baca!",
                                    style: new TextStyle(
                                        color: widget._btnText,
                                        fontFamily: "NeoSansBold",
                                        fontSize: 12.0
                                    ),
                                  ),
                                  new Container(width: 5,),
                                  new Icon(
                                    FontAwesomeIcons.arrowRight,
                                    size: 12,
                                    color: widget._btnText,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                width: double.infinity,
                height: 1.0,
                color: Colors.grey[200],
              ),
            ],
          ),
        )
    );
  }
}