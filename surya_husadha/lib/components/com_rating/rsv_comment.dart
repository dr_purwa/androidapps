import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surya_husadha/components/com_reservasi/reservasi_models.dart';
import 'dart:ui' as ui;
import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/dialogue/customDialogue.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'dart:convert' as convert;

class RsvComment extends StatefulWidget {

  var _akun;
  Color _color1;
  Color _color2;
  String _rsvid;
  var _TextColorBar = Colors.white;
  Reservasi_Mdl rsvdt;

  RsvComment(rsvdt, akun, cabang){
    this._akun = akun;
    this.rsvdt = rsvdt;
    this._rsvid = rsvdt.idrsv;
    if(cabang=="group"){
      this._TextColorBar = Colors.grey[600];
      this._color1 = WarnaCabang.group;
      this._color2= WarnaCabang.group2;
    }
    if(cabang=="denpasar"){
      this._color1 = WarnaCabang.shh;
      this._color2= WarnaCabang.shh2;
    }
    if(cabang=="ubung"){
      this._color1 = WarnaCabang.ubung;
      this._color2= WarnaCabang.ubung2;
    }
    if(cabang=="nusadua"){
      this._color1 = WarnaCabang.nusadua;
      this._color2= WarnaCabang.nusadua2;
    }
    if(cabang=="kmc"){
      this._color1 = WarnaCabang.kmc;
      this._color2= WarnaCabang.kmc2;
    }
  }

  @override
  _RsvComment createState() => _RsvComment();
}

class _RsvComment extends State<RsvComment> with SingleTickerProviderStateMixin, NavigatorObserver{
  bool israte = true;
  var newdata;
  double _Rateval;
  bool _error = false;
  String titleHeder;
  var _Rate_color = Colors.grey[600];
  var _commentController = TextEditingController();

  AnimationController _controllerappBar;

  @override
  void dispose(){
    _controllerappBar.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _controllerappBar = new AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
    _controllerappBar.forward();
    _commentController.text = widget.rsvdt.comment;
    if(widget.rsvdt.star.toString() != "0" && widget.rsvdt.star.toString() != ""  && widget.rsvdt.star.toString() != null ){
      _Rateval = double.parse(widget.rsvdt.star);
      titleHeder = "Terima kasih atas rating untuk pelayanan kami beserta kritik dan saran yang telah Anda berikan!";
      israte = false;
    }else{
      titleHeder = "Silahkan berikan rating untuk pelayanan kami beserta kritik dan saran Anda guna peningkatan layanan PT. Surya Husadha Hospital Group.\n\nPenilaian ini akan sangat membantu kami dalam meningkatkan layanan kami kepada Anda,\n\nTerima kasih!";
      _Rateval = 0.0;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context){
    return SafeArea(
      child: Scaffold(
        appBar: new PreferredSize(
          preferredSize: Size.fromHeight(200.0),
          child: _appbar(),
        ),
        body: WillPopScope(
          onWillPop: (){
            Navigator.pop(context, newdata);
          },
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              new Container(
                color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      new Expanded(
                          child: new ListView(
                            children: <Widget>[
                              _buildheader(),
                              new Container(
                                margin: EdgeInsets.only(bottom: 16),
                                child: new Column(
                                  children: <Widget>[
                                    new Stack(
                                      children: <Widget>[
                                        new Container(
                                            padding: EdgeInsets.only(top: 20, bottom: 20, left: 24, right: 24),
                                            child: TextField(
                                              enabled: israte,
                                              controller: _commentController,
                                              maxLines: null,
                                              textInputAction: TextInputAction.done,
                                              cursorColor: Colors.grey[800],
                                              style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                              keyboardType: TextInputType.multiline,
                                              minLines: 8,
                                              decoration: InputDecoration(
                                                errorText: _error ? "" : null,
                                                filled: false,
                                                fillColor: Colors.white,
                                                labelStyle: TextStyle(
                                                  color: Colors.grey[800],
                                                ),
                                                focusedBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                enabledBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                  borderSide: BorderSide(
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                              ),
                                            )
                                        ),
                                        Positioned(
                                          top: -8,
                                          child: new Container(
                                            color: Colors.grey[50],
                                            margin: EdgeInsets.only(left: 45, top:20, bottom: 5),
                                            child: new Row(
                                              children: <Widget>[
                                                new Container(
                                                  padding: EdgeInsets.only(left: 5, right: 5),
                                                  child: Column(
                                                    children: <Widget>[
                                                      Text("Kritik dan Saran Anda:", style: TextStyle(fontSize: 12),)
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              new Visibility(
                                visible: israte,
                                child: new Container(
                                  margin: EdgeInsets.only(top: 20),
                                  child: new Center(
                                    child: new Text(
                                      "Salam Sehat Selalu!",
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700
                                      ),
                                    ),
                                  ),
                                )
                              ),
                              new Visibility(
                                visible: israte,
                                child: new Container(
                                  padding: EdgeInsets.only(
                                      bottom: 40,
                                      top: 40,
                                      left: 24,
                                      right: 24
                                  ),
                                  child: new RawMaterialButton(
                                    onPressed: (){
                                      setState(() {
                                        _error = false;
                                        var next = true;
                                        if(_Rateval.toString() == "0.0"){
                                          next = false;
                                          _Rate_color = Colors.red[600];
                                        }
                                        if(_commentController.text.isEmpty){
                                          _error = true;
                                          next = false;
                                        }
                                        if(next){
                                          List<Map<String, dynamic>> dtNext=[];
                                          dtNext.add({
                                            "insertid": widget._akun[0]["id"].toString(),
                                            "_rsvid": widget._rsvid.toString(),
                                            "_Rateval": _Rateval.toString(),
                                            "_comment": _commentController.text.toString(),
                                          });
                                          showDialog(
                                            barrierDismissible: false,
                                            context: context,
                                            builder: (BuildContext context) => showNotif(context, null, dtNext),
                                          ).whenComplete((){
                                            setState(() {});
                                          });
                                        }
                                      });
                                    },
                                    padding: EdgeInsets.only(top: 16, bottom: 16),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                    ),
                                    fillColor: Colors.redAccent,
                                    child: Container(
                                      child: new Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          new Expanded(
                                              child: new Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    "Kirim",
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        color: Colors.white
                                                    ),
                                                  ),
                                                ],
                                              )
                                          ),
                                          new Container(
                                            padding: EdgeInsets.only(right: 16),
                                            child: new Icon(
                                              FontAwesomeIcons.arrowRight,
                                              size: 14,
                                              color: Colors.white,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          )
                      ),
                    ],
                  )
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildheader() {
    return new Padding(
      padding: const EdgeInsets.only(left: 24.0, right: 24.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Row(
              children: <Widget>[
                new Column(
                    children: <Widget>[
                      new Container(
                        width: 80.0,
                        height: 80.0,
                        decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          border: new Border.all(color: Colors.grey[300]),
                        ),
                        margin: const EdgeInsets.only(top: 16.0, right: 16.0),
                        padding: const EdgeInsets.all(3.0),
                        child: new ClipOval(
                          child: new Image.asset(
                              "assets/suri/suri2.png"
                          ),
                        ),
                      ),
                    ]
                ),
                new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 18.0),
                      ),
                      new Text(
                        "RATING",
                        style: new TextStyle(
                          color: Colors.grey[800],
                          fontWeight: FontWeight.bold,
                          fontSize: 24.0,
                        ),
                      ),
                      new Text(
                        israte ? "Berikan penilaian Anda!" : "Hasil penilaian Anda!",
                        style: new TextStyle(
                          color: Colors.grey[800],
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ]
                )
              ]
          ),
          new Container(
            color: Colors.grey[300],
            margin: const EdgeInsets.symmetric(vertical: 16.0),
            width: MediaQuery.of(context).size.width,
            height: 1.0,
          ),
          new Visibility(
            child: Text(
              titleHeder,
              style: new TextStyle(
                color: Colors.grey[800],
              ),
            ),
          ),
          new Container(
            color: Colors.grey[300],
            margin: const EdgeInsets.symmetric(vertical: 16.0),
            width: MediaQuery.of(context).size.width,
            height: 1.0,
          ),
          RowRate(),
        ],
      ),
    );
  }

  Widget RowRate(){
    return new Container(
      padding: EdgeInsets.only(left: 24, right: 24, top: 20, bottom: 30),
      margin: EdgeInsets.only(bottom: 2),
      width: MediaQuery.of(context).size.width,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Visibility(
            visible: israte,
            child: new Container(
              child: FlutterRatingBar(
                initialRating: _Rateval,
                borderColor: _Rate_color,
                allowHalfRating: true,
                ignoreGestures: false,
                tapOnlyMode: false,
                itemSize: 40,
                itemCount: 5,
                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                onRatingUpdate: (rating) {
                  setState((){
                      _Rateval = rating;
                      if(_Rateval.toString() == "0.0"){
                        _Rate_color = Colors.red[600];
                      }else{
                        _Rate_color = Colors.grey[600];
                      }
                  });
                },
              ),
            ),
          ),
          Visibility(
            visible: israte,
            child: FlutterRatingBarIndicator(
              rating: _Rateval,
              itemCount: 5,
              itemSize: 40.0,
              fillColor: Colors.yellow[600],
              emptyColor: Colors.grey[300],
            ),
          ),
        ],
      ),
    );
  }

  Widget _appbar(){
    return new Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        new Container(
            color: widget._color1,
            height: 55,
            child:Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: new Row(
                    children: <Widget>[
                      new Container(
                        child: new InkWell(
                          onTap: () =>  Navigator.pop(context),
                          child: new Container(
                            padding: EdgeInsets.only(left: 24, right: 24),
                            alignment: Alignment.center,
                            child: new Icon(
                              FontAwesomeIcons.arrowLeft,
                              color: widget._TextColorBar,
                              size: 24.0,
                            ),
                          ),
                        ),
                      ),
                      new Text(
                        "RATING / PENILAIAN ANDA",
                        style: TextStyle(
                            fontSize: 16,
                            color: widget._TextColorBar,
                            fontWeight: FontWeight.w700
                        ),
                      ),

                    ],
                  ),
                ),
              ],
            )
        ),
        new Container(
          color: widget._color1,
          height: 2,
          child: new SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(-1, 0),
              end: const Offset(0, 0),
            ).animate(_controllerappBar),
            child: new SizedBox(
              child: Container(
                  color: Colors.white
              ),
            ),
          ),
        ),
      ],
    );
  }

  bool sendingData = true;
  @override
  Future<String> _postData(context, List<Map<String, dynamic>> dtNext) async {
    var jsonResponse;
    if(sendingData){
      var db = new Http_Controller();
      var _data = await db.postResponse("https://suryahusadha.com/api/rsvrating", dtNext);
      jsonResponse = convert.jsonEncode(_data);
      Navigator.pop(context);
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => showNotif(context, jsonResponse.toString(), null),
      ).whenComplete((){
        setState(() {});
      });
    }
    return jsonResponse.toString();
  }

  Widget showNotif(BuildContext context, _jsonResponse, List<Map<String, dynamic>> dtNext){
    if(dtNext != null){
      _postData(context, dtNext);
      sendingData = false;
      return new Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.grey,
        ),
      );
    } else {
      if(_jsonResponse != "" || _jsonResponse != null) {
        var decode = convert.jsonDecode(_jsonResponse);
        if(decode["statusCode"] == 200){
          var jsonResponse = convert.jsonDecode(decode["responseBody"]);
          String response = jsonResponse["rsvrating"][0]["response"];
          String msg = jsonResponse["rsvrating"][0]["msg"];
          newdata = jsonResponse["rsvrating"][0]["data"];
          if(response == "0"){
            return Dialogue(context, msg, Colors.red, widget._color2, null, null);
          }
          if(response == "1"){
            israte = false;
            titleHeder = "Terima kasih atas rating beserta kritik dan saran yang telah Anda berikan!";
            return Dialogue(context, msg, widget._color1, widget._color2, null, null);
          }
        }else{
          return Dialogue(context,
              "Mohon maaf, terjadi kesalahan pengolahan data pada saat simpan data, silahkan ulangi beberapa saat lagi!",
              Colors.red, Colors.red, null, null);
        }
      }else {
        return Dialogue(context,
            "Mohon maaf, terjadi kesalahan koneksi pada saat simpan data, silahkan ulangi beberapa saat lagi!",
            Colors.red, Colors.red, null, null);
      }
    }
  }

}