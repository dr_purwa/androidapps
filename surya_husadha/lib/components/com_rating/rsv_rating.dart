import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:surya_husadha/components/com_reservasi/reservasi_models.dart';
import 'dart:ui' as ui;

import 'package:surya_husadha/helper/constans/constans.dart';
import 'package:surya_husadha/helper/dialogue/customDialogue.dart';
import 'package:surya_husadha/helper/http/http_controller.dart';
import 'dart:convert' as convert;

class RsvRating extends StatefulWidget {

  String _cabang;
  Color _color1;
  Color _color2;
  String _rsvid;
  var _TextColorBar = Colors.white;
  var _rating;

  RsvRating(Reservasi_Mdl rsvdt, akun, rating, cabang){
    this._rsvid = rsvdt.idrsv;
    this._rating = rating;
    if(cabang=="group"){
      this._TextColorBar = Colors.grey[600];
      this._color1 = WarnaCabang.group;
      this._color2= WarnaCabang.group2;
    }
    if(cabang=="denpasar"){
      this._color1 = WarnaCabang.shh;
      this._color2= WarnaCabang.shh2;
    }
    if(cabang=="ubung"){
      this._color1 = WarnaCabang.ubung;
      this._color2= WarnaCabang.ubung2;
    }
    if(cabang=="nusadua"){
      this._color1 = WarnaCabang.nusadua;
      this._color2= WarnaCabang.nusadua2;
    }
    if(cabang=="kmc"){
      this._color1 = WarnaCabang.kmc;
      this._color2= WarnaCabang.kmc2;
    }
  }

  @override
  _RsvRating createState() => _RsvRating();
}

class _RsvRating extends State<RsvRating> with SingleTickerProviderStateMixin, NavigatorObserver{

  bool israte = true;
  var newdata;


  double _PtgsPendaftaranRate;
  Color _PtgsPendaftaranRateColor1 = Colors.grey[800];
  Color _PtgsPendaftaranRateColor2 = Colors.grey[600];
  var _PtgsPendaftaranRateCtrl = TextEditingController();

  double _PtgsKasirRate;
  Color _PtgsKasirRateColor1 = Colors.grey[800];
  Color _PtgsKasirRateColor2 = Colors.grey[600];
  var _PtgsKasirRateCtrl = TextEditingController();

  double _FparkirRate;
  Color _FparkirRateColor1 = Colors.grey[800];
  Color _FparkirRateColor2 = Colors.grey[600];
  var _FparkirRateCtrl = TextEditingController();

  double _PtgsParkirRate;
  Color _PtgsParkirRateColor1 = Colors.grey[800];
  Color _PtgsParkirRateColor2 = Colors.grey[600];
  var _PtgsParkirRateCtrl = TextEditingController();

  double _SatpamRate;
  Color _SatpamRateColor1 = Colors.grey[800];
  Color _SatpamRateColor2 = Colors.grey[600];
  var _SatpamRateCtrl = TextEditingController();

  double _PtgsKebersihanRate;
  Color _PtgsKebersihanRateColor1 = Colors.grey[800];
  Color _PtgsKebersihanRateColor2 = Colors.grey[600];
  var _PtgsKebersihanRateCtrl = TextEditingController();

  double _ToiletRate;
  Color _ToiletRateColor1 = Colors.grey[800];
  Color _ToiletRateColor2 = Colors.grey[600];
  var _ToiletRateCtrl = TextEditingController();

  double _PetunjukArahRate;
  Color _PetunjukArahRateColor1 = Colors.grey[800];
  Color _PetunjukArahRateColor2 = Colors.grey[600];
  var _PetunjukArahRateCtrl = TextEditingController();

  double _DokterRJRate;
  Color _DokterRJRateColor1 = Colors.grey[800];
  Color _DokterRJRateColor2 = Colors.grey[600];
  var _DokterRJRateCtrl = TextEditingController();

  double _PerawatRJRate;
  Color _PerawatRJRateColor1 = Colors.grey[800];
  Color _PerawatRJRateColor2 = Colors.grey[600];
  var _PerawatRJRateCtrl = TextEditingController();

  double _DokterRIRate;
  Color _DokterRIRateColor1 = Colors.grey[800];
  Color _DokterRIRateColor2 = Colors.grey[600];
  var _DokterRIRateCtrl = TextEditingController();

  double _PerwataRIRate;
  Color _PerwataRIRateColor1 = Colors.grey[800];
  Color _PerwataRIRateColor2 = Colors.grey[600];
  var _PerwataRIRateCtrl = TextEditingController();

  double _PramusajiRIRate;
  Color _PramusajiRIRateColor1 = Colors.grey[800];
  Color _PramusajiRIRateColor2 = Colors.grey[600];
  var _PramusajiRIRateCtrl = TextEditingController();

  double _DokterUGDRate;
  Color _DokterUGDRateColor1 = Colors.grey[800];
  Color _DokterUGDRateColor2 = Colors.grey[600];
  var _DokterUGDRateCtrl = TextEditingController();

  double _PerawatUGDRate;
  Color _PerawatUGDRateColor1 = Colors.grey[800];
  Color _PerawatUGDRateColor2 = Colors.grey[600];
  var _PerawatUGDRateCtrl = TextEditingController();

  double _ObatFarmasiRate;
  Color _ObatFarmasiRateColor1 = Colors.grey[800];
  Color _ObatFarmasiRateColor2 = Colors.grey[600];
  var _ObatFarmasiRateCtrl = TextEditingController();

  var _masukanNonMedis = TextEditingController();
  var _masukanRI = TextEditingController();
  var _masukanRJ = TextEditingController();
  var _masukanUGD = TextEditingController();
  var _masukanFarmasi = TextEditingController();

  AnimationController _controllerappBar;

  @override
  void dispose(){
    _controllerappBar.dispose();
    super.dispose();
  }

  @override
  void initState() {
    //newdata = widget._rating;
    _controllerappBar = new AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
    setState(() {
      _controllerappBar.forward();
      if(widget._rating.isEmpty || widget._rating == null){
        _PtgsPendaftaranRateCtrl.text = "0.0";
        _PtgsKasirRateCtrl.text = "0.0";
        _FparkirRateCtrl.text = "0.0";
        _PtgsParkirRateCtrl.text = "0.0";
        _SatpamRateCtrl.text = "0.0";
        _PtgsKebersihanRateCtrl.text = "0.0";
        _ToiletRateCtrl.text = "0.0";
        _PetunjukArahRateCtrl.text = "0.0";
        _DokterRJRateCtrl.text = "0.0";
        _PerawatRJRateCtrl.text = "0.0";
        _DokterRIRateCtrl.text = "0.0";
        _PerwataRIRateCtrl.text = "0.0";
        _PramusajiRIRateCtrl.text = "0.0";
        _DokterUGDRateCtrl.text = "0.0";
        _PerawatUGDRateCtrl.text = "0.0";
        _ObatFarmasiRateCtrl.text = "0.0";
      }else{
        israte = false;
        _PtgsPendaftaranRateCtrl.text = widget._rating[0]["ptgs_pendaftaran"];
        _PtgsKasirRateCtrl.text = widget._rating[0]["ptgs_kasir"];
        _FparkirRateCtrl.text = widget._rating[0]["fs_parkir"];
        _PtgsParkirRateCtrl.text = widget._rating[0]["ptgs_parkir"];
        _SatpamRateCtrl.text = widget._rating[0]["ptgs_satpam"];
        _PtgsKebersihanRateCtrl.text = widget._rating[0]["ptgs_kebersiahan"];
        _ToiletRateCtrl.text = widget._rating[0]["ly_kebersiahan_toilet"];
        _PetunjukArahRateCtrl.text = widget._rating[0]["ptjk_arah"];
        _DokterRJRateCtrl.text =widget._rating[0]["ly_dokterrj"];
        _PerawatRJRateCtrl.text = widget._rating[0]["ly_perawatrj"];
        _DokterRIRateCtrl.text = widget._rating[0]["ly_dokterri"];
        _PerwataRIRateCtrl.text = widget._rating[0]["ly_perawatri"];
        _PramusajiRIRateCtrl.text = widget._rating[0]["ly_pramusaji"];
        _DokterUGDRateCtrl.text = widget._rating[0]["ly_dokterugd"];
        _PerawatUGDRateCtrl.text = widget._rating[0]["ly_perawatugd"];
        _ObatFarmasiRateCtrl.text = widget._rating[0]["ly_farmasi"];
        _masukanNonMedis.text = widget._rating[0]["masukan_nonmedis"];
        _masukanRI.text = widget._rating[0]["masukan_ri"];
        _masukanRJ.text = widget._rating[0]["masukan_rj"];
        _masukanUGD.text = widget._rating[0]["masukan_ugd"];
        _masukanFarmasi.text = widget._rating[0]["masukan_farmasi"];
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context){
    return SafeArea(
      child: Scaffold(
        appBar: new PreferredSize(
          preferredSize: Size.fromHeight(200.0),
          child: _appbar(),
        ),
        body: WillPopScope(
          onWillPop: (){
            Navigator.pop(context, newdata);
          },
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              new Container(
                  child: Column(
                    children: <Widget>[
                      new Expanded(
                          child: new ListView(
                            children: <Widget>[
                              new Container(
                                margin: EdgeInsets.only(bottom: 16),
                                decoration: BoxDecoration(
                                    color: Colors.grey[100],
                                    border: Border(
                                        bottom: BorderSide(
                                          color: Colors.grey[200],
                                        )
                                    )
                                ),
                                child: new Column(
                                  children: <Widget>[
                                    HeaderRate("LAYANAN NON MEDIS"),
                                    RowRate("Layanan Petugas Pendaftaran", FontAwesomeIcons.pencilAlt, "_PtgsPendaftaranRate"),
                                    RowRate("Layanan Petugas Kasir", FontAwesomeIcons.cashRegister, "_PtgsKasirRate"),
                                    RowRate("Layanan Fasilitas Parkir", FontAwesomeIcons.parking, "_FparkirRate"),
                                    RowRate("Layanan Petugas Parkir", FontAwesomeIcons.userSecret, "_PtgsParkirRate"),
                                    RowRate("Layanan Satpam", FontAwesomeIcons.shieldAlt, "_SatpamRate"),
                                    RowRate("Layanan Petugas Kebersihan", FontAwesomeIcons.paintBrush, "_PtgsKebersihanRate"),
                                    RowRate("Layanan Kebersihan Toilet", FontAwesomeIcons.toilet, "_ToiletRate"),
                                    RowRate("Kejelasan Petunjuk Arah", FontAwesomeIcons.directions, "_PetunjukArahRate"),
                                    new Stack(
                                      children: <Widget>[
                                        new Container(
                                            padding: EdgeInsets.only(top: 20, bottom: 20, left: 24, right: 24),
                                            decoration: BoxDecoration(
                                                color: Colors.white
                                            ),
                                            child: TextField(
                                              enabled: israte ? true : false,
                                              controller: _masukanNonMedis,
                                              maxLines: null,
                                              textInputAction: TextInputAction.done,
                                              cursorColor: Colors.grey[800],
                                              style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                              keyboardType: TextInputType.multiline,
                                              minLines: 5,
                                              decoration: InputDecoration(
                                                filled: true,
                                                fillColor: Colors.white,
                                                labelStyle: TextStyle(
                                                  color: Colors.grey[800],
                                                ),
                                                focusedBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                enabledBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                  borderSide: BorderSide(
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                              ),
                                            )
                                        ),
                                        Positioned(
                                          top: -6,
                                          child: new Container(
                                            color: Colors.white,
                                            margin: EdgeInsets.only(left: 45, top:20, bottom: 5),
                                            child: new Row(
                                              children: <Widget>[
                                                new Container(
                                                  padding: EdgeInsets.only(left: 5, right: 5),
                                                  child: Column(
                                                    children: <Widget>[
                                                      Text("Masukan untuk layanan non medis:", style: TextStyle(fontSize: 12),)
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(bottom: 16),
                                decoration: BoxDecoration(
                                    color: Colors.grey[100],
                                    border: Border(
                                        bottom: BorderSide(
                                          color: Colors.grey[200],
                                        )
                                    )
                                ),
                                child: new Column(
                                  children: <Widget>[
                                    HeaderRate("LAYANAN RAWAT JALAN"),
                                    RowRate("Layanan dokter di Poliklinik", FontAwesomeIcons.stethoscope, "_DokterRJRate"),
                                    RowRate("Layanan perawat di Poliklinik", FontAwesomeIcons.userMd, "_PerawatRJRate"),
                                    new Stack(
                                      children: <Widget>[
                                        new Container(
                                            padding: EdgeInsets.only(top: 20, bottom: 20, left: 24, right: 24),
                                            decoration: BoxDecoration(
                                                color: Colors.white
                                            ),
                                            child: TextField(
                                              enabled: israte ? true : false,
                                              controller: _masukanRJ,
                                              maxLines: null,
                                              textInputAction: TextInputAction.done,
                                              cursorColor: Colors.grey[800],
                                              style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                              keyboardType: TextInputType.multiline,
                                              minLines: 5,
                                              decoration: InputDecoration(
                                                filled: true,
                                                fillColor: Colors.white,
                                                labelStyle: TextStyle(
                                                  color: Colors.grey[800],
                                                ),
                                                focusedBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                enabledBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                  borderSide: BorderSide(
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                              ),
                                            )
                                        ),
                                        Positioned(
                                          top: -6,
                                          child: new Container(
                                            color: Colors.white,
                                            margin: EdgeInsets.only(left: 45, top:20, bottom: 5),
                                            child: new Row(
                                              children: <Widget>[
                                                new Container(
                                                  padding: EdgeInsets.only(left: 5, right: 5),
                                                  child: Column(
                                                    children: <Widget>[
                                                      Text("Masukan untuk layanan Rawat Jalan:", style: TextStyle(fontSize: 12),)
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(bottom: 16),
                                decoration: BoxDecoration(
                                    color: Colors.grey[100],
                                    border: Border(
                                        bottom: BorderSide(
                                          color: Colors.grey[200],
                                        )
                                    )
                                ),
                                child: new Column(
                                  children: <Widget>[
                                    HeaderRate("LAYANAN RAWAT INAP"),
                                    RowRate("Layanan dokter di ruang rawat inap", FontAwesomeIcons.stethoscope, "_DokterRIRate"),
                                    RowRate("Layanan perawat di ruang rawat inap", FontAwesomeIcons.userMd, "_PerwataRIRate"),
                                    RowRate("Layanan pramusaji", FontAwesomeIcons.utensilSpoon, "_PramusajiRIRate"),
                                    new Stack(
                                      children: <Widget>[
                                        new Container(
                                            padding: EdgeInsets.only(top: 20, bottom: 20, left: 24, right: 24),
                                            decoration: BoxDecoration(
                                                color: Colors.white
                                            ),
                                            child: TextField(
                                              enabled: israte ? true : false,
                                              controller: _masukanRI,
                                              maxLines: null,
                                              textInputAction: TextInputAction.done,
                                              cursorColor: Colors.grey[800],
                                              style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                              keyboardType: TextInputType.multiline,
                                              minLines: 5,
                                              decoration: InputDecoration(
                                                filled: true,
                                                fillColor: Colors.white,
                                                labelStyle: TextStyle(
                                                  color: Colors.grey[800],
                                                ),
                                                focusedBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                enabledBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                  borderSide: BorderSide(
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                              ),
                                            )
                                        ),
                                        Positioned(
                                          top: -6,
                                          child: new Container(
                                            color: Colors.white,
                                            margin: EdgeInsets.only(left: 45, top:20, bottom: 5),
                                            child: new Row(
                                              children: <Widget>[
                                                new Container(
                                                  padding: EdgeInsets.only(left: 5, right: 5),
                                                  child: Column(
                                                    children: <Widget>[
                                                      Text("Masukan untuk layanan Rawat Inap:", style: TextStyle(fontSize: 12),)
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(bottom: 16),
                                decoration: BoxDecoration(
                                    color: Colors.grey[100],
                                    border: Border(
                                        bottom: BorderSide(
                                          color: Colors.grey[200],
                                        )
                                    )
                                ),
                                child: new Column(
                                  children: <Widget>[
                                    HeaderRate("LAYANAN UGD"),
                                    RowRate("Layanan dokter di UGD", FontAwesomeIcons.stethoscope, "_DokterUGDRate"),
                                    RowRate("Layanan perawat di UGD", FontAwesomeIcons.userMd, "_PerawatUGDRate"),
                                    new Stack(
                                      children: <Widget>[
                                        new Container(
                                            padding: EdgeInsets.only(top: 20, bottom: 20, left: 24, right: 24),
                                            decoration: BoxDecoration(
                                                color: Colors.white
                                            ),
                                            child: TextField(
                                              enabled: israte ? true : false,
                                              controller: _masukanUGD,
                                              maxLines: null,
                                              textInputAction: TextInputAction.done,
                                              cursorColor: Colors.grey[800],
                                              style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                              keyboardType: TextInputType.multiline,
                                              minLines: 5,
                                              decoration: InputDecoration(
                                                filled: true,
                                                fillColor: Colors.white,
                                                labelStyle: TextStyle(
                                                  color: Colors.grey[800],
                                                ),
                                                focusedBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                enabledBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                  borderSide: BorderSide(
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                              ),
                                            )
                                        ),
                                        Positioned(
                                          top: -6,
                                          child: new Container(
                                            color: Colors.white,
                                            margin: EdgeInsets.only(left: 45, top:20, bottom: 5),
                                            child: new Row(
                                              children: <Widget>[
                                                new Container(
                                                  padding: EdgeInsets.only(left: 5, right: 5),
                                                  child: Column(
                                                    children: <Widget>[
                                                      Text("Masukan untuk layanan UGD:", style: TextStyle(fontSize: 12),)
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(bottom: 16),
                                decoration: BoxDecoration(
                                    color: Colors.grey[100],
                                    border: Border(
                                        bottom: BorderSide(
                                          color: Colors.grey[200],
                                        )
                                    )
                                ),
                                child: new Column(
                                  children: <Widget>[
                                    HeaderRate("LAYANAN OBAT / FARMASI"),
                                    RowRate("Layanan Obat / Farmasi", FontAwesomeIcons.pills, "_ObatFarmasiRate"),
                                    new Stack(
                                      children: <Widget>[
                                        new Container(
                                            padding: EdgeInsets.only(top: 20, bottom: 20, left: 24, right: 24),
                                            decoration: BoxDecoration(
                                                color: Colors.white
                                            ),
                                            child: TextField(
                                              enabled: israte ? true : false,
                                              controller: _masukanFarmasi,
                                              maxLines: null,
                                              textInputAction: TextInputAction.done,
                                              cursorColor: Colors.grey[800],
                                              style: TextStyle(color: Colors.grey[800], fontSize: 14),
                                              keyboardType: TextInputType.multiline,
                                              minLines: 5,
                                              decoration: InputDecoration(
                                                filled: true,
                                                fillColor: Colors.white,
                                                labelStyle: TextStyle(
                                                  color: Colors.grey[800],
                                                ),
                                                focusedBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 1.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                enabledBorder: const OutlineInputBorder(
                                                  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                ),
                                                border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                                  borderSide: BorderSide(
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                              ),
                                            )
                                        ),
                                        Positioned(
                                          top: -6,
                                          child: new Container(
                                            color: Colors.white,
                                            margin: EdgeInsets.only(left: 45, top:20, bottom: 5),
                                            child: new Row(
                                              children: <Widget>[
                                                new Container(
                                                  padding: EdgeInsets.only(left: 5, right: 5),
                                                  child: Column(
                                                    children: <Widget>[
                                                      Text("Masukan untuk Obat / Farmasi:", style: TextStyle(fontSize: 12),)
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              new Visibility(
                                visible: israte ? true : false,
                                child: new Container(
                                  padding: EdgeInsets.only(
                                      bottom: 40,
                                      top: 40,
                                      left: 24,
                                      right: 24
                                  ),
                                  child: new RawMaterialButton(
                                    onPressed: (){
                                      setState(() {
                                        _PtgsPendaftaranRateColor1 = Colors.grey[800];
                                        _PtgsPendaftaranRateColor2 = Colors.grey[600];

                                        _PtgsKasirRateColor1 = Colors.grey[800];
                                        _PtgsKasirRateColor2 = Colors.grey[600];

                                        _FparkirRateColor1 = Colors.grey[800];
                                        _FparkirRateColor2 = Colors.grey[600];

                                        _PtgsParkirRateColor1 = Colors.grey[800];
                                        _PtgsParkirRateColor2 = Colors.grey[600];

                                        _SatpamRateColor1 = Colors.grey[800];
                                        _SatpamRateColor2 = Colors.grey[600];

                                        _PtgsKebersihanRateColor1 = Colors.grey[800];
                                        _PtgsKebersihanRateColor2 = Colors.grey[600];

                                        _ToiletRateColor1 = Colors.grey[800];
                                        _ToiletRateColor2 = Colors.grey[600];

                                        _PetunjukArahRateColor1 = Colors.grey[800];
                                        _PetunjukArahRateColor2 = Colors.grey[600];

                                        _DokterRJRateColor1 = Colors.grey[800];
                                        _DokterRJRateColor2 = Colors.grey[600];

                                        _PerawatRJRateColor1 = Colors.grey[800];
                                        _PerawatRJRateColor2 = Colors.grey[600];

                                        _DokterRIRateColor1 = Colors.grey[800];
                                        _DokterRIRateColor2 = Colors.grey[600];

                                        _PerwataRIRateColor1 = Colors.grey[800];
                                        _PerwataRIRateColor2 = Colors.grey[600];

                                        _PramusajiRIRateColor1 = Colors.grey[800];
                                        _PramusajiRIRateColor2 = Colors.grey[600];

                                        _DokterUGDRateColor1 = Colors.grey[800];
                                        _DokterUGDRateColor2 = Colors.grey[600];

                                        _PerawatUGDRateColor1 = Colors.grey[800];
                                        _PerawatUGDRateColor2 = Colors.grey[600];

                                        _ObatFarmasiRateColor1 = Colors.grey[800];
                                        _ObatFarmasiRateColor2 = Colors.grey[600];

                                        var next = true;
                                        if(_PtgsPendaftaranRateCtrl.text == "0.0"){
                                          next = false;
                                          _PtgsPendaftaranRateColor1 = Colors.red[800];
                                          _PtgsPendaftaranRateColor2 = Colors.red[600];
                                        }
                                        if(_PtgsKasirRateCtrl.text == "0.0"){
                                          next = false;
                                          _PtgsKasirRateColor1 = Colors.red[800];
                                          _PtgsKasirRateColor2 = Colors.red[600];
                                        }
                                        if(_FparkirRateCtrl.text == "0.0"){
                                          next = false;
                                          _FparkirRateColor1 = Colors.red[800];
                                          _FparkirRateColor2 = Colors.red[600];
                                        }
                                        if(_PtgsParkirRateCtrl.text == "0.0"){
                                          next = false;
                                          _PtgsParkirRateColor1 = Colors.red[800];
                                          _PtgsParkirRateColor2 = Colors.red[600];
                                        }
                                        if(_SatpamRateCtrl.text == "0.0"){
                                          next = false;
                                          _SatpamRateColor1 = Colors.red[800];
                                          _SatpamRateColor2 = Colors.red[600];
                                        }
                                        if(_PtgsKebersihanRateCtrl.text == "0.0"){
                                          next = false;
                                          _PtgsKebersihanRateColor1 = Colors.red[800];
                                          _PtgsKebersihanRateColor2 = Colors.red[600];
                                        }
                                        if(_ToiletRateCtrl.text == "0.0"){
                                          next = false;
                                          _ToiletRateColor1 = Colors.red[800];
                                          _ToiletRateColor2 = Colors.red[600];
                                        }
                                        if(_PetunjukArahRateCtrl.text == "0.0"){
                                          next = false;
                                          _PetunjukArahRateColor1 = Colors.red[800];
                                          _PetunjukArahRateColor2 = Colors.red[600];
                                        }
                                        if(_DokterRJRateCtrl.text == "0.0"){
                                          next = false;
                                          _DokterRJRateColor1 = Colors.red[800];
                                          _DokterRJRateColor2 = Colors.red[600];
                                        }
                                        if(_PerawatRJRateCtrl.text == "0.0"){
                                          next = false;
                                          _PerawatRJRateColor1 = Colors.red[800];
                                          _PerawatRJRateColor2 = Colors.red[600];
                                        }
                                        if(_DokterRIRateCtrl.text == "0.0"){
                                          next = false;
                                          _DokterRIRateColor1 = Colors.red[800];
                                          _DokterRIRateColor2 = Colors.red[600];
                                        }
                                        if(_PerwataRIRateCtrl.text == "0.0"){
                                          next = false;
                                          _PerwataRIRateColor1 = Colors.red[800];
                                          _PerwataRIRateColor2 = Colors.red[600];
                                        }
                                        if(_PramusajiRIRateCtrl.text == "0.0"){
                                          next = false;
                                          _PramusajiRIRateColor1 = Colors.red[800];
                                          _PramusajiRIRateColor2 = Colors.red[600];
                                        }
                                        if(_DokterUGDRateCtrl.text == "0.0"){
                                          next = false;
                                          _DokterUGDRateColor1 = Colors.red[800];
                                          _DokterUGDRateColor2 = Colors.red[600];
                                        }
                                        if(_PerawatUGDRateCtrl.text == "0.0"){
                                          next = false;
                                          _PerawatUGDRateColor1 = Colors.red[800];
                                          _PerawatUGDRateColor2 = Colors.red[600];
                                        }
                                        if(_ObatFarmasiRateCtrl.text == "0.0"){
                                          next = false;
                                          _ObatFarmasiRateColor1 = Colors.red[600];
                                          _ObatFarmasiRateColor2 = Colors.red[600];
                                        }

                                        if(next){
                                          List<Map<String, dynamic>> dtNext=[];
                                          dtNext.add({
                                            "rt_rsvid": widget._rsvid.toString(),
                                            "ptgs_pendaftaran": _PtgsPendaftaranRateCtrl.text.toString(),
                                            "ptgs_kasir": _PtgsKasirRateCtrl.text.toString(),
                                            "fs_parkir": _FparkirRateCtrl.text.toString(),
                                            "ptgs_parkir": _PtgsParkirRateCtrl.text.toString(),
                                            "ptgs_satpam": _SatpamRateCtrl.text.toString(),
                                            "ptgs_kebersiahan": _PtgsKebersihanRateCtrl.text.toString(),
                                            "ly_kebersiahan_toilet": _ToiletRateCtrl.text.toString(),
                                            "ptjk_arah": _PetunjukArahRateCtrl.text.toString(),
                                            "ly_dokterrj": _DokterRJRateCtrl.text.toString(),
                                            "ly_perawatrj": _PerawatRJRateCtrl.text.toString(),
                                            "ly_dokterri": _DokterRIRateCtrl.text.toString(),
                                            "ly_perawatri": _PerwataRIRateCtrl.text.toString(),
                                            "ly_pramusaji": _PramusajiRIRateCtrl.text.toString(),
                                            "ly_dokterugd": _DokterUGDRateCtrl.text.toString(),
                                            "ly_perawatugd": _PerawatUGDRateCtrl.text.toString(),
                                            "ly_farmasi": _ObatFarmasiRateCtrl.text.toString(),
                                            "masukan_nonmedis": _masukanNonMedis.text.toString(),
                                            "masukan_ri": _masukanRI.text.toString(),
                                            "masukan_rj": _masukanRJ.text.toString(),
                                            "masukan_ugd": _masukanUGD.text.toString(),
                                            "masukan_farmasi": _masukanFarmasi.text.toString(),
                                          });
                                          showDialog(
                                            barrierDismissible: false,
                                            context: context,
                                            builder: (BuildContext context) => showNotif(context, null, dtNext),
                                          ).whenComplete((){
                                            setState(() {

                                            });
                                          });
                                        }

                                      });
                                    },
                                    padding: EdgeInsets.only(top: 16, bottom: 16),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(ui.Radius.elliptical(10, 10)),
                                    ),
                                    fillColor: Colors.redAccent,
                                    child: Container(
                                      child: new Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          new Expanded(
                                              child: new Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    "Kirim",
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        color: Colors.white
                                                    ),
                                                  ),
                                                ],
                                              )
                                          ),
                                          new Container(
                                            padding: EdgeInsets.only(right: 16),
                                            child: new Icon(
                                              FontAwesomeIcons.arrowRight,
                                              size: 14,
                                              color: Colors.white,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          )
                      ),
                    ],
                  )
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget HeaderRate(title){
    return new Container(
      margin: EdgeInsets.only(bottom: 5),
      padding: EdgeInsets.only(left: 16, right: 16, top: 12, bottom: 12),
      decoration: BoxDecoration(
         color: Colors.grey[200],
         border: Border(
           bottom: BorderSide(
             color: Colors.grey[300]
           )
         )
      ),
      width: MediaQuery.of(context).size.width,
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Icon(
            FontAwesomeIcons.starHalfAlt,
            size: 16,
            color: Colors.grey[600],
          ),
          new Container(
            padding: EdgeInsets.only(left: 10),
            child: new Text(
              title,
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w700,
                color: Colors.grey[600],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget RowRate(title, icon, controller){
    Color txtColor1;
    Color txtColor2;
    if(controller == "_PtgsPendaftaranRate"){
      txtColor1 = _PtgsPendaftaranRateColor1;
      txtColor2 = _PtgsPendaftaranRateColor2;
    };
    if(controller == "_PtgsKasirRate"){
      txtColor1 = _PtgsKasirRateColor1;
      txtColor2 = _PtgsKasirRateColor2;
    };
    if(controller == "_FparkirRate"){
      txtColor1 = _FparkirRateColor1;
      txtColor2 = _FparkirRateColor2;
    };
    if(controller == "_PtgsParkirRate"){
      txtColor1 = _PtgsParkirRateColor1;
      txtColor2 = _PtgsParkirRateColor2;
    };
    if(controller == "_SatpamRate"){
      txtColor1 = _SatpamRateColor1;
      txtColor2 = _SatpamRateColor2;
    };
    if(controller == "_PtgsKebersihanRate"){
      txtColor1 = _PtgsKebersihanRateColor1;
      txtColor2 = _PtgsKebersihanRateColor2;
    };
    if(controller == "_ToiletRate"){
      txtColor1 = _ToiletRateColor1;
      txtColor2 = _ToiletRateColor2;
    };
    if(controller == "_PetunjukArahRate"){
      txtColor1 = _PetunjukArahRateColor1;
      txtColor2 = _PetunjukArahRateColor2;
    };
    if(controller == "_DokterRJRate"){
      txtColor1 = _DokterRJRateColor1;
      txtColor2 = _DokterRJRateColor2;
    };
    if(controller == "_PerawatRJRate"){
      txtColor1 = _PerawatRJRateColor1;
      txtColor2 = _PerawatRJRateColor2;
    };
    if(controller == "_DokterRIRate"){
      txtColor1 = _DokterRIRateColor1;
      txtColor2 = _DokterRIRateColor2;
    };
    if(controller == "_PerwataRIRate"){
      txtColor1 = _PerwataRIRateColor1;
      txtColor2 = _PerwataRIRateColor2;
    };
    if(controller == "_PramusajiRIRate"){
      txtColor1 = _PramusajiRIRateColor1;
      txtColor2 = _PramusajiRIRateColor2;
    };
    if(controller == "_DokterUGDRate"){
      txtColor1 = _DokterUGDRateColor1;
      txtColor2 = _DokterUGDRateColor2;
    };
    if(controller == "_PerawatUGDRate"){
      txtColor1 = _PerawatUGDRateColor1;
      txtColor2 = _PerawatUGDRateColor2;
    };
    if(controller == "_ObatFarmasiRate"){
      txtColor1 = _ObatFarmasiRateColor1;
      txtColor2 = _ObatFarmasiRateColor2;
    };

    return new Container(
      padding: EdgeInsets.only(left: 24, right: 24, top: 10, bottom: 10),
      margin: EdgeInsets.only(bottom: 2),
      decoration: BoxDecoration(
        border: Border(
            bottom: BorderSide(
                color: Colors.grey[300]
            ),
            top: BorderSide(
                color: Colors.grey[200]
            )
        ),
        color: Colors.white,
      ),
      width: MediaQuery.of(context).size.width,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Container(
                child: new Row(
                  children: <Widget>[
                    new Icon(
                      icon,
                      size:14,
                      color: txtColor1,
                    ),
                    new Container(
                      padding: EdgeInsets.only(left: 10, top: 3),
                      child:  Text(
                        title,
                        style: TextStyle(
                          fontSize: 14.0,
                          color: txtColor1,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    )
                  ],
                ),
              ),
              new Container(
                padding: EdgeInsets.only(left: 10, top: 3),
                child: Text(
                  controller == "_PtgsPendaftaranRate" ? _PtgsPendaftaranRateCtrl.text :
                  controller == "_PtgsKasirRate" ? _PtgsKasirRateCtrl.text :
                  controller == "_FparkirRate" ? _FparkirRateCtrl.text :
                  controller == "_PtgsParkirRate" ? _PtgsParkirRateCtrl.text :
                  controller == "_SatpamRate" ? _SatpamRateCtrl.text :
                  controller == "_PtgsKebersihanRate" ? _PtgsKebersihanRateCtrl.text :
                  controller == "_ToiletRate" ? _ToiletRateCtrl.text :
                  controller == "_PetunjukArahRate" ? _PetunjukArahRateCtrl.text :
                  controller == "_DokterRJRate" ? _DokterRJRateCtrl.text :
                  controller == "_PerawatRJRate" ? _PerawatRJRateCtrl.text :
                  controller == "_DokterRIRate" ? _DokterRIRateCtrl.text :
                  controller == "_PerwataRIRate" ? _PerwataRIRateCtrl.text :
                  controller == "_PramusajiRIRate" ? _PramusajiRIRateCtrl.text :
                  controller == "_DokterUGDRate" ? _DokterUGDRateCtrl.text :
                  controller == "_PerawatUGDRate" ? _PerawatUGDRateCtrl.text :
                  controller == "_ObatFarmasiRate" ? _ObatFarmasiRateCtrl.text : "0.0",
                  style: TextStyle(
                    color: txtColor2,
                  ),
                )
              )
            ],
          ),
          Visibility(
            visible: israte,
            child: new Container(
              child: FlutterRatingBar(
                initialRating: controller == "_PtgsPendaftaranRate" ? double.parse(_PtgsPendaftaranRateCtrl.text) :
                controller == "_PtgsKasirRate" ? double.parse(_PtgsKasirRateCtrl.text) :
                controller == "_FparkirRate" ? double.parse(_FparkirRateCtrl.text) :
                controller == "_PtgsParkirRate" ? double.parse(_PtgsParkirRateCtrl.text) :
                controller == "_SatpamRate" ? double.parse(_SatpamRateCtrl.text) :
                controller == "_PtgsKebersihanRate" ? double.parse(_PtgsKebersihanRateCtrl.text) :
                controller == "_ToiletRate" ? double.parse(_ToiletRateCtrl.text) :
                controller == "_PetunjukArahRate" ? double.parse(_PetunjukArahRateCtrl.text) :
                controller == "_DokterRJRate" ? double.parse(_DokterRJRateCtrl.text) :
                controller == "_PerawatRJRate" ? double.parse(_PerawatRJRateCtrl.text) :
                controller == "_DokterRIRate" ? double.parse(_DokterRIRateCtrl.text) :
                controller == "_PerwataRIRate" ? double.parse(_PerwataRIRateCtrl.text) :
                controller == "_PramusajiRIRate" ? double.parse(_PramusajiRIRateCtrl.text) :
                controller == "_DokterUGDRate" ? double.parse(_DokterUGDRateCtrl.text) :
                controller == "_PerawatUGDRate" ? double.parse(_PerawatUGDRateCtrl.text) :
                controller == "_ObatFarmasiRate" ? double.parse(_ObatFarmasiRateCtrl.text) : 0.0,
                borderColor: txtColor2,
                allowHalfRating: true,
                ignoreGestures: false,
                tapOnlyMode: false,
                itemSize: 30,
                itemCount: 5,
                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                onRatingUpdate: (rating) {
                  setState((){
                    if(controller == "_PtgsPendaftaranRate"){
                      _PtgsPendaftaranRate = rating;
                      _PtgsPendaftaranRateCtrl.text = _PtgsPendaftaranRate.toString();
                      if(rating.toString() != "0.0"){
                        _PtgsPendaftaranRateColor1 = Colors.grey[800];
                        _PtgsPendaftaranRateColor2 = Colors.grey[600];
                      }else{
                        _PtgsPendaftaranRateColor1 = Colors.red[800];
                        _PtgsPendaftaranRateColor2 = Colors.red[600];
                      }
                    };
                    if(controller == "_PtgsKasirRate"){
                      _PtgsKasirRate = rating;
                      _PtgsKasirRateCtrl.text = _PtgsKasirRate.toString();
                      if(rating.toString() != "0.0"){
                        _PtgsKasirRateColor1 = Colors.grey[800];
                        _PtgsKasirRateColor2 = Colors.grey[600];
                      }else{
                        _PtgsKasirRateColor1 = Colors.red[800];
                        _PtgsKasirRateColor2 = Colors.red[600];
                      }
                    };
                    if(controller == "_FparkirRate"){
                      _FparkirRate = rating;
                      _FparkirRateCtrl.text = _FparkirRate.toString();
                      if(rating.toString() != "0.0"){
                        _FparkirRateColor1 = Colors.grey[800];
                        _FparkirRateColor2 = Colors.grey[600];
                      }else{
                        _FparkirRateColor1 = Colors.red[800];
                        _FparkirRateColor2 = Colors.red[600];
                      }
                    };
                    if(controller == "_PtgsParkirRate"){
                      _PtgsParkirRate = rating;
                      _PtgsParkirRateCtrl.text = _PtgsParkirRate.toString();
                      if(rating.toString() != "0.0"){
                        _PtgsParkirRateColor1 = Colors.grey[800];
                        _PtgsParkirRateColor2 = Colors.grey[600];
                      }else{
                        _PtgsParkirRateColor1 = Colors.red[800];
                        _PtgsParkirRateColor2 = Colors.red[600];
                      }
                    };
                    if(controller == "_SatpamRate"){
                      _SatpamRate = rating;
                      _SatpamRateCtrl.text = _SatpamRate.toString();
                      if(rating.toString() != "0.0"){
                        _SatpamRateColor1 = Colors.grey[800];
                        _SatpamRateColor2 = Colors.grey[600];
                      }else{
                        _SatpamRateColor1 = Colors.red[800];
                        _SatpamRateColor2 = Colors.red[600];
                      }
                    };
                    if(controller == "_PtgsKebersihanRate"){
                      _PtgsKebersihanRate = rating;
                      _PtgsKebersihanRateCtrl.text = _PtgsKebersihanRate.toString();
                      if(rating.toString() != "0.0"){
                        _PtgsKebersihanRateColor1 = Colors.grey[800];
                        _PtgsKebersihanRateColor2 = Colors.grey[600];
                      }else{
                        _PtgsKebersihanRateColor1 = Colors.red[800];
                        _PtgsKebersihanRateColor2 = Colors.red[600];
                      }
                    };
                    if(controller == "_ToiletRate"){
                      _ToiletRate = rating;
                      _ToiletRateCtrl.text = _ToiletRate.toString();
                      if(rating.toString() != "0.0"){
                        _ToiletRateColor1 = Colors.grey[800];
                        _ToiletRateColor2 = Colors.grey[600];
                      }else{
                        _ToiletRateColor1 = Colors.red[800];
                        _ToiletRateColor2 = Colors.red[600];
                      }
                    };
                    if(controller == "_PetunjukArahRate"){
                      _PetunjukArahRate = rating;
                      _PetunjukArahRateCtrl.text = _PetunjukArahRate.toString();
                      if(rating.toString() != "0.0"){
                        _PetunjukArahRateColor1 = Colors.grey[800];
                        _PetunjukArahRateColor2 = Colors.grey[600];
                      }else{
                        _PetunjukArahRateColor1 = Colors.red[800];
                        _PetunjukArahRateColor2 = Colors.red[600];
                      }
                    };
                    if(controller == "_DokterRJRate"){
                      _DokterRJRate = rating;
                      _DokterRJRateCtrl.text = _DokterRJRate.toString();
                      if(rating.toString() != "0.0"){
                        _DokterRJRateColor1 = Colors.grey[800];
                        _DokterRJRateColor2 = Colors.grey[600];
                      }else{
                        _DokterRJRateColor1 = Colors.red[800];
                        _DokterRJRateColor2 = Colors.red[600];
                      }
                    };
                    if(controller == "_PerawatRJRate"){
                      _PerawatRJRate = rating;
                      _PerawatRJRateCtrl.text = _PerawatRJRate.toString();
                      if(rating.toString() != "0.0"){
                        _PerawatRJRateColor1 = Colors.grey[800];
                        _PerawatRJRateColor2 = Colors.grey[600];
                      }else{
                        _PerawatRJRateColor1 = Colors.red[800];
                        _PerawatRJRateColor2 = Colors.red[600];
                      }
                    };
                    if(controller == "_DokterRIRate"){
                      _DokterRIRate = rating;
                      _DokterRIRateCtrl.text = _DokterRIRate.toString();
                      if(rating.toString() != "0.0"){
                        _DokterRIRateColor1 = Colors.grey[800];
                        _DokterRIRateColor2 = Colors.grey[600];
                      }else{
                        _DokterRIRateColor1 = Colors.red[800];
                        _DokterRIRateColor2 = Colors.red[600];
                      }
                    };
                    if(controller == "_PerwataRIRate"){
                      _PerwataRIRate = rating;
                      _PerwataRIRateCtrl.text = _PerwataRIRate.toString();
                      if(rating.toString() != "0.0"){
                        _PerwataRIRateColor1 = Colors.grey[800];
                        _PerwataRIRateColor2 = Colors.grey[600];
                      }else{
                        _PerwataRIRateColor1 = Colors.red[800];
                        _PerwataRIRateColor2 = Colors.red[600];
                      }
                    };
                    if(controller == "_PramusajiRIRate"){
                      _PramusajiRIRate = rating;
                      _PramusajiRIRateCtrl.text = _PramusajiRIRate.toString();
                      if(rating.toString() != "0.0"){
                        _PramusajiRIRateColor1 = Colors.grey[800];
                        _PramusajiRIRateColor2 = Colors.grey[600];
                      }else{
                        _PramusajiRIRateColor1 = Colors.red[800];
                        _PramusajiRIRateColor2 = Colors.red[600];
                      }
                    };
                    if(controller == "_DokterUGDRate"){
                      _DokterUGDRate = rating;
                      _DokterUGDRateCtrl.text = _DokterUGDRate.toString();
                      if(rating.toString() != "0.0"){
                        _DokterUGDRateColor1 = Colors.grey[800];
                        _DokterUGDRateColor2 = Colors.grey[600];
                      }else{
                        _DokterUGDRateColor1 = Colors.red[800];
                        _DokterUGDRateColor2 = Colors.red[600];
                      }
                    };
                    if(controller == "_PerawatUGDRate"){
                      _PerawatUGDRate = rating;
                      _PerawatUGDRateCtrl.text = _PerawatUGDRate.toString();
                      if(rating.toString() != "0.0"){
                        _PerawatUGDRateColor1 = Colors.grey[800];
                        _PerawatUGDRateColor2 = Colors.grey[600];
                      }else{
                        _PerawatUGDRateColor1 = Colors.red[800];
                        _PerawatUGDRateColor2 = Colors.red[600];
                      }
                    };
                    if(controller == "_ObatFarmasiRate"){
                      _ObatFarmasiRate = rating;
                      _ObatFarmasiRateCtrl.text = _ObatFarmasiRate.toString();
                      if(rating.toString() != "0.0"){
                        _ObatFarmasiRateColor1 = Colors.grey[800];
                        _ObatFarmasiRateColor2 = Colors.grey[600];
                      }else{
                        _ObatFarmasiRateColor1 = Colors.red[800];
                        _ObatFarmasiRateColor2 = Colors.red[600];
                      }
                    };
                  });
                },
              ),
            ),
          ),
          Visibility(
            visible: israte ? false: true,
            child: FlutterRatingBarIndicator(
              rating: controller == "_PtgsPendaftaranRate" ? double.parse(_PtgsPendaftaranRateCtrl.text) :
              controller == "_PtgsKasirRate" ? double.parse(_PtgsKasirRateCtrl.text) :
              controller == "_FparkirRate" ? double.parse(_FparkirRateCtrl.text) :
              controller == "_PtgsParkirRate" ? double.parse(_PtgsParkirRateCtrl.text) :
              controller == "_SatpamRate" ? double.parse(_SatpamRateCtrl.text) :
              controller == "_PtgsKebersihanRate" ? double.parse(_PtgsKebersihanRateCtrl.text) :
              controller == "_ToiletRate" ? double.parse(_ToiletRateCtrl.text) :
              controller == "_PetunjukArahRate" ? double.parse(_PetunjukArahRateCtrl.text) :
              controller == "_DokterRJRate" ? double.parse(_DokterRJRateCtrl.text) :
              controller == "_PerawatRJRate" ? double.parse(_PerawatRJRateCtrl.text) :
              controller == "_DokterRIRate" ? double.parse(_DokterRIRateCtrl.text) :
              controller == "_PerwataRIRate" ? double.parse(_PerwataRIRateCtrl.text) :
              controller == "_PramusajiRIRate" ? double.parse(_PramusajiRIRateCtrl.text) :
              controller == "_DokterUGDRate" ? double.parse(_DokterUGDRateCtrl.text) :
              controller == "_PerawatUGDRate" ? double.parse(_PerawatUGDRateCtrl.text) :
              controller == "_ObatFarmasiRate" ? double.parse(_ObatFarmasiRateCtrl.text) : 0.0,
              itemCount: 5,
              itemSize: 20.0,
              fillColor: Colors.yellow[600],
              emptyColor: Colors.grey[300],
            ),
          )
        ],
      ),
    );
  }

  Widget _appbar(){
    return new Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        new Container(
            color: widget._color1,
            height: 55,
            child:Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: new Row(
                    children: <Widget>[
                      new Container(
                        child: new InkWell(
                          onTap: () =>  Navigator.pop(context),
                          child: new Container(
                            padding: EdgeInsets.only(left: 24, right: 24),
                            alignment: Alignment.center,
                            child: new Icon(
                              FontAwesomeIcons.arrowLeft,
                              color: widget._TextColorBar,
                              size: 24.0,
                            ),
                          ),
                        ),
                      ),
                      new Text(
                        "RATING / PENILAIAN ANDA",
                        style: TextStyle(
                            fontSize: 16,
                            color: widget._TextColorBar,
                            fontWeight: FontWeight.w700
                        ),
                      ),

                    ],
                  ),
                ),
              ],
            )
        ),
        new Container(
          color: widget._color1,
          height: 2,
          child: new SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(-1, 0),
              end: const Offset(0, 0),
            ).animate(_controllerappBar),
            child: new SizedBox(
              child: Container(
                  color: Colors.white
              ),
            ),
          ),
        ),
      ],
    );
  }

  bool sendingData = true;
  @override
  Future<String> _postData(context, List<Map<String, dynamic>> dtNext) async {
    var jsonResponse;
    if(sendingData){
      var db = new Http_Controller();
      var _data = await db.postResponse("https://suryahusadha.com/api/rsvrating", dtNext);
      jsonResponse = convert.jsonEncode(_data);
      Navigator.pop(context);
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => showNotif(context, jsonResponse.toString(), null),
      );
    }
    return jsonResponse.toString();
  }

  Widget showNotif(BuildContext context, _jsonResponse, List<Map<String, dynamic>> dtNext){
    if(dtNext != null){
      _postData(context, dtNext);
      sendingData = false;
      return new Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.grey,
        ),
      );
    } else {
      if(_jsonResponse != "" || _jsonResponse != null) {
        var decode = convert.jsonDecode(_jsonResponse);
        if(decode["statusCode"] == 200){
          var jsonResponse = convert.jsonDecode(decode["responseBody"]);
          String response = jsonResponse["rsvrating"][0]["response"];
          String msg = jsonResponse["rsvrating"][0]["msg"];
          newdata = jsonResponse["rsvrating"][0]["data"];
          if(response == "0"){
            return Dialogue(context, msg, Colors.red, widget._color2, null, null);
          }
          if(response == "1"){
            israte = false;
            return Dialogue(context, msg, widget._color1, widget._color2, null, null);
          }
        }else{
          return Dialogue(context,
              "Mohon maaf, terjadi kesalahan pengolahan data pada saat simpan data, silahkan ulangi beberapa saat lagi!",
              Colors.red, Colors.red, null, null);
        }
      }else {
        return Dialogue(context,
            "Mohon maaf, terjadi kesalahan koneksi pada saat simpan data, silahkan ulangi beberapa saat lagi!",
            Colors.red, Colors.red, null, null);
      }
    }
  }

}