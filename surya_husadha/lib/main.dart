import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'components/com_launcher/launcher.dart';
import 'helper/constans/constans.dart';


void main(){
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: WarnaCabang.shblue, // navigation bar color
      statusBarColor: WarnaCabang.shblue, // status bar color
    ));

    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'PT. Surya Husadha Group',
      theme: new ThemeData(
        fontFamily: 'NeoSans',
        primaryColor: Colors.white,
        accentColor: Colors.white70,
        //primarySwatch: Colors.blue,
      ),
      home: Launcher(),
    );
  }
}
